export const environment = {
  production: true,
  urlForAdaptationCoordinator: window["env"]["urlForAdaptationCoordinator"] || "default",
  urlForModelVisuals: window["env"]["urlForModelVisuals"] || "default",
  urlForWebSocket: window["env"]["urlForWebSocket"] || "default"
};
