(function(window) {
  window.env = window.env || {};

  // Environment variables
  window["env"]["urlForAdaptationCoordinator"] = "${AdaptationCoordinator_REST_URL}";
  window["env"]["urlForWebSocket"] = "${AdaptationCoordinator_WebSocket_URL}";
  window["env"]["urlForModelVisuals"] = "${ModelVisuals_URL}";
})(this);
