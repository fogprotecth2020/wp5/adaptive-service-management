(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  //window["env"]["apiUrl"] = "https://api.myapp.com";

  window["env"]["urlForAdaptationCoordinator"] = "http://localhost:7771/fogprotect/adaptiveApplicationManagement/";
  window["env"]["urlForWebSocket"] = "ws://localhost:7771/live";
  window["env"]["urlForModelVisuals"] = "http://localhost:7775/";
})(this);
