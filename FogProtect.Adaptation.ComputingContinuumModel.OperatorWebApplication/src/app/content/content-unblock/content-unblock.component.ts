import { Component, OnDestroy, OnInit } from '@angular/core';
import {WebSocketService} from '../../data/web-socket.service';
import {Subscription} from 'rxjs';
import {ConfirmationDialogService} from '../../miscellaneous/confirmation-dialog/confirmation-dialog.service';
import {DataFetchService} from '../../data/data-fetch.service';
import testjson from '../../../assets/smart_media.json'
import {SendInputDTO} from '../../data/sendinput-dto.model';
import {tosca_nodes_root} from 'src/app/data/toscanode.model';

@Component({
  selector: 'app-content-unblock',
  templateUrl: './content-unblock.component.html',
  styleUrls: ['./content-unblock.component.css']
})

export class ContentUnblockComponent implements OnInit {

  subscriptionWebSocket: Subscription;
  subscriptionWebSocket2: Subscription;
  errorMessage: string;
  sendInput: SendInputDTO;
  data: any = testjson;
  statusInfoString: string;
  infoOfUsers: tosca_nodes_root[];
  blockedUsers: tosca_nodes_root[];
  tempNodes:tosca_nodes_root[] =[];

  constructor(private fetch: DataFetchService, private websocket: WebSocketService, private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit(): void {
    // if the data is directly received from AdaptationCoordinator (not from testjson), this can be deleted
    this.statusInfoString = JSON.stringify(this.data);
    this.statusInfoString = this.statusInfoString.replace(/@/g, "ATZeichen")
    const obj = JSON.parse(this.statusInfoString);
    this.data = obj;
    //end of the part to be deleted
    this.extractUserInfo();
    this.subscriptionWebSocket2 = this.websocket.getLiveIntervention().subscribe((x) => {
      console.log(x)
      this.confirmationDialogService.confirm('The intervention of the operator is required', 'Go to Decision-menu to look at the problem')
        .then((confirmed) =>
        {
            //TODO change path to decisions tab

            //console.log('User confirmed:', confirmed)
        });
      
    });

    this.subscriptionWebSocket = this.websocket.getLiveUpdateContent().subscribe((x) => {
        console.log(x)
        this.confirmationDialogService.confirm('A new "As is" model is available!', 'Go to Home-menu to look at the changes')
          .then((confirmed) =>
          {
            //console.log('User confirmed:', confirmed)
          });
      });
  }

  ngOnDestroy() {
    this.subscriptionWebSocket.unsubscribe();
    this.subscriptionWebSocket2.unsubscribe();
  }

  initRequest(): void
  {
    this.fetch.getStatusInformation().subscribe((data: SendInputDTO) =>
    {
      this.sendInput = data;
      this.statusInfoString = JSON.stringify(this.sendInput);
      this.statusInfoString = this.statusInfoString.replace(/@/g, "ATZeichen")
      const obj = JSON.parse(this.statusInfoString);

      this.sendInput = obj;

      }, error => {
        console.log(error)
        this.errorMessage = error
      })

  }

  unblockUser(user: tosca_nodes_root){
    //TODO: replace(/ATZeichen/g, "@")
    this.confirmationDialogService.confirm('Unblock a user', 'Should the following user be unblocked?: ' + user.name)
          .then((confirmed) =>
          {
            console.log("unblockUser Button clicked for user: " + user.name)
            
          });
    
  }

  loadInfo(){
    
    this.extractBlockedUserInfo();
    console.log("loadInfo Button clicked")
  }

  //helper

  //extracts the nodes with the capability of being blocked
  
  extractUserInfo(){
    this.infoOfUsers = [];
    this.tempNodes = [];
    let nodes = Array.from(this.data.tosca_nodes_root)
    if(Array.isArray(nodes)){
      for(let i=0; i<nodes.length; i++)
    {
      if(nodes[i].hasOwnProperty('blocked')){
        this.tempNodes.push(nodes[i])
      }
    }
    }
    this.infoOfUsers = this.tempNodes
  }
  //extracts the nodes where at least one attribute of 'blocked' is true
  extractBlockedUserInfo(){
    this.extractUserInfo();
    this.tempNodes = [];
    
    if(Array.isArray(this.infoOfUsers)){
      for(let i=0; i<this.infoOfUsers.length; i++)
    {
      if(this.infoOfUsers[i].blocked == true || this.infoOfUsers[i].blockedReading == true  || this.infoOfUsers[i].blockedWriting == true){
        this.tempNodes.push(this.infoOfUsers[i])
      }
    }
    }
    this.blockedUsers = this.tempNodes
  }
}
