import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentUnblockComponent } from './content-unblock.component';

describe('ContentUnblockComponent', () => {
  let component: ContentUnblockComponent;
  let fixture: ComponentFixture<ContentUnblockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentUnblockComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContentUnblockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
