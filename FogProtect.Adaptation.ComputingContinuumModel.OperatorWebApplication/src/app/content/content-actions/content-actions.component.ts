import {Component, OnDestroy, OnInit} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {DataFetchService} from '../../data/data-fetch.service';
import {AsIsModelSVGRepresentation} from '../../data/As is model/as-is-model.model';
import {SendInputDTO} from '../../data/sendinput-dto.model';
import {WebSocketService} from '../../data/web-socket.service';
import {Subscription} from 'rxjs';
import {ConfirmationDialogService} from '../../miscellaneous/confirmation-dialog/confirmation-dialog.service';
import {LegendElement} from '../../miscellaneous/legend/legend-element.model';
import {LegendService} from '../../miscellaneous/legend/legend.service';
import {RisksService} from '../../miscellaneous/risk-service/risks.service';
import { ConnectionType, tosca_nodes_root, Jurisdictions, Trustworthy, NodeType, Relation, relationClass, listOfRelations } from 'src/app/data/toscanode.model';


@Component({
  selector: 'app-content-actions',
  templateUrl: 'content-actions.component.html',
  styleUrls: ['./content-actions.component.scss']
})
export class ContentActionsComponent implements OnInit, OnDestroy {

  svg: SafeHtml;
  asIsModelSVGRepresentation: AsIsModelSVGRepresentation;
  asIsModelSVGRepresentationComparison: AsIsModelSVGRepresentation;
  errorMessage: string;

  sendInput: SendInputDTO;

  subscriptionWebSocket: Subscription;
  subscriptionWebSocket2: Subscription;

  addNodeClicked: boolean = false
  addRelationClicked: boolean = false
  changeAttributeClicked: boolean = false
  deleteNodeClicked: boolean = false
  deleteRelationClicked: boolean = false
  viewAdaptationsClicked: boolean = false

  statusInfoString: string;
  statusInfoJson: any;
  nodes: tosca_nodes_root[];
  targetnodes: tosca_nodes_root[];
  nodeIDButtonName: string = "Node ID"
  targetnodeIDButtonName: string = "Targetnode ID"
  listOfAttributes: string[]

  currentAttributeValue: string = "Current Value";
  currentAttribute: string = "No Node Selected"
  currentNode: tosca_nodes_root;
  changeDropDown: string[];
  currentChange: string = "New Value"
  currentAttributeType: string;
  currentTargetNode: tosca_nodes_root;

  allRelations: Relation[] = []
  currentRelation: Relation
  possibleRelations: Relation[]
  relationButtonName: string = "No Relation selected"

  listOfRelationNames: string[]
  relationNameDropDown: string = "No Relation selected"
  currentRelationName: string;

  value: string = ''
  valuetarget: string = ''
  clickedConfirmIncorrectValue: boolean = false
  nodeTypes: string[];
  typenode: string = "Node Type"
  changedBoolean: boolean;
  //DropDown, TextField, Boolean
  dropdown: boolean = false
  textfield: boolean = false
  Boolean: boolean = false


  arrayOfNodeAttributes: string[] = ["type", "ATZeichenid", "id", "name", "encrypted", "personal", "sensitive", "disab", "hasToBeDeployedOnFogNode", "amountOfDataInGB", "connectionType", "neededCapacity", "transferCostPerGB", "usageCostPerDay", "costIncurred", "jurisdiction", "compromised", "capacity", "trustworthy", "location"]

  constructor(public sanitizer: DomSanitizer, private fetch: DataFetchService, private websocket: WebSocketService, private confirmationDialogService: ConfirmationDialogService,
    public legendService: LegendService, public risksService: RisksService) { }

  ngOnInit(): void {
    this.initRequest();
    
    this.subscriptionWebSocket2 = this.websocket.getLiveIntervention().subscribe(() => {
      
      this.confirmationDialogService.confirm('The intervention of the operator is required', 'Go to Decision-menu to look at the problem')
        .then((confirmed) =>
        {
            //TODO change path to decisions tab

            //console.log('User confirmed:', confirmed)
        });
      
    });

    this.subscriptionWebSocket = this.websocket.getLiveUpdateContent().subscribe(() => {

      if(this.sendInput.AsIs.AsIsModel == null && this.sendInput.AsIs.AsIsModel == null)
      {
        this.initRequest();
      }

      if(this.sendInput.AsIs.AsIsModel != null)
      {
        this.initRequest();

        this.confirmationDialogService.confirm('A new "As is" model is available!', 'The changed nodes are highlighted in color.')
          .then((confirmed) =>
          {
            //console.log('User confirmed:', confirmed)
          }/*)
          .catch(() =>
          {
            console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)')
          }*/);
      }
    });
  }

  ngOnDestroy() {
    this.subscriptionWebSocket.unsubscribe();
    this.subscriptionWebSocket2.unsubscribe();
  }

  initRequest(): void
  {
    this.fetch.getStatusInformation().subscribe((data: SendInputDTO) =>
    {
      this.sendInput = data;
      this.statusInfoJson = data;
      
      this.fetch.postModel(this.sendInput).subscribe((data: AsIsModelSVGRepresentation) =>
      {
        this.asIsModelSVGRepresentation = data[0]
        this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));
        this.statusInfoString = JSON.stringify(this.sendInput);
      
        let x = ["No Node Selected"]
        this.listOfAttributes = x

        this.statusInfoString = this.statusInfoString.replace(/@/g, "ATZeichen")
        const obj = JSON.parse(this.statusInfoString);

        this.sendInput = obj;
        this.nodes = this.sendInput.AsIs.AsIsModel.tosca_nodes_root;

        for(let n of this.nodes){
          //console.log(n.ATZeichenid)
          let array = Object.keys(n);
          let result = array.filter(a => !this.arrayOfNodeAttributes.includes(a))
          
          for(let r of result){
            //console.log("header nach Aussortieren (excess properties): " + r);
            let attribut = Object.getOwnPropertyDescriptor(n, r)
            let relationarray = attribut.value
            //console.log("typ der übrigen Properties ist Array? " + Array.isArray(attribut.value))

            if(Array.isArray(relationarray)){
              for(let b of relationarray){
                const rel = new relationClass()
                rel.relationname = r;
                rel.startnodeid = n.ATZeichenid
                let rela = Object.getOwnPropertyDescriptor(b, "referencedObjectID")
                rel.targetnodeid = rela.value
                //console.log(rel.toString())
                this.allRelations.push(rel);
              }
            }
          }
        }

        //console.log("allRelations: ")
        //for(let re of this.allRelations){
          //let str = "Startnode: " + re.startnodeid + ", Targetnode: " + re.targetnodeid + ", Relationname: " + re.relationname
          //console.log(str);
        //}
        
      }, error => {
        console.log(error)
        this.errorMessage = error
      })

      })
  }

  addNode(): void{
    this.reset()
    if(this.addNodeClicked){
      this.addNodeClicked = false
    }else{
      this.addNodeClicked = true
    }
    this.addRelationClicked = false
    this.changeAttributeClicked = false
    this.deleteNodeClicked = false
    this.deleteRelationClicked = false
    this.viewAdaptationsClicked = false

    this.nodeTypes = Object.values(NodeType);

  }
  
  addRelation(): void{
    this.reset()
    if(this.addRelationClicked){
      this.addRelationClicked = false
    }else{
      this.addRelationClicked = true
    }
    this.addNodeClicked = false
    this.changeAttributeClicked = false
    this.deleteNodeClicked = false
    this.deleteRelationClicked = false
    this.viewAdaptationsClicked = false

    this.listOfRelationNames = Object.values(listOfRelations);
  }

  changeAttribute(): void{
    this.reset()
    if(this.changeAttributeClicked){
      this.changeAttributeClicked = false
    }else{
      this.changeAttributeClicked = true
    }
    this.addNodeClicked = false
    this.addRelationClicked = false
    this.deleteNodeClicked = false
    this.deleteRelationClicked = false
    this.viewAdaptationsClicked = false
  }

  deleteNode():void{
    this.reset()
    if(this.deleteNodeClicked){
      this.deleteNodeClicked = false
    }else{
      this.deleteNodeClicked = true
    }
    this.addNodeClicked = false
    this.addRelationClicked = false
    this.changeAttributeClicked = false
    this.deleteRelationClicked = false
    this.viewAdaptationsClicked = false
  }

  deleteRelation():void{
    this.reset()
    if(this.deleteRelationClicked){
      this.deleteRelationClicked = false
    }else{
      this.deleteRelationClicked = true
    }
    this.addNodeClicked = false
    this.addRelationClicked = false
    this.changeAttributeClicked = false
    this.deleteNodeClicked = false
    this.viewAdaptationsClicked = false
  
    this.nodes = this.nodes.filter(n => this.nodeIDisInRelation(this.allRelations, n.ATZeichenid))
    this.targetnodes = this.nodes.filter(n => this.nodeIDisInRelation(this.allRelations, n.ATZeichenid))
  }

  viewAdaptations(): void{
    this.reset()
    if(this.viewAdaptationsClicked){
      this.viewAdaptationsClicked = false
    }else{
      this.viewAdaptationsClicked = true
    }
    this.addNodeClicked = false
    this.addRelationClicked = false
    this.changeAttributeClicked = false
    this.deleteNodeClicked = false
    this.deleteRelationClicked = false
  }

  changeConfirmed(): void{
    if(this.currentNode == null || this.currentChange == "New Value"){
      this.clickedConfirmIncorrectValue = true
      return
    }
    if(this.textfield){
      console.log(this.value)
      if(this.isEmpty(this.value) || this.value == "New Value"){
        console.log("incorrect Value")
          this.clickedConfirmIncorrectValue = true
          return
      }
      else{
        this.currentChange = this.value
        if(this.currentAttribute != "name"){
          if(!isNaN(Number(this.value))){
            //is Number -> has to be positive
            var numberValue = Number(this.value)
            if(numberValue >= 0){
              //z.T. double-Werte nicht erlaubt
              if((this.currentAttribute == "capacity" || this.currentAttribute == "neededCapacity") && !Number.isInteger(numberValue)){
                console.log("incorrect Value")
                this.clickedConfirmIncorrectValue = true
                return
              }
            }
            else{
              console.log("incorrect Value")
              this.clickedConfirmIncorrectValue = true
              return
            }
          }
          else{
            console.log("incorrect Value")
            this.clickedConfirmIncorrectValue = true
            return
          }
        }
      }
    }
    this.confirmationDialogService.confirm('Confirmation of change of an attribute', 'Should the following change be executed?: Node:' + this.currentNode.ATZeichenid + ', Attribute: ' + this.currentAttribute + ', Old Value: ' + this.currentAttributeValue + ', New Value: ' + this.currentChange)
          .then((confirmed) =>
          {
            
            console.log("Change confirmed")
            let x = this.getMonitoringEventChangeAttribute().replace(/ATZeichen/g, "@")

            console.log(x)
            this.fetch.postMonitoringEvent(x).subscribe((data: any) =>
            {
              console.log("successful")
              console.log(data)
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
  }

  deletionNode():void{
    if(this.currentNode == null){
      this.clickedConfirmIncorrectValue = true
      return
    }
    this.confirmationDialogService.confirm('Confirmation of deletion of a node', 'Should the following node be deleted?: Node:' + this.currentNode.ATZeichenid)
          .then((confirmed) =>
          {
            
            console.log("Deletion confirmed")
            let x = this.getMonitoringEventDeleteNode().replace(/ATZeichen/g, "@")

            console.log(x)
            this.fetch.postMonitoringEvent(x).subscribe((data: any) =>
            {
              console.log("successful")
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
  }

  deletionRelation(): void{
    
    if(this.currentRelation != null){
      let str = "Startnode: " + this.currentRelation.startnodeid + ", Targetnode: " + this.currentRelation.targetnodeid + ", Relationname: " + this.currentRelation.relationname
      console.log(str);
      this.confirmationDialogService.confirm('Confirmation of deletion of a relation', 'Should the following relation be deleted?: Startnode: ' + this.currentRelation.startnodeid + ', Targetnode: ' + this.currentRelation.targetnodeid + ', Relationname: ' + this.currentRelation.relationname)
          .then((confirmed) =>
          {
            
            console.log("Deletion confirmed")
            let x = this.getMonitoringEventDeleteRelation().replace(/ATZeichen/g, "@")

            console.log(x)
            this.fetch.postMonitoringEvent(x).subscribe((data: any) =>
            {
              console.log("successful")
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
    }
    else{
      this.clickedConfirmIncorrectValue = true
    }
  }

  additionNode():void{
    
    console.log(this.value)
    if(this.isEmpty(this.value) || this.typenode == "Node Type"){
      console.log("incorrect Value")
      this.clickedConfirmIncorrectValue = true
      return
    }

    this.confirmationDialogService.confirm('Confirmation of addition of a node', 'Should the following node be added?: Node:' + this.value)
          .then((confirmed) =>
          {
            
            console.log("Addition confirmed")
            let x = this.getMonitoringEventAddNode()

            console.log(x)
            this.fetch.postMonitoringEvent(x).subscribe((data: any) =>
            {
              console.log("successful")
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
  }

  additionRelation(): void{
    console.log(this.value)
    if(this.currentRelationName == null|| this.currentNode == null || this.currentTargetNode == null){
      console.log("incorrect Value")
      this.clickedConfirmIncorrectValue = true
      return
    }
    this.confirmationDialogService.confirm('Confirmation of addition of a relation', 'Should the relation be added?')
          .then((confirmed) =>
          {
            
            console.log("addition confirmed")
            let x = this.getMonitoringEventAddRelation().replace(/ATZeichen/g, "@")

            console.log(x)
            this.fetch.postMonitoringEvent(x).subscribe((data: any) =>
            {
              console.log("successful")
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
  }

  selectNodeID(node: tosca_nodes_root){
    this.nodeIDButtonName = node.ATZeichenid;
    this.currentNode = node;
    this.listOfAttributes = this.getAttributesForType(node.type);
    this.currentAttribute = "Attribute to be changed"
    this.targetnodes = this.nodes.filter(obj => obj !== this.currentNode)
    if(this.addRelationClicked){
      this.nodes = this.sendInput.AsIs.AsIsModel.tosca_nodes_root;
      this.targetnodes = this.nodes.filter(obj => obj !== node)
    }
    if(this.deleteRelationClicked){
      this.currentTargetNode = null
      this.targetnodeIDButtonName = "Targetnode ID"
      this.relationButtonName = "No Relation selected"
      this.currentRelation = null;
      this.targetnodes = this.nodes.filter(n => n !== this.currentNode && this.nodeIDisInRelationWithNode(this.allRelations, n.ATZeichenid, false));
    }
    
  }

  selectAttribute(str: string){
    this.currentAttribute = str
    //if Node selected
    if(this.currentNode != null && str != "Keine Attribute"){
      switch(str){
        //DROPDOWN START
        case "connectionType":{
          this.currentAttributeValue = this.currentNode.connectionType.toString();
          this.changeDropDown = Object.values(ConnectionType);
          this.currentChange = "New Value"
          this.currentAttributeType = "ConnectionType"
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        case "jurisdiction":{
          this.currentAttributeValue = this.currentNode.jurisdiction.toString();
          this.changeDropDown = Object.values(Jurisdictions)
          this.currentAttributeType = "Jurisdictions"
          this.currentChange = "New Value"
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        case "location":{
          this.currentAttributeValue = this.currentNode.location.toString();
          this.changeDropDown = Object.values(Jurisdictions)
          this.currentAttributeType = "Jurisdictions"
          this.currentChange = "New Value"
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        case "trustworthy":{
          this.currentAttributeValue = this.currentNode.trustworthy.toString();
          this.changeDropDown = Object.values(Trustworthy)
          this.currentAttributeType = "Trustworthy"
          this.currentChange = "New Value"
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        //DROPDOWN END
        //boolean start
        case "encrypted":{
          this.currentAttributeValue = this.currentNode.encrypted.toString();
          this.currentAttributeType = typeof this.currentNode.encrypted
          this.changedBoolean = !this.currentNode.encrypted;
          this.currentChange = this.changedBoolean.toString();
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "personal":{
          this.currentAttributeValue = this.currentNode.personal.toString();
          this.currentAttributeType = typeof this.currentNode.personal
          this.changedBoolean = !this.currentNode.personal
          this.currentChange = this.changedBoolean.toString();
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "sensitive":{
          this.currentAttributeValue = this.currentNode.sensitive.toString();
          this.currentAttributeType = typeof this.currentNode.sensitive
          this.changedBoolean = !this.currentNode.sensitive
          this.currentChange = this.changedBoolean.toString();
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "disab":{
          this.currentAttributeValue = this.currentNode.disab.toString();
          this.currentAttributeType = typeof this.currentNode.disab
          this.changedBoolean = !this.currentNode.disab
          this.currentChange = this.changedBoolean.toString();
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "hasToBeDeployedOnFogNode":{
          this.currentAttributeValue = this.currentNode.hasToBeDeployedOnFogNode.toString();
          this.currentAttributeType = typeof this.currentNode.hasToBeDeployedOnFogNode
          this.changedBoolean = !this.currentNode.hasToBeDeployedOnFogNode
          this.currentChange = this.changedBoolean.toString();
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "costIncurred":{
          this.currentAttributeValue = this.currentNode.costIncurred.toString();
          this.currentAttributeType = typeof this.currentNode.costIncurred
          this.changedBoolean = !this.currentNode.costIncurred
          this.currentChange = this.changedBoolean.toString();
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "compromised":{
          this.currentAttributeValue = this.currentNode.compromised.toString();
          this.currentAttributeType = typeof this.currentNode.compromised
          this.changedBoolean = !this.currentNode.compromised
          this.currentChange = this.changedBoolean.toString();
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        //boolean end
        //textfield start
        case "name":{
          this.currentAttributeValue = this.currentNode.name.toString();
          this.currentAttributeType = "String"
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "amountOfDataInGB":{
          this.currentAttributeValue = this.currentNode.amountOfDataInGB.toString();
          this.currentAttributeType = "double"
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "neededCapacity":{
          this.currentAttributeValue = this.currentNode.neededCapacity.toString();
          this.currentAttributeType = "int"
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "transferCostPerGB":{
          this.currentAttributeValue = this.currentNode.transferCostPerGB.toString();
          this.currentAttributeType = "double"
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "usageCostPerDay":{
          this.currentAttributeValue = this.currentNode.usageCostPerDay.toString();
          this.currentAttributeType = "double"
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "capacity":{
          this.currentAttributeValue = this.currentNode.capacity.toString();
          this.currentAttributeType = "int"
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        //textfield end
      }
    }
  }
  
  selectNewValue(change: string){
    this.clickedConfirmIncorrectValue = false
    this.currentChange = change
  }

  selectNodeType(type: string){
    this.typenode = type
  }

  selectTargetNodeID(node: tosca_nodes_root): void{
    this.currentTargetNode = node
    this.targetnodeIDButtonName = node.ATZeichenid
    if(this.addRelationClicked){
      this.targetnodes = this.sendInput.AsIs.AsIsModel.tosca_nodes_root;
      this.nodes = this.targetnodes.filter(obj => obj !== node)
    }
    if(this.deleteRelationClicked){
      this.relationButtonName = "No Relation selected"
      this.currentRelation = null;
      this.possibleRelations = this.allRelations.filter(rel => rel.targetnodeid == this.currentTargetNode.ATZeichenid && rel.startnodeid == this.currentNode.ATZeichenid)
    }
  }

  selectRelation(relat: Relation): void{
    this.currentRelation = relat
    this.relationButtonName = this.currentRelation.relationname;
  }

  selectRelationName(relation: string): void{
    this.currentRelationName = relation;
    this.relationNameDropDown = this.currentRelationName
    console.log(this.currentRelationName)
  }

  reset():void{
    this.currentNode = null
    this.currentTargetNode = null
    this.currentRelation = null
    this.targetnodeIDButtonName = "Targetnode ID"
    this.relationButtonName = "No Relation selected"
    this.nodeIDButtonName = "Node ID"
    this.typenode = "Node Type"
    this.listOfAttributes = null
    this.currentChange = "New Value"
    this.currentAttribute = "No Node Selected"
    this.currentAttributeType = ""
    this.currentAttributeValue = "Current Value"
    this.Boolean = false
    this.textfield = false
    this.dropdown = false
    this.possibleRelations = null
    this.value = ''
    this.changedBoolean = null
    this.relationNameDropDown = "No Relation selected";
    this.currentRelationName = null;
  }
  //--------------------------------------------------------------------------------------------------------------------------------------------------------//
  //Helper-Method(s)
  getAttributesForType(nodeType: string): string[]{
    
    if(nodeType == "Compute" || nodeType == "CloudCompute" || nodeType == "FogCompute" || nodeType == "IoTDevice"){
      let compute = ["jurisdiction", "usageCostPerDay", "capacity", "transferCostPerGB", "costIncurred", "compromised", "name"]
      return compute
    }
    else if(nodeType == "DataSpecificRole" || nodeType == "DataProducer" || nodeType == "DataSubject" || nodeType == "DataProcessor" || nodeType == "DataController"){
      let data = ["location", "name"]
      return data
    }
    else if(nodeType == "Database"){
      let base = ["encrypted", "name"]
      return base
    }
    else if(nodeType == "Record"){
      let record = ["personal", "sensitive", "encrypted", "name"]
      return record
    }
    else if(nodeType == "DataFlow"){
      let flow = ["disab", "amountOfDataInGB", "connectionType", "name"]
      return flow
    }
    else if(nodeType == "SoftwareComponent" || nodeType == "Gateway" || nodeType == "DBMS" || nodeType == "WebServer" || nodeType == "Container_Runtime"){
      let software = ["neededCapacity", "hasToBeDeployedOnFogNode", "name"]
      return software
    }
    else if(nodeType == "PrivateSpace"){
      let space = ["trustworthy", "name"]
      return space
    }
    else{
      let x = ["name"]
      return x
    }
  }

  getMonitoringEventChangeAttribute(): string{
    //Problem: Darstellung des @ in Variablenname: in Typescript nicht möglich
    return JSON.stringify({RequestType : "changeAttribute", ATZeichenid : this.currentNode.ATZeichenid, attribute: this.currentAttribute, newValue: this.currentChange, attributeType : this.currentAttributeType});
}

getMonitoringEventDeleteNode():string{
  return JSON.stringify({RequestType : "deleteNode", ATZeichenid : this.currentNode.ATZeichenid});
}

getMonitoringEventAddNode():string{
  return JSON.stringify({RequestType : "addNode", nodeType : this.typenode, nodeName : this.value})
}

getMonitoringEventAddRelation():string{
  return JSON.stringify({RequestType : "addRelation" , ATZeichenid : this.currentNode.ATZeichenid, targetid : this.currentTargetNode.ATZeichenid, relationName : this.currentRelationName , OppositeRelationName : "irrelevant"})
}

getMonitoringEventDeleteRelation():string{
  return JSON.stringify({RequestType : "deleteRelation" , ATZeichenid : this.currentNode.ATZeichenid, targetid : this.currentTargetNode.ATZeichenid, relationName : this.currentRelation.relationname })
}

isEmpty(str: string):boolean{
  if(!str || str.length === 0){
    return true
  }
  else{
    return false
  }
}

//nodeID: ATZeichenID, start = true => übergebener Knoten ist startknoten der Relation; start= false => Id in Target
nodeIDisInRelationWithNode(array: Relation[], nodeID: string, start: boolean): boolean{
  
  if(start && this.currentTargetNode != null){
    let newArray = array.filter(re => re.targetnodeid == this.currentTargetNode.ATZeichenid)
    for(let rel of newArray){
      if(rel.startnodeid == nodeID){
        return true
      }
    }
    return false
  }
  else if(!start && this.currentNode != null){
    let newArray = array.filter(re => re.startnodeid == this.currentNode.ATZeichenid)
    for(let rel of newArray){
      if(rel.targetnodeid == nodeID){
        return true
      }
    }
    return false
  }
}

//generelle überprüfung, ob der Knoten eine Relation hat
nodeIDisInRelation(array: Relation[], nodeID: string): boolean{
  
    for(let rel of array){
      if(rel.startnodeid == nodeID){
        return true
      }
    }
    for(let rel of array){
      if(rel.targetnodeid == nodeID){
        return true
      }
    }
    return false
  
}


}
