import {Component, OnDestroy, OnInit, NgModule} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {DataFetchService} from '../../../data/data-fetch.service';
import {SendInputDTO} from '../../../data/sendinput-dto.model';
import {Adaptation} from '../../../data/Adaptations/adaptation.model';
import {AsIsModel, AsIsModelSVGRepresentation} from '../../../data/As is model/as-is-model.model';
import {HttpClient} from '@angular/common/http';
import {Risk} from '../../../data/Risks/risk.model';
import {Subscription} from 'rxjs';
import {WebSocketService} from '../../../data/web-socket.service';
import {ConfirmationDialogService} from '../../../miscellaneous/confirmation-dialog/confirmation-dialog.service';
import {LegendService} from '../../../miscellaneous/legend/legend.service';
import {LegendElement} from '../../../miscellaneous/legend/legend-element.model';
import {RouterModule} from '@angular/router';
import { changeAttribute } from 'src/app/data/Monitoring Events/changeAttribute';
import { ConnectionType, tosca_nodes_root, Jurisdictions, Trustworthy } from 'src/app/data/toscanode.model';

@Component({
  selector: 'app-monitoring-events',
  templateUrl: './monitoring-events.component.html',
  styleUrls: ['./monitoring-events.component.css']
})
export class MonitoringEventsComponent implements OnInit, OnDestroy {
  svg: SafeHtml;
  asIsModelSVGRepresentation: AsIsModelSVGRepresentation;
  asIsModelSVGRepresentationComparison: AsIsModelSVGRepresentation;
  currentAdaptation: Adaptation;
  sendInput: SendInputDTO;
  errorMessage: string;

  subscriptionWebSocket: Subscription;
  subscriptionWebSocket2: Subscription;

  intervention: boolean = false;

  statusInfoString: string;
  change: changeAttribute;
  nodes: tosca_nodes_root[];
  nodeIDButtonName: string = "Node ID"

  listOfAttributes: string[]

  currentAttributeValue: string = "Current Value";
  currentAttribute: string = "No Node Selected"
  currentNode: tosca_nodes_root;
  changeDropDown: string[];
  currentChange: string = "New Value"
  currentAttributeType: string;

  value: string = ''
  clickedConfirmIncorrectValue: boolean = false

  //DropDown, TextField, Boolean
  dropdown: boolean = false
  textfield: boolean = false
  Boolean: boolean = false
  constructor(private http: HttpClient, public sanitizer: DomSanitizer, private fetch: DataFetchService, private websocket: WebSocketService, private confirmationDialogService: ConfirmationDialogService, public legendService: LegendService) {  }
  
  ngOnInit(): void
  {

    this.initRequest();
    this.subscriptionWebSocket = this.websocket.getLiveUpdateContent().subscribe(() => {
      //this.initRequest()
      if(this.sendInput.AsIs.AsIsModel != null)
      {
        this.confirmationDialogService.confirm('A new as is model is available!', 'Got to the Home-menu to look at the changes')
          .then((confirmed) =>
          {
            
            //TODO change path to content-home
            //console.log('User confirmed:', confirmed)
          }/*)
          .catch(() =>
          {
            console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)')
          }*/);
      }
    })
    this.subscriptionWebSocket2 = this.websocket.getLiveIntervention().subscribe(() => {
      this.initRequest();
      
      this.confirmationDialogService.confirm('OperatorWeb Intervention', 'Please take a look at the risks and proposed adaptations.')
          .then((confirmed) =>
          {
            //console.log('User confirmed:', confirmed)
          });
      
    });
  }

  ngOnDestroy() {
    this.subscriptionWebSocket.unsubscribe();
    this.subscriptionWebSocket2.unsubscribe();
  }

  initRequest(): void
  {
    //maybe needs correction (s.o.)
    this.fetch.getStatusInformation().subscribe((data: SendInputDTO) =>
    {
      this.sendInput = data;
      
      this.fetch.postModel(this.sendInput).subscribe((data: AsIsModelSVGRepresentation) =>
      {
        this.asIsModelSVGRepresentation = data[0]
        this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));
        this.statusInfoString = JSON.stringify(this.sendInput);
      
        let x = ["No Node Selected"]
        this.listOfAttributes = x

        this.statusInfoString = this.statusInfoString.replace(/@/g, "ATZeichen")
        const obj = JSON.parse(this.statusInfoString);
        this.sendInput = obj;
        console.log("replaced: " + this.statusInfoString)
        this.nodes = this.sendInput.AsIs.AsIsModel.tosca_nodes_root;
        console.log(this.nodes[0].ATZeichenid);
      }, error => {
        console.log(error)
        this.errorMessage = error
      })

      })
  }

  changeConfirmed(): void{
    if(this.textfield){
      console.log(this.value)
      this.currentChange = this.value
      if(this.currentAttribute != "name"){
        if(!isNaN(Number(this.value))){
          //is Number -> has to be positive
          var numberValue = Number(this.value)
          if(numberValue >= 0){
            //z.T. double-Werte nicht erlaubt
            if((this.currentAttribute == "capacity" || this.currentAttribute == "neededCapacity") && !Number.isInteger(numberValue)){
              console.log("incorrect Value")
              this.clickedConfirmIncorrectValue = true
              return
            }
          }
          else{
            console.log("incorrect Value")
            this.clickedConfirmIncorrectValue = true
            return
          }
        }
        else{
          console.log("incorrect Value")
          this.clickedConfirmIncorrectValue = true
          return
        }
      }
    }

    this.confirmationDialogService.confirm('Confirmation of change of an attribute', 'Should the following change be executed?: Node:' + this.currentNode.ATZeichenid + ', Attribute: ' + this.currentAttribute + ', Old Value: ' + this.currentAttributeValue + ', New Value: ' + this.currentChange)
          .then((confirmed) =>
          {
            
            console.log("Change confirmed")
            let x = this.getMonitoringEventChangeAttribute().replace(/ATZeichen/g, "@")

            console.log(x)
            this.fetch.postMonitoringEvent(x).subscribe((data: any) =>
            {
              console.log("successful")
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
  }

  deleteNode(){
    this.confirmationDialogService.confirm('Confirmation of deletion of a node', 'Should the following node be deleted?: Node:' + this.currentNode.ATZeichenid)
          .then((confirmed) =>
          {
            
            console.log("Deletion confirmed")
            let x = this.getMonitoringEventDeleteNode().replace(/ATZeichen/g, "@")

            console.log(x)
            this.fetch.postMonitoringEvent(x).subscribe((data: any) =>
            {
              console.log("successful")
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
  }

  selectNodeID(node: tosca_nodes_root){
    this.nodeIDButtonName = node.ATZeichenid;
    this.currentNode = node;
    this.listOfAttributes = this.getAttributesForType(node.type);
    this.currentAttribute = "Attribute to be changed"
  }

  selectAttribute(str: string){
    
    this.currentAttribute = str
    
    
    //if Node selected
    if(this.currentNode != null && str != "Keine Attribute"){
      switch(str){
        //DROPDOWN START
        case "connectionType":{
          this.currentAttributeValue = this.currentNode.connectionType.toString();
          this.changeDropDown = Object.values(ConnectionType);
          this.currentAttributeType = typeof this.currentNode.connectionType
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        case "jurisdiction":{
          this.currentAttributeValue = this.currentNode.jurisdiction.toString();
          this.changeDropDown = Object.values(Jurisdictions)
          this.currentAttributeType = typeof this.currentNode.jurisdiction
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        case "location":{
          this.currentAttributeValue = this.currentNode.location.toString();
          this.changeDropDown = Object.values(Jurisdictions)
          this.currentAttributeType = typeof this.currentNode.jurisdiction
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        case "trustlevel":{
          this.currentAttributeValue = this.currentNode.trustlevel.toString();
          this.changeDropDown = Object.values(Trustworthy)
          this.currentAttributeType = typeof this.currentNode.trustlevel
          this.dropdown = true
          this.textfield = false
          this.Boolean = false
          break;
        }
        //DROPDOWN END
        //boolean start
        case "encrypted":{
          this.currentAttributeValue = this.currentNode.encrypted.toString();
          this.currentAttributeType = typeof this.currentNode.encrypted
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "personal":{
          this.currentAttributeValue = this.currentNode.personal.toString();
          this.currentAttributeType = typeof this.currentNode.personal
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "sensitive":{
          this.currentAttributeValue = this.currentNode.sensitive.toString();
          this.currentAttributeType = typeof this.currentNode.sensitive
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "disab":{
          this.currentAttributeValue = this.currentNode.disab.toString();
          this.currentAttributeType = typeof this.currentNode.disab
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "hasToBeDeployedOnFogNode":{
          this.currentAttributeValue = this.currentNode.hasToBeDeployedOnFogNode.toString();
          this.currentAttributeType = typeof this.currentNode.hasToBeDeployedOnFogNode
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "costIncurred":{
          this.currentAttributeValue = this.currentNode.costIncurred.toString();
          this.currentAttributeType = typeof this.currentNode.costIncurred
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        case "compromised":{
          this.currentAttributeValue = this.currentNode.compromised.toString();
          this.currentAttributeType = typeof this.currentNode.compromised
          this.dropdown = false
          this.textfield = false
          this.Boolean = true
          break;
        }
        //boolean end
        //textfield start
        case "name":{
          this.currentAttributeValue = this.currentNode.name.toString();
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "amountOfDataInGB":{
          this.currentAttributeValue = this.currentNode.amountOfDataInGB.toString();
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "neededCapacity":{
          this.currentAttributeValue = this.currentNode.neededCapacity.toString();
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "transferCostPerGB":{
          this.currentAttributeValue = this.currentNode.transferCostPerGB.toString();
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "usageCostPerDay":{
          this.currentAttributeValue = this.currentNode.usageCostPerDay.toString();
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        case "capacity":{
          this.currentAttributeValue = this.currentNode.capacity.toString();
          this.dropdown = false
          this.textfield = true
          this.Boolean = false
          break;
        }
        //textfield end
      }
    }else{

    }
  }
  
  selectNewValue(change: string){
    this.currentChange = change
  }

  //Helper-Method(s)
  getAttributesForType(nodeType: string): string[]{
    
    if(nodeType == "Compute" || nodeType == "CloudCompute" || nodeType == "FogCompute" || nodeType == "IoTDevice"){
      let compute = ["jurisdiction", "usageCostPerDay", "capacity", "transferCostPerGB", "costIncurred", "compromised", "name"]
      return compute
    }
    else if(nodeType == "DataSpecificRole" || nodeType == "DataProducer" || nodeType == "DataSubject" || nodeType == "DataProcessor" || nodeType == "DataController"){
      let data = ["location", "name"]
      return data
    }
    else if(nodeType == "Database"){
      let base = ["encrypted", "name"]
      return base
    }
    else if(nodeType == "Record"){
      let record = ["personal", "sensitive", "encrypted", "name"]
      return record
    }
    else if(nodeType == "DataFlow"){
      let flow = ["disab", "amountOfDataInGB", "connectionType", "name"]
      return flow
    }
    else if(nodeType == "SoftwareComponent" || nodeType == "Gateway" || nodeType == "DBMS" || nodeType == "WebServer" || nodeType == "Container_Runtime"){
      let software = ["neededCapacity", "hasToBeDeployedOnFogNode", "name"]
      return software
    }
    else if(nodeType == "PrivateSpace"){
      let space = ["trustworthy", "name"]
      return space
    }
    else{
      let x = ["name"]
      return x
    }
  }

  getMonitoringEventChangeAttribute(): string{
    //Problem: Darstellung des @ in Variablenname: in Typescript nicht möglich
    return JSON.stringify({RequestType : "changeAttribute", ATZeichenid : this.currentNode.ATZeichenid, attribute: this.currentAttribute, newValue: this.currentChange, attributeType : this.currentAttributeType});
}

getMonitoringEventDeleteNode():string{
  return JSON.stringify({RequestType : "deleteNode", ATZeichenid : this.currentNode.ATZeichenid});
}

}
