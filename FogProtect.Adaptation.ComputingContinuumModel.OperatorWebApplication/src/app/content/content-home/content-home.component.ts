import {Component, OnDestroy, OnInit} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {DataFetchService} from '../../data/data-fetch.service';
import {AsIsModelSVGRepresentation} from '../../data/As is model/as-is-model.model';
import {SendInputDTO} from '../../data/sendinput-dto.model';
import {Risk} from '../../data/Risks/risk.model';
import {WebSocketService} from '../../data/web-socket.service';
import {Subscription} from 'rxjs';
import {ConfirmationDialogService} from '../../miscellaneous/confirmation-dialog/confirmation-dialog.service';
import {LegendElement} from '../../miscellaneous/legend/legend-element.model';
import {LegendService} from '../../miscellaneous/legend/legend.service';
import {RisksService} from '../../miscellaneous/risk-service/risks.service';

@Component({
  selector: 'app-content-home',
  templateUrl: './content-home.component.html',
  styleUrls: ['./content-home.component.scss'],
})
export class ContentHomeComponent implements OnInit, OnDestroy {

  svg: SafeHtml;
  asIsModelSVGRepresentation: AsIsModelSVGRepresentation;
  asIsModelSVGRepresentationComparison: AsIsModelSVGRepresentation;
  errorMessage: string;

  sendInput: SendInputDTO;
  currentRisks: number[] = [];
  legendElements: LegendElement[] = [];

  subscriptionWebSocket: Subscription;
  subscriptionWebSocket2: Subscription;

  comparisonButton: HTMLElement;
  showComparison: boolean = true;

  constructor(public sanitizer: DomSanitizer, private fetch: DataFetchService, private websocket: WebSocketService, private confirmationDialogService: ConfirmationDialogService,
              public legendService: LegendService, public risksService: RisksService) {
  }

  ngOnInit(): void
  {
    this.initRequest();
    
    this.subscriptionWebSocket2 = this.websocket.getLiveIntervention().subscribe(() => {
      
      this.confirmationDialogService.confirm('The intervention of the operator is required', 'Go to Decision-menu to look at the problem')
        .then((confirmed) =>
        {
            //TODO change path to decisions tab

            //console.log('User confirmed:', confirmed)
        });
      
    });

    this.subscriptionWebSocket = this.websocket.getLiveUpdateContent().subscribe(() => {

      if(this.sendInput.AsIs.AsIsModel == null && this.sendInput.AsIs.AsIsModel == null)
      {
        this.initRequest();
      }

      if(this.sendInput.AsIs.AsIsModel != null)
      {
        this.initRequest();

        this.confirmationDialogService.confirm('A new "As is" model is available!', 'The changed nodes are highlighted in color.')
          .then((confirmed) =>
          {
            //console.log('User confirmed:', confirmed)
          }/*)
          .catch(() =>
          {
            console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)')
          }*/);
      }
    });
  }

  ngOnDestroy() {
    this.subscriptionWebSocket.unsubscribe();
    this.subscriptionWebSocket2.unsubscribe();
  }

  initRequest(): void
  {
    this.fetch.getStatusInformation().subscribe((data: SendInputDTO) =>
    {
      this.sendInput = data;

      if(this.sendInput.AsIs.AsIsModel != null)
      {
        this.fetch.postModel(data).subscribe((data: AsIsModelSVGRepresentation) =>
        {
          this.asIsModelSVGRepresentation = data[0];
          if(this.sendInput.AsIs.OldAsIsModel == null)
          {
            this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));
          }
          else
          {
            this.fetch.getComparison("OldAsIsModel", "AsIsModel").subscribe((data: AsIsModelSVGRepresentation) =>
            {
              this.asIsModelSVGRepresentationComparison = data[0];
              this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));

              this.showComparison = true;
              this.comparisonButton = document.getElementById('buttonShowDifferenceFromOldAsIsModel') as HTMLElement;
              this.legendService.createLegendList(this.legendElements, true,null);

              if(this.comparisonButton != null)
                this.comparisonButton.className = "btn btn-success";

              if(this.errorMessage)
                this.errorMessage = null;
            });
          }
        }, error => {
          console.log(error);
          this.errorMessage = error;
        });
      }
      else
      {
        this.errorMessage = '"As is" model is null!';
      }

    }, error =>
    {
      this.errorMessage = error;
    });
  }

  selectRisks(risk: Risk, index: number): void
  {
    this.legendElements.length = 0;
    if(this.showComparison)
      this.toogleShowComparison();

    let clickedButton = document.getElementById('riskElement' + index) as HTMLElement;

    if(this.currentRisks.includes(index))
    {
      this.currentRisks = this.currentRisks.filter(e => e != (index));
      clickedButton.className = "btn btn-danger";

      if(this.currentRisks.length == 0)
      {
        this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentation.encodedSvg));
        return;
      }
    }
    else
    {
      this.currentRisks.push(risk.ID);
      clickedButton.className = "btn btn-success";
    }

    this.fetch.getRisks("AsIsModel", this.risksService.createRiskString(this.currentRisks), false).subscribe((data: AsIsModelSVGRepresentation) =>
    {
      this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));

      this.legendService.createLegendList(this.legendElements, false, data[0].printedRisks);
    });
  }

  disableAllRisks(): void
  {
    for(var i = 0; i < this.currentRisks.length; i++)
    {
      let button = document.getElementById('riskElement' + i) as HTMLElement;
      button.className = "btn btn-danger";
    }
    this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentation.encodedSvg));
    this.currentRisks.length = 0;
  }

  toogleShowComparison(): void
  {
    this.comparisonButton = document.getElementById('buttonShowDifferenceFromOldAsIsModel') as HTMLElement;
    if(this.showComparison)
    {
      this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentation.encodedSvg))
      this.comparisonButton.className = "btn btn-danger"
    }
    else
    {
      if(this.currentRisks.length > 0)
        this.disableAllRisks();

      this.legendService.createLegendList(this.legendElements, true,null);
      this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentationComparison.encodedSvg))
      this.comparisonButton.className = "btn btn-success"
    }

    this.showComparison = !this.showComparison;
  }
}
