import {Component, OnDestroy, OnInit} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {DataFetchService} from '../../../data/data-fetch.service';
import {SendInputDTO} from '../../../data/sendinput-dto.model';
import {Adaptation} from '../../../data/Adaptations/adaptation.model';
import {AsIsModel, AsIsModelSVGRepresentation} from '../../../data/As is model/as-is-model.model';
import {HttpClient} from '@angular/common/http';
import {Risk} from '../../../data/Risks/risk.model';
import {Subscription} from 'rxjs';
import {WebSocketService} from '../../../data/web-socket.service';
import {ConfirmationDialogService} from '../../../miscellaneous/confirmation-dialog/confirmation-dialog.service';
import {LegendService} from '../../../miscellaneous/legend/legend.service';
import {LegendElement} from '../../../miscellaneous/legend/legend-element.model';
import {RouterModule} from '@angular/router';


@Component({
  selector: 'app-adaption-menu',
  templateUrl: './adaption-menu.component.html',
  styleUrls: ['./adaption-menu.component.scss']
})
export class AdaptionMenuComponent implements OnInit, OnDestroy {

  svg: SafeHtml;
  asIsModelSVGRepresentation: AsIsModelSVGRepresentation;
  asIsModelSVGRepresentationComparison: AsIsModelSVGRepresentation;
  currentAdaptation: Adaptation;
  sendInput: SendInputDTO;
  currentRisks: number[] = [];
  errorMessage: string;

  legendElements: LegendElement[] = [];

  clickedConfirmAdaptionLockedButton = false;
  clickedConfirmAdaptionButton = false;

  subscriptionWebSocket: Subscription;
  subscriptionWebSocket2: Subscription;

  currentAdaptationButton: HTMLElement;
  comparisonButton: HTMLElement;
  showComparison: boolean = true;

  router: RouterModule
  //TODO correction if testData is no longer in use
  AsIsRisks: Risk[] //= testData.AsIs.Risks
  Adaptations: Adaptation[] //= testData.Adaptations
  adaptation: Adaptation
  
 
  intervention: boolean = false;

  //to show the Risks of the selected Adaptation
  showAdaptationRisks: Adaptation = null
  showAdaptationRisksIndex: number
  showAdaptationRiskTable: boolean = false;

  svgStatus: String;

  constructor(private http: HttpClient, public sanitizer: DomSanitizer, private fetch: DataFetchService, private websocket: WebSocketService, private confirmationDialogService: ConfirmationDialogService, public legendService: LegendService) {  }
  
  ngOnInit(): void
  {

    this.initRequest();
    this.subscriptionWebSocket = this.websocket.getLiveUpdateContent().subscribe(() => {
      //this.initRequest()
      if(this.sendInput.AsIs.AsIsModel != null)
      {
        this.confirmationDialogService.confirm('A new as is model is available!', 'Got to the Home-menu to look at the changes')
          .then((confirmed) =>
          {
            
            //TODO change path to content-home
            //console.log('User confirmed:', confirmed)
          }/*)
          .catch(() =>
          {
            console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)')
          }*/);
      }
    })
    this.subscriptionWebSocket2 = this.websocket.getLiveIntervention().subscribe(() => {
      this.initRequest();
      
      this.confirmationDialogService.confirm('OperatorWeb Intervention', 'Please take a look at the risks and proposed adaptations.')
          .then((confirmed) =>
          {
            //console.log('User confirmed:', confirmed)
          });
      
    });
  }

  ngOnDestroy() {
    this.subscriptionWebSocket.unsubscribe();
    this.subscriptionWebSocket2.unsubscribe();
  }

  initRequest(): void
  {
    //maybe needs correction (s.o.)
    this.fetch.getStatusInformation().subscribe((data: SendInputDTO) =>
    {
      this.sendInput = data;
    
     
      //comment in when communication with server works
      this.fetch.postModel(this.sendInput).subscribe((data: AsIsModelSVGRepresentation) =>
      {
        this.asIsModelSVGRepresentation = data[0]
        this.Adaptations = this.sendInput.Adaptations
        this.AsIsRisks = this.sendInput.AsIs.Risks
        this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));
        this.svgStatus = "The Current AsIs-Model"
      }, error => {
        console.log(error)
        this.errorMessage = error
      })

      //comment out when communication with server works
      //this.fetch.postModel(this.demoData).subscribe((data: AsIsModelSVGRepresentation) =>
      //  {
      //    this.asIsModelSVGRepresentation = data[0];
          
      //    this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));
      //    this.svgStatus = "The Current AsIs-Model"
          
      //  }, error => {
      
      //console.log(error);
      //    this.errorMessage = error;
      //  });
      })
  }

  selectRisks(risk: Risk, index: number): void
  {

    //every adaptationbutton + inspectRiskbutton red
    this.showAdaptationRiskTable = false
    
    this.svgStatus = "Risk: " + risk.Name
    
    //alle Butttons von Adaptionen auf rot setzen
    for(var i = 0; i < this.Adaptations.length; i++)
    {
      console.log(i)
      let adaptationRiskButton = document.getElementById('adaptationRiskButton' + i) as HTMLElement;
      if(adaptationRiskButton != null){
        adaptationRiskButton.className = "btn btn-danger";
      }
      let adaptationButton =  document.getElementById('adaptionButton' + i) as HTMLElement;
      if(adaptationButton == null){
        console.log("adaptationButton null")
      }

      
      adaptationButton.className = "btn btn-danger";
    }
    
    let clickedButton = document.getElementById('risk' + index) as HTMLElement;
    //clickedButton.className = "btn btn-success";
    this.showComparison = true

    //this.toogleShowComparison()
    for(var i = 0; i < this.AsIsRisks.length; i++)
    {
      let riskButton = document.getElementById('risk' + i) as HTMLElement;
      console.log("index: " + index)
      console.log("i: " + i)
      if(i != index)
      {
        riskButton.className = "btn btn-danger";
        console.log("1. if")
      }
      else if(i==index && clickedButton.className == "btn btn-success"){
        clickedButton.className = "btn btn-danger";
        console.log("2. if")
        this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentation.encodedSvg));
        this.svgStatus = "The Current AsIs-Model"
        //this.legendService.createLegendList(this.legendElements, true, null)
        return;
      }
      else if(i==index && clickedButton.className == "btn btn-danger"){
        clickedButton.className = "btn btn-success";
        console.log("3. if")
        this.fetch.getRisks("AsIsModel", risk.ID.toString(), false).subscribe((data: AsIsModelSVGRepresentation) =>
        {
        this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));
        console.log(data[0].printedRisks)

        //this.legendService.createLegendList(this.legendElements, false, data[0].printedRisks);

        });
      }
      
    }

  }

  selectAdaption(adaptation: Adaptation, index: number): void
  {

    //every risk-button red
    for(var i = 0; i < this.AsIsRisks.length; i++)
    {
      let riskbutton = document.getElementById('risk' + i) as HTMLElement;
      riskbutton.className = "btn btn-danger"
    }

    this.currentAdaptation = adaptation;
    
    //Comparison between OldAsIsModel and current AsIsModel is not shown -> Button should be red
    this.showComparison = true
    //this.toogleShowComparison()

    //the same adaptation is selected twice
    if(this.currentAdaptationButton == (document.getElementById('adaptionButton' + index) as HTMLElement))
    {
      this.currentAdaptationButton.className = "btn btn-danger";
      this.currentAdaptationButton = null;
      this.currentAdaptation = null;


      this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentation.encodedSvg));
      this.svgStatus = "The Current AsIs-Model"
      return;
    }
    else{
      
      this.svgStatus = "Adaptation: " + this.currentAdaptation.Name

      for(var i = 0; i < this.Adaptations.length; i++)
      {
      let button = document.getElementById('adaptionButton' + i) as HTMLElement;
      if(index == i)
      {
        this.currentAdaptationButton = document.getElementById('adaptionButton' + i) as HTMLElement;
        button.className = "btn btn-success";

        
        continue;
      }
      button.className = "btn btn-danger";
    }

      this.fetch.getComparison("AsIsModel", this.currentAdaptation.ID.toString()).subscribe((data: AsIsModelSVGRepresentation)=>
      {
        this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(data[0].encodedSvg));
        
      })
    }

    
  }

  toogleShowComparison(): void
  {
    this.comparisonButton = document.getElementById('buttonShowDifferenceFromOldAsIsModel') as HTMLElement;
    if(this.showComparison)
    {
      this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentation.encodedSvg))
      this.comparisonButton.className = "btn btn-danger"
    }
    else
    {
      this.svg = this.sanitizer.bypassSecurityTrustHtml(atob(this.asIsModelSVGRepresentationComparison.encodedSvg))
      //this.legendService.createLegendList(this.legendElements, true,null);
      this.comparisonButton.className = "btn btn-success"
    }
    this.showComparison = !this.showComparison;
  }

  createRiskString(): string
  {
    var sortedArray: number[] = this.currentRisks.sort((n1,n2) => n1 - n2);
    let tmp = "";
    for(var i = 0; i < sortedArray.length - 1; i++)
    {
      tmp += sortedArray[i] + ",";
    }
    tmp += sortedArray[sortedArray.length - 1];
    return tmp;
  }

  alertNoAdaptionSelected(): void
  {
    this.clickedConfirmAdaptionLockedButton = true;
  }

  inspectRiskAdaptation(adaptation: Adaptation, index: number)
  {
    console.log("Risks inspected")
    
    let clickedButton = document.getElementById('adaptionRiskButton' + index) as HTMLElement;
    
    for(var i = 0; i < this.Adaptations.length; i++)
    {
      if(index == i && clickedButton.className == "btn btn-success"){
        clickedButton.className = "btn btn-danger"
        this.showAdaptationRiskTable = false
        this.showAdaptationRisks = null;
      }
      else if (index == i)
      {
        this.showAdaptationRisks = adaptation
        this.showAdaptationRisksIndex = index+1
        clickedButton.className = "btn btn-success"
        this.showAdaptationRiskTable = true;
      }
      else
      {
        let button = document.getElementById('adaptionRiskButton' + i) as HTMLElement;
        button.className = "btn btn-danger"
      }
    }
  }

  inspectSpecificRisk(risk: Risk)
  {
    console.log("Specific Risk inspected")
    //TODO show the risk??
  }

  adaptationConfirmed(): void
  {
    console.log("Adaptation chosen")

    if(this.currentAdaptation != null)
    {
      this.confirmationDialogService.confirm('Confirmation of selectected adaptation', 'Should the following adaptation be executed?: ' + this.currentAdaptation.Name)
          .then((confirmed) =>
          {
            console.log("Adaptation confirmed")
            this.fetch.postAdaptation(this.currentAdaptation).subscribe((data: any) =>
            {
              console.log("successful")
            }, error => {
            console.log(error)
          this.errorMessage = error
             })
          });
    }
  }
}
