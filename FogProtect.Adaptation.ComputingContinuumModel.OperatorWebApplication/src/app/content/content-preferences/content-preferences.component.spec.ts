import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentPreferencesComponent } from './content-preferences.component';

describe('ContentPreferencesComponent', () => {
  let component: ContentPreferencesComponent;
  let fixture: ComponentFixture<ContentPreferencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentPreferencesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
