import { Component, OnDestroy,OnInit } from '@angular/core';
import {WebSocketService} from '../../data/web-socket.service';
import {Subscription} from 'rxjs';
import {ConfirmationDialogService} from '../../miscellaneous/confirmation-dialog/confirmation-dialog.service';
import {DataFetchService} from '../../data/data-fetch.service';

@Component({
  selector: 'app-content-preferences',
  templateUrl: './content-preferences.component.html',
  styleUrls: ['./content-preferences.component.css']
})
export class ContentPreferencesComponent implements OnInit {

  subscriptionWebSocket: Subscription;
  subscriptionWebSocket2: Subscription;
  errorMessage: string;

  firstPreference: string;
  secondPreference: string;
  thirdPreference: string;
  fourthPreference: string;

  preferences: string[];

  clickedConfirmIncorrectValue: boolean;

  constructor(private fetch: DataFetchService, private websocket: WebSocketService, private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit(): void {
    this.resetChoices();

    this.subscriptionWebSocket2 = this.websocket.getLiveIntervention().subscribe(() => {
      
      this.confirmationDialogService.confirm('The intervention of the operator is required', 'Go to Decision-menu to look at the problem')
        .then((confirmed) =>
        {
            //TODO change path to decisions tab

            //console.log('User confirmed:', confirmed)
        });
      
    });

    this.subscriptionWebSocket = this.websocket.getLiveUpdateContent().subscribe(() => {
     
        this.confirmationDialogService.confirm('A new "As is" model is available!', 'Go to Home-menu to look at the changes')
          .then((confirmed) =>
          {
            //console.log('User confirmed:', confirmed)
          });
      });

  }

  ngOnDestroy() {
    this.subscriptionWebSocket.unsubscribe();
    this.subscriptionWebSocket2.unsubscribe();
  }

  resetChoices():void{
    console.log("reset clicked");
    this.firstPreference = "First";
    this.secondPreference = "Second";
    this.thirdPreference = "Third";
    this.fourthPreference = "Fourth";

    this.preferences = ['Cost', 'EnergyConsumption', 'Function', 'Performance'];

    this.clickedConfirmIncorrectValue = false;
  }

  selectFirst(p: string):void{
    this.firstPreference = p;
    this.preferences = this.preferences.filter(pre => pre != this.firstPreference);
  }

  selectSecond(p: string):void{
    this.secondPreference = p;
    this.preferences = this.preferences.filter(pre => pre != this.secondPreference);
  }

  selectThird(p: string):void{
    this.thirdPreference = p;
    this.preferences = this.preferences.filter(pre => pre != this.thirdPreference);
  }

  selectFourth(p: string):void{
    this.fourthPreference = p;
    this.preferences = this.preferences.filter(pre => pre != this.fourthPreference);
  }

  sendPreferences(): void{
    let x = this.getJSONString();
    console.log(x)
    if(this.firstPreference != "First" && this.secondPreference != "Second" && this.thirdPreference != "Third" && this.fourthPreference != "Fourth"){
      this.clickedConfirmIncorrectValue = false
      this.fetch.postFactorChange(x).subscribe((data: any) =>
      {
        console.log("successful")
        console.log(data)
      }, error => {
        console.log(error)
        this.errorMessage = error
      })
    }
    else{
      this.clickedConfirmIncorrectValue = true
    }
    
  }

  getJSONString():string{
    return JSON.stringify([this.firstPreference, this.secondPreference, this.thirdPreference, this.fourthPreference]);
  }
}
