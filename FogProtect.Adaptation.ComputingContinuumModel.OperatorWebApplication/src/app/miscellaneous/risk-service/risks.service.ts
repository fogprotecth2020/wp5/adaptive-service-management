import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export class RisksService {

  createRiskString(currentRisks: number[]): string
  {
    const sortedArray: number[] = currentRisks.sort((n1, n2) => n1 - n2);
    let tmp = "";
    for(let i = 0; i < sortedArray.length - 1; i++)
    {
      tmp += sortedArray[i] + ",";
    }
    tmp += sortedArray[sortedArray.length - 1];
    return tmp;
  }
}
