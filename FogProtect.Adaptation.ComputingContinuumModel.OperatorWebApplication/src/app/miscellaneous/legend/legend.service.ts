import {LegendElement} from './legend-element.model';
import {Injectable} from '@angular/core';
import {PrintedRisk} from '../../data/As is model/as-is-model.model';

@Injectable({
  providedIn: 'root',
})

export class LegendService {

  comparisonElements: LegendElement[] = [
    {colorcode: "#20F92A", description: "ADDED"},
    {colorcode: "#F3534A", description: "DELETED"},
    {colorcode: "#F1C40F", description: "CHANGED "},
  ]

  createLegendList(legendList: LegendElement[], showComparisonElementsEnabled: boolean, printedRisks: PrintedRisk[]): void
  {
    if(legendList != null)
      legendList.length = 0;

    if(showComparisonElementsEnabled == true)
    {
      for(var i = 0; i < this.comparisonElements.length ; i++){
        legendList.push(this.comparisonElements[i]);
      }
      return;
    }

    if(printedRisks != null)
    {
      for(var i = 0; i < printedRisks.length; i++)
      {
        legendList.push({colorcode: printedRisks[i].colorCode,
          description: printedRisks[i].name});
      }
    }
  }
}

