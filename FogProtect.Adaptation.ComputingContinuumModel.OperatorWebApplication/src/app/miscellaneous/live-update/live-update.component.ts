import { Component, OnInit } from '@angular/core';
import {ConfirmationDialogService} from '../confirmation-dialog/confirmation-dialog.service';

@Component({
  selector: 'app-live-update',
  templateUrl: './live-update.component.html',
  styleUrls: ['./live-update.component.css']
})
export class LiveUpdateComponent implements OnInit {

  constructor(private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit(): void
  {

  }

  public openConfirmationDialog() {
    this.confirmationDialogService.confirm('A new as is model is available!', 'Would you like to see the differences from the old model? ')
      .then((confirmed) => console.log('User confirmed:', confirmed))
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

}
