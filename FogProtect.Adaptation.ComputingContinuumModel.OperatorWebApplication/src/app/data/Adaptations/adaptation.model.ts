import {Risk} from '../Risks/risk.model';
export interface Adaptation
{
  Name: string;
  Description: string;
  ID: number
  ReferenceModel: string
  Risklevel: string
  Risks: Risk[]
  //encodedSvg: string;
  
}
