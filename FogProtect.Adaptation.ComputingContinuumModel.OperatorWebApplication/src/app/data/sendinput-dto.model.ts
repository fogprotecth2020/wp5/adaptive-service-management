import {Adaptation} from './Adaptations/adaptation.model';
import {AsIs} from './As is model/as-is.model';

export interface SendInputDTO
{
  AsIs: AsIs;
  Adaptations: Adaptation[];
}
