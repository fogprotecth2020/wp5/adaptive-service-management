import {Injectable} from '@angular/core';
import * as over from '@stomp/stompjs';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})

export class WebSocketService
{
  ws: any;
  socketURL: string = environment.urlForWebSocket;
  channelSubscription: string = "/queue/statusInformation";
  //TODO channel to subscribe to
  operatorInterventionSubscription: string = "/queue/operatorIntervention";

  private updateContent$ = new Subject<any>();
  private updateIntervention$ = new Subject<any>();

  _connect() {
    const socket = new WebSocket(this.socketURL);
    this.ws = over.Stomp.over(socket);
    this.ws.connect({}, () => {
      this.ws.subscribe(this.channelSubscription, data => {
        this.updateContent$.next(data);
      });
      
      this.ws.subscribe(this.operatorInterventionSubscription, data => {
        this.updateIntervention$.next(data);
      });
    });
  };

  _disconnect()
  {
    if(this.ws)
    {
      this.ws.unsubscribe();
      this.ws.close();
    }
  }

  getLiveUpdateContent(): Observable<any>
  {
    return this.updateContent$.asObservable();
  }

  getLiveIntervention(): Observable<any>
  {
    return this.updateIntervention$.asObservable();
  }
}
