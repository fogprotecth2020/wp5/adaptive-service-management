export interface tosca_nodes_root
{
  //nicht alle Knoten haben alle Attribute -> type ist Klassenname
  readonly type: string //lässt sich nicht ändern, ist Klassenname
  readonly ATZeichenid: string //lässt sich nicht ändern
  readonly id: number //lässt sich nicht ändern
  readonly name: string 
  readonly encrypted?: boolean
  readonly personal?: boolean
  readonly sensitive?: boolean
  readonly disab?: boolean
  readonly hasToBeDeployedOnFogNode?: boolean
  readonly amountOfDataInGB?: number
  readonly connectionType?: ConnectionType
  readonly neededCapacity?: number
  readonly transferCostPerGB?: number
  readonly usageCostPerDay?: number
  readonly costIncurred?: boolean
  readonly jurisdiction?: Jurisdictions
  readonly compromised?: boolean
  readonly capacity?: number
  readonly trustworthy?: Trustworthy
  readonly location?: Jurisdictions
  readonly [propName: string]: any
  readonly blocked?: boolean
  readonly blockedReading?: boolean
  readonly blockedWriting?: boolean
  readonly groupsActor?: any
  readonly groupsGroup?: any
  readonly groupGroupedByGroup?: any
  readonly actorGroupedByGroup?: any
}

export enum Jurisdictions{
  BE = "BE",
  BG = "BG",
  CZ = "CZ",
  DK = "DK",
  DE = "DE",
  EE = "EE",
  IE = "IE",
  EL = "EL",
  ES = "ES",
  FR = "FR",
  HR = "HR",
  IT = "IT",
  CY = "CY",
  LV = "LV",
  LT = "LT",
  LU = "LU",
  HU = "HU",
  MT = "MT",
  NL = "NL",
  AT = "AT",
  PL = "PL",
  PT = "PT",
  RO = "RO",
  SI = "SI",
  SK = "SK",
  Fi = "Fi",
  SE = "SE",
  UK = "UK",
  CH = "CH",
  EU = "EU",
  US = "US",
}

export enum Trustworthy{
  VERYHIGH = "VERYHIGH",
  HIGH = "HIGH",
  MEDIUM = "MEDIUM",
  LOW = "LOW",
  VERYLOW = "VERYLOW",
}

export enum ConnectionType{
  Local = "Local",
  VPN = "VPN",
  Internet = "Internet",
}

export enum NodeType{
  Compute = "Compute",
  CloudCompute = "CloudCompute",
  FogCompute = "FogCompute",
  IoTDevice = "IoTDevice",
  DataSpecificRole = "DataSpecificRole",
  DataProducer = "DataProducer",
  DataSubject = "DataSubject",
  DataProcessor = "DataProcessor",
  DataController = "DataController",
  Database = "Database",
  Record = "Record",
  DataFlow = "DataFlow",
  SoftwareComponent = "SoftwareComponent",
  Gateway = "Gateway",
  DBMS = "DBMS",
  WebServer = "WebServer",
  Container_Runtime = "Container_Runtime"
}

export interface Relation{
  startnodeid: string
  targetnodeid: string
  relationname: string
}

export class relationClass implements Relation{
  startnodeid: string
  targetnodeid: string
  relationname: string

  toString():string{
    let str = "Startnode: " + this.startnodeid + ", Targetnode: " + this.targetnodeid + ", Relationname: " + this.relationname
    return str
  }
}

export enum listOfRelations{
  operates = "operates",
  operatesCompute = "operatesCompute",
  uses = "uses",
  creates = "creates",
  owns = "owns",
  consistsOf = "consistsOf",
  transfersTo = "transfersTo",
  hosts = "hosts",
  controls = "controls",
  sendsTo = "sendsTo",
  accesses = "accesses",
  manages = "manages",
  transferBy = "transferBy",
  partOf = "partOf",
  stores = "stores",
  operatedByIaaSOperator = "operatedByIaaSOperator",
  operatedByPaaSOperator = "operatedByPaaSOperator"
}