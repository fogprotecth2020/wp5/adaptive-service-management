export class deleteRelation{
    RequestType: string;
    atid: string;
    targetid: string;
    relationName: string;

    constructor(atID: string, targetid: string, relationName: string) {
        this.RequestType = "deleteRelation"
        this.atid = atID;
        this.targetid = targetid;
        this.relationName = relationName;
    }

    getMonitoringEvent(): any{
        //Problem: Darstellung des @ in Variablenname: in Typescript nicht möglich
        return JSON.stringify({RequestType : this.RequestType, atid : this.atid, targetid: this.targetid, 
            relationName: this.relationName});
    }
}