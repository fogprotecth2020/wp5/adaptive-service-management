export class deleteNode{
    RequestType: string;
    atid: string;

    constructor(atID: string) {
        this.RequestType = "deleteNode"
        this.atid = atID;
    }

    getMonitoringEvent(): any{
        //Problem: Darstellung des @ in Variablenname: in Typescript nicht möglich
        return JSON.stringify({RequestType : this.RequestType, atid : this.atid});
    }
}