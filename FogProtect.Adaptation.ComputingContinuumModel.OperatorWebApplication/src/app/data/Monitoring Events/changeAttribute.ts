export class changeAttribute{
    RequestType: string ="changeAttribute"
    ATZeichenid: string;
    attribute: string;
    newValue: string;
    attributeType: string;

    constructor(atID: string, attribute: string, newValue: string, attributeType: string) {
        this.ATZeichenid = atID;
        this.attribute = attribute;
        this.newValue = newValue;
        this.attributeType = attributeType;
    }

    setAtID(newID: string) : void{
        this.ATZeichenid = newID;
    }

    setAttribute(newAttribute: string):void{
        this.attribute = newAttribute
    }

    setNewValue(newNewValue: string):void{
        this.newValue = newNewValue
    }

    setAttributeType(newAttributeType: string):void{
        this.attributeType = newAttributeType;
    }
    
    getMonitoringEvent(): any{
        //Problem: Darstellung des @ in Variablenname: in Typescript nicht möglich
        return JSON.stringify({RequestType : this.RequestType, ATZeichenid : this.ATZeichenid, attribute: this.attribute, newValue: this.newValue, attributeType : this.attributeType});
    }
}


