export class addRelation{
    RequestType: string;
    atid: string;
    targetid: string;
    relationName: string;
    OppositeRelationName: string;

    constructor(atID: string, targetid: string, relationName: string, OppositeRelationName: string) {
        this.RequestType = "addRelation"
        this.atid = atID;
        this.targetid = targetid;
        this.relationName = relationName;
        this.OppositeRelationName = OppositeRelationName;
    }

    getMonitoringEvent(): any{
        //Problem: Darstellung des @ in Variablenname: in Typescript nicht möglich
        return JSON.stringify({RequestType : this.RequestType, atid : this.atid, targetid: this.targetid, 
            relationName: this.relationName, OppositeRelationName : this.OppositeRelationName});
    }
}