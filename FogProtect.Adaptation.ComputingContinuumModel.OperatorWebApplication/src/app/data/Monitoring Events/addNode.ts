export class addNode{
    RequestType: string;
    nodeType: string;
    nodeName: string;
    
   

    constructor(nodeType: string, nodeName: string) {
        this.RequestType = "addNode"
        this.nodeType = nodeType
        this.nodeName = nodeName       
    }

    getMonitoringEvent(): any{
        return JSON.stringify({RequestType : this.RequestType, nodeType : this.nodeType, nodeName: this.nodeName});
    }
}