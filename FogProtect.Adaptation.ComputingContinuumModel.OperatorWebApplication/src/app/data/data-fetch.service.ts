import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SendInputDTO} from './sendinput-dto.model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import { Adaptation } from './Adaptations/adaptation.model';

@Injectable({
  providedIn: 'root',
})

export class DataFetchService
{

  urlForAdaptationCoordinator = environment.urlForAdaptationCoordinator;
  urlForModelVisuals = environment.urlForModelVisuals;

  constructor(private http: HttpClient) {
  }

  getStatusInformation(): Observable<SendInputDTO>
  {
    return this.http.get<SendInputDTO>(this.urlForAdaptationCoordinator + 'statusInformation');
  }

  getStatusInformationJSON(): Observable<any>
  {
    return this.http.get<any>(this.urlForAdaptationCoordinator + 'statusInformation');
  }

  getStatusInformationString(): Observable<string>
  {
    return this.http.get<string>(this.urlForAdaptationCoordinator + 'statusInformation');
  }

  //modelName / modelID: -1: AsIsModel; -2: OldAsIsModel
  //risksString besteht aus IDs der Risiken (ggf. getrennt durch Komma)
  getRisks(modelName: string, risksString: string, allRisks: boolean): any
  {
    if(allRisks == true)
    {
      return this.http.get<any>(this.urlForModelVisuals + 'models/risks/all', {
        params: {
        modelname: modelName,
    }});
    }
    return this.http.get<any>(this.urlForModelVisuals + 'models/risks/select', {
      params: {
        modelname: modelName,
        risknames: risksString
      }});
  }

   //modelName / modelID: -1: AsIsModel; -2: OldAsIsModel
   //bei Vergleich mit Adaptionen: ID der Adaption schicken
  getComparison(before: string, after: string): any
  {
    return this.http.get<any>(this.urlForModelVisuals + 'models/comparison', {
      params: {
        before: before,
        after: after
      }});
  }

  //benötigt als Input ein Objekt "SendInputDTO"
  //oder ein JSON der Status Information
  //modelName / modelID: -1: AsIsModel; -2: OldAsIsModel, -3: Comparison between two Models
  //(IDs im Rückgabe JSOn)
  postModel(data: any): any
  {
    return this.http.post<any>(this.urlForModelVisuals + 'models', data);
  }

  //zum Senden der Adaption, die Operator ausgesucht hat (adaption-menu / Operator Intervention). Gesendet wird Adaption
  postAdaptation(data: Adaptation): any{
    let x = this.http.post<any>(this.urlForAdaptationCoordinator + 'humanIntervention', data);
    return x
  }

  //Input: JSON Monitoring-Event (von Monitoring-Events generiert)
  postMonitoringEvent(data: any): any{
    console.log(data.toString())
    console.log(this.urlForAdaptationCoordinator + 'humanAdaptationRequest')
    return this.http.post<any>(this.urlForAdaptationCoordinator + 'humanAdaptationRequest', data);
  }

  postFactorChange(data: any): any{
    console.log(data.toString())
    console.log(this.urlForAdaptationCoordinator + 'changeFactors')
    var a = this.urlForAdaptationCoordinator;
    var adress = a.substring(0, a.indexOf('f'));
    console.log(adress);
    return this.http.post<any>(adress + 'changeFactors', data);
  }

  postUnblockUser(data: any): any{
    return this.http.post<any>(this.urlForAdaptationCoordinator + 'unblockUser', data);
  }
}

