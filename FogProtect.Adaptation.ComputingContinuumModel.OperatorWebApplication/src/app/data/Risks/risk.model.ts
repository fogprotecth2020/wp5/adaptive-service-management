export interface Risk
{
  Name: string;
  Description: string;
  ID: number;
}
