import { tosca_nodes_root } from "../toscanode.model";

export interface AsIsModel
{
  type: string;
  tosca_nodes_root : tosca_nodes_root[];
}

export interface AsIsModelSVGRepresentation
{
  name: string;
  modeltype: string;
  encodedSvg: string;
  printedRisks: PrintedRisk[];
}

export interface PrintedRisk
{
  name: string;
  colorCode: string;
  rgb: Number[];
}
