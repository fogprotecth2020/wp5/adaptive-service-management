import {AsIsModel} from './as-is-model.model';
import {Risk} from '../Risks/risk.model';

export interface AsIs
{
  AsIsModel: AsIsModel;
  OldAsIsModel: AsIsModel;
  Risks: Risk[];
}
