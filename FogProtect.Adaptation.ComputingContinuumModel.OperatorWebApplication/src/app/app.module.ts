import {BrowserModule, HammerModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NavComponent } from './miscellaneous/nav/nav.component';
import { ContentDecisionsComponent } from './content/content-decisions/content-decisions.component';
import { RouterModule} from '@angular/router';
import { ContentHomeComponent } from './content/content-home/content-home.component';
import { ContentActionsComponent } from './content/content-actions/content-actions.component';
import { AdaptionMenuComponent } from './content/content-decisions/adaption-menu/adaption-menu.component';
import { ZoomPanContainerComponent } from './miscellaneous/zoom-pan-container/zoom-pan-container.component';
import { FormsModule } from '@angular/forms';
import { LoadingScreenComponent } from './miscellaneous/loading-screen/loading-screen.component';

import 'hammerjs';
import { LiveUpdateComponent } from './miscellaneous/live-update/live-update.component';
import { ConfirmationDialogComponent } from './miscellaneous/confirmation-dialog/confirmation-dialog.component';
import { MonitoringEventsComponent } from './content/content-actions/monitoring-events/monitoring-events.component';
import { ContentPreferencesComponent } from './content/content-preferences/content-preferences.component';
import { ContentUnblockComponent } from './content/content-unblock/content-unblock.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ContentDecisionsComponent,
    ContentHomeComponent,
    ContentActionsComponent,
    AdaptionMenuComponent,
    ZoomPanContainerComponent,
    LoadingScreenComponent,
    LiveUpdateComponent,
    ConfirmationDialogComponent,
    MonitoringEventsComponent,
    ContentPreferencesComponent,
    ContentUnblockComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    NgbModule,
    HammerModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'home',
        component: ContentHomeComponent
      },
      {
        path: 'monitoring',
        component: MonitoringEventsComponent
      },
      {
        path: 'decisions',
        component: AdaptionMenuComponent
      },
      {
        path: 'actions',
        component: ContentActionsComponent
      },
      {
        path: 'preferences',
        component: ContentPreferencesComponent
      },
      {
        path: 'unblock',
        component: ContentUnblockComponent
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      {
        path: '**',
        component: ContentHomeComponent
      }
      ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
