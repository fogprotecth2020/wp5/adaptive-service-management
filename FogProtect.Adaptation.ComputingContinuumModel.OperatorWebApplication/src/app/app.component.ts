import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebSocketService} from './data/web-socket.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(private websocket: WebSocketService) {
  }

  ngOnInit(): void {
    this.websocket._connect();
  }

  ngOnDestroy(): void {
    this.websocket._disconnect();
  }
}
