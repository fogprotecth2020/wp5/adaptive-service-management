package emfCompare;

public class AttributePrecondition<T> implements Precondition {

	private String instanceTypeName;
	private String objName;
	private String attributeName;
	private T preconditionValue;
	
	
	
	public AttributePrecondition(String instanceTypeName, String objName, String attributeName, T preconditionValue) {
		this.instanceTypeName = instanceTypeName;
		this.objName = objName;
		this.attributeName = attributeName;
		this.preconditionValue = preconditionValue;
	}
	
	public String getInstanceTypeName() {
		return this.instanceTypeName;
	}
	
	public String getObjName() {
		return this.objName;
	}
	
	public String getAttributeName() {
		return this.attributeName;
	}
	
	public T getPreconditionValue() {
		return this.preconditionValue;
	}
	
}
