package emfCompare;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;

import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Conflict;
import org.eclipse.emf.compare.ConflictKind;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.DifferenceSource;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.internal.spec.AttributeChangeSpec;
import org.eclipse.emf.compare.internal.spec.ReferenceChangeSpec;
import org.eclipse.emf.compare.match.*;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.IdentifierEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.merge.AbstractMerger;
import org.eclipse.emf.compare.merge.AdditiveConflictMerger;
import org.eclipse.emf.compare.merge.AttributeChangeMerger;
import org.eclipse.emf.compare.merge.ConflictMerger;
import org.eclipse.emf.compare.merge.IMergeCriterion;
import org.eclipse.emf.compare.merge.IMerger;
import org.eclipse.emf.compare.merge.ReferenceChangeMerger;
import org.eclipse.emf.compare.merge.ResourceAttachmentChangeMerger;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.or;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static org.eclipse.emf.compare.utils.EMFComparePredicates.fromSide;
import static org.eclipse.emf.compare.utils.EMFComparePredicates.hasConflict;

public class CompareClient {
    
    public static void testMergeRightToLeft(EObject left, EObject right, EObject origin) {
    	//System.out.println("start copy");
    	IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.NEVER);
    	IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
    	IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(matcher, comparisonFactory);
            matchEngineFactory.setRanking(20);
            IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
            matchEngineRegistry.add(matchEngineFactory);
    	EMFCompare comparator = EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry).build();
    	
    	
        final IComparisonScope scope = new DefaultComparisonScope(right, left, origin);
        final Comparison comparisonBefore = comparator.compare(scope);
        EList<Diff> differences = comparisonBefore.getDifferences();
        final IMerger.Registry registry = IMerger.RegistryImpl.createStandaloneInstance();
        //final IMerger umlMerger = new IMerger();
        //umlMerger.setRanking(11);
        //registry.add(umlMerger);
        for (Diff diff : differences) {
            IMerger expectedMerger;
            if (diff instanceof AttributeChange) {
                expectedMerger = new AttributeChangeMerger();
            } else if (diff instanceof ReferenceChange) {
                expectedMerger = new ReferenceChangeMerger();
            } else {
                expectedMerger = new ResourceAttachmentChangeMerger();
            }
            
            expectedMerger.copyRightToLeft(diff, new BasicMonitor());
        }
        final Comparison comparisonAfter = comparator.compare(scope);
        //System.out.println("must be empty after copyAllRightToLeft " + comparisonAfter.getConflicts().isEmpty());
        
        //comparisonAfter.getConflicts().forEach(c -> System.out.println(c));
        
        
        EList<EReference> attrs = left.eClass().getEAllContainments();
        for(EReference a : attrs) {
            //System.out.println(a.getName() + " = " + left.eGet(a));
        }
        
    	//System.out.println("end copy");
    }
    
    public static void checkConflicts(List<ComperationTupel> adaptationModels, EObject asIsModel) {
	
	IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.NEVER);
	IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
	IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(matcher, comparisonFactory);
    matchEngineFactory.setRanking(20);
    IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
    matchEngineRegistry.add(matchEngineFactory);
	EMFCompare comparator = EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry).build();
	
	
	for(int i = 0; i < adaptationModels.size() - 1; i++) {
		for(int j = (i + 1); j < adaptationModels.size(); j++) {
			IComparisonScope scope = EMFCompare.createDefaultScope(adaptationModels.get(i).getCloudEnvironment(), adaptationModels.get(j).getCloudEnvironment(), asIsModel);
			
			IComparisonScope scope2 = EMFCompare.createDefaultScope(adaptationModels.get(i).getCloudEnvironment(), adaptationModels.get(j).getCloudEnvironment());
    		System.out.println("Checking adaptation " + i + " vs adaptation " + j);
			
    		boolean tmp = false;
    		
	    	List<Diff> diffs = comparator.compare(scope).getDifferences()
	    			.stream()
	    	    	  //.filter(d -> d.getKind().equals(DifferenceKind.DELETE))
	    	    	  .collect(Collectors.toList());
			
	        List<AttributeChangeSpec> changedAttributes = diffs.stream()
	                .filter(diff -> diff instanceof AttributeChangeSpec)
	                .map(changes -> (AttributeChangeSpec) changes)
	                .collect(Collectors.toList());
	        
	        // TBD: Add preconditions for references
	        
	        List<AttributePrecondition> leftAdaptation_attributePreconditions = adaptationModels.get(i).getPreconditions().stream()
	                .filter(diff -> diff instanceof AttributePrecondition)
	                .map(changes -> (AttributePrecondition) changes)
	                .collect(Collectors.toList());
	        
	        List<AttributePrecondition> rightAdaptation_attributePreconditions = adaptationModels.get(j).getPreconditions().stream()
	                .filter(diff -> diff instanceof AttributePrecondition)
	                .map(changes -> (AttributePrecondition) changes)
	                .collect(Collectors.toList());
	    	
	        if(!leftAdaptation_attributePreconditions.isEmpty() || !rightAdaptation_attributePreconditions.isEmpty()) {
	        	
	        	for(AttributePrecondition attributePrecondition : leftAdaptation_attributePreconditions)  {
	        		// TBD: Add check. If the node no longer exists, then the precondition is not met
			        for(AttributeChangeSpec attributeChangeSpec : changedAttributes) {
			    		if(attributeChangeSpec.getMatch().getRight() != null) {
			                EObject match = attributeChangeSpec.getMatch().getRight();
			                EStructuralFeature nameFeature = match.eClass().getEStructuralFeature("name");
			    			
				    		if(nameFeature != null) {
				                String objName = match.eGet(nameFeature).toString();
					    		System.out.println("[ " + attributeChangeSpec.getKind() + " ] " + match.eClass().getInstanceTypeName() + " \t" + objName + " " + attributeChangeSpec.getAttribute().getEType().getName() +
					    				" " + attributeChangeSpec.getAttribute().getName() + " " + match.eGet(attributeChangeSpec.getAttribute()));
					    		
					    		if(match.eClass().getInstanceTypeName().equals(attributePrecondition.getInstanceTypeName()) 
					    				&& objName.equals(attributePrecondition.getObjName())
					    				&& attributeChangeSpec.getAttribute().getName().equals(attributePrecondition.getAttributeName())
					    				&& match.eGet(attributeChangeSpec.getAttribute()) != attributePrecondition.getPreconditionValue()) {
					    			System.out.println("PRECONDITION NOT MET FOR ADAPTATION " + i + ": " + "[ " + attributeChangeSpec.getKind() + " ] " + match.eClass().getInstanceTypeName() + " \t" + objName + " " + attributeChangeSpec.getAttribute().getEType().getName() +
						    				" " + attributeChangeSpec.getAttribute().getName() + " " + match.eGet(attributeChangeSpec.getAttribute()));
					    			tmp = true;
					    		}
				    		}
			    		}	
			        }
	        	}
	        	
	        	for(AttributePrecondition attributePrecondition : rightAdaptation_attributePreconditions)  {
	        		
			        for(AttributeChangeSpec attributeChangeSpec : changedAttributes) {
			    		if(attributeChangeSpec.getMatch().getLeft() != null) {
			                EObject match = attributeChangeSpec.getMatch().getLeft();
			                EStructuralFeature nameFeature = match.eClass().getEStructuralFeature("name");
			    			
				    		if(nameFeature != null) {
				                String objName = match.eGet(nameFeature).toString();
					    		System.out.println("[ " + attributeChangeSpec.getKind() + " ] " + match.eClass().getInstanceTypeName() + " \t" + objName + " " + attributeChangeSpec.getAttribute().getEType().getName() +
					    				" " + attributeChangeSpec.getAttribute().getName() + " " + match.eGet(attributeChangeSpec.getAttribute()));
					    		
					    		if(match.eClass().getInstanceTypeName().equals(attributePrecondition.getInstanceTypeName()) 
					    				&& objName.equals(attributePrecondition.getObjName())
					    				&& attributeChangeSpec.getAttribute().getName().equals(attributePrecondition.getAttributeName())
					    				&& match.eGet(attributeChangeSpec.getAttribute()) != attributePrecondition.getPreconditionValue()) {
					    			System.out.println("PRECONDITION NOT MET FOR ADAPTATION " + j + ": " + "[ " + attributeChangeSpec.getKind() + " ] " + match.eClass().getInstanceTypeName() + " \t" + objName + " " + attributeChangeSpec.getAttribute().getEType().getName() +
						    				" " + attributeChangeSpec.getAttribute().getName() + " " + match.eGet(attributeChangeSpec.getAttribute()));
					    			tmp = true;
					    		}
				    		}
			    		}	
			        }
	        	}
	        	
	        }
	       
	    	
	    	List<Conflict> conflicts = comparator.compare(scope).getConflicts()
	    			.stream()
	    	    	  .filter(conflict -> conflict.getKind() == ConflictKind.REAL)
	    	    	  .collect(Collectors.toList());
	    	
	    	if(!conflicts.isEmpty() || tmp) {
	    		System.out.println("\tThere are conflicts between adaptation " + i + " and adaptation " + j);
	    	} else {
	    		System.out.println("\tAdaptation " + i + " and adaptation " + j +" dont have any conflicts");
	    	}
		}
	}

    }
	
    /*public static void checkConflicts(List<EObject> adaptationModels, EObject asIsModel) {
    	
    	IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.NEVER);
    	IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
    	IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(matcher, comparisonFactory);
            matchEngineFactory.setRanking(20);
            IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
            matchEngineRegistry.add(matchEngineFactory);
    	EMFCompare comparator = EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry).build();
    	
    	// Compare the two models
    	IComparisonScope scope = EMFCompare.createDefaultScope(adap1, adap3, reference);
    	
    	List<Diff> differences = comparator.compare(scope).getDifferences();
    	//comparator.compare(scope).getDifferences().forEach(s -> System.out.println(s));
    	
    	//System.out.println("MIOD");
    	
    	List<Conflict> c = comparator.compare(scope).getConflicts()
    			.stream()
    	    	  .filter(conflict -> conflict.getKind() == ConflictKind.REAL)
    	    	  .collect(Collectors.toList());
    	
    	
    	c.forEach(cs -> {
    		System.out.println(cs);
    		cs.getDifferences().forEach(dd -> System.out.println("\t " + dd));
    	});
    	
    	System.out.println("CONFLICTS? " + !c.isEmpty());
    }*/
    
    /**
     * @param before model before changes were applied
     * @param after model with changed objects & refs
     * @return JSONObject containing the differences in objects and references
     */
    public static JsonObject compareModels(EObject before, EObject after) {

        List<ChangedObj> changedObjects = new ArrayList<>();
        List<ChangedReference> changedRefObjects = new ArrayList<>();

        //test2(before, after, reference);
        
        List<Diff> differences = getDiffs(after, before);

        //differences.forEach(a -> System.out.println(a));

        List<AttributeChangeSpec> changedAttributes = differences.stream()
                //.filter(diff -> diff.getKind().equals(DifferenceKind.CHANGE))
                .filter(diff -> diff instanceof AttributeChangeSpec)
                .map(changes -> (AttributeChangeSpec) changes)
                .collect(Collectors.toList());

        List<ReferenceChangeSpec> changedReferences = differences.stream()
                //.filter(diff -> diff.getKind().equals(DifferenceKind.CHANGE))
                .filter(diff -> diff instanceof ReferenceChangeSpec)
                .map(changes -> (ReferenceChangeSpec) changes)
                .collect(Collectors.toList());

        changedObjects = getChangedObjects(changedAttributes);
        changedRefObjects = getChangedRelations(changedReferences);

        return createDiffJson(changedObjects, changedRefObjects);
    }

    static private List<ChangedObj> getChangedObjects(List<AttributeChangeSpec> changedAttributes) {

        List<ChangedObj> changedObjects = new ArrayList<>();

        for (AttributeChangeSpec changeSpec : changedAttributes) {

            Object oldAttrValue = null;
            Object newAttrValue = changeSpec.getValue();

            Match match = changeSpec.getMatch();

            EObject oldEObj = match.getRight();
            EObject newEObj = match.getLeft();

            // find old value
            if (newEObj != null) {
                oldAttrValue = oldEObj.eGet(changeSpec.getAttribute());
            } else {
            	//ignored for logger
                System.out.println("getRight() is null in compareModels()");
            }

            EStructuralFeature nameFeature = oldEObj.eClass().getEStructuralFeature("name");
            // EStructuralFeature typeFeature = asIs.eClass().getEStructuralFeature("type");

            String objName = oldEObj.eGet(nameFeature).toString();
            String objAtId = oldEObj.eResource().getURIFragment(oldEObj);
            String objType = oldEObj.eClass().getInstanceTypeName();

            String attrName = changeSpec.getAttribute().getName();
            String attrType = changeSpec.getAttribute().getEType().getName();
            String changeType = changeSpec.getKind().toString();

            // all values set
            ChangedObj obj = new ChangedObj(objAtId, objName, objType);
            ChangedAttribute attr = new ChangedAttribute(attrName, attrType, oldAttrValue.toString(),
                    newAttrValue.toString(), changeType);

            if (changedObjects.contains(obj)) {
                changedObjects.stream().forEach(a -> {
                    if (a.equals(obj)) {
                        a.getChanges().add(attr);
                    }
                });
            } else {
                obj.getChanges().add(attr);
                changedObjects.add(obj);
            }
        }
        return changedObjects;
    }

    static private List<ChangedReference> getChangedRelations(List<ReferenceChangeSpec> changedAttributes) {
        List<ChangedReference> changedRefObjects = new ArrayList<>();

        for (ReferenceChangeSpec changeSpec : changedAttributes) {
            String changeType = changeSpec.getKind().toString();
            String refName = changeSpec.getReference().getName();

            Match match = changeSpec.getMatch();

            EObject objFrom = match.getLeft();
            EObject objTo = changeSpec.getValue();

            //String objectName, String objectType, String objectAtID
            //System.out.println("objFrom " + objFrom);
            EStructuralFeature nameFeature = objFrom.eClass().getEStructuralFeature("name");
            String objFromName = "default objFromName";
            if(nameFeature != null) {
                objFromName = objFrom.eGet(nameFeature).toString();	
            }
            String objFromtAtId = objFrom.eResource().getURIFragment(objFrom);
            String objFromType = objFrom.eClass().getInstanceTypeName();

            EStructuralFeature nameFeature1 = objFrom.eClass().getEStructuralFeature("name");
            String objToName = "default objToName";
            if(nameFeature1 != null) {
            	objToName = objTo.eGet(nameFeature1).toString();
            	
            }
            String objTotAtId = objTo.eResource().getURIFragment(objTo);
            String objToType = objTo.eClass().getInstanceTypeName();

            ObjectToIdentify objectToIdentify_From = new ObjectToIdentify(objFromName, objFromType, objFromtAtId);
            ObjectToIdentify objectToIdentify_To = new ObjectToIdentify(objToName, objToType, objTotAtId);

            ChangedReference cr = new ChangedReference(refName, objectToIdentify_From, objectToIdentify_To, changeType);
            changedRefObjects.add(cr);
        }

        return changedRefObjects;
    }
   
    
    /**
     * @return differences between objects based on custom matching (matching by URIFragment)
     */
    public static List<Diff> getDiffs(EObject before, EObject after) {

        // only us
        IEObjectMatcher fallBackMatcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.WHEN_AVAILABLE);
        IEObjectMatcher customNameMatcher = new IdentifierEObjectMatcher(fallBackMatcher, idFunction1);

        IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
        IMatchEngine.Factory.Registry registry = MatchEngineFactoryRegistryImpl.createStandaloneInstance();
        final MatchEngineFactoryImpl matchEngineFactory = new MatchEngineFactoryImpl(customNameMatcher,
                comparisonFactory);

        matchEngineFactory.setRanking(20); // default engine ranking is 10, must be higher to override.
        registry.add(matchEngineFactory);

        IComparisonScope scope = new DefaultComparisonScope(before, after, null);
        Comparison result = EMFCompare
        		.builder()
        		.setMatchEngineFactoryRegistry(registry)
        		.build()
        		.compare(scope);

        return result.getDifferences();
    }

    /**
     * compare based on name
     */
    static Function<EObject, String> idFunction = new com.google.common.base.Function<EObject, String>() {
        public String apply(EObject input) {
            if (input instanceof EObject) {
                EObject obj = input;
                EStructuralFeature nameFeature = obj.eClass().getEStructuralFeature("name");
                if (nameFeature != null) {
                    return obj.eGet(nameFeature).toString();
                } else {
                    return null;
                }
            }
            // a null return here tells the match engine to fall back to the other matchers
            return null;
        }
    };

    /**
     * compare based on URIFragemnt
     */
   static Function<EObject, String> idFunction1 = new com.google.common.base.Function<EObject, String>() {
        public String apply(EObject input) {
            if (input instanceof EObject) {
                EObject obj = input;

                String objAtId = obj.eResource().getURIFragment(obj);

                return objAtId;
            }
            // a null return here tells the match engine to fall back to the other matchers
            return null;
        }
    };

    static private JsonObject createDiffJson(List<ChangedObj> changes, List<ChangedReference> refChanges) {
    	Gson gson = new Gson();
    	JsonArray jarr = new JsonArray();
    

    	for(ChangedObj change : changes) {
    	
    		if(change.getChanges().stream().noneMatch(c -> c.getChangeType().equals("MOVE"))) {
        		JsonObject jobj = (JsonObject) new JsonParser().parse(gson.toJson(change));
        		jarr.add(jobj);	
    		}
    	}
    	JsonObject jObj = new JsonObject();
        jObj.add("ChangesMadeToAsIsModel", jarr);

        JsonArray jarr1 = new JsonArray();
        for(ChangedReference change : refChanges) {
        	
    		if(!change.getChangeType().equals("MOVE")) {
        		JsonObject jobj = (JsonObject) new JsonParser().parse(gson.toJson(change));
        		jarr.add(jobj);
    		}
    	}
        jObj.add("ReferenceChanges", jarr1);

        
        jObj = (JsonObject) new JsonParser().parse(jObj.toString().replace("object", "Object").replace("AtID", "@ID")
                .replace("changes", "Changes").replace("attribute", "Attribute"));

        /*
         * jObj = new JSONObject(jObj.toString() .replaceAll("", replacement))
         *
         */

        return jObj;
    }

    private EObject searchForObjectInGivenModel(CloudEnvironment cloudEnvironment, String id) {
        EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
        for (tosca_nodes_Root element : nodes) {
            Resource resource = element.eResource();
            String toTest = (resource).getURIFragment(element);

            if (toTest.equals(id)) {
                return element;
            }
        }
        return null;
    }
}
