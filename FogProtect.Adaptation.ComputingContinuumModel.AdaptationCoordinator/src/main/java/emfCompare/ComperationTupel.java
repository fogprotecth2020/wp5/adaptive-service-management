package emfCompare;

import java.util.ArrayList;
import java.util.List;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;

public class ComperationTupel {
	
	private CloudEnvironment cloudEnvironment;
	private List<Precondition> preconditions;
	
	public ComperationTupel(CloudEnvironment cloudEnvironment) {
		this.cloudEnvironment = cloudEnvironment;
		this.preconditions = new ArrayList<Precondition>();
	}
	
	public CloudEnvironment getCloudEnvironment() {
		return this.cloudEnvironment;
	}
	
	public List<Precondition> getPreconditions() {
		return this.preconditions;
	}
	

}
