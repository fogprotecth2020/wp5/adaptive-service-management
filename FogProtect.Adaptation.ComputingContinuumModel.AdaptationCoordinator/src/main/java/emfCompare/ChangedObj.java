package emfCompare;

import java.util.ArrayList;
import java.util.List;

public class ChangedObj {
    //identification Attributes (e.g. name, id)
    public ObjectToIdentify ObjectToIdentify;

    //Changes
    List<ChangedAttribute> Changes = new ArrayList<>();

    public ChangedObj(String objectID, String objectName, String objectType) {
        super();

        this.ObjectToIdentify = new ObjectToIdentify(objectName, objectType, objectID);
    }

    public ObjectToIdentify getObjectToIdentify() {
        return ObjectToIdentify;
    }


    public List<ChangedAttribute> getChanges() {
        return Changes;
    }

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof ChangedObj) {

            retVal = ((ChangedObj) v).getObjectToIdentify().getName().equals(this.getObjectToIdentify().getName());
        }
        return retVal;
    }
}
