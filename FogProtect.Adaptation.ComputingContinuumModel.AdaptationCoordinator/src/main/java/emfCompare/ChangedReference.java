package emfCompare;

public class ChangedReference {

    String changeType;
    String referenceName;

    ObjectToIdentify source;
    ObjectToIdentify destination;


    public ChangedReference(String referenceName, ObjectToIdentify source, ObjectToIdentify destination, String changeType) {
        super();
        this.referenceName = referenceName;
        this.source = source;
        this.destination = destination;
        this.changeType = changeType;
    }


    public String getChangeType() {
        return changeType;
    }


    public String getReferenceName() {
        return referenceName;
    }


    public ObjectToIdentify getSource() {
        return source;
    }


    public ObjectToIdentify getDestination() {
        return destination;
    }


    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }


    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }


    public void setSource(ObjectToIdentify source) {
        this.source = source;
    }


    public void setDestination(ObjectToIdentify destination) {
        this.destination = destination;
    }
}
