package HelperClasses;

public class ManagementComponentObject {

	//Representation of ManagementComponent, used to save Rest-URLs
	
	private String urlAdaptationRequest;
	private String urlCallForAction;
	private String typeOfManagementComponent;
	private String urlImmediateAction;
	private String urlAdaptationCollection;
	private String urlDisallowAdaptations;

	public ManagementComponentObject(String urlAdaptationRequest, String urlCallForAction, String typeOfManagementComponent, String urlImmediateAction, String urlAdaptationCollection, String urlDisallowAdaptations) {
		this.urlAdaptationRequest = urlAdaptationRequest;
		this.urlCallForAction = urlCallForAction;
		this.typeOfManagementComponent = typeOfManagementComponent;
		this.urlImmediateAction = urlImmediateAction;
		this.urlAdaptationCollection = urlAdaptationCollection;
		this.urlDisallowAdaptations = urlDisallowAdaptations;
	}

	public String getUrlAdaptationRequest() {
		return urlAdaptationRequest;
	}

	public void setUrlAdaptationRequest(String urlAdaptationRequest) {
		this.urlAdaptationRequest = urlAdaptationRequest;
	}

	public String getUrlCallForAction() {
		return urlCallForAction;
	}

	public void setUrlCallForAction(String urlCallForAction) {
		this.urlCallForAction = urlCallForAction;
	}

	public String getTypeOfManagementComponent() {
		return typeOfManagementComponent;
	}

	public void setTypeOfManagementComponent(String typeOfManagementComponent) {
		this.typeOfManagementComponent = typeOfManagementComponent;
	}

	public String getUrlImmediateAction() {
		return urlImmediateAction;
	}

	public void setUrlImmediateAction(String urlImmediateAction) {
		this.urlImmediateAction = urlImmediateAction;
	}

	public String getUrlAdaptationCollection() {
		return urlAdaptationCollection;
	}

	public void setUrlAdaptationCollection(String urlAdaptationCollection) {
		this.urlAdaptationCollection = urlAdaptationCollection;
	}

	public String getUrlDisallowAdaptations() {
		return urlDisallowAdaptations;
	}

	public void setUrlDisallowAdaptations(String urlDisallowAdaptations) {
		this.urlDisallowAdaptations = urlDisallowAdaptations;
	}

	
	
}