package HelperClasses;

public enum RiskLevel {
	VeryLow, 
	Low,
	Medium, 
	High, 
	VeryHigh;
}
