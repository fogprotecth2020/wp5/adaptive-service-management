package HelperClasses;

import java.util.ArrayList;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;

public class Adaptation {

	private CloudEnvironment referenceModel;
	private String name;
	private String description;
	private String typeOfManagementComponent;
	RiskLevel riskLevel;
	private ArrayList<Risk> risks = new ArrayList<Risk>();
	
	
	public Adaptation(CloudEnvironment referenceModel, String name, String description, String typeOfManagementComponent) {
		super();
		this.referenceModel = referenceModel;
		this.name = name;
		this.description = description;
		this.typeOfManagementComponent = typeOfManagementComponent;
		this.riskLevel = RiskLevel.VeryHigh;
	}
	
	public CloudEnvironment getReferenceModel() {
		return referenceModel;
	}
	public void setReferenceModel(CloudEnvironment referenceModel) {
		this.referenceModel = referenceModel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getTypeOfManagementComponent() {
		return typeOfManagementComponent;
	}

	public void setTypeOfManagementComponent(String typeOfManagementComponent) {
		this.typeOfManagementComponent = typeOfManagementComponent;
	}

	public RiskLevel getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(RiskLevel riskLevel) {
		this.riskLevel = riskLevel;
	}

	public ArrayList<Risk> getRisks() {
		return risks;
	}

	public void setRisks(ArrayList<Risk> risks) {
		this.risks = risks;
	}	
	
	
}
