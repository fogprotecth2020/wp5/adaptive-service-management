package HelperClasses;

import java.util.ArrayList;

public class Risk {

	private String name;
	
	private String description;
	
	private int id;
	
	private ArrayList<Node> nodes;

	public Risk(String name, String description, int id, ArrayList<Node> nodes) {
		super();
		this.name = name;
		this.description = description;
		this.id = id;
		this.nodes = nodes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Node> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}
	
	
	
}
