package HelperClasses;

import org.springframework.stereotype.Component;

public class RiskAssessmentObject {

	//Representation of RiskAssesment, used to save Rest-URLs
	
	private String url;

	public RiskAssessmentObject(String urlRuntimeModel) {
		this.url = urlRuntimeModel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	
	
}
