package HelperClasses;

public class DisallowedRule {

	private String fileName;
	private String ruleName;
	
	public DisallowedRule(String fileName, String ruleName) {
		this.fileName = fileName;
		this.ruleName = ruleName;
	}

	public String getFileName() {
		return fileName;
	}

	public String getRuleName() {
		return ruleName;
	}
	
	public String getCompleteName() {
		return fileName + "-" + ruleName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((ruleName == null) ? 0 : ruleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj instanceof DisallowedRule) {
			DisallowedRule other = (DisallowedRule) obj;
			return this.fileName.equals(other.fileName) && this.ruleName.equals(other.ruleName);
		}
		
		return false;
	}
}
