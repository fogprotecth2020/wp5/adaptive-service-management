package HelperClasses;

public class RuleConflict {
	
	private String nameOfManagerA;
	private String fileOfManagerA;
	private String ruleOfManagerA;
	
	private String nameOfManagerB;
	private String fileOfManagerB;
	private String ruleOfManagerB;
	

	public RuleConflict(String managerA, String fileOfManagerA, String ruleOfManagerA, 
			String managerB, String fileOfManagerB, String ruleOfManagerB) {
		this.nameOfManagerA = managerA;
		this.fileOfManagerA = fileOfManagerA;
		this.ruleOfManagerA = ruleOfManagerA;
		this.nameOfManagerB = managerB;
		this.fileOfManagerB = fileOfManagerB;
		this.ruleOfManagerB = ruleOfManagerB;
	}


	public String getNameOfManagerA() {
		return nameOfManagerA;
	}


	public String getFileOfManagerA() {
		return fileOfManagerA;
	}


	public String getRuleOfManagerA() {
		return ruleOfManagerA;
	}


	public String getNameOfManagerB() {
		return nameOfManagerB;
	}


	public String getFileOfManagerB() {
		return fileOfManagerB;
	}


	public String getRuleOfManagerB() {
		return ruleOfManagerB;
	}
	
	
	
}
