//package restController.testing;
//
//import org.springframework.web.bind.annotation.RestController;
//
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import com.opencsv.CSVWriter;
//
//import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
//import fogprotect.adaptation.ComputingContinuumModel.Gateway;
//import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
//import fogprotect.adaptation.ComputingContinuumModel.impl.tosca_nodes_RootImpl;
//import riskpatternfinder.AdaptationFinderToMitigateRisks;
//import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
//import runtime.EMFModelLoad;
//import runtime.RiskFinder;
//import runtime.RuntimeModelLogic;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.nio.charset.Charset;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.LinkedList;
//
//import javax.annotation.PreDestroy;
//
//import org.eclipse.emf.ecore.util.EcoreUtil;
//import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
////OPTIONAL Performance Test Rework
//@RestController
//public class RadarTester {
//
//	@Autowired
//	RuntimeModelLogic logic;
//	@Autowired
//	EMFModelLoad loader;
//	@Autowired
//	RuntimeModelLogic runtimeModelLogic;
//	@Autowired
//	private RiskFinder riskFinder;
//	@Autowired
//	private EnlargeModelController enlargeModelController;
//
//	public final String filepathToCSV = new File(System.getProperty("user.dir")).getAbsolutePath() + File.separator
//			+ "performanceTest" + File.separator + "output.csv";
//	public final String filepathToCSVAverage = new File(System.getProperty("user.dir")).getAbsolutePath()
//			+ File.separator + "performanceTest" + File.separator + "outputAverage.csv";
//	public final String filepathToDiagramFormat = new File(System.getProperty("user.dir")).getAbsolutePath()
//			+ File.separator + "performanceTest" + File.separator + "diagramFormat.csv";
//
//	// Which model do you want to use?
//	static final String fileName = "CloudExample.computingcontinuummodel";
//
//	// Do you want to measure time?
//	public boolean performanceTest = false;
//	public LinkedList<Long> performanceTimeCollection = new LinkedList<Long>();
//	private String subscribtionID;
//
////	private ArrayList<RandomizedAdaptationRule> randomizedAdaptationRules;
//
//	boolean oldEnlargementMethod;
//
////	@RequestMapping(path = "/performanceTest", method = RequestMethod.GET)
////	public String performanceTests() {
////		// How often do you want to iterate trough the process
////		int amountOfTests = 2;
////		// Which enlargement method do you want to use?
////		oldEnlargementMethod = false;
////		// After how many seconds the algorithm should stop searching for a better solution?
////		int[] maxSecondsArray = { 10 };
////		// Set enlargement steps
//////		String[] enlargements = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
//////		String[] enlargements = { "0", "20", "40", "60", "80", "100", "120", "140", "160", "180", "200", "220", "240", "260", "280", "300" };
////		String[] enlargements = { "0", "10" };
////		for (int maxSeconds : maxSecondsArray) {
////			// adding header to csv
////			String[] header = { "Algorithm name", "Nr. of nodes", "Experiment number", "Overall time used",
////					"Time used to find the best solution", "Found PCP instances", "Executed adaptations",
////					"PCP instances left over", "Functionality restrictions", "Costs", "Amount of Henshin calls",
////					"Time consumed by searching for PCP instances", "Time consumed to execute adaptations",
////					"Time consumed to calculate working functions", "Time consumed to calculate costs",
////					"Time consumed for EMF Compare Method", "Time consumed for CompareForBFS method" };
////			writeDataLineByLine(insertNumberBeforeFileEnding(filepathToCSV, maxSeconds), header);
////			// adding header to csv
////			String[] header2 = { "Algorithm name", "Nr. of nodes", "Average overall time used",
////					"Average time used to find the best solution", "Average amount of PCP instances left over",
////					"Average amount of functionality restrictions", "Average costs", "Average amount of Henshin calls",
////					"Average time consumed by searching for PCP instances",
////					"Average time consumed to execute adaptations",
////					"Average time consumed to calculate working functions", "Average time consumed to calculate costs",
////					"Average time consumed for EMF Compare Method",
////					"Average time consumed for CompareForBFS method" };
////			writeDataLineByLine(insertNumberBeforeFileEnding(filepathToCSVAverage, maxSeconds), header2);
////			// adding header to csv
////			String[] header3 = { "Nr. of nodes", "Best-First", "Depth-First", "Breadth-First", "Random", "AStar" };
////			writeDataLineByLine(insertNumberBeforeFileEnding(filepathToDiagramFormat, maxSeconds), header3);
////			for (String enlargement : enlargements) {
////				AdaptationAlgorithm[] algorithms = AdaptationAlgorithm.values();
////				int arrayLength = AdaptationAlgorithm.values().length;
////
////				double[][] averageNumberOfNodes = new double[arrayLength][amountOfTests];
////				double[][] averageTime = new double[arrayLength][amountOfTests];
////				double[][] averageTimeToBestSolution = new double[arrayLength][amountOfTests];
////				double[][] averagePCPinstancesLeft = new double[arrayLength][amountOfTests];
////				double[][] averageMissingFunctions = new double[arrayLength][amountOfTests];
////				double[][] averageCosts = new double[arrayLength][amountOfTests];
////				double[][] averageAmountHenshinCalls = new double[arrayLength][amountOfTests];
////				double[][] averageTimeToFindPCPInstances = new double[arrayLength][amountOfTests];
////				double[][] averageTimeToExecuteAdaptations = new double[arrayLength][amountOfTests];
////				double[][] averageTimeToCalculateWF = new double[arrayLength][amountOfTests];
////				double[][] averageTimeToCalculateCost = new double[arrayLength][amountOfTests];
////				double[][] averageTimeForEMFCompareMethod = new double[arrayLength][amountOfTests];
////				double[][] averageTimeForBFSCompareMethod = new double[arrayLength][amountOfTests];
////
////				for (int testnumber = 0; testnumber < amountOfTests; testnumber++) {
////					randomizedAdaptationRules = new ArrayList<AdaptationFinderToMitigateRisks.RandomizedAdaptationRule>();
////
////					// Insert EMF Model String here
////					String model = runtimeModelLogic.returnentiremodelasjsonFromFile(fileName);
////					// load model from JSON String to EMF
////					CloudEnvironment cloudEnvironment = null;
////					try {
////						cloudEnvironment = (CloudEnvironment) loader.loadEObjectFromString(model);
////						loader.setAsIsModel(cloudEnvironment);;
////					} catch (IOException e) {
////						//  Auto-generated catch block
////						System.err.println("Can't create EMF instance");
////					}
////
////					// Enlargement
////					if (oldEnlargementMethod) {
////						enlargeModelController.enlargeCloudExample(enlargement);
//////							enlargeModelController.enlargeFogExample(enlargement);
////					} else {
////						enlargeModelController.newEnlargerMethodForCloudExample(Integer.valueOf(enlargement),
////								cloudEnvironment);
////					}
////					CloudEnvironment cloudEnvironmentCopy = EcoreUtil.copy(cloudEnvironment);
////					for (int i = 0; i < arrayLength; i++) {
////						System.out.println(
////								"Algorithm: " + algorithms[i].toString() + ". Iteration: " + (testnumber + 1));
////						String[] data = riskFinder.lookForRisks(cloudEnvironment, algorithms[i], enlargement, testnumber, maxSeconds);
////						// Write CSV
////						if (!data[0].equals("")) {
////							writeDataLineByLine(insertNumberBeforeFileEnding(filepathToCSV, maxSeconds), data);
////							averageNumberOfNodes[i][testnumber] = Double.valueOf(data[1]);
////							averageTime[i][testnumber] = Double.valueOf(data[3]);
////							averageTimeToBestSolution[i][testnumber] = Double.valueOf(data[4]);
////							averagePCPinstancesLeft[i][testnumber] = calculatePCPInstancesLeft((data[7]));
////							averageMissingFunctions[i][testnumber] = Double.valueOf(data[8]);
////							averageCosts[i][testnumber] = Double.valueOf(data[9]);
////							averageAmountHenshinCalls[i][testnumber] = Double.valueOf(data[10]);
////							averageTimeToFindPCPInstances[i][testnumber] = Double.valueOf(data[11]);
////							averageTimeToExecuteAdaptations[i][testnumber] = Double.valueOf(data[12]);
////							averageTimeToCalculateWF[i][testnumber] = Double.valueOf(data[13]);
////							averageTimeToCalculateCost[i][testnumber] = Double.valueOf(data[14]);
////							averageTimeForEMFCompareMethod[i][testnumber] = Double.valueOf(data[15]);
////							averageTimeForBFSCompareMethod[i][testnumber] = Double.valueOf(data[16]);
////						} else {
////							writeDataLineByLine(insertNumberBeforeFileEnding(filepathToCSV, maxSeconds),
////									new String[] { "No risks found" });
////							averageNumberOfNodes[i][testnumber] = 10000;
////							averageTime[i][testnumber] = 10000;
////							averageTimeToBestSolution[i][testnumber] = 10000;
////							averagePCPinstancesLeft[i][testnumber] = 10000;
////							averageMissingFunctions[i][testnumber] = 10000;
////							averageCosts[i][testnumber] = 10000;
////							averageAmountHenshinCalls[i][testnumber] = 10000;
////							averageTimeToFindPCPInstances[i][testnumber] = 10000;
////							averageTimeToExecuteAdaptations[i][testnumber] = 10000;
////							averageTimeToCalculateWF[i][testnumber] = 10000;
////							averageTimeToCalculateCost[i][testnumber] = 10000;
////							averageTimeForEMFCompareMethod[i][testnumber] = 10000;
////							averageTimeForBFSCompareMethod[i][testnumber] = 10000;
////						}
////						cloudEnvironment = EcoreUtil.copy(cloudEnvironmentCopy);
////					}
////				}
////				double[] averageEnlargement = new double[arrayLength];
////				// Write CSV Average
////				for (int i = 0; i < arrayLength; i++) {
////					averageEnlargement[i] = calcAverage(averageNumberOfNodes[i]);
////					String[] data = { algorithms[i].toString(), String.valueOf(calcAverage(averageNumberOfNodes[i])),
////							String.valueOf(calcAverage(averageTime[i])),
////							String.valueOf(calcAverage(averageTimeToBestSolution[i])),
////							String.valueOf(calcAverage(averagePCPinstancesLeft[i])),
////							String.valueOf(calcAverage(averageMissingFunctions[i])),
////							String.valueOf(calcAverage(averageCosts[i])),
////							String.valueOf(calcAverage(averageAmountHenshinCalls[i])),
////							String.valueOf(calcAverage(averageTimeToFindPCPInstances[i])),
////							String.valueOf(calcAverage(averageTimeToExecuteAdaptations[i])),
////							String.valueOf(calcAverage(averageTimeToCalculateWF[i])),
////							String.valueOf(calcAverage(averageTimeToCalculateCost[i])),
////							String.valueOf(calcAverage(averageTimeForEMFCompareMethod[i])),
////							String.valueOf(calcAverage(averageTimeForBFSCompareMethod[i])) };
////					writeDataLineByLine(insertNumberBeforeFileEnding(filepathToCSVAverage, maxSeconds), data);
////				}
////				String nrOfNodes = "";
////				if (oldEnlargementMethod) {
////					nrOfNodes = String.valueOf(calcAverage(averageNumberOfNodes[0]));
////				} else {
////					nrOfNodes = String.valueOf(calcAverage(averageEnlargement));
////				}
////				String best = String.valueOf((Double) (100.0 * calcAverage(averagePCPinstancesLeft[0]))
////						+ (10.0 * calcAverage(averageMissingFunctions[0])) + (1.0 * calcAverage(averageCosts[0])));
////				String depth = String.valueOf((Double) (100.0 * calcAverage(averagePCPinstancesLeft[1]))
////						+ (10.0 * calcAverage(averageMissingFunctions[1])) + (1.0 * calcAverage(averageCosts[1])));
////				String breadth = String.valueOf((Double) (100.0 * calcAverage(averagePCPinstancesLeft[2]))
////						+ (10.0 * calcAverage(averageMissingFunctions[2])) + (1.0 * calcAverage(averageCosts[2])));
////				String random = String.valueOf((Double) (100.0 * calcAverage(averagePCPinstancesLeft[3]))
////						+ (10.0 * calcAverage(averageMissingFunctions[3])) + (1.0 * calcAverage(averageCosts[3])));
////				String astar = String.valueOf((Double) (100.0 * calcAverage(averagePCPinstancesLeft[4]))
////						+ (10.0 * calcAverage(averageMissingFunctions[4])) + (1.0 * calcAverage(averageCosts[4])));
////				String[] data = { nrOfNodes, best, depth, breadth, random, astar };
////				writeDataLineByLine(insertNumberBeforeFileEnding(filepathToDiagramFormat, maxSeconds), data);
////			}
////			double average = 0;
////			for (long x : performanceTimeCollection) {
////				average += x;
////			}
////			if (performanceTimeCollection.size() != 0) {
////				average = average / performanceTimeCollection.size();
////				average = (average / 1e9d);
////				System.out.println("Average is: " + average);
////			} else {
////				System.out.println("Can not calculate average");
////			}
////
////		}
////		return "Done";
////
////	}
//
//	private double calcAverage(double[] numbers) {
//		double sum = 0;
//		for (double i : numbers) {
//			sum += i;
//		}
//		double average = (sum / numbers.length);
//		return average;
//	}
//
//	private double calcMedian(double[] numbers) {
//		Arrays.sort(numbers);
//		double median;
//		if (numbers.length % 2 == 0)
//			median = ((double) numbers[numbers.length / 2] + (double) numbers[numbers.length / 2 - 1]) / 2;
//		else
//			median = (double) numbers[numbers.length / 2];
//
//		return median;
//	}
//
//	private double calculatePCPInstancesLeft(String string) {
//		String[] values = string.split("[|]");
//		return values.length - 1;
//	}
//
////	@RequestMapping(path = "/loadInitialInstance", method = RequestMethod.GET)
////	public String index() {
////		// Insert EMF Model String here
////		String model = runtimeModelLogic.returnentiremodelasjsonFromFile(fileName);
////		// load model from JSON String to EMF
////		CloudEnvironment cloudEnvironment = null;
////		try {
////			cloudEnvironment = (CloudEnvironment) loader.loadEObjectFromString(model);
////			loader.setAsIsModel(cloudEnvironment);
//////			System.out.println("Loaded model after registration event as:\n" + runtimeModelLogic.returnentiremodelasjson(cloudEnvironment));
////			System.out.println("Loaded model after registration event.");
////		} catch (IOException e) {
////			//  Auto-generated catch block
////			System.err.println("Can't create EMF instance");
////		}
////		if (cloudEnvironment != null) {
////			riskFinder.lookForRisks(cloudEnvironment,  "");
////		}
////
////		return "Done";
////	}
//
////	@RequestMapping(path = "/restart", method = RequestMethod.GET)
////	public String restart() {
//////		String model = "{ \"type\":\"CloudEnvironment\", \"@id\":\"/\", \"tosca_nodes_root\":[ { \"type\":\"Compute\", \"@id\":\"1\", \"name\":\"DBServer_1\", \"id\":1, \"hosts\":[ { \"type\":\"DBMS\", \"referencedObjectID\":\"8\" }, { \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"35\" } ], \"connectedToNetwork\":[ { \"type\":\"Network_Network\", \"referencedObjectID\":\"2\" } ], \"jurisdiction\":\"DE\" }, { \"type\":\"Network_Network\", \"@id\":\"2\", \"name\":\"DataLAN\", \"id\":2, \"connectedToCompute\":[ { \"type\":\"Compute\", \"referencedObjectID\":\"1\" }, { \"type\":\"Compute\", \"referencedObjectID\":\"17\" }, { \"type\":\"Compute\", \"referencedObjectID\":\"32\" } ], \"connectedTo\":{ \"type\":\"Network_Network\", \"referencedObjectID\":\"4\" } }, { \"type\":\"Compute\", \"@id\":\"3\", \"name\":\"QueryServer\", \"id\":3, \"hosts\":[ { \"type\":\"Gateway\", \"referencedObjectID\":\"16\" } ], \"connectedToNetwork\":[ { \"type\":\"Network_Network\", \"referencedObjectID\":\"4\" } ], \"jurisdiction\":\"DE\" }, { \"type\":\"Network_Network\", \"@id\":\"4\", \"name\":\"ServiceLAN\", \"id\":4, \"connectedToCompute\":[ { \"type\":\"Compute\", \"referencedObjectID\":\"3\" }, { \"type\":\"Compute\", \"referencedObjectID\":\"5\" } ], \"connectedToOpposite\":{ \"type\":\"Network_Network\", \"referencedObjectID\":\"2\" } }, { \"type\":\"Compute\", \"@id\":\"5\", \"name\":\"KGPortalServer\", \"id\":5, \"hosts\":[ { \"type\":\"WebServer\", \"referencedObjectID\":\"6\" } ], \"connectedToNetwork\":[ { \"type\":\"Network_Network\", \"referencedObjectID\":\"4\" } ], \"jurisdiction\":\"DE\" }, { \"type\":\"WebServer\", \"@id\":\"6\", \"name\":\"Webserver-KGPortalServer\", \"id\":6, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"5\" }, \"hosts\":[ { \"type\":\"WebApplication\", \"referencedObjectID\":\"7\" } ] }, { \"type\":\"WebApplication\", \"@id\":\"7\", \"name\":\"KnowGoPortal\", \"id\":7, \"hostedOn\":{ \"type\":\"WebServer\", \"referencedObjectID\":\"6\" }, \"usedBySoftwareComponent\":[ { \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"14\" } ], \"usesGateway\":[ { \"type\":\"Gateway\", \"referencedObjectID\":\"16\" } ], \"usesSoftwareComponent\":[ { \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"35\" }, { \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"36\" } ] }, { \"type\":\"DBMS\", \"@id\":\"8\", \"name\":\"DBMS-DBServer_1\", \"id\":8, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"1\" }, \"hosts\":[ { \"type\":\"Database\", \"referencedObjectID\":\"9\" } ], \"uses\":[ { \"type\":\"Gateway\", \"referencedObjectID\":\"16\" } ] }, { \"type\":\"Database\", \"@id\":\"9\", \"name\":\"KnowGoDB_1\", \"id\":9, \"hostedOn\":{ \"type\":\"DBMS\", \"referencedObjectID\":\"8\" }, \"stores\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"11\" }, { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"21\" } ], \"streamsFrom\":[ { \"type\":\"DataFlow\", \"referencedObjectID\":\"23\" }, { \"type\":\"DataFlow\", \"referencedObjectID\":\"22\" } ] }, { \"type\":\"DataSubject\", \"@id\":\"10\", \"name\":\"Driver\", \"id\":10, \"location\":\"DE\", \"owns\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"11\" }, { \"type\":\"DataFlow\", \"referencedObjectID\":\"22\" }, { \"type\":\"DataFlow\", \"referencedObjectID\":\"23\" }, { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"21\" }, { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"33\" }, { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"24\" } ], \"manages\":[ { \"type\":\"MobileHost\", \"referencedObjectID\":\"26\" }, { \"type\":\"MobileHost\", \"referencedObjectID\":\"27\" } ] }, { \"type\":\"StoredDataSet\", \"@id\":\"11\", \"name\":\"DataSet-KnowGoDB-BiometricData\", \"id\":11, \"consistsOf\":[ { \"type\":\"Record\", \"referencedObjectID\":\"12\" } ], \"belongsTo\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"storedIn\":{ \"type\":\"Database\", \"referencedObjectID\":\"9\" } }, { \"type\":\"Record\", \"@id\":\"12\", \"name\":\"BiometricData\", \"id\":12, \"partOf\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"11\" }, { \"type\":\"DataFlow\", \"referencedObjectID\":\"22\" } ], \"sensitive\":true }, { \"type\":\"Record\", \"@id\":\"13\", \"name\":\"CarData\", \"id\":13, \"partOf\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"21\" }, { \"type\":\"DataFlow\", \"referencedObjectID\":\"23\" } ] }, { \"type\":\"SoftwareComponent\", \"@id\":\"14\", \"name\":\"AccessBrowser\", \"id\":14, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"15\" }, \"controlledBy\":[ { \"type\":\"DataController\", \"referencedObjectID\":\"20\" } ], \"usesWebApplication\":[ { \"type\":\"WebApplication\", \"referencedObjectID\":\"7\" } ] }, { \"type\":\"Compute\", \"@id\":\"15\", \"name\":\"AccessPC\", \"id\":15, \"hosts\":[ { \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"14\" } ], \"jurisdiction\":\"CH\" }, { \"type\":\"Gateway\", \"@id\":\"16\", \"name\":\"QueryGW\", \"id\":16, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"3\" }, \"usedByWebApplication\":[ { \"type\":\"WebApplication\", \"referencedObjectID\":\"7\" } ], \"usedBy\":[ { \"type\":\"DBMS\", \"referencedObjectID\":\"8\" }, { \"type\":\"DBMS\", \"referencedObjectID\":\"18\" } ], \"usedByGateway\":[ { \"type\":\"Gateway\", \"referencedObjectID\":\"31\" } ] }, { \"type\":\"Compute\", \"@id\":\"17\", \"name\":\"DBServer_2\", \"id\":17, \"hosts\":[ { \"type\":\"DBMS\", \"referencedObjectID\":\"18\" }, { \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"36\" } ], \"connectedToNetwork\":[ { \"type\":\"Network_Network\", \"referencedObjectID\":\"2\" } ], \"jurisdiction\":\"AT\" }, { \"type\":\"DBMS\", \"@id\":\"18\", \"name\":\"DBMS-DBServer_2\", \"id\":18, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"17\" }, \"hosts\":[ { \"type\":\"Database\", \"referencedObjectID\":\"19\" } ], \"uses\":[ { \"type\":\"Gateway\", \"referencedObjectID\":\"16\" } ] }, { \"type\":\"Database\", \"@id\":\"19\", \"name\":\"KnowGoDB_2\", \"id\":19, \"hostedOn\":{ \"type\":\"DBMS\", \"referencedObjectID\":\"18\" } }, { \"type\":\"DataController\", \"@id\":\"20\", \"name\":\"AccessingParty\", \"id\":20, \"location\":\"CH\", \"controls\":[ { \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"14\" } ] }, { \"type\":\"StoredDataSet\", \"@id\":\"21\", \"name\":\"DataSet-KnowGoDB-CarData\", \"id\":21, \"consistsOf\":[ { \"type\":\"Record\", \"referencedObjectID\":\"13\" } ], \"belongsTo\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"storedIn\":{ \"type\":\"Database\", \"referencedObjectID\":\"9\" } }, { \"type\":\"DataFlow\", \"@id\":\"22\", \"name\":\"DataFlow-KnowGoDB-BiometricData\", \"id\":22, \"consistsOf\":[ { \"type\":\"Record\", \"referencedObjectID\":\"12\" } ], \"belongsTo\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"streamsTo\":[ { \"type\":\"Database\", \"referencedObjectID\":\"9\" } ] }, { \"type\":\"DataFlow\", \"@id\":\"23\", \"name\":\"DataFlow-KnowGoDB-CarData\", \"id\":23, \"consistsOf\":[ { \"type\":\"Record\", \"referencedObjectID\":\"13\" } ], \"belongsTo\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"streamsTo\":[ { \"type\":\"Database\", \"referencedObjectID\":\"9\" } ] }, { \"type\":\"StoredDataSet\", \"@id\":\"24\", \"name\":\"DataSet-KnowGoDB-SensorData\", \"id\":24, \"consistsOf\":[ { \"type\":\"Record\", \"referencedObjectID\":\"25\" } ], \"belongsTo\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"storedInMobileHost\":{ \"type\":\"MobileHost\", \"referencedObjectID\":\"26\" }, \"createdBy\":{ \"type\":\"MobileHost\", \"referencedObjectID\":\"27\" } }, { \"type\":\"Record\", \"@id\":\"25\", \"name\":\"SensorData\", \"id\":25, \"partOf\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"24\" } ], \"sensitive\":true }, { \"type\":\"MobileHost\", \"@id\":\"26\", \"name\":\"DriverPhone\", \"id\":26, \"jurisdiction\":\"DE\", \"managedBy\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"hostsContainerApplication\":[ { \"type\":\"Container_Application\", \"referencedObjectID\":\"28\" } ], \"stores\":{ \"type\":\"StoredDataSet\", \"referencedObjectID\":\"24\" } }, { \"type\":\"MobileHost\", \"@id\":\"27\", \"name\":\"Car\", \"id\":27, \"jurisdiction\":\"DE\", \"managedBy\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"hostsContainerApplication\":[ { \"type\":\"Container_Application\", \"referencedObjectID\":\"29\" } ], \"creates\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"24\" } ] }, { \"type\":\"Container_Application\", \"@id\":\"28\", \"name\":\"KnowGoApp\", \"id\":28, \"hostedBy\":{ \"type\":\"MobileHost\", \"referencedObjectID\":\"26\" }, \"sendsThrough\":{ \"type\":\"DataFlow\", \"referencedObjectID\":\"30\" }, \"usedBy\":[ { \"type\":\"Container_Application\", \"referencedObjectID\":\"29\" } ] }, { \"type\":\"Container_Application\", \"@id\":\"29\", \"name\":\"CarDataMarshall\", \"id\":29, \"hostedBy\":{ \"type\":\"MobileHost\", \"referencedObjectID\":\"27\" }, \"uses\":[ { \"type\":\"Container_Application\", \"referencedObjectID\":\"28\" } ] }, { \"type\":\"DataFlow\", \"@id\":\"30\", \"name\":\"DataFlow-KnowGoApp-DataGW\", \"id\":30, \"sendsFrom\":{ \"type\":\"Container_Application\", \"referencedObjectID\":\"28\" }, \"sendsTo\":{ \"type\":\"Gateway\", \"referencedObjectID\":\"31\" } }, { \"type\":\"Gateway\", \"@id\":\"31\", \"name\":\"DataGW\", \"id\":31, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"32\" }, \"receivesFrom\":[ { \"type\":\"DataFlow\", \"referencedObjectID\":\"30\" } ], \"usesGateway\":[ { \"type\":\"Gateway\", \"referencedObjectID\":\"16\" } ] }, { \"type\":\"Compute\", \"@id\":\"32\", \"name\":\"DataGWServer\", \"id\":32, \"hosts\":[ { \"type\":\"Gateway\", \"referencedObjectID\":\"31\" } ], \"connectedToNetwork\":[ { \"type\":\"Network_Network\", \"referencedObjectID\":\"2\" } ], \"jurisdiction\":\"DE\" }, { \"type\":\"StoredDataSet\", \"@id\":\"33\", \"name\":\"DataSet-KGAnalytics-RiskData\", \"id\":33, \"consistsOf\":[ { \"type\":\"Record\", \"referencedObjectID\":\"34\" } ], \"belongsTo\":{ \"type\":\"DataSubject\", \"referencedObjectID\":\"10\" }, \"createdBySoftwareComponent\":{ \"type\":\"SoftwareComponent\", \"referencedObjectID\":\"35\" } }, { \"type\":\"Record\", \"@id\":\"34\", \"name\":\"RiskData\", \"id\":34, \"partOf\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"33\" } ] }, { \"type\":\"SoftwareComponent\", \"@id\":\"36\", \"name\":\"KGAnalytics_2\", \"id\":36, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"17\" }, \"usedByWebApp\":[ { \"type\":\"WebApplication\", \"referencedObjectID\":\"7\" } ] }, { \"type\":\"SoftwareComponent\", \"@id\":\"35\", \"name\":\"KGAnalytics_1\", \"id\":35, \"hostedOn\":{ \"type\":\"Compute\", \"referencedObjectID\":\"1\" }, \"creates\":[ { \"type\":\"StoredDataSet\", \"referencedObjectID\":\"33\" } ], \"usedByWebApp\":[ { \"type\":\"WebApplication\", \"referencedObjectID\":\"7\" } ] } ] }";
////		String model = "{ \"type\" : \"CloudEnvironment\", \"@id\" : \"/\", \"tosca_nodes_root\" : [ { \"type\" : \"Compute\", \"@id\" : \"1\", \"name\" : \"DBServer_1\", \"id\" : 1, \"hosts\" : [ { \"type\" : \"DBMS\", \"referencedObjectID\" : \"8\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"35\" } ], \"connectedToNetwork\" : [ { \"type\" : \"Network_Network\", \"referencedObjectID\" : \"2\" } ], \"jurisdiction\" : \"DE\" }, { \"type\" : \"Network_Network\", \"@id\" : \"2\", \"name\" : \"DataLAN\", \"id\" : 2, \"connectedToCompute\" : [ { \"type\" : \"Compute\", \"referencedObjectID\" : \"1\" }, { \"type\" : \"Compute\", \"referencedObjectID\" : \"17\" }, { \"type\" : \"Compute\", \"referencedObjectID\" : \"32\" } ], \"connectedTo\" : { \"type\" : \"Network_Network\", \"referencedObjectID\" : \"4\" } }, { \"type\" : \"Compute\", \"@id\" : \"3\", \"name\" : \"QueryServer\", \"id\" : 3, \"hosts\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"16\" } ], \"connectedToNetwork\" : [ { \"type\" : \"Network_Network\", \"referencedObjectID\" : \"4\" } ], \"jurisdiction\" : \"DE\" }, { \"type\" : \"Network_Network\", \"@id\" : \"4\", \"name\" : \"ServiceLAN\", \"id\" : 4, \"connectedToCompute\" : [ { \"type\" : \"Compute\", \"referencedObjectID\" : \"3\" }, { \"type\" : \"Compute\", \"referencedObjectID\" : \"5\" } ], \"connectedToOpposite\" : { \"type\" : \"Network_Network\", \"referencedObjectID\" : \"2\" } }, { \"type\" : \"Compute\", \"@id\" : \"5\", \"name\" : \"KGPortalServer\", \"id\" : 5, \"hosts\" : [ { \"type\" : \"WebServer\", \"referencedObjectID\" : \"6\" } ], \"connectedToNetwork\" : [ { \"type\" : \"Network_Network\", \"referencedObjectID\" : \"4\" } ], \"jurisdiction\" : \"DE\" }, { \"type\" : \"WebServer\", \"@id\" : \"6\", \"name\" : \"Webserver-KGPortalServer\", \"id\" : 6, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"5\" }, \"hosts\" : [ { \"type\" : \"WebApplication\", \"referencedObjectID\" : \"7\" } ] }, { \"type\" : \"WebApplication\", \"@id\" : \"7\", \"name\" : \"KnowGoPortal\", \"id\" : 7, \"hostedOn\" : { \"type\" : \"WebServer\", \"referencedObjectID\" : \"6\" }, \"usedBySoftwareComponent\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"14\" } ], \"usesGateway\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"16\" } ], \"usesSoftwareComponent\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"35\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"36\" } ] }, { \"type\" : \"DBMS\", \"@id\" : \"8\", \"name\" : \"DBMS-DBServer_1\", \"id\" : 8, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"1\" }, \"hosts\" : [ { \"type\" : \"Database\", \"referencedObjectID\" : \"9\" } ], \"uses\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"16\" } ] }, { \"type\" : \"Database\", \"@id\" : \"9\", \"name\" : \"KnowGoDB_1\", \"id\" : 9, \"hostedOn\" : { \"type\" : \"DBMS\", \"referencedObjectID\" : \"8\" }, \"stores\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"11\" }, { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"21\" } ], \"streamsFrom\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"23\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"22\" } ] }, { \"type\" : \"DataSubject\", \"@id\" : \"10\", \"name\" : \"Driver\", \"id\" : 10, \"location\" : \"DE\", \"owns\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"11\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"22\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"23\" }, { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"21\" }, { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"33\" }, { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"24\" } ], \"manages\" : [ { \"type\" : \"MobileHost\", \"referencedObjectID\" : \"26\" }, { \"type\" : \"MobileHost\", \"referencedObjectID\" : \"27\" } ] }, { \"type\" : \"StoredDataSet\", \"@id\" : \"11\", \"name\" : \"DataSet-KnowGoDB-BiometricData_1\", \"id\" : 11, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"12\" } ], \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"storedIn\" : { \"type\" : \"Database\", \"referencedObjectID\" : \"9\" } }, { \"type\" : \"Record\", \"@id\" : \"12\", \"name\" : \"BiometricData_1\", \"id\" : 12, \"partOf\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"11\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"22\" } ], \"sensitive\" : true }, { \"type\" : \"Record\", \"@id\" : \"13\", \"name\" : \"CarData_1\", \"id\" : 13, \"partOf\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"21\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"23\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"14\", \"name\" : \"AccessBrowser\", \"id\" : 14, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"15\" }, \"controlledBy\" : [ { \"type\" : \"DataController\", \"referencedObjectID\" : \"20\" } ], \"usesWebApplication\" : [ { \"type\" : \"WebApplication\", \"referencedObjectID\" : \"7\" } ] }, { \"type\" : \"Compute\", \"@id\" : \"15\", \"name\" : \"AccessPC\", \"id\" : 15, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"14\" } ], \"jurisdiction\" : \"CH\" }, { \"type\" : \"Gateway\", \"@id\" : \"16\", \"name\" : \"QueryGW\", \"id\" : 16, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"3\" }, \"usedByWebApplication\" : [ { \"type\" : \"WebApplication\", \"referencedObjectID\" : \"7\" } ], \"usedBy\" : [ { \"type\" : \"DBMS\", \"referencedObjectID\" : \"8\" }, { \"type\" : \"DBMS\", \"referencedObjectID\" : \"18\" } ], \"usedByGateway\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"31\" } ], \"gatewayUsedBySoftwareComponent\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"36\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"35\" } ] }, { \"type\" : \"Compute\", \"@id\" : \"17\", \"name\" : \"DBServer_2\", \"id\" : 17, \"hosts\" : [ { \"type\" : \"DBMS\", \"referencedObjectID\" : \"18\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"36\" } ], \"connectedToNetwork\" : [ { \"type\" : \"Network_Network\", \"referencedObjectID\" : \"2\" } ], \"jurisdiction\" : \"AT\" }, { \"type\" : \"DBMS\", \"@id\" : \"18\", \"name\" : \"DBMS-DBServer_2\", \"id\" : 18, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"17\" }, \"hosts\" : [ { \"type\" : \"Database\", \"referencedObjectID\" : \"19\" } ], \"uses\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"16\" } ] }, { \"type\" : \"Database\", \"@id\" : \"19\", \"name\" : \"KnowGoDB_2\", \"id\" : 19, \"hostedOn\" : { \"type\" : \"DBMS\", \"referencedObjectID\" : \"18\" } }, { \"type\" : \"DataController\", \"@id\" : \"20\", \"name\" : \"AccessingParty\", \"id\" : 20, \"location\" : \"CH\", \"controls\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"14\" } ] }, { \"type\" : \"StoredDataSet\", \"@id\" : \"21\", \"name\" : \"DataSet-KnowGoDB-CarData_1\", \"id\" : 21, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"13\" } ], \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"storedIn\" : { \"type\" : \"Database\", \"referencedObjectID\" : \"9\" } }, { \"type\" : \"DataFlow\", \"@id\" : \"22\", \"name\" : \"DataFlow-BiometricData-KnowGoApp-KnowGoDB\", \"id\" : 22, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"12\" } ], \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"streamsTo\" : [ { \"type\" : \"Database\", \"referencedObjectID\" : \"9\" } ] }, { \"type\" : \"DataFlow\", \"@id\" : \"23\", \"name\" : \"DataFlow-CarData-KnowGoApp-KnowGoDB\", \"id\" : 23, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"13\" } ], \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"streamsTo\" : [ { \"type\" : \"Database\", \"referencedObjectID\" : \"9\" } ] }, { \"type\" : \"StoredDataSet\", \"@id\" : \"24\", \"name\" : \"DataSet-SensorData-DriverPhone\", \"id\" : 24, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"25\" } ], \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"storedInMobileHost\" : { \"type\" : \"MobileHost\", \"referencedObjectID\" : \"26\" }, \"createdBy\" : { \"type\" : \"MobileHost\", \"referencedObjectID\" : \"27\" } }, { \"type\" : \"Record\", \"@id\" : \"25\", \"name\" : \"SensorData\", \"id\" : 25, \"partOf\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"24\" } ], \"sensitive\" : true }, { \"type\" : \"MobileHost\", \"@id\" : \"26\", \"name\" : \"DriverPhone\", \"id\" : 26, \"jurisdiction\" : \"DE\", \"managedBy\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"hostsContainerApplication\" : [ { \"type\" : \"Container_Application\", \"referencedObjectID\" : \"28\" } ], \"stores\" : { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"24\" } }, { \"type\" : \"MobileHost\", \"@id\" : \"27\", \"name\" : \"Car\", \"id\" : 27, \"jurisdiction\" : \"DE\", \"managedBy\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"hostsContainerApplication\" : [ { \"type\" : \"Container_Application\", \"referencedObjectID\" : \"29\" } ], \"creates\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"24\" } ] }, { \"type\" : \"Container_Application\", \"@id\" : \"28\", \"name\" : \"KnowGoApp\", \"id\" : 28, \"hostedBy\" : { \"type\" : \"MobileHost\", \"referencedObjectID\" : \"26\" }, \"sendsThrough\" : { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"30\" }, \"usedBy\" : [ { \"type\" : \"Container_Application\", \"referencedObjectID\" : \"29\" } ] }, { \"type\" : \"Container_Application\", \"@id\" : \"29\", \"name\" : \"CarDataMarshall\", \"id\" : 29, \"hostedBy\" : { \"type\" : \"MobileHost\", \"referencedObjectID\" : \"27\" }, \"uses\" : [ { \"type\" : \"Container_Application\", \"referencedObjectID\" : \"28\" } ] }, { \"type\" : \"DataFlow\", \"@id\" : \"30\", \"name\" : \"DataFlow-KnowGoApp-DataGW\", \"id\" : 30, \"sendsFrom\" : { \"type\" : \"Container_Application\", \"referencedObjectID\" : \"28\" }, \"sendsTo\" : { \"type\" : \"Gateway\", \"referencedObjectID\" : \"31\" } }, { \"type\" : \"Gateway\", \"@id\" : \"31\", \"name\" : \"DataGW\", \"id\" : 31, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"32\" }, \"receivesFrom\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"30\" } ], \"usesGateway\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"16\" } ] }, { \"type\" : \"Compute\", \"@id\" : \"32\", \"name\" : \"DataGWServer\", \"id\" : 32, \"hosts\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"31\" } ], \"connectedToNetwork\" : [ { \"type\" : \"Network_Network\", \"referencedObjectID\" : \"2\" } ], \"jurisdiction\" : \"DE\" }, { \"type\" : \"StoredDataSet\", \"@id\" : \"33\", \"name\" : \"DataSet-KnowGoDB-RiskData\", \"id\" : 33, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"34\" } ], \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"10\" }, \"createdBySoftwareComponent\" : { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"35\" } }, { \"type\" : \"Record\", \"@id\" : \"34\", \"name\" : \"RiskData\", \"id\" : 34, \"partOf\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"33\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"36\", \"name\" : \"KGAnalytics_2\", \"id\" : 36, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"17\" }, \"usedByWebApp\" : [ { \"type\" : \"WebApplication\", \"referencedObjectID\" : \"7\" } ], \"softwareComponentUsesGateway\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"16\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"35\", \"name\" : \"KGAnalytics_1\", \"id\" : 35, \"hostedOn\" : { \"type\" : \"Compute\", \"referencedObjectID\" : \"1\" }, \"creates\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"33\" } ], \"usedByWebApp\" : [ { \"type\" : \"WebApplication\", \"referencedObjectID\" : \"7\" } ], \"softwareComponentUsesGateway\" : [ { \"type\" : \"Gateway\", \"referencedObjectID\" : \"16\" } ] } ] }";
////		// load model from JSON String to EMF
////		CloudEnvironment cloudEnvironment = null;
////		try {
////			cloudEnvironment = (CloudEnvironment) loader.loadEObjectFromString(model);
////			loader.setAsIsModel(cloudEnvironment);
//////			System.out.println("Loaded model after restart event as:\n" + runtimeModelLogic.returnentiremodelasjson(cloudEnvironment));
////			System.out.println("Loaded model after restart event.");
////		} catch (IOException e) {
////			//  Auto-generated catch block
////			System.err.println("Can't create EMF instance");
////		}
////		if (cloudEnvironment != null) {
////			riskFinder.lookForRisks(cloudEnvironment, "");
////		}
////
////		return "Done";
////	}
//
//
//	public static void writeDataLineByLine(String filePath, String[] data) {
//		// first create file object for file placed at location
//		// specified by filepath
//		File file = new File(filePath);
//		try {
//			// create FileWriter object with file as parameter
//			FileWriter outputfile = new FileWriter(file, true);
//
//			// create CSVWriter object filewriter object as parameter
//			CSVWriter writer = new CSVWriter(outputfile);
//
//			writer.writeNext(data);
//
//			// closing writer connection
//			writer.close();
//		} catch (IOException e) {
//			//  Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	private String insertNumberBeforeFileEnding(String fileName, int number) {
//		String firstPart = fileName.split("[.]csv")[0];
//		String returner = firstPart + number + ".csv";
////		System.out.println(returner);
//		return returner;
//	}
//}
