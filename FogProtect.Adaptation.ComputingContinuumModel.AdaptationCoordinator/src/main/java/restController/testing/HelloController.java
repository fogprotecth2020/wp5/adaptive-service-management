package restController.testing;

import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ChangeDetector.Changes;
import emfCompare.AttributePrecondition;
import emfCompare.CompareClient;
import emfCompare.ComperationTupel;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import fogprotect.adaptation.ComputingContinuumModel.impl.tosca_nodes_RootImpl;
import runtime.EMFModelLoad;
import runtime.RuntimeModelLogic;
import utility.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
//import org.eclipse.emf.compare.Diff;
//import org.eclipse.emf.compare.EMFCompare;
//import org.eclipse.emf.compare.merge.BatchMerger;
//import org.eclipse.emf.compare.merge.IBatchMerger;
//import org.eclipse.emf.compare.merge.IMerger;
//import org.eclipse.emf.compare.scope.DefaultComparisonScope;
//import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class HelloController {

	@Autowired
	RuntimeModelLogic logic;
	@Autowired
	EMFModelLoad loader;
	@Autowired
	RuntimeModelLogic runtimeModelLogic;

//	static final String fileName = "ExampleModel.cloudmodel";
//	static final String fileName = "FogExample.cloudmodel";
//	static final String fileName = "CloudExample.cloudmodel";
//	static final String fileName = "SmartCityFirstDraft.computingcontinuummodel";
	static final String fileName = "SmartManufacturingFirstDraft_adap1.computingcontinuummodel";
	static final String fileName3 = "SmartManufacturingFirstDraft_adap3.computingcontinuummodel";
	
	static final String fileName1 = "SmartManufacturingFirstDraft_original.computingcontinuummodel";
	static final String fileName2 = "SmartManufacturingFirstDraft_adap2.computingcontinuummodel";
	static final String fileName4 = "SmartManufacturingFirstDraft_adap4.computingcontinuummodel";
	static final String fileName5 = "SmartManufacturingFirstDraft_adap5.computingcontinuummodel";

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String index() {
		return getChanges();
		//return returnAsIsModel();
		//this.doSomething();
		//return "ok";
	}
	
	private void doSomething() {
        String jsonString = "{ \"type\" : \"CloudEnvironment\", \"@id\" : \"/\", \"tosca_nodes_root\" : [ { \"type\" : \"FogCompute\", \"@id\" : \"//@tosca_nodes_root.0\", \"name\" : \"NUC 1 (Botcraft VM and IDS VM)\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.1\", \"name\" : \"Dashboard Server\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.13\" }, \"controlledBy\" : [ { \"type\" : \"DataController\", \"referencedObjectID\" : \"//@tosca_nodes_root.32\" } ], \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.28\" } ] }, { \"type\" : \"Database\", \"@id\" : \"//@tosca_nodes_root.2\", \"name\" : \"Database\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" }, \"stores\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"//@tosca_nodes_root.11\" } ], \"encrypted\" : false }, { \"type\" : \"DBMS\", \"@id\" : \"//@tosca_nodes_root.3\", \"name\" : \"IDS Data Lake\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.13\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.28\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" } ], \"hosts\" : [ { \"type\" : \"Database\", \"referencedObjectID\" : \"//@tosca_nodes_root.2\" } ] }, { \"type\" : \"DataSubject\", \"@id\" : \"//@tosca_nodes_root.4\", \"name\" : \"Person\", \"id\" : 0, \"trust\" : [ { \"type\" : \"DataController\", \"referencedObjectID\" : \"//@tosca_nodes_root.32\" } ], \"location\" : \"DE\", \"owns\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.23\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.5\", \"name\" : \"Robot MES\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.0\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.19\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.20\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.6\", \"name\" : \"Kafka Server\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.0\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.8\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.7\", \"name\" : \"Scene Detection\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.14\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.21\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.8\" } ] }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.8\", \"name\" : \"Data Flow (Scene Detection - Kafka Server)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.7\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"IoTDevice\", \"@id\" : \"//@tosca_nodes_root.9\", \"name\" : \"ABB Robot\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.18\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.25\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"IoTDevice\", \"@id\" : \"//@tosca_nodes_root.10\", \"name\" : \"IP Camera\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.22\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"StoredDataSet\", \"@id\" : \"//@tosca_nodes_root.11\", \"name\" : \"Stored Data Set\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.30\" } ], \"storedIn\" : { \"type\" : \"Database\", \"referencedObjectID\" : \"//@tosca_nodes_root.2\" } }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.12\", \"name\" : \"Scene Data\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.8\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.28\" }, { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"//@tosca_nodes_root.11\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" } ], \"sensitive\" : false, \"encrypted\" : false, \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"//@tosca_nodes_root.4\" }, \"personal\" : true }, { \"type\" : \"FogCompute\", \"@id\" : \"//@tosca_nodes_root.13\", \"name\" : \"NUC 2 (FogProtectVM)\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" }, { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"FogCompute\", \"@id\" : \"//@tosca_nodes_root.14\", \"name\" : \"Google ML Board-1\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.7\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.15\", \"name\" : \"Robot Monitor Data\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.16\", \"name\" : \"Data Flow (Dashboard Server - Kafka Server)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.15\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.26\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.30\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.17\", \"name\" : \"Data Flow (Robot MES - Kafka Server)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.26\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.15\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.18\", \"name\" : \"Robot Control\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.9\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.19\" } ] }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.19\", \"name\" : \"Data Flow (Robot Control - Robot MES)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.24\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.18\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.20\", \"name\" : \"Data Flow (Robot Monitor - Robot MES)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.27\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.25\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.21\", \"name\" : \"Data Flow (Camera Streamer - Scene Detection)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.23\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.22\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.7\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.22\", \"name\" : \"Camera Streamer\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.10\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.21\" } ] }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.23\", \"name\" : \"Camera Stream\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.21\" } ], \"sensitive\" : false, \"encrypted\" : false, \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"//@tosca_nodes_root.4\" }, \"personal\" : true }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.24\", \"name\" : \"Robot Control Data\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.19\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.25\", \"name\" : \"Robot Monitor\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.9\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.20\" } ] }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.26\", \"name\" : \"Order\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.27\", \"name\" : \"Robot Monitor Data Stream\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.20\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.28\", \"name\" : \"Data Flow (Dashboard Server - IDS Data Lake)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" }, { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.29\", \"name\" : \"Data Flow (Kafka Server - IDS Data Lake)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.30\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.30\", \"name\" : \"Security Flag\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"//@tosca_nodes_root.11\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"PrivateSpace\", \"@id\" : \"//@tosca_nodes_root.31\", \"name\" : \"FiaB Shipping Container\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.0\" }, { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.13\" }, { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.14\" }, { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.9\" }, { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.10\" } ], \"trustworthy\" : \"HIGH\" }, { \"type\" : \"DataController\", \"@id\" : \"//@tosca_nodes_root.32\", \"name\" : \"FiaB Worker\", \"id\" : 0, \"location\" : \"DE\", \"controls\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" } ] } ] }";
        try {
			CloudEnvironment cloudEnvironment = (CloudEnvironment) loader.loadEObjectFromString(jsonString);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String getChanges() {
		CloudEnvironment adap1 = loader.loadJSON(fileName);
		CloudEnvironment adap2 = loader.loadJSON(fileName2);
		CloudEnvironment adap3 = loader.loadJSON(fileName3);
		CloudEnvironment adap4 = loader.loadJSON(fileName4);
		CloudEnvironment adap5 = loader.loadJSON(fileName5);
		CloudEnvironment ref = loader.loadJSON(fileName1);
		
		
		ComperationTupel comperationTupelAdaptation1 = new ComperationTupel(adap1);
		ComperationTupel comperationTupelAdaptation2 = new ComperationTupel(adap2);
		ComperationTupel comperationTupelAdaptation3 = new ComperationTupel(adap3);
		ComperationTupel comperationTupelAdaptation4 = new ComperationTupel(adap4);
		ComperationTupel comperationTupelAdaptation5 = new ComperationTupel(adap5);
		
		AttributePrecondition<Boolean> attributePreconditionAdaptation3_1 = new AttributePrecondition<Boolean>("fogprotect.adaptation.ComputingContinuumModel.FogCompute", "NUC 1 (Botcraft VM and IDS VM)", "compromised", false);
		comperationTupelAdaptation3.getPreconditions().add(attributePreconditionAdaptation3_1);
		
		List<ComperationTupel> tmp = new ArrayList<>();
		tmp.add(comperationTupelAdaptation1);
		tmp.add(comperationTupelAdaptation2);
		tmp.add(comperationTupelAdaptation5);
		tmp.add(comperationTupelAdaptation3);
		
		CompareClient.checkConflicts(tmp, ref);
		
		//JsonObject json = CompareClient.compareModels(adap1, adap2, ref);
		//String json = Changes.compareModels(before, after).toString();
		return "test";
	}

	private String returnAsIsModel() {
		return runtimeModelLogic.returnentiremodelasjsonFromFile(fileName);
	}

	private String createStatusInformationJsonProposal() {
		JsonObject head = new JsonObject();

		JsonObject asIsModel = new JsonParser().parse(runtimeModelLogic.returnentiremodelasjsonFromFile(fileName))
				.getAsJsonObject();
		JsonObject asIs = new JsonObject();
		asIs.add("AsIsModel", asIsModel);

		JsonArray risks = new JsonArray();

		JsonObject risk1 = new JsonObject();
		risk1.addProperty("Name", "Risk 1");
		risk1.addProperty("Description", "Risk Example Description 1");
		JsonArray nodesForRisk1 = new JsonArray();
		JsonObject node1ForNodesForRisk1 = new JsonObject();
		node1ForNodesForRisk1.addProperty("Name", "Fog Node");
		node1ForNodesForRisk1.addProperty("@id", "@tosca_nodes_root.0");
		JsonObject node2ForNodesForRisk1 = new JsonObject();
		node2ForNodesForRisk1.addProperty("Name", "VIdeo Streamer");
		node2ForNodesForRisk1.addProperty("@id", "@tosca_nodes_root.4");
		JsonObject node3ForNodesForRisk1 = new JsonObject();
		node3ForNodesForRisk1.addProperty("Name", "Data Controller");
		node3ForNodesForRisk1.addProperty("@id", "@tosca_nodes_root.13");
		nodesForRisk1.add(node1ForNodesForRisk1);
		nodesForRisk1.add(node2ForNodesForRisk1);
		nodesForRisk1.add(node3ForNodesForRisk1);
		risk1.add("Nodes", nodesForRisk1);

		JsonObject risk2 = new JsonObject();
		risk2.addProperty("Name", "Risk 2");
		risk2.addProperty("Description", "Risk Example Description 2");
		JsonArray nodesForRisk2 = new JsonArray();
		JsonObject node1ForNodesForRisk2 = new JsonObject();
		node1ForNodesForRisk2.addProperty("Name", "Camera Streaming Service");
		node1ForNodesForRisk2.addProperty("@id", "@tosca_nodes_root.6");
		JsonObject node2ForNodesForRisk2 = new JsonObject();
		node2ForNodesForRisk2.addProperty("Name", "Data Flow (camera - fog node)");
		node2ForNodesForRisk2.addProperty("@id", "@tosca_nodes_root.7");
		JsonObject node3ForNodesForRisk2 = new JsonObject();
		node3ForNodesForRisk2.addProperty("Name", "Pre-processing & Analysis");
		node3ForNodesForRisk2.addProperty("@id", "@tosca_nodes_root.3");
		nodesForRisk2.add(node1ForNodesForRisk2);
		nodesForRisk2.add(node2ForNodesForRisk2);
		nodesForRisk2.add(node3ForNodesForRisk2);
		risk2.add("Nodes", nodesForRisk2);

		risks.add(risk1);
		risks.add(risk2);

		asIs.add("Risks", risks);
		head.add("AsIs", asIs);

		JsonArray adaptations = new JsonArray();
		JsonObject adaptation1 = new JsonObject();
		adaptation1.addProperty("Name", "Adaptation 1");
		adaptation1.addProperty("Description", "Adaptation Example Description 1");
		JsonObject referenceModel1 = new JsonParser().parse(runtimeModelLogic.returnentiremodelasjsonFromFile(fileName))
				.getAsJsonObject();
		adaptation1.add("ReferenceModel", referenceModel1);

		JsonArray risksA1 = new JsonArray();

		JsonObject riskA11 = new JsonObject();
		riskA11.addProperty("Name", "Risk 1");
		riskA11.addProperty("Description", "Risk Example Description 1");
		JsonArray nodesForRiskA11 = new JsonArray();
		JsonObject node1ForNodesForRiskA11 = new JsonObject();
		node1ForNodesForRiskA11.addProperty("Name", "Fog Node");
		node1ForNodesForRiskA11.addProperty("@id", "@tosca_nodes_root.0");
		JsonObject node2ForNodesForRiskA11 = new JsonObject();
		node2ForNodesForRiskA11.addProperty("Name", "VIdeo Streamer");
		node2ForNodesForRiskA11.addProperty("@id", "@tosca_nodes_root.4");
		JsonObject node3ForNodesForRiskA11 = new JsonObject();
		node3ForNodesForRiskA11.addProperty("Name", "Data Controller");
		node3ForNodesForRiskA11.addProperty("@id", "@tosca_nodes_root.13");
		nodesForRiskA11.add(node1ForNodesForRiskA11);
		nodesForRiskA11.add(node2ForNodesForRiskA11);
		nodesForRiskA11.add(node3ForNodesForRiskA11);
		riskA11.add("Nodes", nodesForRiskA11);

		JsonObject riskA12 = new JsonObject();
		riskA12.addProperty("Name", "Risk 2");
		riskA12.addProperty("Description", "Risk Example Description 2");
		JsonArray nodesForRiskA12 = new JsonArray();
		JsonObject node1ForNodesForRiskA12 = new JsonObject();
		node1ForNodesForRiskA12.addProperty("Name", "Camera Streaming Service");
		node1ForNodesForRiskA12.addProperty("@id", "@tosca_nodes_root.6");
		JsonObject node2ForNodesForRiskA12 = new JsonObject();
		node2ForNodesForRiskA12.addProperty("Name", "Data Flow (camera - fog node)");
		node2ForNodesForRiskA12.addProperty("@id", "@tosca_nodes_root.7");
		JsonObject node3ForNodesForRiskA12 = new JsonObject();
		node3ForNodesForRiskA12.addProperty("Name", "Pre-processing & Analysis");
		node3ForNodesForRiskA12.addProperty("@id", "@tosca_nodes_root.3");
		nodesForRiskA12.add(node1ForNodesForRiskA12);
		nodesForRiskA12.add(node2ForNodesForRiskA12);
		nodesForRiskA12.add(node3ForNodesForRiskA12);
		riskA12.add("Nodes", nodesForRiskA12);

		risksA1.add(riskA11);
		risksA1.add(riskA12);

		adaptation1.add("Risks", risks);

		JsonObject adaptation2 = new JsonObject();
		adaptation2.addProperty("Name", "Adaptation 2");
		adaptation2.addProperty("Description", "Adaptation Example Description 2");
		JsonObject referenceModel2 = new JsonParser().parse(runtimeModelLogic.returnentiremodelasjsonFromFile(fileName))
				.getAsJsonObject();
		adaptation2.add("ReferenceModel", referenceModel2);

		JsonArray risksA2 = new JsonArray();

		JsonObject riskA21 = new JsonObject();
		riskA21.addProperty("Name", "Risk 1");
		riskA21.addProperty("Description", "Risk Example Description 1");
		JsonArray nodesForRiskA21 = new JsonArray();
		JsonObject node1ForNodesForRiskA21 = new JsonObject();
		node1ForNodesForRiskA21.addProperty("Name", "Fog Node");
		node1ForNodesForRiskA21.addProperty("@id", "@tosca_nodes_root.0");
		JsonObject node2ForNodesForRiskA21 = new JsonObject();
		node2ForNodesForRiskA21.addProperty("Name", "VIdeo Streamer");
		node2ForNodesForRiskA21.addProperty("@id", "@tosca_nodes_root.4");
		JsonObject node3ForNodesForRiskA21 = new JsonObject();
		node3ForNodesForRiskA21.addProperty("Name", "Data Controller");
		node3ForNodesForRiskA21.addProperty("@id", "@tosca_nodes_root.13");
		nodesForRiskA21.add(node1ForNodesForRiskA21);
		nodesForRiskA21.add(node2ForNodesForRiskA21);
		nodesForRiskA21.add(node3ForNodesForRiskA21);
		riskA21.add("Nodes", nodesForRiskA21);

		JsonObject riskA22 = new JsonObject();
		riskA22.addProperty("Name", "Risk 2");
		riskA22.addProperty("Description", "Risk Example Description 2");
		JsonArray nodesForRiskA22 = new JsonArray();
		JsonObject node1ForNodesForRiskA22 = new JsonObject();
		node1ForNodesForRiskA22.addProperty("Name", "Camera Streaming Service");
		node1ForNodesForRiskA22.addProperty("@id", "@tosca_nodes_root.6");
		JsonObject node2ForNodesForRiskA22 = new JsonObject();
		node2ForNodesForRiskA22.addProperty("Name", "Data Flow (camera - fog node)");
		node2ForNodesForRiskA22.addProperty("@id", "@tosca_nodes_root.7");
		JsonObject node3ForNodesForRiskA22 = new JsonObject();
		node3ForNodesForRiskA22.addProperty("Name", "Pre-processing & Analysis");
		node3ForNodesForRiskA22.addProperty("@id", "@tosca_nodes_root.3");
		nodesForRiskA22.add(node1ForNodesForRiskA22);
		nodesForRiskA22.add(node2ForNodesForRiskA22);
		nodesForRiskA22.add(node3ForNodesForRiskA22);
		riskA22.add("Nodes", nodesForRiskA22);

		risksA2.add(riskA21);
		risksA2.add(riskA22);

		adaptation2.add("Risks", risks);

		adaptations.add(adaptation1);
		adaptations.add(adaptation2);

		head.add("Adaptations", adaptations);

		return head.toString();
	}

//	System.out.println("TRY");
//	CloudEnvironment cloudEnvironment = loader.getCloudEnvironmentsMonitored().get(Long.valueOf("1"));
//	EObject object = logic.searchForObjectInGivenModel(cloudEnvironment, "//@tosca_nodes_root.13");
//	System.out.println(object.getClass().getSimpleName());
//	System.out.println(((tosca_nodes_RootImpl)object).getName());
//	
//	return "Done";

//	URI uri1 = URI.createFileURI(Constants.RUNTIME_MODEL_PATH + "AdaptedModel1.cloudmodel");
//    URI uri2 = URI.createFileURI(Constants.RUNTIME_MODEL_PATH + "ServiceInstance100.cloudmodel");
//
//    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
//
//    ResourceSet resourceSet1 = new ResourceSetImpl();
//    ResourceSet resourceSet2 = new ResourceSetImpl();
//
//    resourceSet1.getResource(uri1, true);
//    resourceSet2.getResource(uri2, true);

//    IComparisonScope scope = new DefaultComparisonScope(resourceSet1, resourceSet2, resourceSet2);
//    Comparison comparison = EMFCompare.builder().build().compare(scope);
//
//    List<Diff> differences = comparison.getDifferences();
//    
//    if(!differences.isEmpty())
//    {
//    	System.out.println("Differenzen: ");
//    	System.out.println(differences);
//    }
//    
//    // Let's merge every single diff
//    IMerger.Registry mergerRegistry = new IMerger.RegistryImpl();
//    IBatchMerger merger = new BatchMerger(mergerRegistry);
//    merger.copyAllLeftToRight(differences, new BasicMonitor());
//    return differences.toString();

}