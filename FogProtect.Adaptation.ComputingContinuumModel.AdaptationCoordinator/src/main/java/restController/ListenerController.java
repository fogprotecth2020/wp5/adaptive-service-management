package restController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import runtime.EMFModelLoad;
import runtime.PersonalLogger;
import runtime.RuntimeModelLogic;

@RestController
public class ListenerController {

	@Autowired
	EMFModelLoad loader;
	@Autowired
	RuntimeModelLogic logic;
	@Autowired
	PersonalLogger log;
	
	private static String className = ListenerController.class.getName();
	
	@RequestMapping(path = "/notification", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean notification(@RequestBody(required = true) String bodyJSON) {
		log.info("Received Model Change information... Forwarding to OperatorWeb!", className);
		loader.removeRisks();
		loader.setRisklevelOfAsIsModel("Unknown");
		loader.getAdaptations().clear();
		logic.notifyAllAboutChanges();
		return true;
	}
}
