package restController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import HelperClasses.ManagementComponentObject;
import HelperClasses.Node;
import HelperClasses.Risk;
import runtime.PersonalLogger;
import runtime.RiskFinder;
import runtime.RuntimeModelLogic;
import conflictIdentification.AdaptationCollector;
import conflictIdentification.ConflictIdentifier;

@RestController
public class ManagementController {

	@Autowired
	RiskFinder riskFinder;
	@Autowired
	@Lazy
	RuntimeModelLogic runtimeModelLogic;
	@Autowired
	@Lazy
	OperatorApplicationController operatorApplicationController;
	@Autowired
	PersonalLogger log;
	
	@Value("${AdaptationResultURL}")
	private String adaptationResult;

	private LinkedList<ManagementComponentObject> managementComponentObjects = new LinkedList<ManagementComponentObject>();

	private int waitingForXAnswers = 0;
	
	private static String className = ManagementController.class.getName();

	LinkedList<JsonElement> adaptationList = new LinkedList<JsonElement>();

	@RequestMapping(path = "/fogprotect/adaptationCoordinator/proposedAdaptation", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean proposedAdaptation(@RequestBody(required = true) String bodyJSON) {
		String decodedWithEqualsSign = "";
		try {
			log.info("Lowering pending answers from " + waitingForXAnswers + " to " + (waitingForXAnswers - 1), className);
			this.lowerPendingAnswer();
			decodedWithEqualsSign = URLDecoder.decode(bodyJSON, "UTF-8");
			log.jsonInfo("Proposed Adaptation: \n" + decodedWithEqualsSign, className);
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
			int id = receivedJson.get("ID").getAsInt();
			String taskId = receivedJson.get("SieaTaskId").getAsString();
			JsonArray adaptations = receivedJson.get("Adaptations").getAsJsonArray();
			for (JsonElement adaptation : adaptations) {
				adaptationList.add(adaptation);
				
				// We send only one adaptation to not confuse RiskAssessment.
				break;
			}
			if (waitingForXAnswers == 0) {
				log.info("Received all answers", className);
				riskFinder.doSomethingWithGivenAdaptations(taskId);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	//FIXME getRisks potentialError
	@RequestMapping(path = "/fogprotect/adaptationCoordinator/getRisks", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean getRisks(@RequestBody(required = true) String bodyJSON) {
		String decodedWithEqualsSign = "";
		try {
			decodedWithEqualsSign = URLDecoder.decode(bodyJSON, "UTF-8");
			log.jsonInfo("Risks: \n" + decodedWithEqualsSign, className);
			JsonObject parsedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
			String taskId = parsedJson.get("SieaTaskId").getAsString();
			JsonArray risks = parsedJson.get("risks").getAsJsonArray();
			for (JsonElement risk : risks) {
				ArrayList<Node> nodes = new ArrayList<Node>();
				for (JsonElement node : risk.getAsJsonObject().get("Nodes").getAsJsonArray()) {
					Node nodeObject = new Node(node.getAsJsonObject().get("Name").getAsString(),node.getAsJsonObject().get("@id").getAsString());
					nodes.add(nodeObject);
				}
				int id = runtimeModelLogic.getLoader().getRisksOfAsIsModel().size();
				Risk riskObject = new Risk(risk.getAsJsonObject().get("Name").getAsString(), risk.getAsJsonObject().get("Description").getAsString(), id, nodes);
				runtimeModelLogic.getLoader().getRisksOfAsIsModel().add(riskObject);
			}
			log.info("Received Risk... Forwarding to OperatorWeb!", className);
			String json = runtimeModelLogic.createStatusInformationJson(taskId);
			operatorApplicationController.broadcastStatusInformation(json);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@RequestMapping(path = "/fogprotect/adaptationCoordinator/adaptationResult", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean adaptationResult(@RequestBody(required = true) String bodyJSON) {
		String decodedWithEqualsSign = "";
		try {
			decodedWithEqualsSign = URLDecoder.decode(bodyJSON, "UTF-8");
			log.jsonInfo("Adaptation Result: \n" + decodedWithEqualsSign, className);
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
			int id = receivedJson.get("ID").getAsInt();
			String taskId = receivedJson.get("SieaTaskId").getAsString();
			boolean successful = receivedJson.get("Successful").getAsBoolean();
			if(successful == true) {
				log.info("Adaptation was successful", className);
				runtimeModelLogic.getLoader().getAdaptations().clear();
				
				//FIXME adaptationResult possibleError
				JsonObject head = new JsonObject();
				head.addProperty("SieaTaskId", taskId);
				head.addProperty("result", "Could be a real value");
				head.add("ExecutedModel", receivedJson.get("ExecutedModel"));
				runtimeModelLogic.makeRequest(adaptationResult, head.toString());
			}else {
				log.info("Adaptation was not successful", className);
				//FIXME implement logic to execute next best adaptation or ask operator for help
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	@RequestMapping(path = "/fogprotect/addManagementComponent", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean addManagementComponent(@RequestBody(required = true) String restUrl) { // "ip:port/urlToManagementComponent"
		try {
			String decodedWithEqualsSign = "";
			try {
				decodedWithEqualsSign = URLDecoder.decode(restUrl, "UTF-8");

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			log.jsonInfo("addManagementComponent: \n" + decodedWithEqualsSign, className);
			JsonObject urls = (JsonObject) new JsonParser().parse(decodedWithEqualsSign);
			boolean exists = false;
			for (ManagementComponentObject managementComponentObject : managementComponentObjects) {
				if (managementComponentObject.getUrlAdaptationRequest()
						.equals(urls.get("urlAdaptationRequest").getAsString())) {
					exists = true;
				}
			}
			if (exists == true) {
				log.info("ManagementComponent already exists", className);
				return false;
			} else {
				managementComponentObjects.add(new ManagementComponentObject(
						urls.get("urlAdaptationRequest").getAsString(), urls.get("urlCallForAction").getAsString(),
						urls.get("TypeOfManagementComponent").getAsString(), urls.get("urlImmediateAction").getAsString(),
						urls.get("urlAdaptationCollection").getAsString(), urls.get("urlDisallowAdaptations").getAsString()));
				log.info("Registered ManagementComponent.", className);
				return true;
			}
		} catch (Exception e) {
			log.error("Error adding ManagementComponent", className);
			return false;
		}
	}
	
	@Autowired
	ConflictIdentifier identifier;	
	
	
	//TODO rename the path?
	@RequestMapping(path = "/conflictIdentification", method = RequestMethod.GET)
	public String startConflictIdentification() {
		// Part 1: collect all adaptations from managers
		log.info("Starting Conflict Identification.", className);
		AdaptationCollector collector = new AdaptationCollector(log);

		for (ManagementComponentObject manager: managementComponentObjects) {
			collector.requestAdaptationRulesFromManager(manager.getTypeOfManagementComponent(), manager.getUrlAdaptationCollection());
		}		
		
		// gets the results of adaptation collection and initializes the identification class with it.
		identifier.init(collector.getResultOfAdaptationCollection());
				
		identifier.identifyConflicts();
			
		return "Conflict Identification Done.";
	}
	
	public LinkedList<ManagementComponentObject> getManagementComponentObjects() {
		return managementComponentObjects;
	}

	public void setManagementComponentObjects(LinkedList<ManagementComponentObject> managementComponentObjects) {
		this.managementComponentObjects = managementComponentObjects;
	}

	public void addPendingAnswer() {
		waitingForXAnswers++;
	}

	public void lowerPendingAnswer() {
		waitingForXAnswers--;
	}

	public LinkedList<JsonElement> getAdaptationList() {
		return adaptationList;
	}

	public void setAdaptationList(LinkedList<JsonElement> adaptationList) {
		this.adaptationList = adaptationList;
	}

}
