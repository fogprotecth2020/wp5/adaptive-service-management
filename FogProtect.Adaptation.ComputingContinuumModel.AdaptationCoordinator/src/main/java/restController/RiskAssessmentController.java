package restController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedList;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import HelperClasses.Adaptation;
import HelperClasses.RiskAssessmentObject;
import HelperClasses.RiskLevel;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import runtime.PersonalLogger;
import runtime.RiskFinder;
import runtime.RuntimeModelLogic;

@RestController
public class RiskAssessmentController {

	@Autowired
	@Lazy
	RuntimeModelLogic runtimeModelLogic;
	@Autowired
	@Lazy
	RiskFinder riskFinder;
	@Autowired
	OperatorApplicationController operatorApplicationController;
	@Autowired
	PersonalLogger log;

	private LinkedList<RiskAssessmentObject> riskAssessmentObjects = new LinkedList<RiskAssessmentObject>();
	private static String className = RiskAssessmentController.class.getName();

	/*
	 * Adds a RiskAssessmentRestListener: This web service works similar to the
	 * /addlistener service, with the only difference that the bodies of the
	 * requests that will be made to the specified URL will contain a longer JSON
	 * string that additionally encodes the entire contents of the run-time model,
	 * in the same way the /riskassessment/ returnentiremodel service does.
	 */
	@RequestMapping(path = "/riskassessment/addriskassessmentlistener", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean addRiskAssessmentListener(@RequestBody(required = true) String restUrl) { // "ip:port/urlToRiskAssessment"
		try {
			String decodedWithEqualsSign = "";
			try {
				decodedWithEqualsSign = URLDecoder.decode(restUrl, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
			boolean exists = false;
			for (RiskAssessmentObject riskAssessmentListener : riskAssessmentObjects) {
				if (riskAssessmentListener.getUrl().equals(decodedWithEqualsSign)) {
					exists = true;
				}
			}
			if (exists == true) {
				log.info("RiskAssessmentListener already exists", className);
				return false;
			} else {
				riskAssessmentObjects.add(new RiskAssessmentObject(decodedWithEqualsSign));
				log.info("Registered RiskAssessment.", className);
				return true;
			}
		} catch (Exception e) {
			log.error("Error adding RiskAssessment", className);
			return false;
		}
	}

	/*
	 * Should be called by the Threat Diagnosis component when it wants to notify
	 * about a risk evaluation: This web service allows the Threat Diagnosis
	 * component to notfiy a model. The adaptation component will try to read the
	 * json and will return true if possible, false if it is not readable
	 */
	@RequestMapping(path = "/fogprotect/adaptationcoordinator/notify", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean notify(@RequestBody(required = true) String bodyJSON) {
		log.jsonInfo("Received JSON from ITI: " + bodyJSON, className);
		String decodedWithEqualsSign = "";
		try {
			decodedWithEqualsSign = URLDecoder.decode(bodyJSON, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		JsonObject stringFromRiskAssessment = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
		String notificationType = stringFromRiskAssessment.get("NotificationType").getAsString();
		String taskId = stringFromRiskAssessment.get("SieaTaskId").getAsString();
		
		switch (notificationType) {
		case "ResultOfRiskCalculation": {
			int id = 0;
			RiskLevel overallRiskLevel = RiskLevel
					.valueOf(stringFromRiskAssessment.get("OverallRiskLevel").getAsString().replace(" ", ""));
			runtimeModelLogic.getLoader().setRisklevelOfAsIsModel(overallRiskLevel.toString());
			RiskLevel acceptableRiskLevel = RiskLevel
					.valueOf(stringFromRiskAssessment.get("AcceptableRiskLevel").getAsString().replace(" ", ""));
			if (overallRiskLevel.compareTo(acceptableRiskLevel) > 0) {
				// Need to find an adaptation
				log.info("Received redlight for \"As is\" model", className);
				JsonArray risks = stringFromRiskAssessment.get("Risks").getAsJsonArray();
				riskFinder.lookForRiskMitigations(runtimeModelLogic.getLoader().getAsIsModel().getModel(), id, risks, taskId);
			} else {
				// time for improvement search
				log.info("Received greenlight for \"As is\" model", className);
				riskFinder.lookForImprovements(runtimeModelLogic.getLoader().getAsIsModel().getModel(), id, taskId);
			}
			break;
		}
		case "EvaluationOfAdaptationProposals": {
			log.info("Received answer from ThreadDiagnosis", className);
			JsonArray adaptationRisks = stringFromRiskAssessment.get("AdaptationRisks").getAsJsonArray();
			RiskLevel acceptableRiskLevel = RiskLevel
					.valueOf(stringFromRiskAssessment.get("AcceptableRiskLevel").getAsString().replace(" ", ""));
			for (JsonElement e : adaptationRisks) {
				JsonObject adaptationProposal = e.getAsJsonObject();
				int adaptationProposalId = Integer
						.valueOf(adaptationProposal.get("AdaptationProposalId").getAsString());
				RiskLevel adaptationProposalRiskLevel = RiskLevel.valueOf(
						adaptationProposal.get("RiskLevel").getAsJsonObject().get("OverallRiskLevel").getAsString().replace(" ", ""));
				Adaptation adaptation = runtimeModelLogic.getLoader().getAdaptations().get(adaptationProposalId - 1);
				adaptation.setRiskLevel(adaptationProposalRiskLevel);
			}
			boolean sentCallForAction = false;
			for (Adaptation adaptation : runtimeModelLogic.getLoader().getAdaptations()) {
				int i = 0;
				if (adaptation.getRiskLevel().compareTo(acceptableRiskLevel) <= 0) {
					sentCallForAction = true;
					runtimeModelLogic.sendCallForAction(adaptation, i, taskId);
					break;
				}
				i++;
			}
			if(sentCallForAction == false) {
				log.info("No Adaptation Proposal was feasable!", className);
				String a = runtimeModelLogic.createStatusInformationJson(taskId);
				log.jsonInfo("StatusInformation with Adaptations:" + a, className);
				operatorApplicationController.broadcastOperatorIntervention(a);
			}
			break;
		}
		case "ImmediateAction": {
			int id = 0;
			String eventName = stringFromRiskAssessment.get("EventName").getAsString();
			// Trigger immediateAction at given Manager
			riskFinder.immediateAction(eventName, runtimeModelLogic.getLoader().getAsIsModel().getModel(), id, taskId);
			break;
		}
		}
		return true;
	}

	public LinkedList<RiskAssessmentObject> getRiskassessmentObjects() {
		return riskAssessmentObjects;
	}

	public void setRiskassessmentObjects(LinkedList<RiskAssessmentObject> riskassessmentlisteners) {
		this.riskAssessmentObjects = riskassessmentlisteners;
	}

	public RuntimeModelLogic getRuntimeModelLogic() {
		return runtimeModelLogic;
	}

	public void setRuntimeModelLogic(RuntimeModelLogic runtimeModelLogic) {
		this.runtimeModelLogic = runtimeModelLogic;
	}

	// DEPRECATED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	/*
	 * Should return the entire runtimemodel as a JSON String: This web service
	 * allows the riskassessment component to receive the entire contents of the
	 * run-time model on request, encoded into a single JSON string. This string
	 * contains an array of objects, that are in turn JSON representations of the
	 * Service-type objects in the run-time model.
	 */
	@RequestMapping(path = "/riskassessment/returnentiremodel/{id}", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public String returnEntireModel(@PathVariable("id") long id) {
		return runtimeModelLogic.returnentiremodelasjson(id);
	}

	@RequestMapping(path = "/riskassessment/returnentiremodel", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public String returnEntireModel() {
		return runtimeModelLogic.returnentiremodelasjson(runtimeModelLogic.getLoader().getAsIsModel().getModel());
	}


}
