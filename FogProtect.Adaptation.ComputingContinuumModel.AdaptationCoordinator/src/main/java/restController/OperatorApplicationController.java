package restController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EReferenceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.google.gson.JsonElement;

import HelperClasses.RiskAssessmentObject;
import HelperClasses.Risk;
import HelperClasses.Node;
import HelperClasses.Adaptation;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelFactory;
import fogprotect.adaptation.ComputingContinuumModel.Trustworthy;
import fogprotect.adaptation.ComputingContinuumModel.Jurisdictions;
import fogprotect.adaptation.ComputingContinuumModel.ConnectionType;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import runtime.PersonalLogger;
import runtime.RiskFinder;
import runtime.RuntimeModelLogic;

@Controller
@CrossOrigin
public class OperatorApplicationController {

	@Autowired
	@Lazy
	RuntimeModelLogic runtimeModelLogic;

	@Autowired
	@Lazy
	RiskFinder riskFinder;

	@Autowired
	PersonalLogger log;

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	private static String className = OperatorApplicationController.class.getName();
	
	@MessageMapping("/hello")
	public void broadcastStatusInformation(String message) {
		this.simpMessagingTemplate.convertAndSend("/queue/statusInformation", message);
	}

	public void broadcastOperatorIntervention(String message) {
		log.info("sending the message", className);
		this.simpMessagingTemplate.convertAndSend("/queue/operatorIntervention", message);
	}

	@RequestMapping(path = "/fogprotect/adaptationCoordinator/humanIntervention", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean humanIntervention(@RequestBody(required = true) String bodyJSON) {
		// NEEDS TO WORK WITH THE new JSON
		String decodedWithEqualsSign = "";
		try {
			decodedWithEqualsSign = URLDecoder.decode(bodyJSON, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		JsonObject adaptationOperator = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
		int adaptationID = Integer.valueOf(adaptationOperator.get("ID").getAsString());

		Adaptation adaptationOperat = runtimeModelLogic.getLoader().getAdaptations().get(adaptationID);

		//fake taskId -1
		runtimeModelLogic.sendCallForAction(adaptationOperat, adaptationID, "-1");

		return true;
	}

//	@RequestMapping(path = "/fogprotect/adaptationCoordinator/humanAdaptationRequest", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
//	@ResponseBody
//	public boolean humanAdaptationRequest(@RequestBody(required = true) String bodyJSON) {
//		// NEEDS TO WORK WITH THE new JSON
//
//		return true;
//	}

	@RequestMapping(path = "/fogprotect/adaptationCoordinator/statusInformation", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public String statusInformation() {
		// -1 is ignored by statusInformation creation
		return runtimeModelLogic.createStatusInformationJson("-1");
	}

	@RequestMapping(path = "/fogprotect/adaptationCoordinator/humanAdaptationRequest", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public long humanAdaptationRequest(@RequestBody(required = true) String jsonAsIsModel) {
		String decodedWithEqualsSign;

		try {
			log.info("Received Monitoring Event", className);
			decodedWithEqualsSign = URLDecoder.decode(jsonAsIsModel, "UTF-8");
			// Create Object out of JSON
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();

			CloudEnvironment cloudEnvironment = runtimeModelLogic.getLoader().getAsIsModel().getModel();
			String temp = runtimeModelLogic.returnentiremodelasjson(cloudEnvironment);
			cloudEnvironment = (CloudEnvironment) runtimeModelLogic.getLoader().loadEObjectFromString(temp);

			String requestType = receivedJson.get("RequestType").getAsString();// Determines what kind of Request the
																				// Json is

			if (requestType.equals("changeAttribute"))// To change the Value of an Existing Attribute
			{
				String atId = receivedJson.get("@id").getAsString();
				String attributeToChange = receivedJson.get("attribute").getAsString();
				String newValue = receivedJson.get("newValue").getAsString();
				String attributeType = receivedJson.get("attributeType").getAsString();

				EObject object = runtimeModelLogic.searchForObjectInGivenModel(cloudEnvironment, atId);
				EStructuralFeature feature = object.eClass().getEStructuralFeature(attributeToChange);

				if (attributeType.equals("String")) {
					object.eSet(feature, newValue);
				} else if (attributeType.equals("boolean")) {
					Boolean newValueBool = Boolean.parseBoolean(newValue);
					object.eSet(feature, newValueBool);
				} else if (attributeType.equals("int")) {
					Integer newValueInt = Integer.parseInt(newValue);
					object.eSet(feature, newValueInt);
				} else if (attributeType.equals("double")) {
					Double newValueDouble = Double.parseDouble(newValue);
					object.eSet(feature, newValueDouble);
				} else if (attributeType.equals("Trustworthy")) {
					object.eSet(feature, Trustworthy.getByName(newValue));
				} else if (attributeType.equals("Jurisdictions")) {
					object.eSet(feature, Jurisdictions.getByName(newValue));
				} else if (attributeType.equals("ConnectionType")) {
					object.eSet(feature, ConnectionType.getByName(newValue));
				}
			}

			if (requestType.equals("addRelation"))// Create a New Relation between Two EObject
			{
				log.info("Add Relation", className);
				String atId = receivedJson.get("@id").getAsString();
				String TargetID = receivedJson.get("targetid").getAsString();
				String relationName = receivedJson.get("relationName").getAsString();

				EObject SourceObject = runtimeModelLogic.searchForObjectInGivenModel(cloudEnvironment, atId);
				EObject TargetObject = runtimeModelLogic.searchForObjectInGivenModel(cloudEnvironment, TargetID);
				EStructuralFeature RelationToChange = SourceObject.eClass().getEStructuralFeature(relationName);

				if (RelationToChange != null) {
					if (RelationToChange.isMany()) {
						List<Object> RelationList = (List<Object>) SourceObject.eGet(RelationToChange);
						RelationList.add(TargetObject);
					} else {
						SourceObject.eSet(RelationToChange, TargetObject);
					}
				} else {
					return 400;
				}

			}

			if (requestType.equals("deleteRelation"))// Delete Relation
			{
				// System.out.println("Delete Relation");
				String atId = receivedJson.get("@id").getAsString();
				String TargetID = receivedJson.get("targetid").getAsString();
				String relationName = receivedJson.get("relationName").getAsString();

				EObject SourceObject = runtimeModelLogic.searchForObjectInGivenModel(cloudEnvironment, atId);
				EObject TargetObject = runtimeModelLogic.searchForObjectInGivenModel(cloudEnvironment, TargetID);
				EStructuralFeature StructureToDelete = SourceObject.eClass().getEStructuralFeature(relationName);

				EReferenceImpl RefToDelete = (EReferenceImpl) SourceObject.eClass().getEStructuralFeature(relationName);
				EReference OppositeRelation = RefToDelete.getEOpposite();
				EStructuralFeature OppositeFeature = TargetObject.eClass()
						.getEStructuralFeature(OppositeRelation.getName());

				// Delete the Relation
				if (StructureToDelete != null) {
					if (StructureToDelete.isMany())// Relation has more Elements
					{
						List<Object> RelationList = (List<Object>) SourceObject.eGet(StructureToDelete);

						if (RelationList.size() == 1)// Relation is Array with 1 Element
						{
							SourceObject.eUnset(StructureToDelete);
						} else {
							RelationList.remove(TargetObject);
						}
					} else {
						SourceObject.eUnset(StructureToDelete);
					}

					// Delete the Opposite
					if (OppositeFeature.isMany())// Relation has more Elements
					{
						List<Object> RelationList = (List<Object>) TargetObject.eGet(OppositeFeature);

						if (RelationList.size() == 1)// Relation is Array with 1 Element
						{
							TargetObject.eUnset(OppositeFeature);
						} else {
							RelationList.remove(SourceObject);
						}
					} else {
						TargetObject.eUnset(OppositeFeature);
					}
				}else {
					return 400;
				}
			}

			// TODO Fix Bug that URI is similiar to the other objects
			if (requestType.equals("addNode")) {
				String NodeType = receivedJson.get("nodeType").getAsString();
				String NodeName = receivedJson.get("nodeName").getAsString();
				EObject nodeToAdd = null;

				ComputingContinuumModelFactory factory = ComputingContinuumModelFactory.eINSTANCE;
				try {
					Method methodToCreateObject = factory.getClass().getMethod("create" + NodeType);
					nodeToAdd = (EObject) methodToCreateObject.invoke(factory);
					if (nodeToAdd != null) {
						EStructuralFeature feature = nodeToAdd.eClass().getEStructuralFeature("name");
						nodeToAdd.eSet(feature, NodeName);
						cloudEnvironment.getTosca_nodes_root().add((tosca_nodes_Root) nodeToAdd);
						log.info("done", className);
					}else {
						return 400;
					}
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
					return 400;
				}
			}

			if (requestType.equals("deleteNode")) {
				String atId = receivedJson.get("@id").getAsString();
				EObject objectToRemove = runtimeModelLogic.searchForObjectInGivenModel(cloudEnvironment, atId);
				if(objectToRemove != null) {
					this.deleteAllRelation(objectToRemove);
					cloudEnvironment.getTosca_nodes_root().remove(objectToRemove);
				}else {
					return 400;
				}
			}

			// store as proposal
			String name = "OperatorAdaptation";
			String description = "This adaptation is created by a human operator.";
			CloudEnvironment referenceModel = cloudEnvironment;
			String typeOfManagementComponent = "Application"; // TODO this needs to be clarified in the future because
																// this may not always be an application manager job
			Adaptation adaptation = new Adaptation(referenceModel, name, description, typeOfManagementComponent);
			// TODO maybe add risks
			runtimeModelLogic.getLoader().getAdaptations().add(adaptation);

			// fake taskId -1
			riskFinder.lookForRisks(runtimeModelLogic.getLoader().getAdaptations(), "-1");

		} catch (UnsupportedEncodingException e) {
			log.error(e.toString(), className);
			return 400;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
			return 400;
		}
		return 200;
	}

	private void deleteAllRelation(EObject object) {
		List<EStructuralFeature> RelationList = object.eClass().getEAllStructuralFeatures();
		for (EStructuralFeature Feature : RelationList) {
			object.eUnset(Feature);
		}
	}
	
	@RequestMapping(path = "/changeFactors", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean changeFactorPrioritization(@RequestBody(required = true) String jsonBody) {
		String decodedWithEqualsSign = "";
		
		try {
			decodedWithEqualsSign = URLDecoder.decode(jsonBody, "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		JsonArray receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonArray();
		
		LinkedList<String> factors = new LinkedList<>();
		factors.add("Risk");
		for (JsonElement factor : receivedJson) {
			factors.add(factor.getAsString());
		}
		
		riskFinder.setFactors(factors.toArray(new String[0]));
		return true;
	}

}
