package ChangeDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.internal.spec.AttributeChangeSpec;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.IdentifierEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;

public class Changes {
	public static JsonObject compareModels(EObject before, EObject after) {

		List<ChangedObj> changedObjects = new ArrayList<>();

		if (after != null && before != null) {

			List<Diff> differences = getDiffs(after, before);

//			differences.forEach(a -> System.out.println(a));

			List<AttributeChangeSpec> changedAttributes = differences.stream()
					.filter(diff -> diff instanceof AttributeChangeSpec)
					.map(changes -> (AttributeChangeSpec) changes).collect(Collectors.toList());

			changedObjects = getChangedObjects(changedAttributes);
//			changedObjects.forEach(a -> System.out.println(a));
			return createDiffJson(changedObjects);
		}else {
			return new JsonObject();
		}
	}
	
	private static List<ChangedObj> getChangedObjects(List<AttributeChangeSpec> changedAttributes) {
		 
		List<ChangedObj> changedObjects = new ArrayList<>();
 
		for (AttributeChangeSpec changeSpec : changedAttributes) {
 
			Object oldAttrValue = null;
			Object newAttrValue = changeSpec.getValue();
 
			Match match = changeSpec.getMatch();
 
			EObject oldEObj = match.getRight();
			EObject newEObj = match.getLeft();
 
			// find old value
			if (newEObj != null) {
				oldAttrValue = oldEObj.eGet(changeSpec.getAttribute());
			} else {
				// ignored for logger
				System.out.println("getRight() is null in compareModels()");
			}
 
			EStructuralFeature nameFeature = oldEObj.eClass().getEStructuralFeature("name");
			// EStructuralFeature typeFeature = asIs.eClass().getEStructuralFeature("type");
 
			String objName = oldEObj.eGet(nameFeature).toString();
			String objAtId = oldEObj.eResource().getURIFragment(oldEObj);
			String objType = oldEObj.eClass().getInstanceTypeName();
 
			String attrName = changeSpec.getAttribute().getName();
			String attrType = changeSpec.getAttribute().getEType().getName();
			String changeType = changeSpec.getKind().toString();
 
			// all values set
			ChangedObj obj = new ChangedObj(objAtId, objName, objType);
			ChangedAttribute attr = new ChangedAttribute(attrName, attrType, oldAttrValue.toString(),
					newAttrValue.toString(), changeType);
 
			if (changedObjects.contains(obj)) {
				changedObjects.stream().forEach(a -> {
					if (a.equals(obj)) {
						a.getChanges().add(attr);
					}
				});
			} else {
				obj.getChanges().add(attr);
				changedObjects.add(obj);
			}
 
		}
		return changedObjects;
	}

//----helpers----------------
	private static List<Diff> getDiffs(EObject before, EObject after) {
		IEObjectMatcher fallBackMatcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.WHEN_AVAILABLE);
		IEObjectMatcher customNameMatcher = new IdentifierEObjectMatcher(fallBackMatcher, idFunction1);
 
		IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
		IMatchEngine.Factory.Registry registry = MatchEngineFactoryRegistryImpl.createStandaloneInstance();
		final MatchEngineFactoryImpl matchEngineFactory = new MatchEngineFactoryImpl(customNameMatcher,
				comparisonFactory);
 
		matchEngineFactory.setRanking(20); // default engine ranking is 10, must be higher to override.
		registry.add(matchEngineFactory);
 
		IComparisonScope scope = new DefaultComparisonScope(before, after, null);
		Comparison result = EMFCompare.builder().setMatchEngineFactoryRegistry(registry).build().compare(scope);
 
		return result.getDifferences();
	}
	
	static com.google.common.base.Function<EObject, String> idFunction1 = new com.google.common.base.Function<EObject, String>() {
		public String apply(EObject input) {
			if (input instanceof EObject) {
				EObject obj = (EObject) input;
 
				String objAtId = obj.eResource().getURIFragment(obj);
 
				return objAtId;
			}
			// a null return here tells the match engine to fall back to the other matchers
			return null;
		}
	};

//just to configure the Json representation
	private static JsonObject createDiffJson(List<ChangedObj> changes) {
		JsonArray jarr = new JsonArray();
		for (ChangedObj change : changes) {
			Gson gson = new Gson();
			String jsonString = gson.toJson(change);
			JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
			jarr.add(jsonObject);
		}

		JsonObject jObj = new JsonObject();
		jObj.add("ChangesMadeToAsIsModel", jarr);

		/*
		 * jObj = new JSONObject(jObj.toString() .replaceAll("", replacement))
		 * 
		 */

		return jObj;
	}

	public EObject searchForObjectInGivenModel(CloudEnvironment cloudEnvironment, String id) {
		EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
		for (tosca_nodes_Root element : nodes) {
			Resource resource = element.eResource();
			String toTest = (resource).getURIFragment(element);

			if (toTest.equals(id)) {
				return element;
			}
		}
		return null;
	}

}