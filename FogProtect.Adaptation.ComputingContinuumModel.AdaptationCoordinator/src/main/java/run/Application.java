package run;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.google.gson.JsonObject;

import restController.ManagementController;
import restController.RiskAssessmentController;
import runtime.LoggingInterface;
import runtime.PersonalLogger;


@SpringBootApplication(scanBasePackages= {"restController", "runtime", "conflictIdentification", "tosca", "restController.testing"})
@EntityScan(basePackages = {"conflictIdentification"})
@EnableJpaRepositories(basePackages = { "conflictIdentification"})
public class Application {
	
	@Autowired
	LoggingInterface loggingInterface;
	@Autowired
	RiskAssessmentController riskAssessmentController;
	@Autowired
	ManagementController managementController;
	@Autowired
	PersonalLogger log;

	@Value("${RiskAssessmentURL}")
	private String riskassessmentURL;
	@Value("${AdaptiveApplicationManagementURL_AdaptationRequest}")
	private String adaptiveApplicationManagementURL_AdaptationRequest;
	@Value("${AdaptiveApplicationManagementURL_CallForAction}")
	private String adaptiveApplicationManagementURL_CallForAction;
	@Value("${AdaptiveApplicationManagementURL_ImmediateAction}")
	private String adaptiveApplicationManagementURL_ImmediateAction;
	@Value("${AdaptiveApplicationManagementURL_AdaptationCollection}")
	private String adaptiveApplicationManagementURL_AdaptationCollection;
	@Value("${AdaptiveApplicationManagementURL_DisallowedAdaptations}")
	private String adaptiveApplicationManagementURL_DisallowedAdaptations;
	@Value("${AdaptiveInfrastructureManagementURL_AdaptationRequest}")
	private String adaptiveInfrastructureManagementURL_AdaptationRequest;
	@Value("${AdaptiveInfrastructureManagementURL_CallForAction}")
	private String adaptiveInfrastructureManagementURL_CallForAction;
	@Value("${AdaptiveInfrastructureManagementURL_ImmediateAction}")
	private String adaptiveInfrastructureManagementURL_ImmediateAction;
	@Value("${AdaptiveInfrastructureManagementURL_AdaptationCollection}")
	private String adaptiveInfrastructureManagementURL_AdaptationCollection;
	@Value("${AdaptiveInfrastructureManagementURL_DisallowedAdaptations}")
	private String adaptiveInfrastructureManagementURL_DisallowedAdaptations;
	@Value("${IncludeAllLogs}")
	private boolean logAll;
	
	private static String className = Application.class.getName();
	
    public static void main(String[] args) {
    	LoggingInterface.setLogToGraylog(false); //True = loggingToGraylogServer
        SpringApplication.run(Application.class, args);
    }
    
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
    	log.setLogAll(logAll);
    	
    	log.info("Spring Boot Started... Setting up environment variables", className);
      
       //RiskAssessment
        if(!riskassessmentURL.equals("null")) {
        	riskAssessmentController.addRiskAssessmentListener(riskassessmentURL);
        }
        
        //Adaptive Application Management
        if(!adaptiveApplicationManagementURL_AdaptationRequest.equals("null") && !adaptiveApplicationManagementURL_CallForAction.equals("null") && !adaptiveApplicationManagementURL_ImmediateAction.equals("null")) {
    		JsonObject head = new JsonObject();
    		head.addProperty("urlAdaptationRequest", adaptiveApplicationManagementURL_AdaptationRequest);
    		head.addProperty("urlCallForAction", adaptiveApplicationManagementURL_CallForAction);
    		head.addProperty("urlImmediateAction", adaptiveApplicationManagementURL_ImmediateAction);
    		head.addProperty("urlAdaptationCollection", adaptiveApplicationManagementURL_AdaptationCollection);
    		head.addProperty("urlDisallowAdaptations", adaptiveApplicationManagementURL_DisallowedAdaptations);
    		head.addProperty("TypeOfManagementComponent", "Application");
        	managementController.addManagementComponent(head.toString());
        }
        
      //Adaptive Infrastructure Management
        if(!adaptiveInfrastructureManagementURL_AdaptationRequest.equals("null") && !adaptiveInfrastructureManagementURL_CallForAction.equals("null") && !adaptiveInfrastructureManagementURL_ImmediateAction.equals("null")) {
    		JsonObject head = new JsonObject();
    		head.addProperty("urlAdaptationRequest", adaptiveInfrastructureManagementURL_AdaptationRequest);
    		head.addProperty("urlCallForAction", adaptiveInfrastructureManagementURL_CallForAction);
    		head.addProperty("urlImmediateAction", adaptiveApplicationManagementURL_ImmediateAction);
    		head.addProperty("urlAdaptationCollection", adaptiveInfrastructureManagementURL_AdaptationCollection);
    		head.addProperty("urlDisallowAdaptations", adaptiveInfrastructureManagementURL_DisallowedAdaptations);
    		head.addProperty("TypeOfManagementComponent", "Infrastructure");
        	managementController.addManagementComponent(head.toString());
        }
        
        log.info("Done!", className);
    }
    
}

