package runtime;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.bind.JsonAdapterAnnotationTypeAdapterFactory;

import ChangeDetector.Changes;
import HelperClasses.Adaptation;

import HelperClasses.ManagementComponentObject;
import HelperClasses.Node;
import HelperClasses.Risk;
import HelperClasses.RiskAssessmentObject;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import restController.ManagementController;
import restController.OperatorApplicationController;
import restController.RiskAssessmentController;
import utility.MatchRepresentation;
import utility.NodeRepresentation;

@Component
public class RuntimeModelLogic {

	@Autowired
	private EMFModelLoad loader;
	@Autowired
	@Lazy
	private RiskAssessmentController riskAssessmentController;
	@Autowired
	@Lazy
	private ManagementController managementController;
	@Autowired
	OperatorApplicationController operatorApplicationController;
	@Autowired
	PersonalLogger log;
	
	private static String className = RuntimeModelLogic.class.getName();

	public RuntimeModelLogic() {
		EPackage.Registry.INSTANCE.getEPackage("www.ComputingContinuumModel.com");
	}

	// Returns the instance for id "i" as a JSON Represenation
	public String returnentiremodelasjson(long i) {
		// Accessing the model information
		String jsonString = "";
		try {
			jsonString = loader.getMapper().writerWithDefaultPrettyPrinter().writeValueAsString(loader.getAsIsModel());
			jsonString = jsonString.replaceAll("#", "");
		} catch (JsonProcessingException e) {
			jsonString = "FAILURE";
			e.printStackTrace();
		}
		log.jsonInfo(jsonString, className);
		return jsonString;
	}

	// Returns a JSON Representation of a .computingcontinuummodel file
	public String returnentiremodelasjsonFromFile(String jsonModelFileName) {
		// Accessing the model information
		String jsonString = "";
		try {
			CloudEnvironment ce = loader.loadJSON(jsonModelFileName);
			jsonString = loader.getMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ce);
			jsonString = jsonString.replaceAll("#", "");
		} catch (JsonProcessingException e) {
			jsonString = "FAILURE";
			e.printStackTrace();
		}
		log.jsonInfo(jsonString, className);
		return jsonString;
	}

	// Returns a JSON Representation of a .computingcontinuummodel file
	public String returnentiremodelasjsonFromFileForJson(String jsonModelFileName) {
		// Accessing the model information
		String jsonString = "";
		try {
			CloudEnvironment ce = loader.loadJSON(jsonModelFileName);
			jsonString = loader.getMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ce);
			jsonString = jsonString.replaceAll("#", "");
			jsonString = jsonString.replaceAll("\\r", "");
			jsonString = jsonString.replaceAll("\\n", "");
		} catch (JsonProcessingException e) {
			jsonString = "FAILURE";
			e.printStackTrace();
		}
		log.jsonInfo(jsonString, className);
		return jsonString;
	}

	// Returns the CloudEnvironment object as a JSON Represenation
	public String returnentiremodelasjson(CloudEnvironment proposal) {
		// Accessing the model information
		String jsonString = "";
		try {
			jsonString = loader.getMapper().writerWithDefaultPrettyPrinter().writeValueAsString(proposal);
			jsonString = jsonString.replaceAll("#", "");
		} catch (JsonProcessingException e) {
			jsonString = "FAILURE";
			e.printStackTrace();
		}
		log.jsonInfo(jsonString, className);
		return jsonString;
	}

	// Post Request to given URL with given String as Body
	public int makeRequest(String url, String requestBody) throws IOException {
//		log.jsonInfo(requestBody, className);
		HttpURLConnection request;
		URL restUrl = new URL(url);
		request = (HttpURLConnection) restUrl.openConnection();
		request.setRequestMethod("POST");
		request.setDoOutput(true);
		Charset encoding = Charset.forName("UTF-8");
		request.setRequestProperty("Content-Type", "application/json");
		request.getOutputStream().write(requestBody.getBytes(encoding));
		log.info("Request sent to " + url, className);
		int responseCode = request.getResponseCode();
		return responseCode;
	}


	public EMFModelLoad getLoader() {
		return loader;
	}

	// Used to find an object by its EMF generated id (@id in json) in a given
	// CloudEnvironment graph
	public EObject searchForObjectInGivenModel(CloudEnvironment cloudEnvironment, String id) {
		EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
		for (tosca_nodes_Root element : nodes) {
			Resource resource = element.eResource();
			String toTest = (resource).getURIFragment(element);
			if (toTest.equals(id)) {
				return element;
			}
		}
		return null;
	}

	public boolean sendRequestToRiskAssessment(ArrayList<Adaptation> adaptations, String taskId) {
		try {
			int returnCode = 0;
			for (RiskAssessmentObject riskAssessmentListener : riskAssessmentController.getRiskassessmentObjects()) {
				String json = createProposedAdaptationJson(loader.getAsIsModel().getModel(), adaptations, taskId);
				json = json.replaceAll("\"@id", "\"atid"); // it is expected that the keys use "at" while the values use "@"
				log.jsonInfo("JSON sent to ITI: " + json, className);
				log.info("Sent request to ThreadDiagnosis: " + riskAssessmentListener.getUrl(), className);
				try {
					log.writeToFile(json);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				returnCode = this.makeRequest(riskAssessmentListener.getUrl(), json);
				log.info("Return Code: " + returnCode, className);
			}
			if (returnCode == 202) {
				// Everything is fine
				return true;
			} else if (returnCode == 400) { // TODO Change Response Codes in order to ITI Interface specification
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error("Could not sent CE to RiskAssessment", className);
			return false;
		}
		return false;
	}

	private String createProposedAdaptationJson(CloudEnvironment asIsModelObject, ArrayList<Adaptation> adaptations, String taskId) {
		JsonObject head = new JsonObject();
		JsonObject asIsModel = new JsonParser().parse(returnentiremodelasjson(asIsModelObject)).getAsJsonObject();
		head.addProperty("SieaTaskId", taskId);
		head.add("AsIsModel", asIsModel);
		JsonArray adaptationProposals = new JsonArray();
		for (Adaptation adaptation : adaptations) {
			int id = (adaptations.indexOf(adaptation) + 1);
			JsonObject adaptationProposal = new JsonObject();
			adaptationProposal.addProperty("ID", id);
			JsonObject adaptationProposalModel = new JsonParser()
					.parse(returnentiremodelasjson(adaptation.getReferenceModel())).getAsJsonObject();
			adaptationProposal.add("AdaptationProposalModel", adaptationProposalModel);
			JsonArray changesMadeToAsIsModel = Changes.compareModels(asIsModelObject, adaptation.getReferenceModel())
					.get("ChangesMadeToAsIsModel").getAsJsonArray();
			adaptationProposal.add("ChangesMadeToAsIsModel", changesMadeToAsIsModel);
			adaptationProposals.add(adaptationProposal);
		}
		head.add("AdaptationProposals", adaptationProposals);
		return head.toString();
	}

	public void sendRequestToManagementComponents(CloudEnvironment ce, int id, JsonArray risks, JsonArray factors, String taskId) {
		for (ManagementComponentObject managementComponentObject : managementController
				.getManagementComponentObjects()) {
			JsonObject head = new JsonObject();
			head.addProperty("ID", id);
			head.addProperty("SieaTaskId", taskId);
			JsonObject model = new JsonParser().parse(returnentiremodelasjson(ce)).getAsJsonObject();
			head.add("AsIsModel", model);
			head.add("Risks", risks);
			head.add("Factors", factors);
			log.info("Sent AdaptationRequest to Management Component", className);
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						int returnCode = makeRequest(managementComponentObject.getUrlAdaptationRequest(),
								head.toString());
						if (returnCode == 400) {
							log.error("Return code for request to ManagementComponent is 400", className);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
			managementController.addPendingAnswer();
		}
	}
	
	public void sendImmediateActionToRequestComponent(CloudEnvironment ce, int id, String managementObjectType, String eventName, JsonArray factors, String taskId) {
		for (ManagementComponentObject managementComponentObject : managementController
				.getManagementComponentObjects()) {
			if(managementComponentObject.getTypeOfManagementComponent().equals(managementObjectType)) {
				JsonObject head = new JsonObject();
				head.addProperty("ID", id);
				head.addProperty("SieaTaskId", taskId);
				JsonObject model = new JsonParser().parse(returnentiremodelasjson(ce)).getAsJsonObject();
				head.add("AsIsModel", model);
				head.addProperty("EventName", eventName);
				head.add("Factors", factors);
				log.info("Sent ImmediateAction to Management Component", className);
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							int returnCode = makeRequest(managementComponentObject.getUrlImmediateAction(),
									head.toString());
							if (returnCode == 400) {
								log.error("Return code of ImmediateAction is 400", className);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				t.start();
			}
		}
	}

	public void sendCallForAction(Adaptation adaptation, int id, String taskId) {
		for (ManagementComponentObject managementComponentObject : managementController
				.getManagementComponentObjects()) {
			if (managementComponentObject.getTypeOfManagementComponent()
					.equals(adaptation.getTypeOfManagementComponent())) {
				JsonObject head = new JsonObject();
				head.addProperty("Name", adaptation.getName());
				head.addProperty("Description", adaptation.getDescription());
				head.addProperty("ID", id);
				head.addProperty("SieaTaskId", taskId);
				JsonObject referenceModelJson = new JsonParser()
						.parse(this.returnentiremodelasjson(adaptation.getReferenceModel())).getAsJsonObject();
				head.add("ReferenceModel", referenceModelJson);

				log.info("Sent CallForAction to Management Component", className);
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							int returnCode = makeRequest(managementComponentObject.getUrlCallForAction(),
									head.toString());
							if (returnCode == 400) {
								log.error("Return code for request to CallForAction is 400", className);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				t.start();
			}
		}

	}
	
	

	public String createStatusInformationJson(String taskId) {
		JsonObject head = new JsonObject();
		JsonObject asIs = new JsonObject();
		JsonArray risks = new JsonArray();
		if (loader.getAsIsModel() != null) {
			JsonObject asIsModel = new JsonParser().parse(returnentiremodelasjson(loader.getAsIsModel().getModel()))
					.getAsJsonObject();
			asIs.add("AsIsModel", asIsModel);
			
			
			if (loader.getRisksOfAsIsModel() != null) {
				for (Risk riskObject : loader.getRisksOfAsIsModel()) {
					JsonObject risk = new JsonObject();
					risk.addProperty("Name", riskObject.getName());
					risk.addProperty("Description", riskObject.getDescription());
					risk.addProperty("ID", riskObject.getId());
					JsonArray nodesForRisk = new JsonArray();
					for (Node nodeObject : riskObject.getNodes()) {
						JsonObject nodeForNodesForRisk = new JsonObject();
						nodeForNodesForRisk.addProperty("Name", nodeObject.getName());
						nodeForNodesForRisk.addProperty("@id", nodeObject.getAtID());
						nodesForRisk.add(nodeForNodesForRisk);
					}
					risk.add("Nodes", nodesForRisk);
					risks.add(risk);
				}
			}			
		} else {
			asIs.add("AsIsModel", null);
		}
		if (loader.getOldAsIsModel() != null) {
			JsonObject oldasIsModel = new JsonParser()
					.parse(returnentiremodelasjson(loader.getOldAsIsModel().getModel())).getAsJsonObject();
			asIs.add("OldAsIsModel", oldasIsModel);
		} else {
			asIs.add("OldAsIsModel", null);
		}

		if (loader.getAsIsModel() != null && loader.getOldAsIsModel() != null) {
			asIs.add("ChangesMadeToAsIsModel",
					Changes.compareModels(loader.getOldAsIsModel().getModel(), loader.getAsIsModel().getModel())
							.get("ChangesMadeToAsIsModel").getAsJsonArray());
		} else {
			asIs.add("ChangesMadeToAsIsModel", new JsonArray());
		}

		asIs.add("Risks", risks);
		
		asIs.addProperty("Risklevel", loader.getRisklevelOfAsIsModel());
		
		if (!taskId.equals("-1")) 
			head.addProperty("SieaTaskId", taskId);
		
		head.add("AsIs", asIs);

		JsonArray adaptations = new JsonArray();
		
		int adaptationID = 0;
		for(Adaptation adaptation : loader.getAdaptations()) {
			JsonObject adaptationJ = new JsonObject();
			
			adaptationJ.addProperty("Name", adaptation.getName());
			
			adaptationJ.addProperty("Description", adaptation.getDescription());
			
			adaptationJ.addProperty("ID", adaptationID);
		
			JsonObject adapModel = new JsonParser().parse(returnentiremodelasjson(loader.getAsIsModel().getModel()))
					.getAsJsonObject();
			adaptationJ.add("ReferenceModel", adapModel);
			
			adaptationJ.addProperty("Risklevel", adaptation.getRiskLevel().toString());
			
			JsonArray adapRisks = new JsonArray();
			
			if (adaptation.getRisks() != null) {
				for (Risk riskObject : adaptation.getRisks()) {
					JsonObject risk = new JsonObject();
					risk.addProperty("Name", riskObject.getName());
					risk.addProperty("Description", riskObject.getDescription());
					risk.addProperty("ID", riskObject.getId());
					JsonArray nodesForRisk = new JsonArray();
					for (Node nodeObject : riskObject.getNodes()) {
						JsonObject nodeForNodesForRisk = new JsonObject();
						nodeForNodesForRisk.addProperty("Name", nodeObject.getName());
						nodeForNodesForRisk.addProperty("@id", nodeObject.getAtID());
						nodesForRisk.add(nodeForNodesForRisk);
					}
					risk.add("Nodes", nodesForRisk);
					adapRisks.add(risk);
				}
			}
			
			adaptationJ.add("Risks", adapRisks);
			
			adaptations.add(adaptationJ);
			adaptationID = adaptationID++;
		}

		head.add("Adaptations", adaptations);
		return head.toString();
	}
	

	public void notifyAllAboutChanges() {

		// taskId -1 will be ignored by StatusInformation creation
		String json = createStatusInformationJson("-1");

		operatorApplicationController.broadcastStatusInformation(json);
	}

	// Deprecated
	// -------------------------------------------------------------------------------------------------------------------------

	// Used to create readable JsonStrings - Not used anymore because mapper can do
	// the same
	public String prettifyJson(String jsonString) {
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);
		return prettyJson;
	}
}
