package runtime;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.emfjson.jackson.annotations.EcoreReferenceInfo;
import org.emfjson.jackson.annotations.EcoreTypeInfo;
import org.emfjson.jackson.module.EMFModule;
import org.emfjson.jackson.resource.JsonResourceFactory;
import org.emfjson.jackson.utils.ValueReader;
import org.emfjson.jackson.utils.ValueWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;

import HelperClasses.Adaptation;
import HelperClasses.Model;
import HelperClasses.Risk;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.impl.ComputingContinuumModelPackageImpl;
import utility.Constants;

@Component
public class EMFModelLoad {
	private ObjectMapper mapper;
	private HenshinResourceSet resourceSet;
	private ArrayList<Adaptation> adaptations = new ArrayList<Adaptation>();
	
	private ArrayList<Risk> risksOfAsIsModel = new ArrayList<Risk>();
	private String risklevelOfAsIsModel = "Unknown";
	
	@Value("${KnowledgebaseasismodelURL}")
	private String knowledgebaseasismodel;
	
	@Value("${KnowledgebaseoldasismodelURL}")
	private String knowledgebaseoldasismodel;
	
	
	public HenshinResourceSet getResourceSet() {
		return resourceSet;
	}

	public void setResourceSet(HenshinResourceSet resourceSet) {
		this.resourceSet = resourceSet;
	}

	// Prepares ResourceSet and Mapper and initializes attributes
	public EMFModelLoad() {
		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.RUNTIME_MODEL_PATH);
		resourceSet.getPackageRegistry().put(ComputingContinuumModelPackageImpl.eNS_URI, ComputingContinuumModelPackageImpl.eINSTANCE);
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("computingcontinuummodel", new XMIResourceFactoryImpl());

		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

		EMFModule module = new EMFModule();
		module.configure(EMFModule.Feature.OPTION_USE_ID, true);
		module.configure(EMFModule.Feature.OPTION_SERIALIZE_DEFAULT_VALUE, true);
		ValueReader<String, EClass> valueReader = new ValueReader<String, EClass>() {
			public EClass readValue(String value, DeserializationContext context) {
				return (EClass) ComputingContinuumModelPackageImpl.eINSTANCE.getEClassifier(value);
			}
		};
		ValueWriter<EClass, String> valueWriter = new ValueWriter<EClass, String>() {

			public String writeValue(EClass value, SerializerProvider context) {
				//  Auto-generated method stub
				return value.getName();
			}
		};
		module.setTypeInfo(new EcoreTypeInfo("type", valueReader, valueWriter));
		module.setReferenceInfo(new EcoreReferenceInfo("referencedObjectID"));

		mapper.registerModule(module);
		JsonResourceFactory factory = new JsonResourceFactory(mapper);
		mapper = factory.getMapper();

		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("json", factory);

		this.mapper = mapper;
		this.resourceSet = resourceSet;
	}

	// load JSON from file and return CloudEnvironment object
	public CloudEnvironment loadJSON(String jsonModelFileName) {
		EGraph graph = new EGraphImpl(resourceSet.getResource(jsonModelFileName));
		CloudEnvironment cloudEnvironment = (CloudEnvironment) graph.getRoots().get(0);
		return cloudEnvironment;
	}

	// Used to create an EObject out of a JSON Representation - needs to fit with
	// our meta model
	public EObject loadEObjectFromString(String model) throws IOException {
		JsonResourceFactory factory = new JsonResourceFactory(mapper);
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, factory);
		resourceSet.getPackageRegistry().put(ComputingContinuumModelPackageImpl.eNS_URI, ComputingContinuumModelPackageImpl.eINSTANCE);
		Resource resource = resourceSet.createResource(URI.createURI("computingcontinuummodel"));
		InputStream stream = new ByteArrayInputStream(model.getBytes(StandardCharsets.UTF_8));
		resource.load(stream, null);
		stream.close();
		return resource.getContents().get(0);
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The computingcontinuummodel file, generated by this method, does not contain any
	// relations.
	// Please check this on your end as well.

	// Saves an EObject (root of a Tree) as a File (represented in XMI Format)
	public void saveEObjectToXMI(EObject toSave, long id) {
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("computingcontinuummodel",
				new XMIResourceFactoryImpl());
		resourceSet.saveEObject(toSave, "ServiceInstance" + id + ".computingcontinuummodel");
		;
	}

	public void saveEObjectToXMI(EObject toSave, String name) {
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("computingcontinuummodel",
				new XMIResourceFactoryImpl());
		resourceSet.saveEObject(toSave, name + ".computingcontinuummodel");
		;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public ObjectMapper getMapper() {
		return mapper;
	}

	public void setMapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public ArrayList<Adaptation> getAdaptations() {
		return adaptations;
	}

	public Model getAsIsModel() {
		String asis = makeGetRequest(knowledgebaseasismodel);
		try {
			Model model = new Model((CloudEnvironment) this.loadEObjectFromString(asis), new ArrayList<Risk>());
			return model;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}


	public Model getOldAsIsModel() {
		String oldasis = makeGetRequest(knowledgebaseoldasismodel);
		try {
			Model model = new Model((CloudEnvironment) this.loadEObjectFromString(oldasis), new ArrayList<Risk>());
			return model;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}	
	
	
	public String makeGetRequest(String url) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(url, String.class);	
	}

	public ArrayList<Risk> getRisksOfAsIsModel() {
		return risksOfAsIsModel;
	}

	public void removeRisks () {
		this.risksOfAsIsModel.clear();
	}

	public String getRisklevelOfAsIsModel() {
		return risklevelOfAsIsModel;
	}

	public void setRisklevelOfAsIsModel(String risklevelOfAsIsModel) {
		this.risklevelOfAsIsModel = risklevelOfAsIsModel;
	}

	
	
	
}
