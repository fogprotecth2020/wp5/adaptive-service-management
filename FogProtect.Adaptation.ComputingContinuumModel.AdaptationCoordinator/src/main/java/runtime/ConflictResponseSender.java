package runtime;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Set;

import com.google.common.io.Files;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import HelperClasses.DisallowedRule;

public class ConflictResponseSender {
	
	private final String JSON_PATH = new File(System.getProperty("user.dir")).getAbsolutePath()
			+ File.separator + "conflictIdentification" + File.separator + "response.json";
	
	private PersonalLogger log;
	private String className = ConflictResponseSender.class.getName();
	
	private JsonObject currentResponse; 
	
	public ConflictResponseSender(PersonalLogger log) {
		this.log = log;
		
		// create Folder "conflictIdentification" if not existing
		File folder = new File(JSON_PATH).getParentFile();
		if (folder != null && !folder.exists()) 
			folder.mkdir();
	}

	public void createResponseJson(Map<String, Set<DisallowedRule>> responseAsMap) {
		log.info("Creating Response JSON.", className);
		
		JsonObject jsonResponse = new JsonObject();
		for (String managerType : responseAsMap.keySet()) {
			
			JsonArray rulesOfManager = new JsonArray();
			for (DisallowedRule disallowedRule : responseAsMap.get(managerType)) {
				String completeName = disallowedRule.getCompleteName();
				JsonPrimitive rule = new JsonPrimitive(completeName);
				rulesOfManager.add(rule);
			}
			
			jsonResponse.add(managerType, rulesOfManager);
		}
		
		persistResponse(jsonResponse);	
	}
	
	private void loadResponseFromFile() {
		String responseAsString = "";
		try {
			responseAsString = java.nio.file.Files.readString(new File(JSON_PATH).toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (!responseAsString.equals("")) {
			currentResponse = new JsonParser().parse(responseAsString).getAsJsonObject();
		}
	}
	
	private void persistResponse(JsonObject jsonResponse) {
		this.currentResponse = jsonResponse;
		try {
			Files.write(jsonResponse.toString().getBytes(), new File(JSON_PATH));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean doesPreviousResponseExist() {
		return new File(JSON_PATH).exists();
	}

	public void sendResponseToManager(String managerType, String urlDisallowAdaptations) {
		if (currentResponse == null) {
			loadResponseFromFile();
		}
		
		if (currentResponse != null) {
			if (currentResponse.get(managerType) != null) {
				sendResponseToManager(managerType, urlDisallowAdaptations, 
						currentResponse.get(managerType).getAsJsonArray());
			}
			else {
				log.info("No Disallowed Rules for " + managerType + "Manager.", className);
			}
		}
		
	}
	
	private void sendResponseToManager(String managerType, String url, JsonArray rulesAsJson) {
		log.info("Sending Results to " + managerType + "Manager.", className);
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				HttpURLConnection request;
				
				try {
					request = (HttpURLConnection) new URL(url).openConnection();
					request.setDoOutput(true);
					request.setRequestMethod("POST");
					request.setRequestProperty("Content-Type", "application/json; utf-8");
					
					request.getOutputStream().write(rulesAsJson.toString().getBytes());
					
					if (request.getResponseCode() == HttpURLConnection.HTTP_OK) {
						log.info(managerType + "Manager received Conflict Identification Response.", className);
					}
					else {
						log.error(managerType + "Manager could not be reached.", className);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		});
		t.start();
	}
}
