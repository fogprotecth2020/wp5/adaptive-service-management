package runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import HelperClasses.Adaptation;
import HelperClasses.Node;
import HelperClasses.Risk;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import restController.ManagementController;
import riskpatternfinder.AdaptationFinderToImproveSystem;
import riskpatternfinder.AdaptationFinderToMitigateRisks;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import utility.AdaptationCombinationRepresentation;
import utility.AtomicAdaptation;
import utility.ComparisonFactor;
import utility.Constants;
import utility.MatchRepresentation;
import utility.Model;

@Component
public class RiskFinder {
	@Autowired
	@Lazy
	private RuntimeModelLogic runtimeModelLogic;
	@Autowired
	@Lazy
	ManagementController managementController;
	@Autowired
	PersonalLogger log;
	
	@Value("${AdaptationResultURL}")
	private String adaptationResult;
	
	private ComparisonFactor[] factors = {ComparisonFactor.Risk, ComparisonFactor.Function, ComparisonFactor.Cost, ComparisonFactor.EnergyConsumption, ComparisonFactor.Performance};
	
	private static String className = RiskFinder.class.getName();
	
	public void lookForImprovements(CloudEnvironment ce, int id, String taskId) {
		JsonArray risks = new JsonArray();
		runtimeModelLogic.sendRequestToManagementComponents(ce, id, risks, this.getFactorsAsJsonArray(), taskId);
		log.info("Waiting for adaptations from ManagementComponents...", className);
	}
	
	public void lookForRiskMitigations (CloudEnvironment ce, int id, JsonArray risks, String taskId) {
		runtimeModelLogic.sendRequestToManagementComponents(ce, id, risks, this.getFactorsAsJsonArray(), taskId);
		log.info("Waiting for adaptations from ManagementComponents...", className);
	}
	
	public void immediateAction(String eventName, CloudEnvironment ce, int id, String taskId) {
		if (eventName.equals("DoorOpen")||eventName.equals("DataManipulationDetected")) {
			String managementObjectType = "Application";
			runtimeModelLogic.sendImmediateActionToRequestComponent(ce, id, managementObjectType, eventName, this.getFactorsAsJsonArray(), taskId);
		}else if(eventName.equals("!?")) {
			
		}
	}
	
	private JsonArray getFactorsAsJsonArray() {
		JsonArray factorArray = new JsonArray();
		for (ComparisonFactor factor : factors) {
			factorArray.add(factor.toString());
		}
		return factorArray;
	}
	
	public void setFactors(String[] newFactorsAsString) {
		
		ComparisonFactor[] newFactors = new ComparisonFactor[5];
		for (int i= 0; i < newFactors.length; i++) {
			newFactors[i] = ComparisonFactor.valueOf(newFactorsAsString[i]);
		}
		
		this.factors = newFactors;
	}
	
	public void doSomethingWithGivenAdaptations(String taskId) {
		if(managementController.getAdaptationList().isEmpty()) {
			log.info("No adaptations needed / possible", className);
			try {
				JsonObject head = new JsonObject();
				head.addProperty("SieaTaskId", taskId);
				head.addProperty("result", "AdaptationNotPossible");
				
				//FIXME adaptationResult possibleError
				runtimeModelLogic.makeRequest(adaptationResult, head.toString());
			} catch (IOException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			//TODO Sort by functionality and cost (or something else)
			for(JsonElement adaptationJson : managementController.getAdaptationList()) {
					JsonObject adaptationCasted = adaptationJson.getAsJsonObject();
					try {
						String name = adaptationCasted.get("Name").getAsString();
						String description = adaptationCasted.get("Description").getAsString();
						CloudEnvironment referenceModel = (CloudEnvironment) runtimeModelLogic.getLoader().loadEObjectFromString(adaptationCasted.get("ReferenceModel").getAsJsonObject().toString());
						String typeOfManagementComponent = adaptationCasted.get("TypeOfManagementComponent").getAsString();
						Adaptation adaptation = new Adaptation(referenceModel, name, description, typeOfManagementComponent);
						for (JsonElement risk : adaptationCasted.get("Risks").getAsJsonArray()) {
							ArrayList<Node> nodes = new ArrayList<Node>();
							for (JsonElement node : risk.getAsJsonObject().get("Nodes").getAsJsonArray()) {
								Node nodeObject = new Node(node.getAsJsonObject().get("Name").getAsString(),node.getAsJsonObject().get("@id").getAsString());
								nodes.add(nodeObject);
							}
							int id = adaptation.getRisks().size();
							Risk riskObject = new Risk(risk.getAsJsonObject().get("Name").getAsString(), risk.getAsJsonObject().get("Description").getAsString(), id, nodes);
							adaptation.getRisks().add(riskObject);
						}
						runtimeModelLogic.getLoader().getAdaptations().add(adaptation);
					} catch (IOException e) {
						// Auto-generated catch block
						e.printStackTrace();
					}
			}
			managementController.getAdaptationList().clear();
			log.info("Amount of adaptations: " + runtimeModelLogic.getLoader().getAdaptations().size(), className);
			lookForRisks(runtimeModelLogic.getLoader().getAdaptations(), taskId);
		}
	}

	@SuppressWarnings(value = { "all" })
	public void lookForRisks(ArrayList<Adaptation> adaptations, String taskId) {		
		if(!runtimeModelLogic.sendRequestToRiskAssessment(adaptations, taskId)) {
			//Request failed
			log.error("Request to RiskAssessment failed", className);
			//TODO Do something if failed
		}else {
			log.info("Waiting for answer from RiskAssessment...", className);
		}
	}
}
