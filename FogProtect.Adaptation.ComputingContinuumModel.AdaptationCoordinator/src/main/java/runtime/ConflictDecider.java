package runtime;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import HelperClasses.DisallowedRule;
import HelperClasses.RuleConflict;

public class ConflictDecider {

	private List<RuleConflict> ruleConflicts;
	
	private Map<String, Set<DisallowedRule>> conflicts;
	
	public ConflictDecider(List<RuleConflict> ruleConflicts) {
		this.ruleConflicts = ruleConflicts;
	}
	
	public Map<String, Set<DisallowedRule>> disallowEveryRuleInConflict() {
		conflicts = new HashMap<>();
		
		for (RuleConflict ruleConflict: ruleConflicts) {
			addToConflictResponse(ruleConflict.getNameOfManagerA(), 
					ruleConflict.getFileOfManagerA(), ruleConflict.getRuleOfManagerA());
			addToConflictResponse(ruleConflict.getNameOfManagerB(), 
					ruleConflict.getFileOfManagerB(), ruleConflict.getRuleOfManagerB());
		}
		
		return conflicts;
	}

	private void addToConflictResponse(String managerName, String fileName, String ruleName) {
		DisallowedRule disallowedRule = new DisallowedRule(fileName, ruleName);
		
		Set<DisallowedRule> disallowedRulesOfManager = conflicts.get(managerName);
		if (disallowedRulesOfManager != null) {
			disallowedRulesOfManager.add(disallowedRule);
		}
		else {
			disallowedRulesOfManager = new HashSet<DisallowedRule>();
			disallowedRulesOfManager.add(disallowedRule);
			conflicts.put(managerName, disallowedRulesOfManager);
		}
	}
}
