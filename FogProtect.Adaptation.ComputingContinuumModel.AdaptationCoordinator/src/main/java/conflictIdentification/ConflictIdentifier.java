package conflictIdentification;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Parameter;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResource;
import org.eclipse.emf.henshin.multicda.cpa.CDAOptions;
import org.eclipse.emf.henshin.multicda.cpa.CpaByAGG;
import org.eclipse.emf.henshin.multicda.cpa.UnsupportedRuleException;
import org.eclipse.emf.henshin.multicda.cpa.result.CPAResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import HelperClasses.RuleConflict;
import runtime.PersonalLogger;

@Service
public class ConflictIdentifier {
	
	private Map<String, List<Rule>> rulesOfEachManager = new LinkedHashMap<>();
	
	private List<RuleConflict> ruleConflicts = new ArrayList<>();

	@Autowired
	PersonalLogger log;
	@Autowired
	CirRepository cirRepository;
	
	private String className = ConflictIdentifier.class.getName();
	
	
	//FIXME how to handle JavaScriptRules?
	// removes JavaScript rules and adds them to the disallowed rules.
	// taken from org.eclipse.emf.henshin.multicda.cpa.InputDataChecker
	private List<Rule> removeRulesWithJavaScriptExpressions(List<Rule> rules) {
		List<Rule> nonJSRules = new ArrayList<>();	
		
		ruleCheck:
		for (Rule rule: rules) {
			ArrayList<Graph> lhsAndRhsGraph = new ArrayList<>() {
				{
					add(rule.getLhs());
					add(rule.getRhs());
				}
			};

			for (Graph graph : lhsAndRhsGraph) {
				for (Node node : graph.getNodes()) {
					for (Attribute attribute : node.getAttributes()) {
						// handling JavaScript expressions
						Parameter param = rule.getParameter(attribute.getValue());
						if (attribute.getType().getEType() == EcorePackage.eINSTANCE.getEString()
								&& attribute.getConstant() == null && param == null) {
							log.warn("Rule with JavaScript Expression identified. Rule " + rule.getName() + " will be exluced from CPA and disallowed.", className);
//							addToRuleConflicts(rule);
							continue ruleCheck;
						}
					}
				}
			}
			nonJSRules.add(rule);
		}
		
		return nonJSRules;
	}
	
	public void identifyConflicts(boolean returnValue) {
		String[] keys = rulesOfEachManager.keySet().toArray(new String[0]);
		
		if (keys.length <= 1) {
			log.info("No CPA necessary.", className);
		}
		
		
	}
	
	// initialization. given map will be cleaned of rules with java script expressions
	public void init(Map<String, List<Rule>> rulesToUse) {
		this.rulesOfEachManager = rulesToUse;
		
		for (Entry<String, List<Rule>> entry : rulesOfEachManager.entrySet()) {
			List<Rule> rules = removeRulesWithJavaScriptExpressions(entry.getValue());
			// need to override the previous value in the map
			rulesOfEachManager.put(entry.getKey(), rules);
		}
	}
	
	public void identifyConflicts() {
		// pairwise comparison of each manager
		String[] keys = rulesOfEachManager.keySet().toArray(new String[0]);
		
		if (keys.length <= 1) {
			log.info("Less than two managers responded. No Conflict Identification necessary.", className);
		}
		
		for (int i = 0; i < keys.length; i++) {
			String managerA = keys[i];
			List<Rule> rulesOfManagerA = rulesOfEachManager.get(managerA);
			for (int j = i + 1; j < keys.length; j++) {
				String managerB = keys[j];
				List<Rule> rulesOfManagerB = rulesOfEachManager.get(managerB);
				
				log.info("Comparing Adaptations from " + managerA + "Manager and " + managerB + "Manager.", className);
				
				performCpaOfTwoManagers(rulesOfManagerA, rulesOfManagerB);
				
				log.info("Finished Comparison of " + managerA + "Manager and " + managerB + "Manager.", className);
			}
		}
		
		log.info("Finished Conflict Identification for all Managers.", className);
	}
	
	// The CPA of two managers has a lot of console output
	// both the possible CPA and the database requests spam
	private void performCpaOfTwoManagers(List<Rule> rulesOfManagerA, List<Rule> rulesOfManagerB) {
		// currently, each comparison has the same timestamp
		Timestamp timestamp = new Timestamp(System.nanoTime());
		
		// for each rule, the XML-representations are created, for possible storage purposes.
		for (Rule firstRule : rulesOfManagerA) {
			String xmlFileOfRuleA = getFileStringFromRule(firstRule);
			for (Rule secondRule : rulesOfManagerB) {
				String xmlFileOfRuleB = getFileStringFromRule(secondRule);
				
				// as AB and BA are different in a CPA, we look for both in the database
				// a not yet done combination is then used for cpa
				if (!isRulepairAlreadyCompared(firstRule, secondRule)) {
					performCpaOfTwoRules(firstRule, secondRule, xmlFileOfRuleA, xmlFileOfRuleB, timestamp);
				}
				else {
					log.info(firstRule.getName() + " and " + secondRule.getName() + " have already been compared.", className);
				}
						
				if (!isRulepairAlreadyCompared(secondRule, firstRule)) {
					performCpaOfTwoRules(secondRule, firstRule, xmlFileOfRuleB, xmlFileOfRuleA, timestamp);
				}
				else {
					log.info(secondRule.getName() + " and " + firstRule.getName() + " have already been compared.", className);
				}
			}
		}
	}
	
	// Henshin-Rules are not serializable
	// rule names (format: ManagerType-FileName-RuleName) are used
	// then the rules are loaded and compared by EcoreUtil
	private boolean isRulepairAlreadyCompared(Rule firstRule, Rule secondRule) {
		List<ConflictIdentificationResult> previousResults = cirRepository.findAllByNameOfFirstRuleAndNameOfSecondRule(firstRule.getName(), secondRule.getName());
		if (previousResults.size() == 0) 
			return false;
		else {
			for (ConflictIdentificationResult previousResult : previousResults) {
				Rule previousFirstRule = loadRuleFromFileString(previousResult.getFirstRule());
				previousFirstRule.setName(previousResult.getNameOfFirstRule());
				Rule previousSecondRule = loadRuleFromFileString(previousResult.getSecondRule());
				previousSecondRule.setName(previousResult.getNameOfSecondRule());
				
				if (EcoreUtil.equals(firstRule, previousFirstRule) && EcoreUtil.equals(secondRule, previousSecondRule)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	// we compare a single pair of rules to store the results in the database
	private void performCpaOfTwoRules(Rule ruleA, Rule ruleB, String xmlFileOfRuleA, String xmlFileOfRuleB, Timestamp timestamp) {
		CDAOptions options = new CDAOptions();
		options.setComplete(false); 						// only interested in the existence of critical pairs
		options.setReduceSameRuleAndSameMatch(false);		// not sure what it does
		options.setIgnoreSameRules(false);					// 
		
		CpaByAGG cpa = new CpaByAGG();
		CPAResult result = null;
		
		try {
			cpa.init(Collections.singleton(ruleA), Collections.singleton(ruleB), options);
			// important note: Henshin-CPA is not commutative.
			result = cpa.runConflictAnalysis();
		} catch (UnsupportedRuleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (result != null) {
			boolean conflict;
			// if getCriticalPairs has no members, there is no identified conflict between the rules.
			if (result.getCriticalPairs().size() == 0) {
				log.info("No Conflict between " + ruleA.getName() + " and " + ruleB.getName() , className);
				conflict = false;
			}
			else {
				log.info("Conflict between " + ruleA.getName() + " and " + ruleB.getName() , className);
				conflict = true;
			}
			
			// result of CPA is stored in the database
			ConflictIdentificationResult conflictResult = new ConflictIdentificationResult(xmlFileOfRuleA, xmlFileOfRuleB, conflict, timestamp, ruleA.getName(), ruleB.getName());
			cirRepository.save(conflictResult);
		}
		
	}
	
	// creates the XML-file representation of the given rule
	// this includes the module (eContainer) bc
	private String getFileStringFromRule(Rule rule) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Resource resource = new HenshinResource();
		
		resource.getContents().add(rule.eContainer());
		try {
			resource.save(baos, null);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return baos.toString();
	}
	
	// loads a Henshin rule from the parameter
	// parameter needs to be a valid XML-file in string format
	private Rule loadRuleFromFileString(String fileString) {
		Resource resource = new HenshinResource();
		InputStream stream = new ByteArrayInputStream(fileString.getBytes());
		
		try {
			resource.load(stream, null);
			stream.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		
		return ((Module) resource.getContents().get(0)).getAllRules().get(0);
	}

}



