package conflictIdentification;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class ConflictIdentificationResult {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column
	private long id;
	
	private String nameOfFirstRule;
	private String nameOfSecondRule;
	
	private String firstRule;
	private String secondRule;
	
	private boolean conflict;
	private Timestamp timestamp;
	
	public ConflictIdentificationResult() { }
	
	public ConflictIdentificationResult(String firstRule, String secondRule, boolean conflict, Timestamp timestamp, String nameOfFirstRule, String nameOfSecondRule) {
		this.nameOfFirstRule = nameOfFirstRule;
		this.nameOfSecondRule = nameOfSecondRule;
		this.conflict = conflict;
		this.timestamp = timestamp;
		this.firstRule = firstRule;
		this.secondRule = secondRule;
	}

	public String getNameOfFirstRule() {
		return nameOfFirstRule;
	}

	public void setNameOfFirstRule(String nameOfFirstRule) {
		this.nameOfFirstRule = nameOfFirstRule;
	}

	public String getNameOfSecondRule() {
		return nameOfSecondRule;
	}

	public void setNameOfRuleB(String nameOfSecondRule) {
		this.nameOfSecondRule = nameOfSecondRule;
	}

	public String getFirstRule() {
		return firstRule;
	}

	public void setFirstRule(String firstRule) {
		this.firstRule = firstRule;
	}

	public String getSecondRule() {
		return secondRule;
	}

	public void setRuleB(String secondRule) {
		this.secondRule = secondRule;
	}

	public boolean isConflict() {
		return conflict;
	}

	public void setConflict(boolean conflict) {
		this.conflict = conflict;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
}
