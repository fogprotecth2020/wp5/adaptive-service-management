package conflictIdentification;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResource;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import runtime.PersonalLogger;

public class AdaptationCollector{
	
	private List<Thread> adaptationCollectorThreads = new ArrayList<Thread>();
	private Charset encoding = Charset.forName("UTF-8");
	
	private PersonalLogger log;
	private String className = AdaptationCollector.class.getName();
	
	private Map<String, Map<String, String>> rules = new ConcurrentHashMap<>();
	
	public AdaptationCollector(PersonalLogger log) {
		this.log = log;
		log.info("Starting Adaptation Collection from available Managers.", className);
	}
	
	public void requestAdaptationRulesFromManager(String managerType, String url) {
		// new Thread for each manager, as time for response is unknown.
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				HttpURLConnection request;
				
				try {
					request = (HttpURLConnection) new URL(url).openConnection();
					request.setRequestProperty("Content-Type", "application/json");
					
					if (request.getResponseCode() == HttpURLConnection.HTTP_OK) {
						receiveAdaptationRules(managerType, request.getInputStream());
					}
				}
				catch (IOException e) {
//					e.printStackTrace();
					log.error("Problems Requesting from " + managerType + "Manager.", className);
				}
			}
		});
		
		adaptationCollectorThreads.add(t);
		t.start();
	}
	
	private synchronized void receiveAdaptationRules(String managerType, InputStream inputStream) {
		JsonArray response = new JsonParser().parse(new InputStreamReader(inputStream, encoding)).getAsJsonArray();
		log.info("Received Adaptation Rules from " + managerType + "Manager.", className);
		
		// add each received adaptation for comparison
		for (JsonElement arrayElement : response) {
			JsonObject arrayObject = arrayElement.getAsJsonObject();
			String fileName = arrayObject.get("Name").getAsString();
			String rulesAsXml = arrayObject.get("File").getAsString();
			addToRules(managerType, fileName, rulesAsXml);
		}
	}
	
	private void addToRules(String managerType, String fileName, String rulesAsString) {
		if (!rules.containsKey(managerType)) {
			Map<String, String> rule = new HashMap<>();
			rule.put(fileName, rulesAsString);
			rules.put(managerType, rule);
		}
		else {
			rules.get(managerType).put(fileName, rulesAsString);
		}
	}
	
	public Map<String, List<Rule>> getResultOfAdaptationCollection() {	
		// need to wait until Managers responded (or failed)
		for (Thread t: adaptationCollectorThreads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
				
		log.info("Collection of Adaptation Rules finished.", className);
		
		return prepareResultsOfAdaptationCollection();
	}
	
	// when the adaptations are received from the managers, they are saved in a string format
	// now we load the rules from the strings, and separate the rules (if more than one rule is in a henshin file)
	// then the rules are put in a map, where the key is the name/type of the manager.
	private Map<String, List<Rule>> prepareResultsOfAdaptationCollection() {
		Map<String, List<Rule>> rulesOfEachManager = new HashMap<>();
		
		for (String managerType : rules.keySet()) {
			List<Rule> rulesOfManager = new ArrayList<>();
			
			for (Entry<String, String> entry : rules.get(managerType).entrySet()) {
				String fileName = entry.getKey().replace(".henshin", "");
				String ruleOfFileAsString = entry.getValue();
				
				Module moduleOfFile = loadModuleFromString(ruleOfFileAsString);
				
				List<Rule> rulesFromFile = separateRules(moduleOfFile, managerType, fileName);
				
				rulesOfManager.addAll(rulesFromFile);
			}
			rulesOfEachManager.put(managerType, rulesOfManager);
		}
		
		return rulesOfEachManager;
	}
	
	// given the module we loaded from the filestring, we separate each possible rule into its own module
	private List<Rule> separateRules(Module module, String managerName, String fileName) {
		Rule[] rules = (Rule[]) module.getUnits().toArray(new Rule[0]);
		
		List<Rule> separatedRules = new ArrayList<>();
		
		for (Rule rule : rules) {
			// both the activation and the "complete" name are important, to identify unchanged rules later
			rule.setActivated(true);
			rule.setName(managerName + "-" + fileName + "-" + rule.getName());
			
			Module copyModule = (Module) EcoreUtil.copy(rule.eContainer());
			copyModule.getUnits().clear();
			copyModule.getUnits().add(rule);
			
			separatedRules.add((Rule) copyModule.getUnits().get(0));
		}
		
		return separatedRules;
	}
	
	// loads the module of a henshin file
	private Module loadModuleFromString(String rulesAsString){
		Resource resource = new HenshinResource();
		InputStream stream = new ByteArrayInputStream(rulesAsString.getBytes());
		
		try {
			resource.load(stream, null);
			stream.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return(Module) resource.getContents().get(0);
	}
	
}
























