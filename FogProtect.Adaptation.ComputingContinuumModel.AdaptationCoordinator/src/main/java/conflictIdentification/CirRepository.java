package conflictIdentification;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CirRepository extends JpaRepository<ConflictIdentificationResult, Long> {
	
	List<ConflictIdentificationResult> findAllByNameOfFirstRuleAndNameOfSecondRule(String nameOfFirstRule, String nameOfSecondRule);

}