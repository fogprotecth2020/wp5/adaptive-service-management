package com.modelvisuals.modelvisuals.Logic;



import com.modelvisuals.modelvisuals.ModelvisualsApplication;
import com.modelvisuals.modelvisuals.diagram.Diagram;
import com.modelvisuals.modelvisuals.diagram.MyEdge;
import com.modelvisuals.modelvisuals.diagram.MyNode;
import com.modelvisuals.modelvisuals.diagram.MyRisk;
import com.modelvisuals.modelvisuals.utils.ChangeEdges;
import com.modelvisuals.modelvisuals.utils.Colors;
import com.modelvisuals.modelvisuals.utils.EdgeCases;
import com.modelvisuals.modelvisuals.utils.NodeCases;

import java.util.List;

public class PlantUmlStringBuilder {

    public String buildWithRisks(Diagram diagram, List<MyRisk> risks) {

    	int diagramId = diagram.getId();
    	
        if (risks == null) {
            return buildNoRisks(diagramId);

        }
        if (risks.size() == 0) {
            return buildNoRisks(diagramId);
        }
        String plantUmlString = initPlantString();
        

        for (MyNode node : ModelvisualsApplication.nodes) {
            if (node.getDiagramIds().contains(diagramId)) {



                MyRisk foundRisk = riskIncludedInBothLists(node.getIsPartOfRisks(), risks);

                if (foundRisk != null) {



                    String colorCode = foundRisk.getColorCode();
                    plantUmlString += getNodeWithColorCodeString(node, diagramId, colorCode) + "\n";
                    foundRisk = null;
                } else {
                    plantUmlString += getNormalNodeString(node, diagramId) + "\n";
                }
            } else {
                plantUmlString += getHiddenNodeString(node) + "\n";
            }
        }

        for (MyEdge edge : ModelvisualsApplication.edges) {
//            if (edge.getIsPartOfDiagrams().contains(diagram)) {
        	if (edge.getDiagramIds().contains(diagramId)) {
                plantUmlString += getNormalEdgeString(edge, EdgeCases.NO_CHANGE);
            } else {
                plantUmlString += getHiddenEdgeString(edge);
            }
        }


        plantUmlString += "@enduml";
        return plantUmlString;
    }

    public String buildNoRisks(int diagramId) {
        String plantUmlString = initPlantString();

        for (MyNode node : ModelvisualsApplication.nodes) {
            if (node.getDiagramIds().contains(diagramId)) {

                plantUmlString += getNormalNodeString(node, diagramId) + "\n";
            } else {
                plantUmlString += getHiddenNodeString(node) + "\n";
            }
        }

        for (MyEdge edge : ModelvisualsApplication.edges) {
//            if (edge.getIsPartOfDiagrams().contains(diagram)) {
        	if (edge.getDiagramIds().contains(diagramId)) {
                plantUmlString += getNormalEdgeString(edge, EdgeCases.NO_CHANGE);
            } else {
                plantUmlString += getHiddenEdgeString(edge);
            }
        }
        plantUmlString += "@enduml";

        return plantUmlString;
    }

    public String buildComparison(Diagram before, Diagram after) {

        if(before == null || after == null){
            return "cant compare diagrams because one diagram is null";
        }
        int beforeId = before.getId();
        int afterId = after.getId();

        String plantUmlString = initPlantString();

        //nodes

        //cases changed, added, deleted

        for (MyNode node : ModelvisualsApplication.nodes) {

            NodeCases option = getNodeCase(before, after, node);

            switch (option) {
                case NO_CHANGE:
                    plantUmlString += getNormalNodeString(node, beforeId) + "\n";
                    break;
                case CHANGED:
//                    plantUmlString += getNodeWithColorCodeString(node, after, Colors.CHANGED) + "\n";
                	plantUmlString += getNodeWithChangedAttributes(node, beforeId, afterId) + "\n";
                    break;
                case ADDED:
                    plantUmlString += getNodeWithColorCodeString(node, afterId, Colors.ADDED) + "\n";
                    break;
                case DELETED:
                    plantUmlString += getNodeWithColorCodeString(node, beforeId, Colors.DELETED) + "\n";
                    break;
                case NOT_IN_EITHER:
                    plantUmlString += getHiddenNodeString(node) + "\n";
                    break;
            }

        }

        //edges
        for (MyEdge edge : ModelvisualsApplication.edges){

            EdgeCases option = getEdgeCase(before, after, edge);

            switch (option){
                case NO_CHANGE:
                    plantUmlString +=  getNormalEdgeString(edge, EdgeCases.NO_CHANGE)+"\n";
                    break;
                case ADDED:
                    plantUmlString += getNormalEdgeString(edge, EdgeCases.ADDED) + "\n";
                    break;
                case DELETED:
                    plantUmlString += getNormalEdgeString(edge, EdgeCases.DELETED) + "\n";
                    break;
                case NOT_IN_EITHER:
                    plantUmlString += getHiddenEdgeString(edge) + "\n";
                    break;
            }
        }

        //cases added, deleted


        plantUmlString += "@enduml";
        return plantUmlString;
    }


    // ----helper---------------------------------------------------------------------------------------
    private String getNormalNodeString(MyNode node, int diagramId) {

        //rectangle "TEXT"  <<Hidden>> as id #Color
        String text = "rectangle " + "\"" + node.getNodeDescriptionAsString(diagramId) + "\"" + " " + "as " + node.getId();
        return text;
    }

    private String getHiddenNodeString(MyNode node) {

        //rectangle "TEXT"  <<Hidden>> as id #Color
        String text = "rectangle " + "\"" + node.tryToFindSomeDescrString() + "\"" + " " + "<<Hidden>> " + "as " + node.getId();
        return text;
    }

    private String getNodeWithColorCodeString(MyNode node, int diagramId, String colorCode) {

        //rectangle "TEXT"  <<Hidden>> as id #Color
        String text = "rectangle " + "\"" + node.getNodeDescriptionAsString(diagramId) + "\"" + " " + "as " + node.getId() + " " + colorCode;
        return text;
    }
    
    private String getNodeWithChangedAttributes(MyNode node, int beforeId, int afterId) {
    	String color = Colors.CHANGED;
    	
    	String text = "rectangle \"" + node.getComparisonNodeDescriptionAsString(beforeId, afterId) + "\" as " + node.getId() + " " + color;
    	
    	return text;
    }

    private String getNormalEdgeString(MyEdge edge, EdgeCases changeOption) {
        String text = "";
        if (!(edge.getSmallerId() == 0 && edge.getBiggerId() == 0)) {

            String colorCode = Colors.BLACK;
            String changeEdge = ChangeEdges.NO_CHANGE;

            EdgeCases option = changeOption;

            switch (option) {
                case NO_CHANGE:
                    colorCode = Colors.BLACK;
                    changeEdge = ChangeEdges.NO_CHANGE;
                    break;
                case DELETED:
                    colorCode = Colors.DELETED;
                    changeEdge = ChangeEdges.CHANGED;
                    break;
                case ADDED:
                    colorCode = Colors.ADDED;
                    changeEdge = ChangeEdges.CHANGED;
                    break;
            }

            text = edge.getSmallerId() + " " + changeEdge + " " + edge.getBiggerId() + " " + colorCode + "\n";

        } else {
            System.out.println("edge String could not be created at PlantumlStringBuilder.getEdgeString()");
        }

        return text;
    }

    private String getHiddenEdgeString(MyEdge edge) {
        String text = "";
        if (!(edge.getSmallerId() == 0 && edge.getBiggerId() == 0)) {
            text = edge.getSmallerId() + " " + "-[hidden]-" + " " + edge.getBiggerId() + " " + Colors.WHITE + "\n";
          //  text = edge.getSmallerId() + " " + "---" + " " + edge.getBiggerId() + " " + "#Blue"+ "\n";

        } else {
            System.out.println("edge String could not be created at PlantumlStringBuilder.getHiddenEdgeString()");
        }
        return text;
    }


    private String initPlantString() {
        String plantUmlString = "@startuml\n";
        
        // including sprites
        plantUmlString += "!includeurl <office/Databases/database.puml>\n";
        plantUmlString += "!includeurl <office/Users/user.puml>\n";
        plantUmlString += "!includeurl <office/Concepts/folder.puml>\n";
        plantUmlString += "!includeurl <office/Devices/workstation.puml>\n";
        plantUmlString += "!includeurl <material/arrow_right.puml>\n";
        plantUmlString += "!includeurl <material/cloud.puml>\n";
        plantUmlString += "!includeurl <material/folder_multiple.puml>\n";
        plantUmlString += "!includeurl <material/application.puml>\n";
        plantUmlString += "!includeurl <material/circle_outline.puml>\n";
        plantUmlString += "!includeurl <material/blur.puml>\n";
        
        plantUmlString += "scale 2/7\n";
        plantUmlString += "skinparam linetype ortho\n";
        plantUmlString += "skinparam nodesep 50\n";
        plantUmlString += "skinparam ranksep 50\n";

        //plantUmlString += "left to right direction\n";
        plantUmlString += "hide stereotype\n";

        //define style of rectangles
        plantUmlString += "skinparam rectangle{\n";

        //normal rectangles
        plantUmlString += "shadowing false\n";
        plantUmlString += "BackgroundColor #White\n";
        plantUmlString += "FontColor #Black\n";
        plantUmlString += "BorderColor #Black \n";

        //hidden rectangles
        plantUmlString += "shadowing<<Hidden>> false\n";
        plantUmlString += "BackgroundColor<<Hidden>> #White\n";
        plantUmlString += "BorderColor<<Hidden>> #White\n";
        plantUmlString += "FontColor<<Hidden>> #White\n";

        plantUmlString += "}\n";

        return plantUmlString;
    }

    private MyRisk riskIncludedInBothLists(List<MyRisk> list1, List<MyRisk> list2) {

        MyRisk risk = null;

        for (MyRisk risk1 : list1) {
            for (MyRisk risk2 : list2) {
                if (risk1.equals(risk2)) {
                    risk = risk1;
                }
            }
        }
        return risk;
    }


    private NodeCases getNodeCase(Diagram diaBefore, Diagram diaAfter, MyNode node) {
        NodeCases option = NodeCases.NO_CHANGE;

        //node in both diagrams -> maybe a change in description
        if (node.getDiagramIds().contains(diaBefore.getId()) && node.getDiagramIds().contains(diaAfter.getId())) {
            String descBefore = node.getNodeDescriptionAsString(diaBefore.getId());
            String descAfter = node.getNodeDescriptionAsString(diaAfter.getId());
            if (!descBefore.equals(descAfter)) {
                option = NodeCases.CHANGED;
            }
        }

        //node not included in adaption -> deleted
//        else if (node.getIsPartOfDiagrams().contains(diaBefore) && !node.getIsPartOfDiagrams().contains(diaAfter)) {
        else if (node.getDiagramIds().contains(diaBefore.getId()) && !node.getDiagramIds().contains(diaAfter.getId())) {
            option = NodeCases.DELETED;
        }

        //node not included before adaption -> added
//        else if (!node.getIsPartOfDiagrams().contains(diaBefore) && node.getIsPartOfDiagrams().contains(diaAfter)) {
        else if (!node.getDiagramIds().contains(diaBefore.getId()) && node.getDiagramIds().contains(diaAfter.getId())) {
            option = NodeCases.ADDED;
        }
//        else if (!node.getIsPartOfDiagrams().contains(diaBefore) && !node.getIsPartOfDiagrams().contains(diaAfter)) {
        else if (!node.getDiagramIds().contains(diaBefore.getId()) && !node.getDiagramIds().contains(diaAfter.getId())) {
            option = NodeCases.NOT_IN_EITHER;
        }

        return option;
    }

    private EdgeCases getEdgeCase(Diagram before, Diagram after, MyEdge edge){
        EdgeCases option = EdgeCases.NO_CHANGE;


//        if(!edge.getIsPartOfDiagrams().contains(before) && !edge.getIsPartOfDiagrams().contains(after)){
        if (!edge.getDiagramIds().contains(before.getId()) && !edge.getDiagramIds().contains(after.getId())) {
            option= EdgeCases.NOT_IN_EITHER;
        }
//        else if(edge.getIsPartOfDiagrams().contains(before) && edge.getIsPartOfDiagrams().contains(after)){
        else if (edge.getDiagramIds().contains(before.getId()) && edge.getDiagramIds().contains(after.getId())) {
            option= EdgeCases.NO_CHANGE;
        }
//        else if(edge.getIsPartOfDiagrams().contains(before) && !edge.getIsPartOfDiagrams().contains(after)){
        else if (edge.getDiagramIds().contains(before.getId()) && !edge.getDiagramIds().contains(after.getId())) {
            option= EdgeCases.DELETED;
        }
//        else if(!edge.getIsPartOfDiagrams().contains(before) && edge.getIsPartOfDiagrams().contains(after)){
        else if (!edge.getDiagramIds().contains(before.getId()) && edge.getDiagramIds().contains(after.getId())) {
            option= EdgeCases.ADDED;
        }

        return option;
    }

}
