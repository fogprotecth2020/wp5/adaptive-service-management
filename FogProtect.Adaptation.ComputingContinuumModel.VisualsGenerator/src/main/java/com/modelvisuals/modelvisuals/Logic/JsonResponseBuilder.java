package com.modelvisuals.modelvisuals.Logic;


import com.modelvisuals.modelvisuals.ModelvisualsApplication;
import com.modelvisuals.modelvisuals.api.JsonResponse;
import com.modelvisuals.modelvisuals.diagram.Diagram;
import com.modelvisuals.modelvisuals.diagram.MyRisk;
import com.modelvisuals.modelvisuals.utils.ModelType;
import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

public class JsonResponseBuilder {

    public List<JsonResponse> getJsonForAllDiagrams(){
        List<JsonResponse> responses = ModelvisualsApplication.diagrams.stream()
                .map(diagram -> diagramToResponse(diagram, new ArrayList<>()))
                .collect(Collectors.toList());
        return responses;
    }

    public JsonResponse getJsonForSingleDiagram(Diagram diagram, List<String> risks){


        List<Integer> riskIds = risks.stream()
                .map(x ->  Integer.parseInt(x))
                .collect(Collectors.toList());


        List<MyRisk> risk1 = diagram.getRisks().stream()
                .filter(myRisk -> {
                   // String adjusted = myRisk.getName().toLowerCase().replaceAll("\\s+", "");
                   // return adjustedStrings.contains(adjusted);
                  return  riskIds.contains(myRisk.getId());
                })
                .collect(Collectors.toList());

        risk1 = risk1.stream().distinct().collect(Collectors.toList());






        return diagramToResponse(diagram, risk1);
    }

    public JsonResponse getJsonForSingleDiagram(Diagram diagram, boolean getAllRisks){
        if (getAllRisks) {
            return diagramToResponse(diagram, diagram.getRisks());
        } else {
            return diagramToResponse(diagram, new ArrayList<>());
        }
    }

    public JsonResponse getJsonForComparison(Diagram before, Diagram after){
        return diagramToResponse(before,after);
    }

    //-----------------actual building stuff----------------
    //1. build PlantUMLString 2. generate Svg from PlantString 3. encode Svg
   //gets Diagram with some set of risks
    private  JsonResponse diagramToResponse(Diagram diagram,  List<MyRisk> risks){

        String plantString = new PlantUmlStringBuilder().buildWithRisks(diagram, risks);
        String encoded = "";
        SourceStringReader reader = new SourceStringReader(plantString);
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        // Write the first image to "os"
        try {
            String desc = reader.generateImage(os, new FileFormatOption(FileFormat.SVG));
            final String svg = new String(os.toByteArray(), Charset.forName("UTF-8"));
            os.close();
            encoded = Base64.getEncoder().encodeToString(svg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JsonResponse(diagram.getName(), diagram.getModelType(), diagram.getId(), encoded, risks);
    }
    
    // comparisonDiagram has an ID of -3 per convention
    private  JsonResponse diagramToResponse(Diagram dia1, Diagram dia2){

        String plantString = new PlantUmlStringBuilder().buildComparison(dia1, dia2);
        String encoded = "";
        SourceStringReader reader = new SourceStringReader(plantString);
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        // Write the first image to "os"
        try {
            String desc = reader.generateImage(os, new FileFormatOption(FileFormat.SVG));
            final String svg = new String(os.toByteArray(), Charset.forName("UTF-8"));
            os.close();
            encoded = Base64.getEncoder().encodeToString(svg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JsonResponse("Comparison_"+dia1.getName()+"_"+dia2.getName(), ModelType.COMPARISON, -3, encoded, null);
    }
    

}
