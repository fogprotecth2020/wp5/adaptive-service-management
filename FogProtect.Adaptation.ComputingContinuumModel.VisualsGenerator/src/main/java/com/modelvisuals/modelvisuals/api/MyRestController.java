package com.modelvisuals.modelvisuals.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class MyRestController {
    private final RestService restService;

    @Autowired
    public MyRestController(RestService restService) {
        this.restService = restService;
    }

    // Definition:
    // modelnames refer to adaptationIds
    // convention: -1 refers to AsIsModel, -2 to OldAsIsModel
    
    //------------------Get Mapping ----------------------------------------------------------------------------------

    //get AsIsModel
    @GetMapping(path = "models/asismodel")
    public ResponseEntity<List<JsonResponse>> getAsIsDiagram() {
        return new ResponseEntity<List<JsonResponse>>(restService.getNoRisksForModel("-1"), HttpStatus.OK);
    }

    //get OldAsIsModel
    @GetMapping(path = "models/oldasismodel")
    public ResponseEntity<List<JsonResponse>> getOldAsIsDiagram() {
        return new ResponseEntity<List<JsonResponse>>(restService.getNoRisksForModel("-2"), HttpStatus.OK);
    }

    //get single Adaptation based on name
    //currently not implemented
    @GetMapping(path = "models/adaptation")
    public ResponseEntity<List<JsonResponse>> getOneAdaptation(
            @RequestParam(value = "adaptationname=", defaultValue = "0") String adaptationname) {
        return new ResponseEntity<List<JsonResponse>>(restService.getNoRisksForModel(adaptationname), HttpStatus.OK);
    }

    //get all risks for one model
    @GetMapping(path = "models/risks/all")
    public ResponseEntity<List<JsonResponse>> getAllRisksForModel(
            @RequestParam(value = "modelname", defaultValue = "-1") String modelname) {
    	
    	modelname = restService.checkString(modelname);
        return new ResponseEntity<List<JsonResponse>>(restService.getAllRisksForModel(modelname), HttpStatus.OK);
    }

    //get only selected risks for model
    @GetMapping(path = "models/risks/select")
    public ResponseEntity<List<JsonResponse>> getSelectedRisksForModel(
            @RequestParam(value = "modelname", defaultValue = "-1") String modelname,
            @RequestParam(value = "risknames", defaultValue = "0") String risks) {

    	modelname = restService.checkString(modelname);
    	
        List<String>  riskStrings = List.of(risks.split(","));

        return new ResponseEntity<List<JsonResponse>>(restService.getSelectedRisksForModel(modelname, riskStrings), HttpStatus.OK);
    }

    //get Comparison between two models
    @GetMapping(path = "models/comparison")
    public ResponseEntity<List<JsonResponse>> getComparison(
            @RequestParam(value = "before", defaultValue = "-1") String before,
            @RequestParam(value = "after", defaultValue = "0") String after) {
    	after = restService.checkString(after);
    	before = restService.checkString(before);
        return new ResponseEntity<List<JsonResponse>>(restService.getComparison(before, after), HttpStatus.OK);
    }


    //-----------------Post Mapping ------------------

    @PostMapping(path = "models",
            consumes = {MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<JsonResponse>> setDiagrams(@RequestBody String jsonString) {
        return new ResponseEntity<List<JsonResponse>>(restService.setDiagrams(jsonString), HttpStatus.OK);
    }
}
