package com.modelvisuals.modelvisuals.api;


import com.modelvisuals.modelvisuals.Logic.JsonResponseBuilder;
import com.modelvisuals.modelvisuals.ModelvisualsApplication;
import com.modelvisuals.modelvisuals.diagram.Diagram;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RestService {
    public List<JsonResponse> getNoRisksForModel(String modelname) {
        Diagram diagram = findDiagramById(modelname);
        return List.of( new JsonResponseBuilder().getJsonForSingleDiagram(diagram, false));
    }

    public List<JsonResponse> getAllRisksForModel(String modelname) {
        Diagram diagram = findDiagramById(modelname);
        return List.of( new JsonResponseBuilder().getJsonForSingleDiagram(diagram, true));
    }

    public List<JsonResponse> getSelectedRisksForModel(String modelname, List<String> riskStrings) {
    	Diagram diagram = findDiagramById(modelname);
        return List.of( new JsonResponseBuilder().getJsonForSingleDiagram(diagram, riskStrings));
    }

    public List<JsonResponse> getComparison(String before, String after) {
        Diagram beforeDia = findDiagramById(before);
        Diagram afterDia = findDiagramById(after);

        if(beforeDia == null){
            System.out.println("cant build Comparison as "+before+" is null");
            return new JsonResponseBuilder().getJsonForAllDiagrams();
        }
        if (afterDia == null){
            System.out.println("cant build Comparison as "+after+" is null");
            return new JsonResponseBuilder().getJsonForAllDiagrams();
        }
        return List.of(new JsonResponseBuilder().getJsonForComparison(beforeDia, afterDia));
    }

    public List<JsonResponse> setDiagrams(String jsonString) {
        new JsonPostRequestProcessing(jsonString).updateAll();


        return new JsonResponseBuilder().getJsonForAllDiagrams();
    }


    //--------------helper Methods---------------------

    private Diagram findDiagramByName(String name) {
        List<Diagram> diagramList = ModelvisualsApplication.diagrams.stream().filter(diagram -> {

            String diaName = diagram.getName();
            diaName = diaName.toLowerCase().replaceAll("\\s+", "");//remove Whitespaces and to LowerCase

            String name1 = name.toLowerCase().replaceAll("\\s+", "");

            return diaName.equals(name1);

        }).collect(Collectors.toList());


        if (diagramList.size() != 1) {
            System.out.println("diagramsList too small or too big in DiagramService.findDiagramByName" + "   searched Diagramname: " + name);
            return null;
        }
        return diagramList.get(0);
    }
    
    private Diagram findDiagramById(String name) {
    	int id = Integer.parseInt(name);
    	Diagram diagram = ModelvisualsApplication.diagrams.stream().filter(d -> d.getId() == id).findFirst().get();
    	if (diagram == null) {
    		System.out.println("Cannot find corresponding diagram");
    	}
    			
    	return diagram;
    }
    
    public String checkString(String s) {
    	if (s.equals("AsIsModel")) {
    		s = "-1";
    	}
    	else if (s.equals("OldAsIsModel")) {
    		s = "-2";
    	}
    	return s;
    }

}
