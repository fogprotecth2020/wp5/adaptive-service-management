package com.modelvisuals.modelvisuals.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.modelvisuals.modelvisuals.diagram.MyRisk;
import com.modelvisuals.modelvisuals.utils.ModelType;

import java.util.List;

public class JsonResponse {
    private String modelName;
    private String encodedSvg;
    private ModelType modelType;
    private int id;
    private List<MyRisk> risks;


    public JsonResponse(@JsonProperty("name") String modelName,
                      @JsonProperty("modeltype") ModelType modelType,
                      @JsonProperty("id") int id,
                      @JsonProperty("encodedSvg") String encodedSvg,
                      @JsonProperty("printedRisks") List<MyRisk> risks) {
        this.modelName = modelName;
        this.encodedSvg = encodedSvg;
        this.modelType = modelType;
        this.id = id;
        this.risks = risks;
    }

    public String getModelName() {
        return modelName;
    }

    public String getEncodedSvg() {
        return encodedSvg;
    }

    public ModelType getModelType() {
        return modelType;
    }

    public List<MyRisk> getRisks() {
        return risks;
    }
    
    public int getId() {
    	return id;
    }
}
