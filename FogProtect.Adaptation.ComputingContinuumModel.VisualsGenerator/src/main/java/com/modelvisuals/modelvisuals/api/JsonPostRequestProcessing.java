package com.modelvisuals.modelvisuals.api;


import com.modelvisuals.modelvisuals.ModelvisualsApplication;
import com.modelvisuals.modelvisuals.diagram.Diagram;
import com.modelvisuals.modelvisuals.diagram.MyEdge;
import com.modelvisuals.modelvisuals.diagram.MyNode;
import com.modelvisuals.modelvisuals.diagram.MyRisk;
import com.modelvisuals.modelvisuals.utils.ModelType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class JsonPostRequestProcessing {

    String jsonString;
    private final int Min_Color_Diff = 15;



    public JsonPostRequestProcessing(String jsonString) {
        this.jsonString = jsonString;
    }

    public void updateAll (){

        ModelvisualsApplication.edges.clear();
        ModelvisualsApplication.nodes.clear();
        ModelvisualsApplication.diagrams.clear();

        JSONObject fullJson = new JSONObject(jsonString);

        Object asIsObj =  fullJson.get("AsIs");

        if(asIsObj != null && asIsObj instanceof JSONObject) {

            //current as is Model
            Object asIsModel = ((JSONObject) asIsObj).get("AsIsModel");
            if (asIsModel != null && asIsModel instanceof JSONObject) {
                JSONArray asIsRisks = (JSONArray) ((JSONObject) fullJson.get("AsIs")).get("Risks");
                Diagram asIsDiagram = new Diagram("AsIsModel", ModelType.AS_IS_MODEL, -1);
                ModelvisualsApplication.diagrams.add(asIsDiagram);
                setAllForModel((JSONObject) asIsModel, asIsRisks, asIsDiagram);
            }else {
                System.out.println("asIsModel is null in JsonPostRequestProcessing.updateAll()");
            }
            //oldAsIsModel

            Object oldAsIsModel =  ((JSONObject) asIsObj).get("OldAsIsModel");
            if (oldAsIsModel != null && oldAsIsModel instanceof JSONObject) {
            	JSONArray oldAsIsRisks = new JSONArray();
            	Diagram oldAsIsDiagram = new Diagram("OldAsIsModel", ModelType.OLD_AS_IS_MODEL, -2);
                ModelvisualsApplication.diagrams.add(oldAsIsDiagram);
                setAllForModel((JSONObject) oldAsIsModel, oldAsIsRisks, oldAsIsDiagram);
            }else{
                System.out.println("oldAsIsModel is null in JsonPostRequestProcessing.updateAll()");

            }
        }
        //adaptations

        Object adaptObj =  fullJson.get("Adaptations");
        if(adaptObj != null && adaptObj instanceof JSONArray) {
            JSONArray adaptations = (JSONArray) adaptObj;
            //iterate adaptations
            for (int i = 0; i < adaptations.length(); i++) {
                JSONObject adaptation = adaptations.getJSONObject(i);
                String name = adaptation.getString("Name");
                int id = adaptation.getInt("ID");
                JSONObject refModel = adaptation.getJSONObject("ReferenceModel");
                JSONArray adaptRisks = adaptation.getJSONArray("Risks");
                Diagram adaptDiagram = new Diagram(name, ModelType.ADAPTATION, id);
                ModelvisualsApplication.diagrams.add(adaptDiagram);
                setAllForModel(refModel, adaptRisks, adaptDiagram);
            }
        }else {
            System.out.println("adaptations is null in JsonPostRequestProcessing.updateAll()");
        }

        ModelvisualsApplication.diagrams.forEach(x -> System.out.println("stored: "+ x.getName() + 
        		" with internal ID : " + x.getId()));


    }

    //-----------helper Methods---------------

    //sets nodesList, edgesList and risks
    private void setAllForModel(JSONObject model, JSONArray risks, Diagram diagram){

        List<int[]> usedRiskColors = new ArrayList<>();

        JSONArray jArray = model.getJSONArray("tosca_nodes_root");

        for (int i = 0; i<jArray.length(); i++){
            JSONObject obj = (JSONObject) jArray.get(i);

            MyNode nodeToConsider = new MyNode(obj.getString("@id"), obj.getString("name"));

            //if node exists, add descr etc to it rather than create a new one
            for(MyNode node : ModelvisualsApplication.nodes){
                if(node.equals(nodeToConsider)){
                    nodeToConsider=node;
                }
            }

            
            nodeToConsider.addToDiagramIds(diagram.getId());

            if(!ModelvisualsApplication.nodes.contains(nodeToConsider)){
                ModelvisualsApplication.nodes.add(nodeToConsider);
            }
            
            // iterate all keys, special treatment  for key=="@id" and relations
            for (String keyStr: obj.keySet()){

                if (keyStr.equals("@id") || keyStr.equals("id")) {
                }
                else if (obj.get(keyStr) instanceof JSONArray){
                    JSONArray jArr = obj.getJSONArray(keyStr);

                    for (int j = 0; j < jArr.length(); j++) {

                        String currentId = nodeToConsider.getFullId();
                        String referencedId = ((JSONObject) jArr.get(j)).getString("referencedObjectID");
                        String currentName = nodeToConsider.getName();
                        String referencedName = findReferencedNameById(referencedId, jArray);

                        if (!referencedName.equals("")) {
                            MyEdge edge = new MyEdge(currentId, referencedId, currentName, referencedName);
                            for(MyEdge e : ModelvisualsApplication.edges){
                                if(edge.equals(e)){
                                    edge=e;
                                }
                            }


                            edge.addToDiagramIds(diagram.getId());

                            if(!ModelvisualsApplication.edges.contains(edge)){
                                ModelvisualsApplication.edges.add(edge);
                            }

                        }
                    }
                }
                else if(obj.get(keyStr) instanceof JSONObject){

                    String currentId = nodeToConsider.getFullId();
                    String referencedId = ((JSONObject) obj.get(keyStr)).getString("referencedObjectID");
                    String currentName = nodeToConsider.getName();
                    String referencedName = findReferencedNameById(referencedId, jArray);

                    if (!referencedName.equals("")) {
                        MyEdge edge = new MyEdge(currentId, referencedId, currentName, referencedName);
                        for(MyEdge e : ModelvisualsApplication.edges){
                            if(edge.equals(e)){
                                edge=e;
                            }
                        }

                        edge.addToDiagramIds(diagram.getId());

                        if(!ModelvisualsApplication.edges.contains(edge)){
                            ModelvisualsApplication.edges.add(edge);
                        }
                    }
                }
                else {
                    nodeToConsider.addLineToDescription(diagram.getId(),keyStr + ": " + String.valueOf(obj.get(keyStr)));
                }
            }
        }

        //--------------set risks --------------------

        for (int i = 0; i<risks.length(); i++){
            JSONObject currRisk = (JSONObject) risks.get(i);

            String riskName = currRisk.getString("Name");
            int riskId = currRisk.getInt("ID");
            String description = currRisk.getString("Description");
            JSONArray riskNodes = currRisk.getJSONArray("Nodes");

            int[] rgb = findDecentColor(usedRiskColors);
            usedRiskColors.add(rgb);

            MyRisk risk = new MyRisk(riskId, riskName,description, rgb, diagram.getId());

            for(int j=0 ; j<riskNodes.length();j++){
                JSONObject currRiskNode = (JSONObject) riskNodes.get(j);

                String fullId = currRiskNode.getString("@id");
                String nodeName = currRiskNode.getString("Name");
                MyNode node = new MyNode(fullId, nodeName);

                if(!ModelvisualsApplication.nodes.contains(node)){
                    System.out.println("smth went wrong in JsonPostRequestProcessing.setAllForModel while setting risks: stored nodeList doesnt contain node from RiskList");
                }

                for (MyNode n :ModelvisualsApplication.nodes){
                    if(n.equals(node)){
                        node =n;
                    }
                }
                
                node.addToIsPartOfRisks(risk);

            }
            diagram.addRiskToDiagram(risk);
        }
    }

//-----------------helper-----------------------------
    private String findReferencedNameById(String referencedId, JSONArray jArray) {
        String referencedName = ""; // name must be found from root

        for (int z = 0; z < jArray.length(); z++) {
            JSONObject obj1 = (JSONObject) jArray.get(z);
            if (obj1.getString("@id").equals(referencedId)) {
                referencedName = obj1.getString("name");
                break;
            }
        }

        return referencedName;
    }


    private int[] findDecentColor(List<int[]> alrdUsedColors) {

        int [] rgb = new int[]{ 255, 255, 255};
        // create object of Random class
        Random obj = new Random();
        int red = 255, green = 255, blue = 255;

        boolean isAcceptedColor = false;
        while (!isAcceptedColor) {
            red = 0 + obj.nextInt(255);
            green = 0 + obj.nextInt(255);
            blue = 0 + obj.nextInt(255);

            rgb[0] = red;
            rgb[1] = green;
            rgb[2] = blue;

            int z = (299 * red + 587 * green + 114 * blue) / 1000;
            if (z > 175  && isNotTooSimilarToOtherRiskColor(rgb, alrdUsedColors )) {
                isAcceptedColor = true;
            }
        }

        return rgb;
    }


    private boolean isNotTooSimilarToOtherRiskColor(int[] rgb, List<int[]> usedColors ) {

        boolean isNotTooSimilar = true;

        List<Integer> allDiffsAsPercentages = usedColors.
                stream()
                .map(color -> calcColorDiffInPercentage(color, rgb))
                .collect(Collectors.toList());

        List<Integer> tooSimilars = allDiffsAsPercentages.stream()
                .filter(diff -> diff < Min_Color_Diff)
                .collect(Collectors.toList());

        if (tooSimilars.size() > 0) {
            isNotTooSimilar = false;
        }

        return isNotTooSimilar;
    }

    private int calcColorDiffInPercentage(int[] rgb1, int[] rgb2) {

        int diffRed = Math.abs(rgb1[0] - rgb2[0]);
        int diffGreen = Math.abs(rgb1[1] - rgb2[1]);
        int diffBlue = Math.abs(rgb1[2] - rgb2[2]);

        float pctDiffRed = (float) diffRed / 255;
        float pctDiffGreen = (float) diffGreen / 255;
        float pctDiffBlue = (float) diffBlue / 255;

        int percentage = (int) ((pctDiffRed + pctDiffGreen + pctDiffBlue) / 3 * 100);

        return percentage;
    }
}
