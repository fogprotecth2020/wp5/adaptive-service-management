package com.modelvisuals.modelvisuals;

import com.modelvisuals.modelvisuals.diagram.Diagram;
import com.modelvisuals.modelvisuals.diagram.MyEdge;
import com.modelvisuals.modelvisuals.diagram.MyNode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ModelvisualsApplication {

	public static List<Diagram> diagrams = new ArrayList<>();
	public static List<MyNode> nodes = new ArrayList<>();
	public static List<MyEdge> edges = new ArrayList<>();


	public static void main(String[] args) {
		SpringApplication.run(ModelvisualsApplication.class, args);

	}

}
