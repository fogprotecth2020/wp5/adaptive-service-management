package com.modelvisuals.modelvisuals.utils;

public enum EdgeCases {
    NO_CHANGE, ADDED, DELETED, NOT_IN_EITHER
}
