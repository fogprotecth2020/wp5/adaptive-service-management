package com.modelvisuals.modelvisuals.utils;

public class Colors {
    public static final String WHITE = "#White";
    public static final String BLACK = "#Black";
    public static final String CHANGED = "#F1C40F";
    public static final String ADDED = "#20F92A";
    public static final String DELETED = "#F3534A";
}
