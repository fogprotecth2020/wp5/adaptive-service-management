package com.modelvisuals.modelvisuals.utils;

public class ChangeEdges {
    public static final String NO_CHANGE = "--";    //solid line
    public static final String CHANGED = "..";        //dashed line
}
