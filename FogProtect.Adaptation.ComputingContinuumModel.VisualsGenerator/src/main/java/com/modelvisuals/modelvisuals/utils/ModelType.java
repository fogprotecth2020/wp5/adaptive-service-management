package com.modelvisuals.modelvisuals.utils;

public enum ModelType {
    AS_IS_MODEL, OLD_AS_IS_MODEL, ADAPTATION, COMPARISON
}
