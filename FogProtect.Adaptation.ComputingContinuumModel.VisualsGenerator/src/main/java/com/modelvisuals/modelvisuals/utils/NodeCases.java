package com.modelvisuals.modelvisuals.utils;

public enum NodeCases {
    NO_CHANGE, ADDED, DELETED, CHANGED, NOT_IN_EITHER
}
