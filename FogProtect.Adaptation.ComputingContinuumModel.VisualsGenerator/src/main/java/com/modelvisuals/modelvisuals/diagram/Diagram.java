package com.modelvisuals.modelvisuals.diagram;


import com.modelvisuals.modelvisuals.utils.ModelType;

import java.util.ArrayList;
import java.util.List;

public class Diagram {

    String name;
    int id;
    ModelType modelType;
    List<MyRisk> risks = new ArrayList<>();

    public Diagram(String name, ModelType modelType, int id) {
        this.name = name;
        this.modelType = modelType;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Diagram diagram = (Diagram) o;
        return this.id == diagram.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelType getModelType() {
        return modelType;
    }

    public void setModelType(ModelType modelType) {
        this.modelType = modelType;
    }

    public List<MyRisk> getRisks() {
        return risks;
    }

    public void setRisks(List<MyRisk> risks) {
        this.risks = risks;
    }
    
    public void addRiskToDiagram(MyRisk risk) {
    	this.risks.add(risk);
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
}
