package com.modelvisuals.modelvisuals.diagram;

import java.util.ArrayList;
import java.util.List;

public class MyEdge {
    String smallerName;
    String biggerName;

    int smallerId;
    int biggerId;

    List<Diagram> isPartOfDiagrams = new ArrayList<>();
//    List<MyRisk> isPartOfRisks = new ArrayList<>();
    List<Integer> diagramIds = new ArrayList<>();

    // Constructor that sets smallerID, biggerId automatically
    public MyEdge(String id1, String id2, String name1, String name2) {

        String[] s1 = id1.split("tosca_nodes_root.");
        String[] s2 = id2.split("tosca_nodes_root.");

        int var1 = Integer.valueOf(s1[1]);
        int var2 = Integer.valueOf(s2[1]);

        if (var1 < var2) { // id1 < id2 -> keep order
            smallerId = var1;
            smallerName = name1;
            biggerId = var2;
            biggerName = name2;
        } else { // id1 > id2 -> swap
            smallerId = var2;
            smallerName = name2;
            biggerId = var1;
            biggerName = name1;
        }
    }

    // --------------override------------------------
    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof MyEdge) {

            // compare by ID
//             retVal = ((MyEdge) v).getSmallerId() == this.getSmallerId() && ((MyEdge)
//             v).getBiggerId() == this.getBiggerId();

            // compare by Name
            retVal = ((MyEdge) v).getSmallerName().trim().toLowerCase().equals(this.getSmallerName().trim().toLowerCase())
                    && ((MyEdge) v).getBiggerName().trim().toLowerCase().equals(this.getBiggerName().trim().toLowerCase());

        }
        return retVal;
    }

    //---------------------getters/setters-----------

    public String getSmallerName() {
        return smallerName;
    }

    public void setSmallerName(String smallerName) {
        this.smallerName = smallerName;
    }

    public String getBiggerName() {
        return biggerName;
    }

    public void setBiggerName(String biggerName) {
        this.biggerName = biggerName;
    }

    public int getSmallerId() {
        return smallerId;
    }

    public void setSmallerId(int smallerId) {
        this.smallerId = smallerId;
    }

    public int getBiggerId() {
        return biggerId;
    }

    public void setBiggerId(int biggerId) {
        this.biggerId = biggerId;
    }

    public List<Diagram> getIsPartOfDiagrams() {
        return isPartOfDiagrams;
    }

    public void setIsPartOfDiagrams(List<Diagram> isPartOfDiagrams) {
        this.isPartOfDiagrams = isPartOfDiagrams;
    }

//    public List<MyRisk> getIsPartOfRisks() {
//        return isPartOfRisks;
//    }

//    public void setIsPartOfRisks(List<MyRisk> isPartOfRisks) {
//        this.isPartOfRisks = isPartOfRisks;
//    }
    
    public void addToDiagramIds(int diagramId) {
    	this.diagramIds.add(diagramId);
    }
    
    public List<Integer> getDiagramIds() {
    	return this.diagramIds;
    }
    
    public void setDiagramIds(List<Integer> diagramIds) {
    	this.diagramIds = diagramIds;
    }
}
