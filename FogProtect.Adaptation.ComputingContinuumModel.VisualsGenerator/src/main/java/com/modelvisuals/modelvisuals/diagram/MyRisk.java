package com.modelvisuals.modelvisuals.diagram;

import java.util.Objects;

public class MyRisk {
    int id;
    String name;
    String Description;
    String colorCode;
    int diagramId;
    private int[] rgb = new int[3]; //[red, green, blue]

    public MyRisk(int id, String name, String description,  int[] rgb, int diagramId) {
        this.name = name;
        this.id= id;
        Description = description;

        this.rgb[0] = rgb[0];
        this.rgb[1] = rgb[1];
        this.rgb[2] = rgb[2];
        
        this.diagramId = diagramId;

        colorCode = String.format("#%02x%02x%02x", this.rgb[0], this.rgb[1], this.rgb[2]);
    }

    //-----override------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyRisk myRisk = (MyRisk) o;

       // String name1 = this.name.toLowerCase().replaceAll("\\s+", "");
        //String name2 = myRisk.getName().toLowerCase().replaceAll("\\s+", "");
        int id1 = this.id;
        int id2 = myRisk.getId();
        int diagramId1 = this.diagramId;
        int diagramId2 = myRisk.getDiagramId();

        return id1 == id2 && diagramId1 == diagramId2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    //------getter-----------------------------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public int[] getRgb() {
        return rgb;
    }

    public void setRgb(int[] rgb) {
        this.rgb = rgb;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void setDiagramId(int diagramId) {
    	this.diagramId = diagramId;
    }
    
    public int getDiagramId() {
    	return this.diagramId;
    }
}
