package com.modelvisuals.modelvisuals.diagram;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class MyNode {

    String name;
    String fullId;
    int id;

    HashMap<Integer, ArrayList<String>> nodeDescription = new HashMap<>();
    List<Diagram> isPartOfDiagrams = new ArrayList<>();
    List<MyRisk> isPartOfRisks = new ArrayList<>();
    List<Integer> diagramIds = new ArrayList<>();

    public MyNode(String fullId, String name) {
        this.name = name;
        this.fullId = fullId;
        String[] s = fullId.split("tosca_nodes_root.");
        this.id = Integer.valueOf(s[1]);
    }

    // ---------helper Methods----------------------------------------------------


    public void addLineToDescription(int diagramId, String line) {


        // order: type > name > rest
        if(nodeDescription.get(diagramId)==null ){
            nodeDescription.put(diagramId, new ArrayList<>());
            nodeDescription.get(diagramId).add(0, "@id: "+ fullId);
            nodeDescription.get(diagramId).add(1, "someType");
            nodeDescription.get(diagramId).add(2, "someName");

            //nodeDescription.get(diagram).add(line);
           // addLineToDescription(diagram, line);
        }

        if (line.contains("type")) {
            nodeDescription.get(diagramId).set(1, line);
            
            String sprite = this.findPossibleSprite(line);
            if (!sprite.equals("")) {
            	nodeDescription.get(diagramId).add(0, sprite);
            }
        } else if (line.contains("name")) {
            nodeDescription.get(diagramId).set(2, line);
        } else {
            nodeDescription.get(diagramId).add(line);
        }
    }

   
    // check typeString if it matches any class from Meta-Model
	private String findPossibleSprite(String line) {

		if (line.contains("DataFlow")) 
			return "<$ma_arrow_right{scale=0.9}>";
			
		
		if (line.contains("SoftwareComponent") || line.contains("DBMS") || line.contains("WebServer"))
			return "<$ma_application{scale=1.2}>";
		
		if (line.contains("Database")) 
			return "<$database{scale=0.85}>";		
		
		if (line.contains("Record")) 
			return "<$folder{scale=0.9}>";
				
		
		if (line.contains("FogCompute"))
			return "<$ma_blur{scale=1.1}>"; 
		
		if (line.contains("CloudCompute"))
			return "<$ma_cloud{scale=0.85}>";
		
		if (line.contains("DataSet")) 
			return "<$ma_folder_multiple{scale=0.9}>";
		
		
		if (line.contains("IoTDevice") || line.contains("Compute")) 
			return "<$workstation{scale=1.05}>";
				
		if (line.contains("WebApplication")) 
			return "<&browser{scale=4.5}>";

		if (line.contains("PrivateSpace")) 
			return "<$ma_circle_outline{scale=1.0}>";
		
				
		// warning: Developer, User, Operator as catch all developers
		if (line.contains("DataSubject") || line.contains("DataController") || line.contains("DataProducer") || line.contains("DataProcessor") 
				|| line.contains("User") || line.contains("Developer") || line.contains("Operator") ) 
			return "<$user{scale=1.0}>";
    	
		// Ignored Classes from Meta-Model:
		// Network_Network, Network_Port, Gateway  
		// Container_Application, Container_Runtime
		// LoadBalancer, Temp
		
		// if none fit, return empty String -> no Sprite
    	return "";
	}

	public String getNodeDescriptionAsString(int diagramId) {
        String text = "";
        for (String s : nodeDescription.get(diagramId)) {
            text += s + "\\n";
        }
        return text;
    }
	
	public String getComparisonNodeDescriptionAsString(int beforeId, int afterId) {
		String text = "";
		for (int i= 0, j=0; i < nodeDescription.get(beforeId).size() && j < nodeDescription.get(afterId).size(); i++, j++ ) {
			String before = nodeDescription.get(beforeId).get(i);
			String after = nodeDescription.get(afterId).get(j);
			if (before.equals(after)) {
				text += before + "\\n";
			}
			else {
				
				// for Red - <color:red> </color:red>
				text += "<b>" + before + " <$ma_arrow_right{scale=0.35}> " + StringUtils.difference(before, after) + "</b> " + "\\n";
			}
		}
		return text;
	}

    public String tryToFindSomeDescrString(){
  
        for (int id : diagramIds) {
        	if (nodeDescription.get(id) != null) {
        		return getNodeDescriptionAsString(id);
        	}
        }
        return "";
    }
    //----------------------override------------------------------------------
    @Override
    public boolean equals(Object v) {
        boolean retVal = false;
        if (v instanceof MyNode) {
            // compare by Name
            retVal = ((MyNode) v).getName().equals(this.getName());
        }
        return retVal;
    }

    //---------------getters/setters ------------------------------
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullId() {
        return fullId;
    }

    public void setFullId(String fullId) {
        this.fullId = fullId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<Integer, ArrayList<String>> getNodeDescription() {
        return nodeDescription;
    }

    public void setNodeDescription(HashMap<Integer, ArrayList<String>> nodeDescription) {
        this.nodeDescription = nodeDescription;
    }

    public List<Diagram> getIsPartOfDiagrams() {
        return isPartOfDiagrams;
    }

    public void setIsPartOfDiagrams(List<Diagram> isPartOfDiagrams) {
        this.isPartOfDiagrams = isPartOfDiagrams;
    }

    public List<MyRisk> getIsPartOfRisks() {
        return isPartOfRisks;
    }

    public void setIsPartOfRisks(List<MyRisk> isPartOfRisks) {
        this.isPartOfRisks = isPartOfRisks;
    }
    
    public void addToIsPartOfRisks(MyRisk risk) {
    	this.isPartOfRisks.add(risk);
    }
    
    public List<Integer> getDiagramIds() {
    	return this.diagramIds;
    }
    
    public void setDiagramIds(List<Integer> diagramIds) {
    	this.diagramIds = diagramIds;
    }
    
    public void addToDiagramIds(int diagramId) {
    	this.diagramIds.add(diagramId);
    }
}
