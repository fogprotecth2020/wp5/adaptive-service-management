package FogProtect.knowledgeBaseAsIsModel.cdo;

import FogProtect.knowledgeBaseAsIsModel.emfCompare.CompareClient;
import FogProtect.knowledgeBaseAsIsModel.utils.ModelLoader;
import FogProtect.knowledgeBaseAsIsModel.utils.Modeltype;
import FogProtect.knowledgeBaseAsIsModel.utils.PersonalLogger;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.eresource.CDOResourceNode;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CdoClient {

	String REPO_NAME = "computingcontinuummodel";
	String DATABASE_NAME = "asIsModels";
	String DATABASE_USER = "test";
	String DATABASE_PASS = "test";

	Repository repo;
	CDOTransaction transaction;
	
	@Autowired
	PersonalLogger log;
	
	private CloudEnvironment mostRecentModel;
	private CloudEnvironment secondMostRecentModel;

	private static String className = CdoClient.class.getName();

	/*
	 * @Autowired ModelLoader loader;
	 */

	public CdoClient() {
		this.startUp();
	}

	public void storeModel(CloudEnvironment e, String type) {
		if(mostRecentModel != null) {
			secondMostRecentModel = mostRecentModel;
		}
		mostRecentModel = e;
		log.info("Model stored", className);

		//CDO is broken
//		try {
//			Timestamp ts = new Timestamp(System.currentTimeMillis());
//
//			String identifier = "/computingcontinuummodel/" + type + "_" + ts;
//
//			CDOResource cdoRes = transaction.getOrCreateResource(identifier);
//			cdoRes.getContents().clear();
//			cdoRes.getContents().add(workaroundStoring(e));
//			
//			transaction.commit();
//
//			log.info("stored: " + cdoRes.getName(), className);
//			
//		} catch (CommitException e1) {
//			e1.printStackTrace();
//		}
	}
	
	public void storeModelWithoutUpdate(CloudEnvironment e, String type) {
		mostRecentModel = e;
		log.info("Model stored without updating secondMostRecent Model", className);
	}

	public EObject retrieveMostRecentModel(String type) {
		return mostRecentModel;
//		try {
//			CDOResourceFolder folder = transaction.getResourceFolder("computingcontinuummodel");
//			List<CDOResourceNode> allNodes = folder.getNodes();
//
//			// List<CDOResourceNode> allNodes = List.of(transaction.getElements());
//
//			List<CDOResourceNode> nodesOfSelectedType = allNodes.stream().filter(node -> node.getName().contains(type))
//					.collect(Collectors.toList());
//
//			CDOResourceNode mostRecentNode = getMostRecentNodeFromList(nodesOfSelectedType);
//			CDOResource cdoRes = transaction.getResource("/computingcontinuummodel/" + mostRecentNode.getName());
//			EList<EObject> objects = cdoRes.getContents();
//
//			/*
//			 * 
//			 * AwesomeClass aw = new AwesomeClass(); aw.EMFModelLoad(); String s =
//			 * aw.modelToJsonString((CloudEnvironment)workaroundRetrieving(objects.get(0)));
//			 * 
//			 * FileWriter writer; try { writer = new FileWriter("E:/emfComp/.json");
//			 * writer.write(s); writer.close(); } catch (IOException e) { // Auto-generated
//			 * catch block e.printStackTrace(); }
//			 */
//
//			return workaroundRetrieving(objects.get(0));
//		} catch (IndexOutOfBoundsException e) {
//			return null;
//		}
	}

	public EObject retrieveSecondRecentModel(String type) {
		return secondMostRecentModel;
//		try {
//			CDOResourceFolder folder = transaction.getResourceFolder("computingcontinuummodel");
//			List<CDOResourceNode> allNodes = folder.getNodes();
//
//			// List<CDOResourceNode> allNodes = List.of(transaction.getElements());
//
//			List<CDOResourceNode> nodesOfSelectedType = allNodes.stream().filter(node -> node.getName().contains(type))
//					.collect(Collectors.toList());
//
//			CDOResourceNode mostRecentNode = getSecondMostRecentNodeFromList(nodesOfSelectedType);
//			CDOResource cdoRes = transaction.getResource("/computingcontinuummodel/" + mostRecentNode.getName());
//			EList<EObject> objects = cdoRes.getContents();
//
//			/*
//			 * 
//			 * AwesomeClass aw = new AwesomeClass(); aw.EMFModelLoad(); String s =
//			 * aw.modelToJsonString((CloudEnvironment)workaroundRetrieving(objects.get(0)));
//			 * 
//			 * FileWriter writer; try { writer = new FileWriter("E:/emfComp/.json");
//			 * writer.write(s); writer.close(); } catch (IOException e) { // Auto-generated
//			 * catch block e.printStackTrace(); }
//			 */
//
//			return workaroundRetrieving(objects.get(0));
//		} catch (IndexOutOfBoundsException e) {
//			return null;
//		}
	}

	//
	public List<Diff> getDiffs() {
		return null;
	}

	public List<Diff> getMostRecentDiffs(String type) {
//		CDOResourceFolder folder = transaction.getResourceFolder("computingcontinuummodel");
//		List<CDOResourceNode> allNodes = folder.getNodes();
//
//		List<CDOResourceNode> nodesOfSelectedType = allNodes.stream().filter(node -> node.getName().contains(type))
//				.collect(Collectors.toList());
//
//		CDOResourceNode mostRecentNode = getMostRecentNodeFromList(nodesOfSelectedType);
//		nodesOfSelectedType.remove(mostRecentNode);
//		CDOResourceNode secondMostRecentNode = getMostRecentNodeFromList(nodesOfSelectedType);
//
//		CDOResource mostRecentRes = transaction.getResource("/computingcontinuummodel/" + mostRecentNode.getName());
//		EList<EObject> objects = mostRecentRes.getContents();
//		EObject mostRecentEObj = workaroundRetrieving(objects.get(0));
//
//		tosca_nodes_Root x = ((CloudEnvironment) mostRecentEObj).getTosca_nodes_root().get(0);
//		String objAtId = x.eResource().getURIFragment(x);
//		log.info("nachher:" + objAtId, className);
//
//		CDOResource secondMostRecentRes = transaction
//				.getResource("/computingcontinuummodel/" + secondMostRecentNode.getName());
//		EList<EObject> objects1 = secondMostRecentRes.getContents();
//		EObject secondMostRecentEObj = workaroundRetrieving(objects.get(0));

		EObject mostRecentEObj = mostRecentModel;
		EObject secondMostRecentEObj = secondMostRecentModel;
		
		CompareClient changes = new CompareClient();

		List<Diff> diffs = changes.getDiffs(secondMostRecentEObj, mostRecentEObj);
		return diffs;
	}

	public EObject retrieveEObject(String identifier) {
		CDOResource cdoRes = transaction.getResource(identifier);

		EList<EObject> objects = cdoRes.getContents();

		log.info(String.valueOf(objects.size()), className);

		CloudEnvironment cloudEnvironmentInstance = (CloudEnvironment) objects.get(0);

		for (EObject e : cloudEnvironmentInstance.getTosca_nodes_root()) {
			EStructuralFeature nameFeature = e.eClass().getEStructuralFeature("name");
			String nameOfEObj = (String) e.eGet(nameFeature);

			log.info(nameOfEObj, className);
		}

		return cloudEnvironmentInstance;
	}

	public void deleteAll() {
		try {
			CDOResourceNode[] allNodes = transaction.getElements();

			for (CDOResourceNode node : allNodes) {
				EcoreUtil.delete(node);
				log.info("deleted: " + node.getName(), className);
			}

			transaction.commit();
		} catch (CommitException e) {
			e.printStackTrace();
		}
	}

	public void deleteOne(String identifier) {
		try {
			CDOResource cdoRes = transaction.getResource(identifier);
			EcoreUtil.delete(cdoRes);
			log.info("deleted: " + cdoRes.getName(), className);
			transaction.commit();
		} catch (CommitException e) {
			e.printStackTrace();
		}
	}

	public void startUp() {
		repo = new Repository(REPO_NAME, DATABASE_NAME, DATABASE_USER, DATABASE_PASS);
		repo.activate();
		transaction = repo.getTransaction();
	}

	public void closeDown() {
		repo.closeRepo();
	}

	// --------------helpers----------------------------------------------------------------
	private CDOResourceNode getMostRecentNodeFromList(List<CDOResourceNode> allNodes) {

		CDOResourceNode mostRecentNode = allNodes.get(0);

		for (CDOResourceNode node : allNodes) {
			Timestamp tsNode = getTimestampFromIdentifier(node.getName());
			Timestamp tsCurrMostRecentNode = getTimestampFromIdentifier(mostRecentNode.getName());
			if (tsNode.after(tsCurrMostRecentNode)) {
				mostRecentNode = node;
			}
		}
		return mostRecentNode;
	}

	private CDOResourceNode getSecondMostRecentNodeFromList(List<CDOResourceNode> allNodes) {

		List<CDOResourceNode> nodesOfSelectedType = allNodes.stream()
				.filter(node -> node.getName().contains(Modeltype.AS_IS_MODEL)).collect(Collectors.toList());

		CDOResourceNode mostRecentNode = getMostRecentNodeFromList(nodesOfSelectedType);
		nodesOfSelectedType.remove(mostRecentNode);
		CDOResourceNode secondMostRecentNode = getMostRecentNodeFromList(nodesOfSelectedType);
		return secondMostRecentNode;
	}

	private Timestamp getTimestampFromIdentifier(String identifier) {

		// identifier : type_Timestamp
		String[] substrings = identifier.split("_");
		String timestampAsString = substrings[1];

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
			Date parsedDate = dateFormat.parse(timestampAsString);
			Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			return timestamp;
		} catch (Exception e) {
			// error was sysout
			log.info("string could not be parsed to timestamp in CdoClient.getTimestampFromIdentifier", className);
		}
		return null;
	}

	/**
	 * store URIFragment to id
	 */
	private EObject workaroundStoring(EObject e) {

		if (e instanceof CloudEnvironment) {

			List<tosca_nodes_Root> nodes = ((CloudEnvironment) e).getTosca_nodes_root();

			nodes.stream().forEach(node -> {
				String objAtId = node.eResource().getURIFragment(node);
				String[] split = objAtId.split("//@tosca_nodes_root.");
				int i = Integer.parseInt(split[1]);
				node.setId(i);
			});
		}
		return e;
	}

	/**
	 * @return a copy where URIFragment is set correctly
	 */
	private EObject workaroundRetrieving(EObject e) {

		ModelLoader loader = new ModelLoader();
		String s = loader.modelToJsonString((CloudEnvironment) e);

		JsonObject jsonObj = (JsonObject) new JsonParser().parse(s);
		jsonObj.addProperty("@id", "/");

		Map<String, String> map = new HashMap<>();

		JsonArray toscaNodes = jsonObj.get("tosca_nodes_root").getAsJsonArray();
		for (int i = 0; i < toscaNodes.size(); i++) {
			String cdoID = ((JsonObject) toscaNodes.get(i)).get("@id").getAsString();
			String toscaID = "//@tosca_nodes_root." + ((JsonObject) toscaNodes.get(i)).get("id").getAsString();
			map.put(cdoID, toscaID);
			((JsonObject) toscaNodes.get(i)).addProperty("id", 0);
		}

		String adjusted = jsonObj.toString();

		for (String key : map.keySet()) {
			adjusted = adjusted.replaceAll(key, map.get(key));
		}

		try {
			return loader.jsonStringToEObject(adjusted);
		} catch (IOException e1) {
			// Auto-generated catch block
			e1.printStackTrace();
		}

		return e;
	}

	public void setSecondMostRecentModel(EObject newModel) {
		this.secondMostRecentModel = (CloudEnvironment) newModel;
		
	}
}
