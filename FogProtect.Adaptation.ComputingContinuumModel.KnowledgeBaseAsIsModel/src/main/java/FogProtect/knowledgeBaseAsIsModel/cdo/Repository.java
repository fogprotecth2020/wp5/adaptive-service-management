package FogProtect.knowledgeBaseAsIsModel.cdo;

import fogprotect.adaptation.ComputingContinuumModel.impl.ComputingContinuumModelPackageImpl;
import org.eclipse.emf.cdo.eresource.CDOResourceFactory;
import org.eclipse.emf.cdo.net4j.CDONet4jSession;
import org.eclipse.emf.cdo.server.IRepository;
import org.eclipse.emf.cdo.server.IStore;
import org.eclipse.emf.cdo.server.db.CDODBUtil;
import org.eclipse.emf.cdo.server.db.IDBStore;
import org.eclipse.emf.cdo.server.db.mapping.IMappingStrategy;
import org.eclipse.emf.cdo.server.embedded.CDOEmbeddedRepositoryConfig;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.net4j.db.DBUtil;
import org.eclipse.net4j.db.IDBConnectionProvider;
import org.eclipse.net4j.db.h2.H2Adapter;
import org.eclipse.net4j.util.container.IManagedContainer;
import org.eclipse.net4j.util.lifecycle.ILifecycle;
import org.eclipse.net4j.util.lifecycle.LifecycleEventAdapter;
import org.h2.jdbcx.JdbcDataSource;

import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;

public class Repository extends CDOEmbeddedRepositoryConfig {

    private CDONet4jSession session;
    private CDOTransaction transaction;
    private ResourceSet resourceSet;
    private String repositoryName;
    
    private boolean resetDBOnDeployment = Boolean.parseBoolean(System.getenv("ResetAsIsModelDB"));

    String DATABASE_NAME;
    String DATABASE_USER;
    String DATABASE_PASS;

    public Repository(String repositoryName, String dbName, String userName, String userPassword) {
        super(repositoryName);
        this.repositoryName = repositoryName;
        DATABASE_NAME = dbName;
        DATABASE_USER = userName;
        DATABASE_PASS = userPassword;
    }

    @Override
    public IStore createStore(IManagedContainer arg0) {
    	if (resetDBOnDeployment) {
    		resetDatabase();
    	}
    	
    	// edit based on db provider
        JdbcDataSource myDataSource = createDataSource();
        myDataSource.setUser(DATABASE_USER);
        myDataSource.setPassword(DATABASE_PASS);
        IDBConnectionProvider conProvider = DBUtil.createConnectionProvider(myDataSource);

        H2Adapter dbAdapter = new H2Adapter();
        // ------------------------------------------------------------------------------------

        IMappingStrategy mappingStrategy = CDODBUtil.createHorizontalMappingStrategy(true, true);
        mappingStrategy.getProperties().put(IMappingStrategy.Props.FORCE_NAMES_WITH_ID, "true");

        IDBStore store = CDODBUtil.createStore(mappingStrategy, dbAdapter, conProvider);
        mappingStrategy.setStore(store);
        
        return store;
    }
    
    private void resetDatabase() {
    	// h2 database has two files, both are deleted (for good measure)
    	String pathToDatabase = new File(System.getProperty("user.dir")).getAbsolutePath() + File.separator + "database" + File.separator;
    	new File(pathToDatabase +  "asIsModels.mv.db").delete();
    	new File(pathToDatabase +  "asIsModels.trace.db").delete();
    }

    protected JdbcDataSource createDataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:./database/asIsModels");
//        dataSource.setURL("jdbc:h2:mem:test");

        return dataSource;
    }

    public synchronized CDONet4jSession getSession(boolean openOnDemand) {
        checkActive();

        if (session == null && openOnDemand) {
            session = openClientSession();
            session.getPackageRegistry().put(ComputingContinuumModelPackageImpl.eNS_URI, ComputingContinuumModelPackageImpl.eINSTANCE);
            session.addListener(new LifecycleEventAdapter() {
                @Override
                protected void onDeactivated(ILifecycle lifecycle) {
                    if (lifecycle == session) {
                        session = null;
                    }
                }
            });
        }

        return session;
    }

    public synchronized CDOTransaction getTransaction() {
        checkActive();

        if (transaction == null) {

            resourceSet = new ResourceSetImpl();

            CDOResourceFactory factory = new CDOResourceFactory() {

                @Override
                public Resource createResource(URI uri) {
                    Resource res = new ResourceImpl();
                    res.setURI(URI.createURI("test/test2"));
                    return res;
                }
            };

            resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
                    .put(Resource.Factory.Registry.DEFAULT_EXTENSION, factory);
            resourceSet.getPackageRegistry().put(ComputingContinuumModelPackageImpl.eNS_URI, ComputingContinuumModelPackageImpl.eINSTANCE);

            CDONet4jSession session = getSession(true);
            transaction = session.openTransaction(resourceSet);

            transaction.addListener(new LifecycleEventAdapter() {
                @Override
                protected void onDeactivated(ILifecycle lifecycle) {
                    if (lifecycle == transaction) {
                        transaction = null;
                        resourceSet = null;
                    }
                }
            });
        }

        return transaction;
    }

    public void closeRepo() {
        if (session != null) {
            if (!session.isClosed()) {
                session.close();
            }
        }

        this.deactivate();
    }

    @Override
    public void initProperties(IManagedContainer container, Map<String, String> properties) {
        properties.put(IRepository.Props.SUPPORTING_AUDITS, Boolean.toString(true));
        properties.put(IRepository.Props.SUPPORTING_BRANCHES, Boolean.toString(true));
        properties.put(IRepository.Props.OVERRIDE_UUID, repositoryName);
    }
}

