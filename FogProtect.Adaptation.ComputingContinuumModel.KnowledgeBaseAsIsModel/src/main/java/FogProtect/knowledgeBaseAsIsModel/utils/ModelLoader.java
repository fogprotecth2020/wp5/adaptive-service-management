package FogProtect.knowledgeBaseAsIsModel.utils;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.emfjson.jackson.annotations.EcoreReferenceInfo;
import org.emfjson.jackson.annotations.EcoreTypeInfo;
import org.emfjson.jackson.module.EMFModule;
import org.emfjson.jackson.resource.JsonResourceFactory;
import org.emfjson.jackson.utils.ValueReader;
import org.emfjson.jackson.utils.ValueWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import fogprotect.adaptation.ComputingContinuumModel.impl.ComputingContinuumModelPackageImpl;
import utility.Constants;


@Component
public class ModelLoader {

    ObjectMapper mapper;
    HenshinResourceSet resourceSet;
    public CloudEnvironment cloudEnvironmentInstance;
    
    @Autowired
    PersonalLogger log;
    
    private static String className = ModelLoader.class.getName();

    // Prepares ResourceSet and Mapper and initializes attributes
    public ModelLoader() {
    	HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.RUNTIME_MODEL_PATH);
		resourceSet.getPackageRegistry().put(ComputingContinuumModelPackageImpl.eNS_URI, ComputingContinuumModelPackageImpl.eINSTANCE);
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("computingcontinuummodel", new XMIResourceFactoryImpl());

		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

		EMFModule module = new EMFModule();
		module.configure(EMFModule.Feature.OPTION_USE_ID, true);
		module.configure(EMFModule.Feature.OPTION_SERIALIZE_DEFAULT_VALUE, true);
		ValueReader<String, EClass> valueReader = new ValueReader<String, EClass>() {
			public EClass readValue(String value, DeserializationContext context) {
				return (EClass) ComputingContinuumModelPackageImpl.eINSTANCE.getEClassifier(value);
			}
		};
		ValueWriter<EClass, String> valueWriter = new ValueWriter<EClass, String>() {

			public String writeValue(EClass value, SerializerProvider context) {
				//  Auto-generated method stub
				return value.getName();
			}
		};
		module.setTypeInfo(new EcoreTypeInfo("type", valueReader, valueWriter));
		module.setReferenceInfo(new EcoreReferenceInfo("referencedObjectID"));

		mapper.registerModule(module);
		JsonResourceFactory factory = new JsonResourceFactory(mapper);
		mapper = factory.getMapper();

		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("json", factory);

		this.mapper = mapper;
		this.resourceSet = resourceSet;
    }

    // Suche innerhalb des CloudEnvObj
    public EObject searchForObjectInGivenModel(CloudEnvironment cloudEnvironment, String id) {
        EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
        for (tosca_nodes_Root element : nodes) {
            Resource resource = element.eResource();
            String toTest = resource.getURIFragment(element); //@id
            if (toTest.equals(id)) {
                return element;
            }
        }
        return null;
    }
    
 // Suche innerhalb des CloudEnvObj
    public EObject searchForObjectInGivenModelByName(CloudEnvironment cloudEnvironment, String name) {
        EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
        for (tosca_nodes_Root element : nodes) {
            EStructuralFeature nameFeature = element.eClass().getEStructuralFeature("name");
            String objName = element.eGet(nameFeature).toString();
            if (objName.equals(name)) {
                return element;
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------------------------------------


    // JsonString laden
    // Used to create an EObject out of a JSON Representation - needs to fit with
    // our meta model
    public EObject jsonStringToEObject(String model) throws IOException {
		JsonResourceFactory factory = new JsonResourceFactory(mapper);
		mapper = factory.getMapper();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, factory);
		resourceSet.getPackageRegistry().put(ComputingContinuumModelPackageImpl.eNS_URI, ComputingContinuumModelPackageImpl.eINSTANCE);
		Resource resource = resourceSet.createResource(URI.createURI("computingcontinuummodel"));
		InputStream stream = new ByteArrayInputStream(model.getBytes(StandardCharsets.UTF_8));
		resource.load(stream, null);
		stream.close();
		return resource.getContents().get(0);
    }


    //Json aus File laden
    public String fileToJsonString(String jsonModelFileName) {

        // Accessing the model information
        String jsonString = "";
        try {
            //this.fileToJsonString(jsonModelFileName);
            jsonString = mapper.writerWithDefaultPrettyPrinter()
                    .writeValueAsString(fileToCloudEnv(jsonModelFileName));
            jsonString = jsonString.replaceAll("#", "");
        } catch (Exception e) {
            // Auto-generated catch block
            jsonString = "FAILURE";
            e.printStackTrace();
        }
        return jsonString;

    }


    public CloudEnvironment fileToCloudEnv(String jsonModelFileName) {
        EGraph graph = new EGraphImpl(resourceSet.getResource(jsonModelFileName));
        CloudEnvironment cloudEnvironment = (CloudEnvironment) graph.getRoots().get(0);
        return cloudEnvironment;
    }


    // JsonString schreiben
    public String modelToJsonString(CloudEnvironment proposal) {
        // Accessing the model information
        String jsonString = "";
        try {
            jsonString = this.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(proposal);
            jsonString = jsonString.replaceAll("#", "");
        } catch (JsonProcessingException e) {
            // Auto-generated catch block
            jsonString = "FAILURE";
            e.printStackTrace();
        }
//        	log.jsonInfo(jsonString, className);
        return jsonString;
    }
}


