package FogProtect.knowledgeBaseAsIsModel.utils;

public class Listener {

	
	private String url;

	public Listener(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
