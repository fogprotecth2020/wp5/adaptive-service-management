package FogProtect.knowledgeBaseAsIsModel.utils;

public class Modeltype {
    public static final String AS_IS_MODEL = "as is model";
    public static final String REFERENCE_MODEL = "reference Model";
    public static final String ADAPTATION_PROPOSAL = "adaptation proposal";
}
