package FogProtect.knowledgeBaseAsIsModel.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataManipulationDetectedDTO {
	
	@JsonProperty("topic")
	private String topic;
	
	@JsonProperty("rule")
	private String rule;
	
	@JsonProperty("agent")
	private Agent agent;
	
	@JsonProperty("reason")
	private String reason;
	
	@JsonProperty("source")
	private String source;
	
	@JsonProperty("filename")
	private String filename;
	
	@JsonProperty("serviceLevel")
	private String serviceLevel;
	
	@JsonProperty("SieaTaskId")
	private String SieaTaskId;
	
	
	public DataManipulationDetectedDTO() {
		
	}
	
	DataManipulationDetectedDTO(
			String topic, 
			String rule,
			Agent agent, 
			String reason, 
			String source, 
			String filename, 
			String serviceLevel,
			String SieaTaskId) {
		this.topic = topic;
		this.rule = rule;
		this.agent = agent;
		this.reason = reason;
		this.source = source;
		this.filename = filename;
		this.SieaTaskId = SieaTaskId;
		this.serviceLevel =serviceLevel;
	}
	
	public String getTopic() {
		return this.topic;
	}
	
	public String getRule() {
		return this.rule;
	}
	
	public Agent getAgent() {
		return this.agent;
	}
	
	public String getReason() {
		return this.reason;
	}
	
	public String getSource() {
		return this.source;
	}
	
	public String getFilename() {
		return this.filename;
	}
	
	public String getServiceLevel() {
		return this.serviceLevel;
	}
	
	public String getSieaTaskId() {
		return this.SieaTaskId;
	}
}
