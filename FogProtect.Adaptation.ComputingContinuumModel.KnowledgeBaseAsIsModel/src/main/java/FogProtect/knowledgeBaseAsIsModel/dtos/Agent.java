package FogProtect.knowledgeBaseAsIsModel.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Agent {

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("ip")
	private String ip;
	
	public Agent() {
		
	}
	
	public Agent(String id, String name, String ip) {
		this.id = id;
		this.name = name;
		this.ip = ip;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getIp() {
		return this.ip;
	}
}
