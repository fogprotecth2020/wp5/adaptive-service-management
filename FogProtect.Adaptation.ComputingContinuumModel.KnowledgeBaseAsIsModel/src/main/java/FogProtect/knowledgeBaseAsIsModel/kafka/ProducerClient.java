package FogProtect.knowledgeBaseAsIsModel.kafka;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import FogProtect.knowledgeBaseAsIsModel.rest.MonitoringController;
import FogProtect.knowledgeBaseAsIsModel.utils.PersonalLogger;

import java.util.concurrent.ExecutionException;

/**
 * uses KafkaTemplate to send messages to topics
 */
@Component
public class ProducerClient {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    
    @Autowired
	PersonalLogger log;

	private static String className = ProducerClient.class.getName();


    /**
     * tries so send a message to the chosen topic
     * prints success or failure message
     *
     * @return true if message was sent successfully
     */
    public boolean sendMessage(String topicName, String msg) {
        boolean successfullySent = false;
        try {
            SendResult<String, String> result = kafkaTemplate.send(topicName, msg).get();
            log.info("Sent message to topic: [ " + topicName + " ] with result: " + result.toString(), msg);
            log.jsonInfo("Sent message=[" + msg + "] with offset=[" + result.getRecordMetadata().offset() + "]", className);
            successfullySent = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return successfullySent;
    }
}
