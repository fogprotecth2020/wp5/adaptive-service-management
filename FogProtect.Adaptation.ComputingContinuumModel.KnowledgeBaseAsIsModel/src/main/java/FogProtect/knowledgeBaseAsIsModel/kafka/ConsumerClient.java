package FogProtect.knowledgeBaseAsIsModel.kafka;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.util.json.JSONParser;
import org.eclipse.emf.ecore.resource.Resource;
import org.h2.util.json.JSONObject;
import org.eclipse.emf.ecore.EObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import FogProtect.knowledgeBaseAsIsModel.cdo.CdoClient;
import FogProtect.knowledgeBaseAsIsModel.dtos.DataManipulationDetectedDTO;
import FogProtect.knowledgeBaseAsIsModel.rest.MonitoringController;
import FogProtect.knowledgeBaseAsIsModel.rest.MyService;
import FogProtect.knowledgeBaseAsIsModel.utils.ModelLoader;
import FogProtect.knowledgeBaseAsIsModel.utils.PersonalLogger;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.Compute;
import fogprotect.adaptation.ComputingContinuumModel.PrivateSpace;
import fogprotect.adaptation.ComputingContinuumModel.DataFlow;
import fogprotect.adaptation.ComputingContinuumModel.SoftwareComponent;
import fogprotect.adaptation.ComputingContinuumModel.DataProcessor;
import fogprotect.adaptation.ComputingContinuumModel.Group;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;

@Service
@RestController
public class ConsumerClient {

	@Autowired
	MonitoringController monitoringController;

	@Autowired
	ModelLoader loader;

	@Value("${AsIsModel}")
	String filename;
	
	@Value("${DashboardResetURL}")
	String dashboardResetURL;

	@Value("${PolicyResetURL}")
	String policyResetURL;

	@Autowired
	private MyService myService;
	
	@Autowired
	private CdoClient cdo;
	
	@Autowired
	PersonalLogger log;

	private static String className = ConsumerClient.class.getName();

	// UC2
	@KafkaListener(topics = "FiaBContainerStatus", groupId = "group_id")
	public void listenToContainerStatus(String msg) {
		log.info("FiaBContainerStatus: " + msg, className);

		/*
		 * Possible contents: b'AdaptationRequired-DoorHasOpened'
		 * b'AdaptationRequired-DoorHasClosed' b'AdaptationRequired-ClearanceGiven'
		 */
		
		if(msg.equals("timeawareadaptation_msg")) {
			System.out.println("MESSAGE " + msg);
			JsonObject head = new JsonObject();
			monitoringController.monitoringEvent(head.toString());
		}

		if (msg.equals("AdaptationRequired-DoorHasOpened")) {
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", "//@tosca_nodes_root.31");
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "LOW");
			head.addProperty("attributeType", "Trustworthy");
			monitoringController.monitoringEvent(head.toString());
		}
		if (msg.equals("AdaptationRequired-DoorHasClosed")) {
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", "//@tosca_nodes_root.31");
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "MEDIUM");
			head.addProperty("attributeType", "Trustworthy");
			monitoringController.monitoringEvent(head.toString());
		}
		if (msg.equals("AdaptationRequired-ClearanceGiven")) {
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", "//@tosca_nodes_root.31");
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "HIGH");
			head.addProperty("attributeType", "Trustworthy");
			monitoringController.monitoringEvent(head.toString());
		}
	}
	
	@KafkaListener(topics = "DataLeakDetected", groupId = "group_id")
	public void listenToDataLeakDetected(String msg) {
		log.info("DataLeakDetected: " + msg, className);
		JsonObject msgJSON = new JsonParser().parse(msg).getAsJsonObject();
			String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
			try {
				cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String taskId = msgJSON.get("SieaTaskId").getAsString();
			
			if (loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
					"Attacker_ReadDataFlow") != null) {

				DataFlow df = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_ReadDataFlow");
				JsonObject head = new JsonObject();
				head.addProperty("RequestType", "changeAttribute");
				head.addProperty("@id", df.eResource().getURIFragment(df));
				head.addProperty("attribute", "disab");
				head.addProperty("newValue", "false");
				head.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head.toString());

				// cdo.storeModel((CloudEnvironment) cdo.retrieveMostRecentModel(""),
				// Modeltype.AS_IS_MODEL);
				myService.notifyAllAboutChanges(taskId);
//				lastEventName = "DataLeakDetected";
			} else {
				myService.notifyAllAboutChanges(taskId);
			}
	}
	
	//UC1 & UC2 overlap
	@KafkaListener(topics = "DataManipulationDetected", groupId = "group_id")
	public void listenToDataManipulationDetected(String msg) {
		log.info("DataManipulationDetected: " + msg, className);
		
		// UC1: Event 1
		if (filename.contains("SmartCity")) {
			/*
			 * message = b'PhysicalTampering'
			 */

	        Gson gson = new Gson();
	        DataManipulationDetectedDTO dataManipulationDetectedDTO = gson.fromJson(msg, DataManipulationDetectedDTO.class);
			
			//identifiziere fog node mit abwandlung von ModelLoader.searchForObjectInGivenModel() -> gibt eObject zurck
	        PrivateSpace privateSpace = (PrivateSpace) loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""), dataManipulationDetectedDTO.getAgent().getName()); //the last part will not work because the fognode is not named "ubitnvidia" as Norbert named it in his JSON
			
			Resource resource = privateSpace.eResource();
			String privateSpaceURI = resource.getURIFragment(privateSpace);
			
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", privateSpaceURI);
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "LOW");
			head.addProperty("attributeType", "Trustworthy");
			monitoringController.monitoringEventWithoutUpdate(head.toString());
			myService.notifyAllAboutChanges(dataManipulationDetectedDTO.getSieaTaskId());
			//monitoringController.monitoringEvent(head.toString());	
		
		}else if (filename.contains("SmartManufacturing")) {
			JsonObject msgJSON = new JsonParser().parse(msg).getAsJsonObject();
			String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
			try {
				cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String taskId = msgJSON.get("SieaTaskId").getAsString();
			
			if (loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
					"Attacker_WriteDataFlow") != null && loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
							"Attacker_ReadDataFlow") != null) {

				DataFlow df = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_WriteDataFlow");
				JsonObject head = new JsonObject();
				head.addProperty("RequestType", "changeAttribute");
				head.addProperty("@id", df.eResource().getURIFragment(df));
				head.addProperty("attribute", "disab");
				head.addProperty("newValue", "false");
				head.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head.toString());
				
				DataFlow df2 = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_ReadDataFlow");
				JsonObject head2 = new JsonObject();
				head2.addProperty("RequestType", "changeAttribute");
				head2.addProperty("@id", df2.eResource().getURIFragment(df2));
				head2.addProperty("attribute", "disab");
				head2.addProperty("newValue", "false");
				head2.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head2.toString());

				// cdo.storeModel((CloudEnvironment) cdo.retrieveMostRecentModel(""),
				// Modeltype.AS_IS_MODEL);
				myService.notifyAllAboutChanges(taskId);
//				lastEventName = "DataManipulationDetected";
			}else{
				myService.notifyAllAboutChanges(taskId);
			}
		}
	}
	
	// UC1 EVENT 2 
	@KafkaListener(topics = "SlpTamperingResolved", groupId = "group_id")
	public void listenToTamperingResolvedUC1(String msg) {
		Gson gson = new Gson();
        DataManipulationDetectedDTO tamperingResolved = gson.fromJson(msg, DataManipulationDetectedDTO.class);

        PrivateSpace privateSpace = (PrivateSpace) loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""), tamperingResolved.getAgent().getName()); //the last part will not work because the fognode is not named "ubitnvidia" as Norbert named it in his JSON
		
		Resource resource = privateSpace.eResource();
		String privateSpaceURI = resource.getURIFragment(privateSpace);
		
		JsonObject head = new JsonObject();
		head.addProperty("RequestType", "changeAttribute");
		head.addProperty("@id", privateSpaceURI);
		head.addProperty("attribute", "trustworthy");
		head.addProperty("newValue", "VERYHIGH");
		head.addProperty("attributeType", "Trustworthy");
		System.out.println(head);
		monitoringController.monitoringEventWithoutUpdate(head.toString());
		myService.notifyAllAboutChanges(tamperingResolved.getSieaTaskId());
		//monitoringController.monitoringEvent(head.toString());
		
		
	}
	
	@KafkaListener(topics = "TamperingResolved", groupId = "group_id")
	public void listenToTamperingResolved(String msg) {
		log.info("TamperingResolved: " + msg, className);
		
		
		// UC1: Event 2
		if (filename.contains("SmartCity")) {
			
	        Gson gson = new Gson();
	        DataManipulationDetectedDTO tamperingResolved = gson.fromJson(msg, DataManipulationDetectedDTO.class);

	        PrivateSpace privateSpace = (PrivateSpace) loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""), tamperingResolved.getAgent().getName()); //the last part will not work because the fognode is not named "ubitnvidia" as Norbert named it in his JSON
			
			Resource resource = privateSpace.eResource();
			String privateSpaceURI = resource.getURIFragment(privateSpace);
			
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", privateSpaceURI);
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "VERYHIGH");
			head.addProperty("attributeType", "Trustworthy");
			System.out.println(head);
			monitoringController.monitoringEventWithoutUpdate(head.toString());
			myService.notifyAllAboutChanges(tamperingResolved.getSieaTaskId());
			//monitoringController.monitoringEvent(head.toString());
			
			
		} else if (filename.contains("SmartManufacturing")) {
			JsonObject msgJSON = new JsonParser().parse(msg).getAsJsonObject();
			String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
			try {
				cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String taskId = msgJSON.get("SieaTaskId").getAsString();
			
			if (loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
					"Attacker_WriteDataFlow") != null && loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
							"Attacker_ReadDataFlow") != null) {

				DataFlow df = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_WriteDataFlow");
				JsonObject head = new JsonObject();
				head.addProperty("RequestType", "changeAttribute");
				head.addProperty("@id", df.eResource().getURIFragment(df));
				head.addProperty("attribute", "disab");
				head.addProperty("newValue", "true");
				head.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head.toString());
				
				DataFlow df2 = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_ReadDataFlow");
				JsonObject head2 = new JsonObject();
				head2.addProperty("RequestType", "changeAttribute");
				head2.addProperty("@id", df2.eResource().getURIFragment(df2));
				head2.addProperty("attribute", "disab");
				head2.addProperty("newValue", "true");
				head2.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head2.toString());

				// cdo.storeModel((CloudEnvironment) cdo.retrieveMostRecentModel(""),
				// Modeltype.AS_IS_MODEL);
				myService.notifyAllAboutChanges(taskId);
//				lastEventName = "DataManipulationDetected";
			} else {
				myService.notifyAllAboutChanges(taskId);
			}	
		}
	}

	// UC1
	@KafkaListener(topics = "SmartLampPostStatus", groupId = "group_id")
	public void listenToLampPostEvent(String msg) {
		log.info("SmartLampPostStatus: " + msg, className);

		/*
		 * message = b'PhysicalTampering'
		 */
		if (msg.equals("AdaptationRequired-PhysicalTampering")) {

			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", "//@tosca_nodes_root.28");
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "LOW");
			head.addProperty("attributeType", "Trustworthy");
			monitoringController.monitoringEvent(head.toString());
		}

		if (msg.equals("AdaptationRequired-ClearanceGiven")) {
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", "//@tosca_nodes_root.28");
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "HIGH");
			head.addProperty("attributeType", "Trustworthy");
			monitoringController.monitoringEvent(head.toString());
		}
	}
	
	@KafkaListener(topics = "VideoStreamTampering", groupId = "group_id")
	public void listenToVideoStreamTamperingEvent(String msg) {
		log.info("VideoStreamTampering: " + msg, className);

		/*
		 * message = b'PhysicalTampering'
		 */

		if (msg.equals("AdaptationRequired-ClearanceGiven")) {
			// muss noch umgebaut werden
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", "//@tosca_nodes_root.28");
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "HIGH");
			head.addProperty("attributeType", "Trustworthy");
			monitoringController.monitoringEvent(head.toString());
		} else {
			// msg lesen und zum json Object �bersetzen

			// identifiziere fog node mit abwandlung von
			// ModelLoader.searchForObjectInGivenModel() -> gibt eObject zur�ck

			// eObject zu compute casten und relation zu PrivateSPace suchen �ber EMF
			// methoden

			// @Id von PrivateSpace abfragen Resource resource = element.eResource();
			// String toTest = resource.getURIFragment(element); //@id

			// JsonObject bauen, dass die Trustworthyness von dem identifizerten
			// PrivateSpace �ndert

			// monitoringController.monitoringEvent();
		}
	}

	// UC1
//	@KafkaListener(topics = "ClearanceEvent", groupId = "group_id")
//	public void listenToClearanceEvent(String msg) {
//		System.out.println("ClearanceEvent: " + msg);
//		/*
//		 * message = b'ClearanceGiven'
//		 */
//		if(msg.equals("ClearanceGiven")) {
//			JsonObject head = new JsonObject();
//			head.addProperty("RequestType", "changeAttribute");
//			head.addProperty("@id", "//@tosca_nodes_root.28");
//			head.addProperty("attribute", "trustworthy");
//			head.addProperty("newValue", "HIGH");
//			head.addProperty("attributeType", "Trustworthy");
//			monitoringController.monitoringEvent(head.toString());
//		}
//	}


    // UC3 - WIP
    /*
       Kafka messages and topics for UC3:

       message topic = "EndPointBlockedReport"
       message timestamp = 1658914511590
       message value =
       {
       "user": "atc-vbu1@atc.be.com",
       "role": [
       "Operator"
       ],
       "org": "ATC",
       "endpoint": "GET http://chatterbox-be.chatterbox.svc.cluster.local:4000/videos/",
       "reason": "Multiple unauthorized read access attempts for user detected: atc-vbu1@atc.be.com_ATC",
       "attempts": 21,
       "threshold": 20,
       "source": "fybrik",
       "windowSize": 60,
       "serviceLevel": "medium",
       "SieaTaskId": "28-fe8621fe-d0f1-47d8-8334-d04866519a3b",
       }


       other topics:
       =============
       EndPointGrantedReport
       EndPointUnblockedReport

     */

	@KafkaListener(topics = "EndPointBlockedReport", groupId = "group_id")
	public void UC3_listenToEndPointBlockedReport(String msg) {
	    log.info("EndPointBlockedReport: " + msg, className);
	    System.out.println("EndPointBlockedReport: " + msg);
	    
		String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
		try {
			cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
		} catch (IOException e) {
			e.printStackTrace();
		}

	    JsonObject msgJSON = new JsonParser().parse(msg).getAsJsonObject();
	    String taskId = msgJSON.get("SieaTaskId").getAsString();
	    String actor = msgJSON.get("actor").getAsString();
	    
	    if (actor.contains("user")) {
	    	String userName = msgJSON.get("user").getAsString();
		    DataProcessor user = (DataProcessor) loader.searchForObjectInGivenModelByName(
											     (CloudEnvironment) cdo.retrieveMostRecentModel(""), userName);
		    JsonObject head = new JsonObject();
		    head.addProperty("RequestType", "changeAttribute");
		    head.addProperty("@id", user.eResource().getURIFragment(user));
		    head.addProperty("attribute", "trustworthy");
		    head.addProperty("newValue", "LOW");
		    head.addProperty("attributeType", "Trustworthy");
	
		    monitoringController.monitoringEventWithoutUpdate(head.toString());
		    myService.notifyAllAboutChanges(taskId);
	    } else if (actor.contains("org")) {
	    	String orgName = msgJSON.get("org").getAsString();
	    	if (orgName.contains("ATC")) {
	    		List<String> userNames = new ArrayList<String>(List.of("atc-vbu1", "atc-eu1"));
	    		for (String userName : userNames) {
	    		    DataProcessor user = (DataProcessor) loader.searchForObjectInGivenModelByName(
						     (CloudEnvironment) cdo.retrieveMostRecentModel(""), userName);
					JsonObject head = new JsonObject();
					head.addProperty("RequestType", "changeAttribute");
					head.addProperty("@id", user.eResource().getURIFragment(user));
					head.addProperty("attribute", "trustworthy");
					head.addProperty("newValue", "LOW");
					head.addProperty("attributeType", "Trustworthy");
					monitoringController.monitoringEventWithoutUpdate(head.toString());
	    		}
	    		myService.notifyAllAboutChanges(taskId);	
	    	}		    
	    } else {
	    	log.error("EndPointBlockedReport parsing failed: " + msg, className);
	    }
	}

	@KafkaListener(topics = "EndPointGrantedReport", groupId = "group_id")
	public void UC3_listenToEndPointGrantedReport(String msg) {
	    log.info("EndPointGrantedReport: " + msg, className);
	    System.out.println("EndPointGrantedReport: " + msg);
	    
		String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
		try {
			cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
		} catch (IOException e) {
			e.printStackTrace();
		}

	    JsonObject msgJSON = new JsonParser().parse(msg).getAsJsonObject();
	    String taskId = msgJSON.get("SieaTaskId").getAsString();
	    String actor = msgJSON.get("actor").getAsString();
	    
	    if (actor.contains("user")) {
	    	String userName = msgJSON.get("user").getAsString();
		    DataProcessor user = (DataProcessor) loader.searchForObjectInGivenModelByName(
											     (CloudEnvironment) cdo.retrieveMostRecentModel(""), userName);
		    JsonObject head = new JsonObject();
		    head.addProperty("RequestType", "changeAttribute");
		    head.addProperty("@id", user.eResource().getURIFragment(user));
		    head.addProperty("attribute", "trustworthy");
		    head.addProperty("newValue", "LOW");
		    head.addProperty("attributeType", "Trustworthy");
	
		    monitoringController.monitoringEventWithoutUpdate(head.toString());
		    myService.notifyAllAboutChanges(taskId);
	    } else {
	    	log.error("EndPointBlockedReport parsing failed: " + msg, className);
	    }
	}

	@KafkaListener(topics = "EndPointUnBlockedReport", groupId = "group_id")
	public void UC3_listenToEndPointUnblockedReport(String msg) {
	    log.info("EndPointUnblockedReport: " + msg, className);
	    System.out.println("EndPointUnblockedReport: " + msg);
	    
		String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
		try {
			cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
		} catch (IOException e) {
			e.printStackTrace();
		}

	    JsonObject msgJSON = new JsonParser().parse(msg).getAsJsonObject();
	    String taskId = msgJSON.get("SieaTaskId").getAsString();
	    String actor = msgJSON.get("actor").getAsString();

	    if (actor.contains("user")) {
	    	String userName = msgJSON.get("user").getAsString();
		    DataProcessor user = (DataProcessor) loader.searchForObjectInGivenModelByName(
												  (CloudEnvironment) cdo.retrieveMostRecentModel(""), userName);
		    JsonObject head = new JsonObject();
		    head.addProperty("RequestType", "changeAttribute");
		    head.addProperty("@id", user.eResource().getURIFragment(user));
		    head.addProperty("attribute", "trustworthy");
		    head.addProperty("newValue", "HIGH");
		    head.addProperty("attributeType", "Trustworthy");
	
		    System.out.println(user.toString());
	
		    monitoringController.monitoringEventWithoutUpdate(head.toString());
		    myService.notifyAllAboutChanges(taskId);
	    } else if (actor.contains("org")) {
	    	String orgName = msgJSON.get("org").getAsString();
	    	if (orgName.contains("ATC")) {
	    		List<String> userNames = new ArrayList<String>(List.of("atc-vbu1", "atc-eu1"));
	    		for (String userName : userNames) {
	    		    DataProcessor user = (DataProcessor) loader.searchForObjectInGivenModelByName(
						     (CloudEnvironment) cdo.retrieveMostRecentModel(""), userName);
					JsonObject head = new JsonObject();
					head.addProperty("RequestType", "changeAttribute");
					head.addProperty("@id", user.eResource().getURIFragment(user));
					head.addProperty("attribute", "trustworthy");
					head.addProperty("newValue", "HIGH");
					head.addProperty("attributeType", "Trustworthy");
					monitoringController.monitoringEventWithoutUpdate(head.toString());
	    		}
	    		myService.notifyAllAboutChanges(taskId);
	    	}	
	    } else {
	    	log.error("EndPointUnBlockedReport parsing failed: " + msg, className);
	    }
	}

	// UC1 & UC2 & UC3
	@KafkaListener(topics = "ResetEvent", groupId = "group_id")
	public void listenToReset(String msg) {
		log.info("Reset: " + msg, className);
		if (msg.equals("Resetting")) {
			if (filename.contains("SmartMedia")) {
				log.info("We don't reset in SmartMedia due to Kafka issues.", className);
				return;
			}
			
			String model = loader.fileToJsonString(filename + ".computingcontinuummodel");
			monitoringController.registerManagedSystem(model);
			String newPolicy = "";
			try {
				if (filename.contains("SmartCity")) {
					newPolicy = new String(Files.readAllBytes(Paths.get("src/main/resources/SmartCityPolicy.txt")));

				} else if (filename.contains("SmartManufacturing")) {
					newPolicy = new String(
							Files.readAllBytes(Paths.get("src/main/resources/SmartManufacturingPolicy.txt")));
					//reset Dashboard
					URL url = new URL(dashboardResetURL);
					URLConnection conn = url.openConnection();
					InputStream is = conn.getInputStream();
					is.close();
				}
				this.sendResetPolicy(newPolicy);
			} catch (IOException e) {
				log.error(e.toString(), className);
			}
			;

			myService.notifyAdaptationCoordinator();
		}
	}

	private void sendResetPolicy(String newPolicy) {
		int response = 0;
		try {
			HashMap<String, String> properties = new HashMap<String, String>();
			properties.put("Content-Type", "application/xml");
			properties.put("Accept", "application/xml");
			response = makeHttpRequest(policyResetURL, "PUT", "UTF-8", properties, newPolicy);
		} catch (Exception e) {
			log.error(e.toString(), className);
		}
		// normal Info : Response Code : -
		log.info("Response Code: " + response, className);
	}

	public int makeHttpRequest(String url, String method, String enc, Map<String, String> properties,
			String requestBody) throws IOException {

		HttpURLConnection request;
		URL restUrl = new URL(url);
		request = (HttpURLConnection) restUrl.openConnection();
		request.setRequestMethod(method);
		request.setDoOutput(true);
		Charset encoding = Charset.forName(enc);
		for (String k : properties.keySet()) {
			request.addRequestProperty(k, properties.get(k));
		}
		request.getOutputStream().write(requestBody.getBytes(encoding));
		int responseCode = request.getResponseCode();
		if (responseCode != 200) {
			InputStream response = request.getInputStream();
			String message = new String(response.readAllBytes(), StandardCharsets.UTF_8);
			log.jsonInfo(method, className);
		}
		return responseCode;
	}
	
		@RequestMapping(path = "/KafkaTestSmartCity", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
		public String testInterfaceForSmartCity(@RequestBody DataManipulationDetectedDTO videoStreamTamperingDTO) {
	        Gson gson = new Gson();
			
			//identifiziere fog node mit abwandlung von ModelLoader.searchForObjectInGivenModel() -> gibt eObject zurck
	        PrivateSpace privateSpace = (PrivateSpace) loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""), videoStreamTamperingDTO.getAgent().getName()); //the last part will not work because the fognode is not named "ubitnvidia" as Norbert named it in his JSON
			
			Resource resource = privateSpace.eResource();
			String privateSpaceURI = resource.getURIFragment(privateSpace);
			
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", privateSpaceURI);
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "LOW");
			head.addProperty("attributeType", "Trustworthy");
			System.out.println(head);
			monitoringController.monitoringEvent(head.toString());
			return "ok";
		}
		
		@RequestMapping(path = "/KafkaTestSmartCityClear", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
		public String testInterfaceForSmartCityClear(@RequestBody DataManipulationDetectedDTO videoStreamTamperingDTO) {
	        Gson gson = new Gson();
			
			//identifiziere fog node mit abwandlung von ModelLoader.searchForObjectInGivenModel() -> gibt eObject zurck
	        PrivateSpace privateSpace = (PrivateSpace) loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""), videoStreamTamperingDTO.getAgent().getName()); //the last part will not work because the fognode is not named "ubitnvidia" as Norbert named it in his JSON
			
			Resource resource = privateSpace.eResource();
			String privateSpaceURI = resource.getURIFragment(privateSpace);
			
			JsonObject head = new JsonObject();
			head.addProperty("RequestType", "changeAttribute");
			head.addProperty("@id", privateSpaceURI);
			head.addProperty("attribute", "trustworthy");
			head.addProperty("newValue", "VERYHIGH");
			head.addProperty("attributeType", "Trustworthy");
			System.out.println(head);
			monitoringController.monitoringEvent(head.toString());
			return "ok";
		}

	// ---------------------HELPER----------------------
	@RequestMapping(path = "/KafkaTest", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public String testInterface(@RequestBody(required = true) String topic) {

		if (topic.equals("DataManipulationDetected")) {
			String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
			try {
				cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String taskId = "2-fe8621fe-d0f1-47d8-8334-d04866519a3b";

			if (loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
					"Attacker_WriteDataFlow") != null && loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
							"Attacker_ReadDataFlow") != null) {

				DataFlow df = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_WriteDataFlow");
				JsonObject head = new JsonObject();
				head.addProperty("RequestType", "changeAttribute");
				head.addProperty("@id", df.eResource().getURIFragment(df));
				head.addProperty("attribute", "disab");
				head.addProperty("newValue", "false");
				head.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head.toString());
				
				DataFlow df2 = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_ReadDataFlow");
				JsonObject head2 = new JsonObject();
				head2.addProperty("RequestType", "changeAttribute");
				head2.addProperty("@id", df2.eResource().getURIFragment(df2));
				head2.addProperty("attribute", "disab");
				head2.addProperty("newValue", "false");
				head2.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head2.toString());

				// cdo.storeModel((CloudEnvironment) cdo.retrieveMostRecentModel(""),
				// Modeltype.AS_IS_MODEL);
				myService.notifyAllAboutChanges(taskId);
//				lastEventName = "DataManipulationDetected";
				return "ok";
			} else {
				myService.notifyAllAboutChanges(taskId);
				return "Problem!";
			}
		} else if (topic.equals("DataLeakDetected")) {
			String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
			try {
				cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String taskId = "3-fe8621fe-d0f1-47d8-8334-d04866519a3b";
			
			if (loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
					"Attacker_ReadDataFlow") != null) {

				DataFlow df = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_ReadDataFlow");
				JsonObject head = new JsonObject();
				head.addProperty("RequestType", "changeAttribute");
				head.addProperty("@id", df.eResource().getURIFragment(df));
				head.addProperty("attribute", "disab");
				head.addProperty("newValue", "false");
				head.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head.toString());

				// cdo.storeModel((CloudEnvironment) cdo.retrieveMostRecentModel(""),
				// Modeltype.AS_IS_MODEL);
				myService.notifyAllAboutChanges(taskId);
//				lastEventName = "DataLeakDetected";
				return "ok";
			} else {
				myService.notifyAllAboutChanges(taskId);
				return "Problem!";
			}
		} else if (topic.equals("TamperingResolved")) {
			String s = loader.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(""));
			try {
				cdo.setSecondMostRecentModel(loader.jsonStringToEObject(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String taskId = "25-fe8621fe-d0f1-47d8-8334-d04866519a3b";

			if (loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
					"Attacker_WriteDataFlow") != null && loader.searchForObjectInGivenModelByName((CloudEnvironment) cdo.retrieveMostRecentModel(""),
							"Attacker_ReadDataFlow") != null) {

				DataFlow df = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_WriteDataFlow");
				JsonObject head = new JsonObject();
				head.addProperty("RequestType", "changeAttribute");
				head.addProperty("@id", df.eResource().getURIFragment(df));
				head.addProperty("attribute", "disab");
				head.addProperty("newValue", "true");
				head.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head.toString());
				
				DataFlow df2 = (DataFlow) loader.searchForObjectInGivenModelByName(
						(CloudEnvironment) cdo.retrieveMostRecentModel(""), "Attacker_ReadDataFlow");
				JsonObject head2 = new JsonObject();
				head2.addProperty("RequestType", "changeAttribute");
				head2.addProperty("@id", df2.eResource().getURIFragment(df2));
				head2.addProperty("attribute", "disab");
				head2.addProperty("newValue", "true");
				head2.addProperty("attributeType", "boolean");
				monitoringController.monitoringEventWithoutUpdate(head2.toString());

				// cdo.storeModel((CloudEnvironment) cdo.retrieveMostRecentModel(""),
				// Modeltype.AS_IS_MODEL);
				myService.notifyAllAboutChanges(taskId);
//				lastEventName = "DataManipulationDetected";
				return "ok";
			} else {
				myService.notifyAllAboutChanges(taskId);
				return "Problem!";
			}
		}
		return "failure";

	}

	@RequestMapping(path = "/resetTest", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public String testInterface() {
		this.listenToReset("Hello I'm resetting!");
		return "200";
	}
}
