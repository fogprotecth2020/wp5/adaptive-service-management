package FogProtect.knowledgeBaseAsIsModel;


import java.util.logging.Level;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import FogProtect.knowledgeBaseAsIsModel.rest.MonitoringController;
import FogProtect.knowledgeBaseAsIsModel.utils.ModelLoader;
import FogProtect.knowledgeBaseAsIsModel.utils.PersonalLogger;

@SpringBootApplication(scanBasePackages= {"FogProtect.knowledgeBaseAsIsModel.utils", "FogProtect.knowledgeBaseAsIsModel.cdo", "FogProtect.knowledgeBaseAsIsModel.emfCompare", "FogProtect.knowledgeBaseAsIsModel.kafka", "FogProtect.knowledgeBaseAsIsModel.rest"})
public class KafkaclientApplication {

	
	@Value("${ListenerURL}")
	private String listenerURL;
	@Value("${ListenerURL2}")
	private String listenerURL2;
	
	@Value("${AsIsModel}")
	String filename;
	
	@Value("${IncludeAllLogs}")
	private boolean logAll;
	
	@Autowired
	MonitoringController monitoringController;
	
	@Autowired
	ModelLoader loader;
	
	@Autowired
	PersonalLogger log;
	
	private static String className = KafkaclientApplication.class.getName();
	

    public static void main(String[] args) {
        SpringApplication.run(KafkaclientApplication.class, args);
    }
    
    
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
    	
    	log.setLogAll(logAll);
    	    	
    	log.info("Spring Boot Started... Setting up environment variables", className);
        
        //Listeners:
        if(!listenerURL.equals("null")) {
        	 monitoringController.addListener(listenerURL);
        }
        
        //Listeners:
        if(!listenerURL2.equals("null")) {
        	 monitoringController.addListener(listenerURL2);
        }
        
        String model = loader.fileToJsonString(filename + ".computingcontinuummodel");
        monitoringController.registerManagedSystem(model);
        
        log.info("Done!", className);
        
        
    }
}
