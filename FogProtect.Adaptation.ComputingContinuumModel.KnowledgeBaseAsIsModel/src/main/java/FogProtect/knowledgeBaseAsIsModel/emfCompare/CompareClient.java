package FogProtect.knowledgeBaseAsIsModel.emfCompare;

import com.google.common.base.Function;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.internal.spec.AttributeChangeSpec;
import org.eclipse.emf.compare.internal.spec.ReferenceChangeSpec;
import org.eclipse.emf.compare.match.*;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.IdentifierEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class CompareClient {


    /**
     * @param before model before changes were applied
     * @param after model with changed objects & refs
     * @return JSONObject containing the differences in objects and references
     */
    public static JsonObject compareModels(EObject before, EObject after) {
   	
        List<ChangedObj> changedObjects = new ArrayList<>();
        List<ChangedReference> changedRefObjects = new ArrayList<>();

        List<Diff> differences = getDiffs(after, before);

        //differences.forEach(a -> System.out.println(a));

        List<AttributeChangeSpec> changedAttributes = differences.stream()
                //.filter(diff -> diff.getKind().equals(DifferenceKind.CHANGE))
                .filter(diff -> diff instanceof AttributeChangeSpec)
                .map(changes -> (AttributeChangeSpec) changes)
                .collect(Collectors.toList());

        List<ReferenceChangeSpec> changedReferences = differences.stream()
                //.filter(diff -> diff.getKind().equals(DifferenceKind.CHANGE))
                .filter(diff -> diff instanceof ReferenceChangeSpec)
                .map(changes -> (ReferenceChangeSpec) changes)
                .collect(Collectors.toList());

        changedObjects = getChangedObjects(changedAttributes);
        changedRefObjects = getChangedRelations(changedReferences);

        return createDiffJson(changedObjects, changedRefObjects);
    }

    static private List<ChangedObj> getChangedObjects(List<AttributeChangeSpec> changedAttributes) {

        List<ChangedObj> changedObjects = new ArrayList<>();

        for (AttributeChangeSpec changeSpec : changedAttributes) {

            Object oldAttrValue = null;
            Object newAttrValue = changeSpec.getValue();

            Match match = changeSpec.getMatch();

            EObject oldEObj = match.getRight();
            EObject newEObj = match.getLeft();

            // find old value
            if (newEObj != null) {
                oldAttrValue = oldEObj.eGet(changeSpec.getAttribute());
            } else {
            	//ignored for logger
                System.out.println("getRight() is null in compareModels()");
            }

            EStructuralFeature nameFeature = oldEObj.eClass().getEStructuralFeature("name");
            // EStructuralFeature typeFeature = asIs.eClass().getEStructuralFeature("type");

            String objName = oldEObj.eGet(nameFeature).toString();
            String objAtId = oldEObj.eResource().getURIFragment(oldEObj);
            String objType = oldEObj.eClass().getInstanceTypeName();

            String attrName = changeSpec.getAttribute().getName();
            String attrType = changeSpec.getAttribute().getEType().getName();
            String changeType = changeSpec.getKind().toString();

            // all values set
            ChangedObj obj = new ChangedObj(objAtId, objName, objType);
            ChangedAttribute attr = new ChangedAttribute(attrName, attrType, oldAttrValue.toString(),
                    newAttrValue.toString(), changeType);

            if (changedObjects.contains(obj)) {
                changedObjects.stream().forEach(a -> {
                    if (a.equals(obj)) {
                        a.getChanges().add(attr);
                    }
                });
            } else {
                obj.getChanges().add(attr);
                changedObjects.add(obj);
            }
        }
        return changedObjects;
    }

    static private List<ChangedReference> getChangedRelations(List<ReferenceChangeSpec> changedAttributes) {
        List<ChangedReference> changedRefObjects = new ArrayList<>();

        for (ReferenceChangeSpec changeSpec : changedAttributes) {
            String changeType = changeSpec.getKind().toString();
            String refName = changeSpec.getReference().getName();

            Match match = changeSpec.getMatch();

            EObject objFrom = match.getLeft();
            EObject objTo = changeSpec.getValue();

            //String objectName, String objectType, String objectAtID
            EStructuralFeature nameFeature = objFrom != null ? objFrom.eClass().getEStructuralFeature("name") : null;
            String objFromName = "";
        	String objFromtAtId = "";
            String objFromType = "";
            try{
            	objFromName = objFrom != null ? objFrom.eGet(nameFeature).toString() : "deleted";
            }catch(Exception e) {
            	objFromName = "/";
            }
            objFromtAtId = objFrom != null ? objFrom.eResource().getURIFragment(objFrom) : "deleted";
            objFromType = objFrom != null ?  objFrom.eClass().getInstanceTypeName() : "deleted";
            
            
            EStructuralFeature nameFeature1 = objTo.eClass().getEStructuralFeature("name");
            String objToName = "";
            String objTotAtId = "";
            String objToType = "";
            try {
            	objToName = objTo.eGet(nameFeature1).toString();
            }catch(Exception e) {
            	objToName = "/";
            }
            objTotAtId = objTo.eResource().getURIFragment(objTo);
            objToType = objTo.eClass().getInstanceTypeName();

            ObjectToIdentify objectToIdentify_From = new ObjectToIdentify(objFromName, objFromType, objFromtAtId);
            ObjectToIdentify objectToIdentify_To = new ObjectToIdentify(objToName, objToType, objTotAtId);

            ChangedReference cr = new ChangedReference(refName, objectToIdentify_From, objectToIdentify_To, changeType);
            changedRefObjects.add(cr);
        }

        return changedRefObjects;
    }

    /**
     * @return differences between objects based on custom matching (matching by URIFragment)
     */
    public static List<Diff> getDiffs(EObject before, EObject after) {

        // only us
        IEObjectMatcher fallBackMatcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.WHEN_AVAILABLE);
        IEObjectMatcher customNameMatcher = new IdentifierEObjectMatcher(fallBackMatcher, idFunction1);

        IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
        IMatchEngine.Factory.Registry registry = MatchEngineFactoryRegistryImpl.createStandaloneInstance();
        final MatchEngineFactoryImpl matchEngineFactory = new MatchEngineFactoryImpl(customNameMatcher,
                comparisonFactory);

        matchEngineFactory.setRanking(20); // default engine ranking is 10, must be higher to override.
        registry.add(matchEngineFactory);

        IComparisonScope scope = new DefaultComparisonScope(before, after, null);
        Comparison result = EMFCompare.builder().setMatchEngineFactoryRegistry(registry).build().compare(scope);

        return result.getDifferences();
    }

    /**
     * compare based on name
     */
    static Function<EObject, String> idFunction = new com.google.common.base.Function<EObject, String>() {
        public String apply(EObject input) {
            if (input instanceof EObject) {
                EObject obj = input;
                EStructuralFeature nameFeature = obj.eClass().getEStructuralFeature("name");
                if (nameFeature != null) {
                    return obj.eGet(nameFeature).toString();
                } else {
                    return null;
                }
            }
            // a null return here tells the match engine to fall back to the other matchers
            return null;
        }
    };

    /**
     * compare based on URIFragemnt
     */
   static Function<EObject, String> idFunction1 = new com.google.common.base.Function<EObject, String>() {
        public String apply(EObject input) {
            if (input instanceof EObject) {
                EObject obj = input;

                String objAtId = obj.eResource().getURIFragment(obj);

                return objAtId;
            }
            // a null return here tells the match engine to fall back to the other matchers
            return null;
        }
    };

    static private JsonObject createDiffJson(List<ChangedObj> changes, List<ChangedReference> refChanges) {
    	Gson gson = new Gson();
    	JsonArray jarr = new JsonArray();
    	
    	for(ChangedObj change : changes) {
    		if(change.getChanges().stream().noneMatch(c -> c.getChangeType().equals("MOVE"))) {
    			JsonObject jobj = (JsonObject) new JsonParser().parse(gson.toJson(change));
    			jarr.add(jobj);
    		}
    	}
    	
    	JsonObject jObj = new JsonObject();
        jObj.add("ChangesMadeToAsIsModel", jarr);

        JsonArray jarr1 = new JsonArray();
        
        for(ChangedReference change : refChanges) {
        	if(!change.getChangeType().equals("MOVE")) {
        		JsonObject jobj = (JsonObject) new JsonParser().parse(gson.toJson(change));
        		jarr.add(jobj);
        	}
    	}
        
        jObj.add("ReferenceChanges", jarr1);

        
        jObj = (JsonObject) new JsonParser().parse(jObj.toString().replace("object", "Object").replace("AtID", "@ID")
                .replace("changes", "Changes").replace("attribute", "Attribute"));

        /*
         * jObj = new JSONObject(jObj.toString() .replaceAll("", replacement))
         *
         */

        return jObj;
    }

    private EObject searchForObjectInGivenModel(CloudEnvironment cloudEnvironment, String id) {
        EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
        for (tosca_nodes_Root element : nodes) {
            Resource resource = element.eResource();
            String toTest = (resource).getURIFragment(element);

            if (toTest.equals(id)) {
                return element;
            }
        }
        return null;
    }
}
