package FogProtect.knowledgeBaseAsIsModel.emfCompare;

public class ObjectToIdentify {
 
	String name;
	String type;
	String atid;
	
	public ObjectToIdentify(String name, String type, String atid) {
		super();
		this.name = name;
		this.type = type;
		this.atid = atid;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAtid() {
		return atid;
	}
	public void setAtid(String atid) {
		this.atid = atid;
	}
 
	
	
}