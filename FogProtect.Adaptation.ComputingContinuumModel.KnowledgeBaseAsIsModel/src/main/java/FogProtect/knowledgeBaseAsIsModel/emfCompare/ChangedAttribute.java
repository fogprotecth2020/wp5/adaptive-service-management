package FogProtect.knowledgeBaseAsIsModel.emfCompare;

public class ChangedAttribute {

    String ChangeType;
    String AttributeChanged;
    String AttributeType;
    String AttributeOldValue;
    String AttributeNewValue;

    public ChangedAttribute(String attrName, String type, String oldValue, String newValue, String changeType) {
        super();
        this.ChangeType = changeType;
        this.AttributeChanged = attrName;
        this.AttributeType = type;
        this.AttributeOldValue = oldValue;
        this.AttributeNewValue = newValue;
    }

    //-------getters and setters---------------

    public String getChangeType() {
        return ChangeType;
    }

    public String getAttributeChanged() {
        return AttributeChanged;
    }

    public String getAttributeType() {
        return AttributeType;
    }

    public String getAttributeOldValue() {
        return AttributeOldValue;
    }

    public String getAttributeNewValue() {
        return AttributeNewValue;
    }

    public void setChangeType(String changeType) {
        ChangeType = changeType;
    }

    public void setAttributeChanged(String attributeChanged) {
        AttributeChanged = attributeChanged;
    }

    public void setAttributeType(String attributeType) {
        AttributeType = attributeType;
    }

    public void setAttributeOldValue(String attributeOldValue) {
        AttributeOldValue = attributeOldValue;
    }

    public void setAttributeNewValue(String attributeNewValue) {
        AttributeNewValue = attributeNewValue;
    }
}
