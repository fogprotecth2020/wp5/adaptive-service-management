package FogProtect.knowledgeBaseAsIsModel.rest;

import FogProtect.knowledgeBaseAsIsModel.cdo.CdoClient;
import FogProtect.knowledgeBaseAsIsModel.emfCompare.CompareClient;
import FogProtect.knowledgeBaseAsIsModel.kafka.ConsumerClient;
import FogProtect.knowledgeBaseAsIsModel.kafka.ProducerClient;
import FogProtect.knowledgeBaseAsIsModel.utils.Listener;
import FogProtect.knowledgeBaseAsIsModel.utils.ModelLoader;
import FogProtect.knowledgeBaseAsIsModel.utils.Modeltype;
import FogProtect.knowledgeBaseAsIsModel.utils.Node;
import FogProtect.knowledgeBaseAsIsModel.utils.PersonalLogger;
import FogProtect.knowledgeBaseAsIsModel.utils.Risk;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.eclipse.emf.ecore.EObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class MyService {

	@Autowired
	ProducerClient producerClient;

	@Autowired
	KafkaAdmin kafkaAdmin;

	@Autowired
	ConsumerClient consumerClient;

	@Autowired
	ModelLoader loader;

	@Autowired
	CdoClient cdo;

	@Autowired
	MonitoringController monitoringController;
	
	@Autowired
	PersonalLogger log;
	
	private static String className = MyService.class.getName();

	// todo logic if neccessary otherwise just pass requests/params forward to
	// consumer or retrieve from producer

	/**
	 * trys to send a message to the selected topic
	 */
	public ResponseEntity<String> writeStringToTopic(String topic, String message) {

		boolean success = producerClient.sendMessage(topic, message);
		return success ? new ResponseEntity<String>("Message: {" + message + "} was sent successfully", HttpStatus.OK)
				: new ResponseEntity<String>("there was an error trying to send the message",
						HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * creates a new topic if the selected name is not already used for an existing
	 * topic
	 */
	public ResponseEntity<String> createTopic(String topicName) {

		AdminClient adminClient = AdminClient.create(kafkaAdmin.getConfigurationProperties());

		boolean topicExists = false;
		try {
			topicExists = adminClient.listTopics().names().get().stream()
					.anyMatch(name -> name.equalsIgnoreCase(topicName));
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		if (!topicExists) {
			NewTopic topic = new NewTopic(topicName, 1, (short) 1);
			adminClient.createTopics(List.of(topic));
			return new ResponseEntity<String>("Created topic: {" + topicName + "}", HttpStatus.OK);
		}
		return new ResponseEntity<String>(
				"could not create topic: {" + topicName + "} because a topic with that name already exists",
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public ResponseEntity<String> deleteTopic(String topicName) {

		AdminClient adminClient = AdminClient.create(kafkaAdmin.getConfigurationProperties());

		boolean topicExists = false;
		try {
			topicExists = adminClient.listTopics().names().get().stream()
					.anyMatch(name -> name.equalsIgnoreCase(topicName));
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		if (topicExists) {
			adminClient.deleteTopics(List.of(topicName));
			return new ResponseEntity<String>("deleted topic: {" + topicName + "}", HttpStatus.OK);
		}
		return new ResponseEntity<String>("topic: {" + topicName + "} does not exist",
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	public void notifyAllAboutChanges() {
		this.notifyAllAboutChanges("-1");
	}

	public void notifyAllAboutChanges(String taskId) {

		String json = createStatusInformationJson(taskId);
		try {
			log.writeToFile(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Listener listener : monitoringController.getListeners()) {

			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						int returnCode = makeRequest(listener.getUrl(), json);
						if (returnCode == 400) {
							log.error("Return code for notifying Listener is 400", className);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		}
		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				writeStringToTopic("ModelChange4SIEA", json);
			}
		});
		t1.start();

	}

	public void notifyAdaptationCoordinator() {

		String json = createStatusInformationJson();

		for (Listener listener : monitoringController.getListeners()) {

			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						if (listener.getUrl().contains("7770")) {
							int returnCode = makeRequest(listener.getUrl(), json);
							if (returnCode == 400) {
								log.error("Return code for notifying Listener is 400", className);
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
		}

	}

	// Post Request to given URL with given String as Body
	public int makeRequest(String url, String requestBody) throws IOException {
		log.jsonInfo(requestBody, className);
		HttpURLConnection request;
		URL restUrl = new URL(url);
		request = (HttpURLConnection) restUrl.openConnection();
		request.setRequestMethod("POST");
		request.setDoOutput(true);
		Charset encoding = Charset.forName("UTF-8");
		request.setRequestProperty("Content-Type", "application/json");
		request.getOutputStream().write(requestBody.getBytes(encoding));
		log.info("Request sent to " + url, className);
		int responseCode = request.getResponseCode();
		return responseCode;
	}
	
	public String createStatusInformationJson() {
		return this.createStatusInformationJson("-1");
	}

	public String createStatusInformationJson(String taskId) {
		JsonObject head = new JsonObject();
		JsonObject asIs = new JsonObject();
		JsonArray risks = new JsonArray();
		if (cdo.retrieveMostRecentModel(Modeltype.AS_IS_MODEL) != null) {
			JsonObject asIsModel = new JsonParser()
					.parse(loader
							.modelToJsonString((CloudEnvironment) cdo.retrieveMostRecentModel(Modeltype.AS_IS_MODEL)))
					.getAsJsonObject();
			asIs.add("AsIsModel", asIsModel);
		} else {
			asIs.add("AsIsModel", null);
		}
		if (cdo.retrieveSecondRecentModel(Modeltype.AS_IS_MODEL) != null) {
			JsonObject oldasIsModel = new JsonParser()
					.parse(loader
							.modelToJsonString((CloudEnvironment) cdo.retrieveSecondRecentModel(Modeltype.AS_IS_MODEL)))
					.getAsJsonObject();
			asIs.add("OldAsIsModel", oldasIsModel);
		} else {
			asIs.add("OldAsIsModel", null);
		}

		if (cdo.retrieveMostRecentModel(Modeltype.AS_IS_MODEL) != null
				&& cdo.retrieveSecondRecentModel(Modeltype.AS_IS_MODEL) != null) {
			EObject asis = cdo.retrieveMostRecentModel(Modeltype.AS_IS_MODEL);
			EObject oldAsis = cdo.retrieveSecondRecentModel(Modeltype.AS_IS_MODEL);
			JsonObject jObj = CompareClient.compareModels(oldAsis, asis);
			asIs.add("ChangesMadeToAsIsModel", jObj.get("ChangesMadeToAsIsModel").getAsJsonArray());
		} else {
			asIs.add("ChangesMadeToAsIsModel", new JsonArray());
		}

		asIs.add("Risks", risks);

		if (!taskId.equals("-1"))
			 head.addProperty("SieaTaskId", taskId);
		
		head.add("AsIs", asIs);

		JsonArray adaptations = new JsonArray();

		head.add("Adaptations", adaptations);
		return head.toString();
	}

	// testing

	public ResponseEntity<String> testCdo1() {
		String jsonString = "{ \"type\" : \"CloudEnvironment\", \"@id\" : \"/\", \"tosca_nodes_root\" : [ { \"type\" : \"FogCompute\", \"@id\" : \"//@tosca_nodes_root.0\", \"name\" : \"NUC 1 (Botcraft VM and IDS VM)\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.1\", \"name\" : \"Dashboard Server\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.13\" }, \"controlledBy\" : [ { \"type\" : \"DataController\", \"referencedObjectID\" : \"//@tosca_nodes_root.32\" } ], \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.28\" } ] }, { \"type\" : \"Database\", \"@id\" : \"//@tosca_nodes_root.2\", \"name\" : \"Database\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" }, \"stores\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"//@tosca_nodes_root.11\" } ], \"encrypted\" : false }, { \"type\" : \"DBMS\", \"@id\" : \"//@tosca_nodes_root.3\", \"name\" : \"IDS Data Lake\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.13\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.28\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" } ], \"hosts\" : [ { \"type\" : \"Database\", \"referencedObjectID\" : \"//@tosca_nodes_root.2\" } ] }, { \"type\" : \"DataSubject\", \"@id\" : \"//@tosca_nodes_root.4\", \"name\" : \"Person\", \"id\" : 0, \"trust\" : [ { \"type\" : \"DataController\", \"referencedObjectID\" : \"//@tosca_nodes_root.32\" } ], \"location\" : \"DE\", \"owns\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.23\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.5\", \"name\" : \"Robot MES\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.0\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.19\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.20\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.6\", \"name\" : \"Kafka Server\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.0\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.8\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" } ] }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.7\", \"name\" : \"Scene Detection\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.14\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.21\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.8\" } ] }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.8\", \"name\" : \"Data Flow (Scene Detection - Kafka Server)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.7\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"IoTDevice\", \"@id\" : \"//@tosca_nodes_root.9\", \"name\" : \"ABB Robot\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.18\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.25\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"IoTDevice\", \"@id\" : \"//@tosca_nodes_root.10\", \"name\" : \"IP Camera\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.22\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"StoredDataSet\", \"@id\" : \"//@tosca_nodes_root.11\", \"name\" : \"Stored Data Set\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.30\" } ], \"storedIn\" : { \"type\" : \"Database\", \"referencedObjectID\" : \"//@tosca_nodes_root.2\" } }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.12\", \"name\" : \"Scene Data\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.8\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.28\" }, { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"//@tosca_nodes_root.11\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" } ], \"sensitive\" : false, \"encrypted\" : false, \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"//@tosca_nodes_root.4\" }, \"personal\" : true }, { \"type\" : \"FogCompute\", \"@id\" : \"//@tosca_nodes_root.13\", \"name\" : \"NUC 2 (FogProtectVM)\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" }, { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"FogCompute\", \"@id\" : \"//@tosca_nodes_root.14\", \"name\" : \"Google ML Board-1\", \"id\" : 0, \"hosts\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.7\" } ], \"jurisdiction\" : \"DE\", \"usageCostPerDay\" : 0.0, \"capacity\" : 0, \"transferCostPerGB\" : 0.0, \"costIncurred\" : false, \"compromised\" : false, \"partOf\" : { \"type\" : \"PrivateSpace\", \"referencedObjectID\" : \"//@tosca_nodes_root.31\" } }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.15\", \"name\" : \"Robot Monitor Data\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.16\", \"name\" : \"Data Flow (Dashboard Server - Kafka Server)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.15\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.26\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.30\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.17\", \"name\" : \"Data Flow (Robot MES - Kafka Server)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.26\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.15\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.18\", \"name\" : \"Robot Control\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.9\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.19\" } ] }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.19\", \"name\" : \"Data Flow (Robot Control - Robot MES)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.24\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.18\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.20\", \"name\" : \"Data Flow (Robot Monitor - Robot MES)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.27\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.25\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.5\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.21\", \"name\" : \"Data Flow (Camera Streamer - Scene Detection)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.23\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.22\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.7\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.22\", \"name\" : \"Camera Streamer\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.10\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.21\" } ] }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.23\", \"name\" : \"Camera Stream\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.21\" } ], \"sensitive\" : false, \"encrypted\" : false, \"belongsTo\" : { \"type\" : \"DataSubject\", \"referencedObjectID\" : \"//@tosca_nodes_root.4\" }, \"personal\" : true }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.24\", \"name\" : \"Robot Control Data\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.19\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"SoftwareComponent\", \"@id\" : \"//@tosca_nodes_root.25\", \"name\" : \"Robot Monitor\", \"id\" : 0, \"hostedOn\" : { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.9\" }, \"neededCapacity\" : 0, \"hasToBeDeployedOnFogNode\" : false, \"transferBy\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.20\" } ] }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.26\", \"name\" : \"Order\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.17\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.27\", \"name\" : \"Robot Monitor Data Stream\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.20\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.28\", \"name\" : \"Data Flow (Dashboard Server - IDS Data Lake)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" }, { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"DataFlow\", \"@id\" : \"//@tosca_nodes_root.29\", \"name\" : \"Data Flow (Kafka Server - IDS Data Lake)\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.12\" }, { \"type\" : \"Record\", \"referencedObjectID\" : \"//@tosca_nodes_root.30\" } ], \"disab\" : false, \"amountOfDataInGB\" : 0.0, \"transfersTo\" : [ { \"type\" : \"DBMS\", \"referencedObjectID\" : \"//@tosca_nodes_root.3\" }, { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.6\" } ], \"connectionType\" : \"Local\" }, { \"type\" : \"Record\", \"@id\" : \"//@tosca_nodes_root.30\", \"name\" : \"Security Flag\", \"id\" : 0, \"partOf\" : [ { \"type\" : \"StoredDataSet\", \"referencedObjectID\" : \"//@tosca_nodes_root.11\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.29\" }, { \"type\" : \"DataFlow\", \"referencedObjectID\" : \"//@tosca_nodes_root.16\" } ], \"sensitive\" : false, \"encrypted\" : false, \"personal\" : false }, { \"type\" : \"PrivateSpace\", \"@id\" : \"//@tosca_nodes_root.31\", \"name\" : \"FiaB Shipping Container\", \"id\" : 0, \"consistsOf\" : [ { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.0\" }, { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.13\" }, { \"type\" : \"FogCompute\", \"referencedObjectID\" : \"//@tosca_nodes_root.14\" }, { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.9\" }, { \"type\" : \"IoTDevice\", \"referencedObjectID\" : \"//@tosca_nodes_root.10\" } ], \"trustworthy\" : \"HIGH\" }, { \"type\" : \"DataController\", \"@id\" : \"//@tosca_nodes_root.32\", \"name\" : \"FiaB Worker\", \"id\" : 0, \"location\" : \"DE\", \"controls\" : [ { \"type\" : \"SoftwareComponent\", \"referencedObjectID\" : \"//@tosca_nodes_root.1\" } ] } ] }";
		log.jsonInfo("----------------------------------------------" + jsonString, className);

		try {
			EObject e = loader.jsonStringToEObject(jsonString);
			cdo.storeModel((CloudEnvironment) e, Modeltype.AS_IS_MODEL);

			log.info("stored model", className);

			EObject e1 = cdo.retrieveMostRecentModel(Modeltype.AS_IS_MODEL);

			// ignored for logger
			e1.eContents().forEach(x -> System.out.println(x.eResource()));

			log.info("retrieved model", className);

			return new ResponseEntity<String>("cdoTest completed", HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
