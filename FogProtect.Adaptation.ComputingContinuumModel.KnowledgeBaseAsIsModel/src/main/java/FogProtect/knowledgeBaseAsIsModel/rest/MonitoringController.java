package FogProtect.knowledgeBaseAsIsModel.rest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.impl.EReferenceImpl;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import FogProtect.knowledgeBaseAsIsModel.cdo.CdoClient;
import FogProtect.knowledgeBaseAsIsModel.utils.Listener;
import FogProtect.knowledgeBaseAsIsModel.utils.ModelLoader;
import FogProtect.knowledgeBaseAsIsModel.utils.Modeltype;
import FogProtect.knowledgeBaseAsIsModel.utils.PersonalLogger;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelFactory;
import fogprotect.adaptation.ComputingContinuumModel.FogCompute;
import fogprotect.adaptation.ComputingContinuumModel.Trustworthy;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import fogprotect.adaptation.ComputingContinuumModel.impl.FogComputeImpl;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;

@RestController
public class MonitoringController {

	@Autowired
	private ModelLoader loader;

	@Autowired
	private CdoClient cdo;

	@Autowired
	private MyService myService;

	@Autowired
	PersonalLogger log;

	private static String className = MonitoringController.class.getName();

	private LinkedList<Listener> listeners = new LinkedList<Listener>();

	/*
	 * Used to register a new AsIsModel with the FogProtect application: This web
	 * service allows the monitoring adapter of a system to register with the
	 * adaptation component. The request body needs to contain a JSON representation
	 * of a AsIsModel to be persisted.
	 */

	@RequestMapping(path = "/fogprotect/knowledgebaseasismodel/registerManagedSystem", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public long registerManagedSystem(@RequestBody(required = true) String jsonAsIsModel) {
		String decodedWithEqualsSign;
		try {
			decodedWithEqualsSign = URLDecoder.decode(jsonAsIsModel, "UTF-8");
			// Create Object out of JSON
			CloudEnvironment cloudEnvironment = (CloudEnvironment) loader.jsonStringToEObject(jsonAsIsModel);

			log.info("Registered Managed System", className);
			cdo.storeModel(cloudEnvironment, Modeltype.AS_IS_MODEL);
//			myService.notifyAllAboutChanges();
		} catch (UnsupportedEncodingException e) {
			log.error(e.toString(), className);
			return 400;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
			return 400;
		}
		return 200;
	}

	/*
	 * This web service allows the monitoring adapter to replace an AsIsModel. The
	 * replacement AsIsModel should be supplied in the request body as a JSON
	 * object.
	 */
	@RequestMapping(path = "/fogprotect/knowledgebaseasismodel/updateAsIsModel", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public long updateAsIsModel(@RequestBody(required = true) String jsonAsIsModel) {
		String decodedWithEqualsSign;
		try {
			decodedWithEqualsSign = URLDecoder.decode(jsonAsIsModel, "UTF-8");
			// Create Object out of JSON
			CloudEnvironment cloudEnvironment = (CloudEnvironment) loader.jsonStringToEObject(jsonAsIsModel);

			log.info("Received Monitoring Event", className);
			cdo.storeModel(cloudEnvironment, Modeltype.AS_IS_MODEL);
			myService.notifyAllAboutChanges();
		} catch (UnsupportedEncodingException e) {
			log.error(e.toString(), className);
			return 400;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
			return 400;
		}
		return 200;
	}

	//FIXME adaptationResult possibleError
	@RequestMapping(path = "/fogprotect/knowledgebaseasismodel/adaptationResult", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public long adaptationResult(@RequestBody(required = true) String bodyJson) {
		
		String decodedWithEqualsSign;
		
		try {			
			decodedWithEqualsSign = URLDecoder.decode(bodyJson, "UTF-8");
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
			String adaptationResult = receivedJson.get("result").getAsString();
			String taskId= receivedJson.get("SieaTaskId").getAsString();
		
			log.jsonInfo("Adaptation Result: " + adaptationResult, className);
			if (adaptationResult.equals("AdaptationNotPossible")) {
				log.info("Adaptation Not Possible", className);
				Thread t1 = new Thread(new Runnable() {
	
					@Override
					public void run() {
						
						JsonObject head = new JsonObject();
						head.addProperty("SieaTaskId", taskId);
						//TODO maybe change property
						head.addProperty("Result", "AdaptationNotPossible");
						log.jsonInfo(head.toString(), className);
						myService.writeStringToTopic("AdaptationChange4SIEA", head.toString());
					}
				});
				t1.start();
			} else {
				log.info("Adaptation Executed", className);
				try {
					String modelJson = receivedJson.get("ExecutedModel").getAsJsonObject().toString();
					
					CloudEnvironment cloudEnvironment = (CloudEnvironment) loader.jsonStringToEObject(modelJson);
					cdo.storeModel(cloudEnvironment, Modeltype.AS_IS_MODEL);
					Thread t1 = new Thread(new Runnable() {
						@Override
						public void run() {
							myService.writeStringToTopic("AdaptationChange4SIEA", myService.createStatusInformationJson(taskId));
						}
					});
					t1.start();
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
				myService.notifyAllAboutChanges(taskId);
			}
		}	
		catch (UnsupportedEncodingException e) {
			log.error(e.toString(), className);
			return 400;
		}
		return 200;
	}

//	@RequestMapping(path = "/fogprotect/knowledgebaseasismodel/monitoringEvent", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
//	@ResponseBody
//	public long monitoringEvent(@RequestBody(required = true) String jsonAsIsModel) {
//		String decodedWithEqualsSign;
//		try {
//			System.out.println("Received Monitoring Event");
//			decodedWithEqualsSign = URLDecoder.decode(jsonAsIsModel, "UTF-8");
//			// Create Object out of JSON
//			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
//			String atId = receivedJson.get("@id").getAsString();
//			String attributeToChange = receivedJson.get("attribute").getAsString();
//			String newValue = receivedJson.get("newValue").getAsString();
//			String attributeType = receivedJson.get("attributeType").getAsString();
//			
//			CloudEnvironment cloudEnvironment = loader.getAsIsModel().getModel();
//			String temp = runtimeModelLogic.returnentiremodelasjson(cloudEnvironment);
//			CloudEnvironment oldAsIsModel = (CloudEnvironment)  loader.loadEObjectFromString(temp);
//			loader.setOldAsIsModel(new Model(oldAsIsModel, (ArrayList<Risk>) loader.getAsIsModel().getRisks().clone()));
//			EObject object = runtimeModelLogic.searchForObjectInGivenModel(cloudEnvironment, atId);
//			EStructuralFeature feature = object.eClass().getEStructuralFeature(attributeToChange);
//			if(attributeType.equals("String")) {
//				object.eSet(feature, newValue);
//			}else if(attributeType.equals("boolean")) {
//				Boolean newValueBool = Boolean.parseBoolean(newValue);
//				object.eSet(feature, newValueBool);
//			}else if(attributeType.equals("int")) {
//				Integer newValueInt = Integer.getInteger(newValue);
//				object.eSet(feature, newValueInt);
//			}else if(attributeType.equals("Trustworthy")) {
//				object.eSet(feature, Trustworthy.getByName(newValue));
//			}
//					
//			
//			//add monitoring event that changes relations / delete or insert new objects
//			loader.getAsIsModel().setRisks(new ArrayList<Risk>());
//			System.out.println("OldAsIsModelRisksTest: " + loader.getOldAsIsModel().getRisks());
//			System.out.println("NewAsIsModelRisksTest: " + loader.getAsIsModel().getRisks());
//			runtimeModelLogic.notifyAllAboutChanges();
//		} catch (UnsupportedEncodingException e) {
//			System.err.println(e);
//			return 400;
//		} catch (IOException e) {
//			//  Auto-generated catch block
//			e.printStackTrace();
//			return 400;
//		}
//		return 200;
//	}

	@RequestMapping(path = "/fogprotect/knowledgebaseasismodel/monitoringEvent", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public long monitoringEvent(@RequestBody(required = true) String jsonAsIsModel) {
		String decodedWithEqualsSign;
		try {
			log.info("Received Monitoring Event", className);
			decodedWithEqualsSign = URLDecoder.decode(jsonAsIsModel, "UTF-8");
			// Create Object out of JSON
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();

			CloudEnvironment cloudEnvironment = (CloudEnvironment) cdo.retrieveMostRecentModel(Modeltype.AS_IS_MODEL);
			String temp = loader.modelToJsonString(cloudEnvironment);
			cloudEnvironment = (CloudEnvironment) loader.jsonStringToEObject(temp);

			String requestType = receivedJson.get("RequestType").getAsString();// Determines what kind of Request the
																				// Json is

			if (requestType.equals("changeAttribute"))// To change the Value of an Existing Attribute
			{
				String atId = receivedJson.get("@id").getAsString();
				String attributeToChange = receivedJson.get("attribute").getAsString();
				String newValue = receivedJson.get("newValue").getAsString();
				String attributeType = receivedJson.get("attributeType").getAsString();

				EObject object = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				EStructuralFeature feature = object.eClass().getEStructuralFeature(attributeToChange);
				if (attributeType.equals("String")) {
					object.eSet(feature, newValue);
				} else if (attributeType.equals("boolean")) {
					Boolean newValueBool = Boolean.parseBoolean(newValue);
					object.eSet(feature, newValueBool);
				} else if (attributeType.equals("int")) {
					Integer newValueInt = Integer.parseInt(newValue);
					object.eSet(feature, newValueInt);
				} else if (attributeType.equals("double")) {
					Double newValueDouble = Double.parseDouble(newValue);
					object.eSet(feature, newValueDouble);
				} else if (attributeType.equals("Trustworthy")) {
					object.eSet(feature, Trustworthy.getByName(newValue));
				}
			}

			if (requestType.equals("addRelation"))// Create a New Relation between Two EObject
			{
				log.info("Add Relation", className);
				String atId = receivedJson.get("@id").getAsString();
				String TargetID = receivedJson.get("targetid").getAsString();
				String relationName = receivedJson.get("relationName").getAsString();

				EObject SourceObject = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				EObject TargetObject = loader.searchForObjectInGivenModel(cloudEnvironment, TargetID);
				EStructuralFeature RelationToChange = SourceObject.eClass().getEStructuralFeature(relationName);
				if (RelationToChange != null) {
					if (RelationToChange.isMany()) {
						List<Object> RelationList = (List<Object>) SourceObject.eGet(RelationToChange);
						RelationList.add(TargetObject);
					} else {
						SourceObject.eSet(RelationToChange, TargetObject);
					}
				} else {
					return 400;
				}
			}

			if (requestType.equals("deleteRelation"))// Delete Relation
			{
				// System.out.println("Delete Relation");
				String atId = receivedJson.get("@id").getAsString();
				String TargetID = receivedJson.get("targetid").getAsString();
				String relationName = receivedJson.get("relationName").getAsString();

				EObject SourceObject = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				EObject TargetObject = loader.searchForObjectInGivenModel(cloudEnvironment, TargetID);
				EStructuralFeature StructureToDelete = SourceObject.eClass().getEStructuralFeature(relationName);

				EReferenceImpl RefToDelete = (EReferenceImpl) SourceObject.eClass().getEStructuralFeature(relationName);
				EReference OppositeRelation = RefToDelete.getEOpposite();
				EStructuralFeature OppositeFeature = TargetObject.eClass()
						.getEStructuralFeature(OppositeRelation.getName());

				if (StructureToDelete != null) {
					// Delete the Relation
					if (StructureToDelete.isMany())// Relation has more Elements
					{
						List<Object> RelationList = (List<Object>) SourceObject.eGet(StructureToDelete);

						if (RelationList.size() == 1)// Relation is Array with 1 Element
						{
							SourceObject.eUnset(StructureToDelete);
						} else {
							RelationList.remove(TargetObject);
						}
					} else {
						SourceObject.eUnset(StructureToDelete);
					}

					// Delete the Opposite
					if (OppositeFeature.isMany())// Relation has more Elements
					{
						List<Object> RelationList = (List<Object>) TargetObject.eGet(OppositeFeature);

						if (RelationList.size() == 1)// Relation is Array with 1 Element
						{
							TargetObject.eUnset(OppositeFeature);
						} else {
							RelationList.remove(SourceObject);
						}
					} else {
						TargetObject.eUnset(OppositeFeature);
					}
				} else {
					return 400;
				}

			}

			// TODO Fix Bug that URI is similiar to the other objects
			if (requestType.equals("addNode")) {
				String NodeType = receivedJson.get("nodeType").getAsString();
				String NodeName = receivedJson.get("nodeName").getAsString();
				EObject nodeToAdd = null;

				ComputingContinuumModelFactory factory = ComputingContinuumModelFactory.eINSTANCE;
				try {
					Method methodToCreateObject = factory.getClass().getMethod("create" + NodeType);
					nodeToAdd = (EObject) methodToCreateObject.invoke(factory);
					if (nodeToAdd != null) {
						EStructuralFeature feature = nodeToAdd.eClass().getEStructuralFeature("name");
						nodeToAdd.eSet(feature, NodeName);
						cloudEnvironment.getTosca_nodes_root().add((tosca_nodes_Root) nodeToAdd);
						log.info("done", className);
					} else {
						return 400;
					}
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
					return 400;
				}
			}

			if (requestType.equals("deleteNode")) {
				String atId = receivedJson.get("@id").getAsString();
				EObject objectToRemove = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				if (objectToRemove != null) {
					this.deleteAllRelation(objectToRemove);
					cloudEnvironment.getTosca_nodes_root().remove(objectToRemove);
				} else {
					return 400;
				}
			}

			cdo.storeModel(cloudEnvironment, Modeltype.AS_IS_MODEL);
			myService.notifyAllAboutChanges();
		} catch (UnsupportedEncodingException e) {
			log.error(e.toString(), className);
			return 400;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
			return 400;
		}
		return 200;
	}
	
	@RequestMapping(path = "/fogprotect/knowledgebaseasismodel/monitoringEventWithoutUpdate", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public long monitoringEventWithoutUpdate(@RequestBody(required = true) String jsonAsIsModel) {
		String decodedWithEqualsSign;
		try {
			log.info("Received Monitoring Event", className);
			decodedWithEqualsSign = URLDecoder.decode(jsonAsIsModel, "UTF-8");
			// Create Object out of JSON
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();

			CloudEnvironment cloudEnvironment = (CloudEnvironment) cdo.retrieveMostRecentModel(Modeltype.AS_IS_MODEL);
			String temp = loader.modelToJsonString(cloudEnvironment);
			cloudEnvironment = (CloudEnvironment) loader.jsonStringToEObject(temp);

			String requestType = receivedJson.get("RequestType").getAsString();// Determines what kind of Request the
																				// Json is

			if (requestType.equals("changeAttribute"))// To change the Value of an Existing Attribute
			{
				String atId = receivedJson.get("@id").getAsString();
				String attributeToChange = receivedJson.get("attribute").getAsString();
				String newValue = receivedJson.get("newValue").getAsString();
				String attributeType = receivedJson.get("attributeType").getAsString();

				EObject object = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				EStructuralFeature feature = object.eClass().getEStructuralFeature(attributeToChange);
				if (attributeType.equals("String")) {
					object.eSet(feature, newValue);
				} else if (attributeType.equals("boolean")) {
					Boolean newValueBool = Boolean.parseBoolean(newValue);
					object.eSet(feature, newValueBool);
				} else if (attributeType.equals("int")) {
					Integer newValueInt = Integer.parseInt(newValue);
					object.eSet(feature, newValueInt);
				} else if (attributeType.equals("double")) {
					Double newValueDouble = Double.parseDouble(newValue);
					object.eSet(feature, newValueDouble);
				} else if (attributeType.equals("Trustworthy")) {
					object.eSet(feature, Trustworthy.getByName(newValue));
				}
			}

			if (requestType.equals("addRelation"))// Create a New Relation between Two EObject
			{
				log.info("Add Relation", className);
				String atId = receivedJson.get("@id").getAsString();
				String TargetID = receivedJson.get("targetid").getAsString();
				String relationName = receivedJson.get("relationName").getAsString();

				EObject SourceObject = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				EObject TargetObject = loader.searchForObjectInGivenModel(cloudEnvironment, TargetID);
				EStructuralFeature RelationToChange = SourceObject.eClass().getEStructuralFeature(relationName);
				if (RelationToChange != null) {
					if (RelationToChange.isMany()) {
						List<Object> RelationList = (List<Object>) SourceObject.eGet(RelationToChange);
						RelationList.add(TargetObject);
					} else {
						SourceObject.eSet(RelationToChange, TargetObject);
					}
				} else {
					return 400;
				}
			}

			if (requestType.equals("deleteRelation"))// Delete Relation
			{
				// System.out.println("Delete Relation");
				String atId = receivedJson.get("@id").getAsString();
				String TargetID = receivedJson.get("targetid").getAsString();
				String relationName = receivedJson.get("relationName").getAsString();

				EObject SourceObject = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				EObject TargetObject = loader.searchForObjectInGivenModel(cloudEnvironment, TargetID);
				EStructuralFeature StructureToDelete = SourceObject.eClass().getEStructuralFeature(relationName);

				EReferenceImpl RefToDelete = (EReferenceImpl) SourceObject.eClass().getEStructuralFeature(relationName);
				EReference OppositeRelation = RefToDelete.getEOpposite();
				EStructuralFeature OppositeFeature = TargetObject.eClass()
						.getEStructuralFeature(OppositeRelation.getName());

				if (StructureToDelete != null) {
					// Delete the Relation
					if (StructureToDelete.isMany())// Relation has more Elements
					{
						List<Object> RelationList = (List<Object>) SourceObject.eGet(StructureToDelete);

						if (RelationList.size() == 1)// Relation is Array with 1 Element
						{
							SourceObject.eUnset(StructureToDelete);
						} else {
							RelationList.remove(TargetObject);
						}
					} else {
						SourceObject.eUnset(StructureToDelete);
					}

					// Delete the Opposite
					if (OppositeFeature.isMany())// Relation has more Elements
					{
						List<Object> RelationList = (List<Object>) TargetObject.eGet(OppositeFeature);

						if (RelationList.size() == 1)// Relation is Array with 1 Element
						{
							TargetObject.eUnset(OppositeFeature);
						} else {
							RelationList.remove(SourceObject);
						}
					} else {
						TargetObject.eUnset(OppositeFeature);
					}
				} else {
					return 400;
				}

			}

			// TODO Fix Bug that URI is similiar to the other objects
			if (requestType.equals("addNode")) {
				String NodeType = receivedJson.get("nodeType").getAsString();
				String NodeName = receivedJson.get("nodeName").getAsString();
				EObject nodeToAdd = null;

				ComputingContinuumModelFactory factory = ComputingContinuumModelFactory.eINSTANCE;
				try {
					Method methodToCreateObject = factory.getClass().getMethod("create" + NodeType);
					nodeToAdd = (EObject) methodToCreateObject.invoke(factory);
					if (nodeToAdd != null) {
						EStructuralFeature feature = nodeToAdd.eClass().getEStructuralFeature("name");
						nodeToAdd.eSet(feature, NodeName);
						cloudEnvironment.getTosca_nodes_root().add((tosca_nodes_Root) nodeToAdd);
						log.info("done", className);
					} else {
						return 400;
					}
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
					return 400;
				}
			}

			if (requestType.equals("deleteNode")) {
				String atId = receivedJson.get("@id").getAsString();
				EObject objectToRemove = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
				if (objectToRemove != null) {
					this.deleteAllRelation(objectToRemove);
					cloudEnvironment.getTosca_nodes_root().remove(objectToRemove);
				} else {
					return 400;
				}
			}

			cdo.storeModelWithoutUpdate(cloudEnvironment, Modeltype.AS_IS_MODEL);
		} catch (UnsupportedEncodingException e) {
			log.error(e.toString(), className);
			return 400;
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
			return 400;
		}
		return 200;
	}

	private void deleteAllRelation(EObject object) {
		List<EStructuralFeature> RelationList = object.eClass().getEAllStructuralFeatures();
		for (EStructuralFeature Feature : RelationList) {
			object.eUnset(Feature);
		}
	}

	@RequestMapping(path = "/fogprotect/addListener", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public boolean addListener(@RequestBody(required = true) String restUrl) { // "ip:port/urlToManagementComponent"
		try {
			String decodedWithEqualsSign = "";
			try {
				decodedWithEqualsSign = URLDecoder.decode(restUrl, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			log.jsonInfo("Add Listener: " + decodedWithEqualsSign, className);
			boolean exists = false;
			for (Listener listener : listeners) {
				if (listener.getUrl().equals(decodedWithEqualsSign)) {
					exists = true;
				}
			}
			if (exists == true) {
				log.info("Listener already exists", className);
				return false;
			} else {
				listeners.add(new Listener(decodedWithEqualsSign));
				log.info("Registered Listener.", className);
				return true;
			}
		} catch (Exception e) {
			log.error("Error adding Listener", className);
			return false;
		}
	}

	public LinkedList<Listener> getListeners() {
		return listeners;
	}

	public void setListeners(LinkedList<Listener> listeners) {
		this.listeners = listeners;
	}

}
