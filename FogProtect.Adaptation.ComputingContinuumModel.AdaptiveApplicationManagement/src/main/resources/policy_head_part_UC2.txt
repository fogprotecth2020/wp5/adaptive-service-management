<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
<ITResourceOrchestration id="UC2" 
	xmlns="http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd ../schema/mspl.xsd">
  
  <!-- AuthN Configuration -->
  <ITResource id="wp4-m4d">
    <configuration xsi:type='RuleSetConfiguration'>
        <capability xsi:type="AuthorizationCapability">
            <Name>Filtering_L7</Name>
        </capability>
        <configurationRule>
            <configurationRuleAction xsi:type='AuthenticationAction' >
                <AuthenticationOption>
                    <AuthenticationTarget />
                    <!-- If AuthenticationMechanism is 'openid-connect', this is the space-separated string that specifies the OpenID Connect acr values, empty string means any value. -->
                    <!-- In this context, each acr value must be an integer corresponding to the required LoA (Level of Authentication RFC 6711) for the authentication method -->
                    <AuthenticationMethod>1</AuthenticationMethod>
                    <!--Use exact same strings as Client Protocol drop-down box in Keycloak Admin console-->
                    <!-- If this is 'openid-connect', Authorization Code flow used by default 
                    and "-jwt" suffix to indicate access tokens must be in JWT format
                    -->
                    <AuthenticationMechanism>openid-connect_jwt</AuthenticationMechanism>
                    <AuthenticationParameters></AuthenticationParameters>
                </AuthenticationOption>
            </configurationRuleAction>
            <configurationCondition xsi:type='FilteringConfigurationCondition' >
                <isCNF>false</isCNF>
                <applicationLayerCondition>
                    <applicationProtocol>HTTP</applicationProtocol>
                </applicationLayerCondition>
            </configurationCondition>
            <Name>authn-rule</Name>
            <isCNF>false</isCNF>
        </configurationRule>
        <configurationRule>
            <configurationRuleAction xsi:type='AuthorizationAction' >
            <!-- Implicit DENY by default -->
              <AuthorizationActionType>ALLOW</AuthorizationActionType>
