package restController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import runtime.PersonalLogger;
import runtime.RiskFinder;
import runtime.RuntimeModelLogic;
import utility.ComparisonFactor;

@RestController
public class ImmediateAction {
	
	@Autowired
	RuntimeModelLogic runtimeModelLogic;
	@Autowired
	RiskFinder riskFinder;
	
	@Autowired
	PersonalLogger log;
	
	private static String className = ImmediateAction.class.getName();

	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/immediateAction", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public int adaptationRequest(@RequestBody(required = true) String adaptationRequestJSON) {
		String decodedWithEqualsSign = "";

		try {
			decodedWithEqualsSign = URLDecoder.decode(adaptationRequestJSON, "UTF-8");
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
			int id = receivedJson.get("ID").getAsInt();
			String taskId = receivedJson.get("SieaTaskId").getAsString();
			String asIsModel = receivedJson.get("AsIsModel").toString();
			String eventName = receivedJson.get("EventName").getAsString();
			LinkedList<ComparisonFactor> factors = new LinkedList<ComparisonFactor>();
			for (JsonElement factor : receivedJson.get("Factors").getAsJsonArray()) {
				factors.add(ComparisonFactor.valueOf(factor.getAsString()));
			}
			log.jsonInfo("AdaptiveApplicationManagement called with following String:" + decodedWithEqualsSign, className);
			
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					CloudEnvironment cloudEnvironment;
					try {
						cloudEnvironment = (CloudEnvironment) runtimeModelLogic.getLoader()
								.loadEObjectFromString(asIsModel);
						riskFinder.executeImmediateAction(cloudEnvironment, eventName, factors, taskId);
					} catch (IOException e) {
						// Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			t.start();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return 400;
		}

		return 200;
	}

}
