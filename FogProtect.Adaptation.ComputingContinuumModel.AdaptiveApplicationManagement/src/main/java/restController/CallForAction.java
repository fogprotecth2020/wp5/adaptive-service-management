package restController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import runtime.PersonalLogger;
import runtime.RuntimeModelLogic;

// TBD: Timer


@RestController
public class CallForAction {
	
	@Autowired
	RuntimeModelLogic runtimeModelLogic;
	
	@Value("${ExecuteAdaptationURL}")
	String executeAdaptationURL;
	@Value("${AdaptationResultURL}")
	String adaptationResultURL;
	
	@Autowired
	PersonalLogger log;
	
	@Autowired
	@Lazy
	private KafkaTemplate<String, String> kafkaTemplate;
	
	private static String className = CallForAction.class.getName();
	
	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/test", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
	
	public String test() {
		this.runtimeModelLogic.triggerTimeAwareAdaptation("test-topic", "time_aware_test", 5000);
		return "TEST";
	}
	
	
	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/callForAction", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public String callForAction(@RequestBody(required = true) String callForActionJSON) { 
		String decodedWithEqualsSign;
		try {
			decodedWithEqualsSign = URLDecoder.decode(callForActionJSON, "UTF-8");
			log.jsonInfo("callForAction: \n " + decodedWithEqualsSign, className);
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
			int id = receivedJson.get("ID").getAsInt();
			String taskId = receivedJson.get("SieaTaskId").getAsString();
			String name = receivedJson.get("Name").getAsString();
			String description = receivedJson.get("Description").getAsString();
			String referenceModel = receivedJson.get("ReferenceModel").getAsJsonObject().toString();
			try {
				runtimeModelLogic.executeAdaptation(id, referenceModel, executeAdaptationURL, adaptationResultURL,
						name, taskId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		return "200";
	}
	
	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/resetKafka", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public String resetKafka() { 
		log.info("Received Dashboard Reset", className);
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				String integrity_status = ""; //robot
				String intruder_alert = "";	//dasboard warning
				JsonObject integrityJSON = new JsonObject();
				JsonObject intruderJSON = new JsonObject();
				integrityJSON.addProperty("status", true);
				intruderJSON.addProperty("status", false);
				integrity_status = integrityJSON.toString();
				intruder_alert = intruderJSON.toString();
				kafkaTemplate.send("integrity_status", integrity_status);
				log.info("Sent " + integrity_status + " to topic " + "integrity_status", className);
				kafkaTemplate.send("intruder_alert", intruder_alert);
				log.info("Sent " + intruder_alert + " to topic " + "intruder_alert", className);
			}
		});		
		t.start();		
		return "200";
	}
}
