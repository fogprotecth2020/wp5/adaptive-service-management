package restController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import runtime.PersonalLogger;
import runtime.RiskFinder;
import runtime.RuntimeModelLogic;
import utility.ComparisonFactor;

@RestController
public class AdaptationRequest {
	
	@Autowired
	@Lazy
	RuntimeModelLogic runtimeModelLogic;
	@Autowired
	RiskFinder riskFinder;
	
	
	final static int maxSeconds = 10;
	
	@Value("${ProposedAdaptationURL}")
	String proposedAdaptationURL;
	@Value("${RegisterAtAdaptationCoordinatorURL}")
	String registerAtAdaptationCoordinatorURL;
	@Value("${FoundRisksURL}")
	String foundRisksURL;
	
	@Autowired
	PersonalLogger log;
	
	private static String className = AdaptationRequest.class.getName();
	
//	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/registerAtAdaptationCoordinator", method = RequestMethod.GET)
//	public void registerAtAdaptationCoordinator() {
//		String urlAdaptationRequest = "http://localhost:7771/fogprotect/adaptiveApplicationManagement/adaptationRequest";
//		String urlCallForAction = "http://localhost:7771/fogprotect/adaptiveApplicationManagement/callForAction";
//	    String urlImmediateAction ="http://localhost:7771/fogprotect/adaptiveApplicationManagement/immediateAction";
//		JsonObject head = new JsonObject();
//		head.addProperty("urlAdaptationRequest", urlAdaptationRequest);
//		head.addProperty("urlCallForAction", urlCallForAction);
//      head.addProperty("urlImmediateAction", urlImmediateAction);	
//		head.addProperty("TypeOfManagementComponent", "Application");
//		try {
//			System.out.println("Trying to register at AdaptationCoordinator");
//			runtimeModelLogic.makeRequest(registerAtAdaptationCoordinatorURL, head.toString());
//			System.out.println("Registered at AdaptationCoordinator");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}





	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/adaptationRequest", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
	@ResponseBody
	public int adaptationRequest(@RequestBody(required = true) String adaptationRequestJSON) {
		String decodedWithEqualsSign = "";

		try {
			decodedWithEqualsSign = URLDecoder.decode(adaptationRequestJSON, "UTF-8");
			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
			int id = receivedJson.get("ID").getAsInt();
			String taskId = receivedJson.get("SieaTaskId").getAsString();
			String asIsModel = receivedJson.get("AsIsModel").toString();
			JsonArray risks = receivedJson.get("Risks").getAsJsonArray();
			log.jsonInfo("AdaptiveApplicationManagement called with following String:" + decodedWithEqualsSign, className);
			LinkedList<String> risklist = new LinkedList<String>();
			for (JsonElement risk : risks) {
				risklist.add(risk.getAsJsonObject().get("RiskName").getAsString());
				log.info("Received risk named: " + risk.getAsJsonObject().get("RiskName").getAsString(), className);
			}
			LinkedList<ComparisonFactor> factors = new LinkedList<ComparisonFactor>();
			for (JsonElement factor : receivedJson.get("Factors").getAsJsonArray()) {
				factors.add(ComparisonFactor.valueOf(factor.getAsString()));
			}
			
			if (risklist.isEmpty()) {
				// Improvement Adaptations
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						CloudEnvironment cloudEnvironment;
						try {
							cloudEnvironment = (CloudEnvironment) runtimeModelLogic.getLoader().loadEObjectFromString(asIsModel);
							riskFinder.lookForImprovementAdaptations(cloudEnvironment, AdaptationAlgorithm.BestFirstSearch, maxSeconds, id, proposedAdaptationURL, factors, taskId);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				t.start();
			} else {
				// FIXME Risks should be used for adaptations
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						CloudEnvironment cloudEnvironment;
						try {
							cloudEnvironment = (CloudEnvironment) runtimeModelLogic.getLoader().loadEObjectFromString(asIsModel);
							riskFinder.lookForRiskAdaptations(cloudEnvironment,  AdaptationAlgorithm.BestFirstSearch, maxSeconds, id, proposedAdaptationURL, foundRisksURL, factors, taskId);
						} catch (IOException e) {
							// Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				});
				t.start();
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return 400;
		}

		return 200;
	}

}
