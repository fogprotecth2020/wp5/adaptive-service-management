package restController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import runtime.DisallowedAdaptationsManager;
import runtime.PersonalLogger;

@RestController
public class AdaptationConflictController {
	
	@Autowired
	DisallowedAdaptationsManager manager;
	
	@Autowired
	PersonalLogger log;
	
	private static String className = AdaptationConflictController.class.getName();
	
	
	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/adaptationCollection", method = RequestMethod.GET)
	@ResponseBody
	public String sendAdaptationRulesForConflictIdentification() {
		log.info("Received Request for Adaptation Collection.", className);
		String response = manager.collectAdaptationRules();
		
		return response;
	}
	
	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/disallowAdaptations", method = RequestMethod.POST,
			consumes = "*/*; charset=UTF-8")
	public boolean receiveDisallowedRules(@RequestBody(required=true) String bodyJson) {
		String decodedWithEqualsSign = "";
		
		try {
			log.info("Received Conflict Identification Response from AdaptationCoordinator.", className);
			decodedWithEqualsSign = URLDecoder.decode(bodyJson, "UTF-8");
			JsonArray receivedDisallowedRules = new JsonParser().parse(decodedWithEqualsSign).getAsJsonArray();
			List<String> disallowedRules = new ArrayList<>();
			
			for (JsonElement disallowedRule : receivedDisallowedRules) {
				disallowedRules.add(disallowedRule.getAsString());
			}
			manager.addDisallowedRules(disallowedRules);
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return true;
	}
}
