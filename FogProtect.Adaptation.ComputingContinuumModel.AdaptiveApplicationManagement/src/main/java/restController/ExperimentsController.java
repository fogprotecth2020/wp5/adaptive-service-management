package restController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EReferenceImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.multicda.cpa.CDAOptions;
import org.eclipse.emf.henshin.multicda.cpa.CpaByAGG;
import org.eclipse.emf.henshin.multicda.cpa.UnsupportedRuleException;
import org.eclipse.emf.henshin.multicda.cpa.result.CPAResult;
import org.eclipse.emf.henshin.multicda.cpa.result.CriticalPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ChangeDetector.Changes;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelFactory;
import fogprotect.adaptation.ComputingContinuumModel.Trustworthy;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import runtime.EMFModelLoad;
import runtime.PersonalLogger;
import runtime.RiskFinder;
import utilityClasses.*;
import utility.ComparisonFactor;
import utility.Constants;
import utility.ModelAnalyzer;

@RestController
@CrossOrigin
public class ExperimentsController {

	@Autowired
	EMFModelLoad loader;
	
	@Autowired
	@Lazy
	RiskFinder riskFinder;

	@Autowired
	PersonalLogger log;
	
	
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	public Model asismodel = null;
	public Model oldasismodel = null;
	int timer = 10000;

	private static String className = ExperimentsController.class.getName();

	LinkedList<ComparisonFactor> factors = new LinkedList<>();
	
//	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/experimentStart", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
//	@ResponseBody
//	public void experimentStart() {
//		String modelFileName = "ModelForPaperWithTUWien.computingcontinuummodel";
//		asismodel = new Model((CloudEnvironment) loader.loadFromFile(modelFileName), new ArrayList<Risk>());
//		this.notifyAllAboutChanges();
//		LinkedList<ComparisonFactor> factors = new LinkedList<ComparisonFactor>();
//		// Standard Factors, can be changed via interface (see below)
//		factors.add(ComparisonFactor.Risk);
//		factors.add(ComparisonFactor.EnergyConsumption);
//		factors.add(ComparisonFactor.Performance);
//		factors.add(ComparisonFactor.Function);
//		factors.add(ComparisonFactor.Cost);
//		this.factors = factors;
//		Thread a = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				// Auto-generated method stub
//				riskFinder.lookForRiskAdaptations(asismodel.getModel(), AdaptationAlgorithm.AStarSearch, timer, 1, "experiment",
//						"experiment", factors, "-1");
//			}
//		});
//		a.start();
//	}
//	
//	
	@MessageMapping("/hello")
	public void broadcastStatusInformation(String message) {
		this.simpMessagingTemplate.convertAndSend("/queue/statusInformation", message);
	}
//	
	public void notifyAllAboutChanges() {

		String json = createStatusInformationJson();
		this.broadcastStatusInformation(json);
		
		utility.Model mod = new utility.Model(new EGraphImpl(asismodel.getModel()), 0, null, null);
		System.out.println("Functions: " + mod.getWorkingFunctions());
		System.out.println("Cost: " + mod.getTotalCost());
		System.out.println("EnergyConsumption: " + mod.getEnergyConsumption());
		System.out.println("Performance: " + mod.getPerformance());
//		try {
//			Thread.sleep(30000);
//		} catch (InterruptedException e) {
//			// Auto-generated catch block
//			e.printStackTrace();
//		} // sleep timer when demo is running to show risks in the model
	}
//	
//
//	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/changeFactors", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
//	@ResponseBody
//	public boolean changeFactorPrioritization(@RequestBody(required = true) String jsonBody) {
//		String decodedWithEqualsSign = "";
//
//		try {
//			decodedWithEqualsSign = URLDecoder.decode(jsonBody, "UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//
//		JsonArray receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonArray();
//
//		LinkedList<ComparisonFactor> factors = new LinkedList<ComparisonFactor>();
//		
//		if(receivedJson.size() == 4) {
//			factors.add(ComparisonFactor.Risk);
//		}
//		
//		for (JsonElement factor : receivedJson) {
//			factors.add(ComparisonFactor.valueOf(factor.getAsString()));
//		}
//
//		this.factors = factors;
//		Thread a = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				// Auto-generated method stub
//				// not considered part of executive radar
//				riskFinder.lookForRiskAdaptations(asismodel.getModel(), AdaptationAlgorithm.AStarSearch, timer, 1, "experiment",
//						"experiment", factors, "-1");
//			}
//		});
//		a.start();
//		return true;
//
//	}
//
//	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/statusInformation", method = RequestMethod.GET, consumes = "*/*; charset=UTF-8")
//	@ResponseBody
//	public String statusInformation() {
//		return this.createStatusInformationJson();
//	}
//	
//	
	public String createStatusInformationJson() {
		JsonObject head = new JsonObject();
		JsonObject asIs = new JsonObject();
		JsonArray risks = new JsonArray();
		if (this.asismodel != null) {
			JsonObject asIsModel = new JsonParser().parse(loader.modelToJsonString(this.asismodel.getModel()))
					.getAsJsonObject();
			asIs.add("AsIsModel", asIsModel);
			
			
			if (asismodel.getRisks() != null) {
				for (Risk riskObject : asismodel.getRisks()) {
					JsonObject risk = new JsonObject();
					risk.addProperty("Name", riskObject.getName());
					risk.addProperty("Description", riskObject.getDescription());
					risk.addProperty("ID", riskObject.getId());
					JsonArray nodesForRisk = new JsonArray();
					for (Node nodeObject : riskObject.getNodes()) {
						JsonObject nodeForNodesForRisk = new JsonObject();
						nodeForNodesForRisk.addProperty("Name", nodeObject.getName());
						nodeForNodesForRisk.addProperty("@id", nodeObject.getAtID());
						nodesForRisk.add(nodeForNodesForRisk);
					}
					risk.add("Nodes", nodesForRisk);
					risks.add(risk);
				}
			}			
		} else {
			asIs.add("AsIsModel", null);
		}
		if (oldasismodel != null) {
			JsonObject oldasIsModel = new JsonParser()
					.parse(loader.modelToJsonString(oldasismodel.getModel())).getAsJsonObject();
			asIs.add("OldAsIsModel", oldasIsModel);
		} else {
			asIs.add("OldAsIsModel", null);
		}

		if (asismodel != null && oldasismodel != null) {
			asIs.add("ChangesMadeToAsIsModel",
					Changes.compareModels(oldasismodel.getModel(), asismodel.getModel())
							.get("ChangesMadeToAsIsModel").getAsJsonArray());
		} else {
			asIs.add("ChangesMadeToAsIsModel", new JsonArray());
		}

		asIs.add("Risks", risks);
		
		asIs.addProperty("Risklevel", "Unknown");
		
		head.add("AsIs", asIs);

		JsonArray adaptations = new JsonArray();

		head.add("Adaptations", adaptations);
		return head.toString();
	}
//	
//
//	@RequestMapping(path = "/fogprotect/adaptiveApplicationManagement/monitoringEvent", method = RequestMethod.POST, consumes = "*/*; charset=UTF-8")
//	@ResponseBody
//	public long monitoringEvent(@RequestBody(required = true) String jsonAsIsModel) {
//		String decodedWithEqualsSign;
//		try {
//			log.info("Received Monitoring Event", className);
//			decodedWithEqualsSign = URLDecoder.decode(jsonAsIsModel, "UTF-8");
//			// Create Object out of JSON
//			JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();
//
//			CloudEnvironment cloudEnvironment = asismodel.getModel();
//			String temp = loader.modelToJsonString(cloudEnvironment);
//			cloudEnvironment = (CloudEnvironment) loader.loadEObjectFromString(temp);
//
//			String requestType = receivedJson.get("RequestType").getAsString();// Determines what kind of Request the
//																				// Json is
//
//			if (requestType.equals("changeAttribute"))// To change the Value of an Existing Attribute
//			{
//				String atId = receivedJson.get("@id").getAsString();
//				String attributeToChange = receivedJson.get("attribute").getAsString();
//				String newValue = receivedJson.get("newValue").getAsString();
//				String attributeType = receivedJson.get("attributeType").getAsString();
//
//				EObject object = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
//				EStructuralFeature feature = object.eClass().getEStructuralFeature(attributeToChange);
//				if (attributeType.equals("String")) {
//					object.eSet(feature, newValue);
//				} else if (attributeType.equals("boolean")) {
//					Boolean newValueBool = Boolean.parseBoolean(newValue);
//					object.eSet(feature, newValueBool);
//				} else if (attributeType.equals("int")) {
//					Integer newValueInt = Integer.parseInt(newValue);
//					object.eSet(feature, newValueInt);
//				} else if (attributeType.equals("double")) {
//					Double newValueDouble = Double.parseDouble(newValue);
//					object.eSet(feature, newValueDouble);
//				} else if (attributeType.equals("Trustworthy")) {
//					object.eSet(feature, Trustworthy.getByName(newValue));
//				}
//			}
//
//			if (requestType.equals("addRelation"))// Create a New Relation between Two EObject
//			{
//				log.info("Add Relation", className);
//				String atId = receivedJson.get("@id").getAsString();
//				String TargetID = receivedJson.get("targetid").getAsString();
//				String relationName = receivedJson.get("relationName").getAsString();
//
//				EObject SourceObject = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
//				EObject TargetObject = loader.searchForObjectInGivenModel(cloudEnvironment, TargetID);
//				EStructuralFeature RelationToChange = SourceObject.eClass().getEStructuralFeature(relationName);
//				if (RelationToChange != null) {
//					if (RelationToChange.isMany()) {
//						List<Object> RelationList = (List<Object>) SourceObject.eGet(RelationToChange);
//						RelationList.add(TargetObject);
//					} else {
//						SourceObject.eSet(RelationToChange, TargetObject);
//					}
//				} else {
//					return 400;
//				}
//			}
//
//			if (requestType.equals("deleteRelation"))// Delete Relation
//			{
//				// System.out.println("Delete Relation");
//				String atId = receivedJson.get("@id").getAsString();
//				String TargetID = receivedJson.get("targetid").getAsString();
//				String relationName = receivedJson.get("relationName").getAsString();
//
//				EObject SourceObject = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
//				EObject TargetObject = loader.searchForObjectInGivenModel(cloudEnvironment, TargetID);
//				EStructuralFeature StructureToDelete = SourceObject.eClass().getEStructuralFeature(relationName);
//
//				EReferenceImpl RefToDelete = (EReferenceImpl) SourceObject.eClass().getEStructuralFeature(relationName);
//				EReference OppositeRelation = RefToDelete.getEOpposite();
//				EStructuralFeature OppositeFeature = TargetObject.eClass()
//						.getEStructuralFeature(OppositeRelation.getName());
//
//				if (StructureToDelete != null) {
//					// Delete the Relation
//					if (StructureToDelete.isMany())// Relation has more Elements
//					{
//						List<Object> RelationList = (List<Object>) SourceObject.eGet(StructureToDelete);
//
//						if (RelationList.size() == 1)// Relation is Array with 1 Element
//						{
//							SourceObject.eUnset(StructureToDelete);
//						} else {
//							RelationList.remove(TargetObject);
//						}
//					} else {
//						SourceObject.eUnset(StructureToDelete);
//					}
//
//					// Delete the Opposite
//					if (OppositeFeature.isMany())// Relation has more Elements
//					{
//						List<Object> RelationList = (List<Object>) TargetObject.eGet(OppositeFeature);
//
//						if (RelationList.size() == 1)// Relation is Array with 1 Element
//						{
//							TargetObject.eUnset(OppositeFeature);
//						} else {
//							RelationList.remove(SourceObject);
//						}
//					} else {
//						TargetObject.eUnset(OppositeFeature);
//					}
//				} else {
//					return 400;
//				}
//
//			}
//
//			// TODO Fix Bug that URI is similiar to the other objects
//			if (requestType.equals("addNode")) {
//				String NodeType = receivedJson.get("nodeType").getAsString();
//				String NodeName = receivedJson.get("nodeName").getAsString();
//				EObject nodeToAdd = null;
//
//				ComputingContinuumModelFactory factory = ComputingContinuumModelFactory.eINSTANCE;
//				try {
//					Method methodToCreateObject = factory.getClass().getMethod("create" + NodeType);
//					nodeToAdd = (EObject) methodToCreateObject.invoke(factory);
//					if (nodeToAdd != null) {
//						EStructuralFeature feature = nodeToAdd.eClass().getEStructuralFeature("name");
//						nodeToAdd.eSet(feature, NodeName);
//						cloudEnvironment.getTosca_nodes_root().add((tosca_nodes_Root) nodeToAdd);
//						log.info("done", className);
//					} else {
//						return 400;
//					}
//				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
//						| InvocationTargetException e) {
//					e.printStackTrace();
//					return 400;
//				}
//			}
//
//			if (requestType.equals("deleteNode")) {
//				String atId = receivedJson.get("@id").getAsString();
//				EObject objectToRemove = loader.searchForObjectInGivenModel(cloudEnvironment, atId);
//				if (objectToRemove != null) {
//					this.deleteAllRelation(objectToRemove);
//					cloudEnvironment.getTosca_nodes_root().remove(objectToRemove);
//				} else {
//					return 400;
//				}
//			}
//
//			// Change AsIsModel
//			if(oldasismodel == null) {
//				oldasismodel = new Model(asismodel.getModel(), new ArrayList<Risk>());
//			}else {
//				oldasismodel.setModel(asismodel.getModel());
//			}
//			asismodel.setModel(cloudEnvironment);
//			asismodel.getRisks().clear();
//			notifyAllAboutChanges();
//
//			// START RISK FINDER
//			Thread a = new Thread(new Runnable() {
//
//				@Override
//				public void run() {
//					// Auto-generated method stub
//					riskFinder.lookForRiskAdaptations(asismodel.getModel(), AdaptationAlgorithm.AStarSearch, timer, 1, "experiment",
//							"experiment", factors, "-1");
//				}
//			});
//			a.start();
//
//		} catch (UnsupportedEncodingException e) {
//			log.error(e.toString(), className);
//			return 400;
//		} catch (IOException e) {
//			// Auto-generated catch block
//			e.printStackTrace();
//			return 400;
//		}
//		return 200;
//	}
//
//	private void deleteAllRelation(EObject object) {
//		List<EStructuralFeature> RelationList = object.eClass().getEAllStructuralFeatures();
//		for (EStructuralFeature Feature : RelationList) {
//			object.eUnset(Feature);
//		}
//	}
//	
//	@RequestMapping(path = "/comp", method = RequestMethod.GET)
//	public String compare() {
//		String fileA = "Conflicts.henshin";
//		String fileB = "RuleD.henshin";
//		
//		HenshinResourceSet set = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
//		
////		set.getResourceFactoryRegistry().getExtensionToFactoryMap().put("henshin", new HenshinResourceFactory());
//
//		Rule[] controlRules = (Rule[]) set.getModule(fileA).getUnits().toArray(new Rule[0]);
//		List<Rule> controlRulesA = new ArrayList<>();
//		controlRulesA.addAll(Arrays.asList(controlRules));
//		controlRulesA.removeIf(rule -> !rule.getName().equals("RuleD"));
//		Rule ruleA = controlRulesA.get(0);
//
//		Rule ruleB = ((Rule[]) set.getModule(fileB, false).getUnits().toArray(new Rule[0]))[0];
//		
//		ruleB.setActivated(false);
//		
//		System.out.println(EcoreUtil.equals(ruleA, ruleB));
//
//		ruleB.setActivated(true);
//		// https://www.jorgemanrubia.com/2008/07/06/comparing-emf-models/
//		// EcoreUtil.equals sensitive to activation of rule.
//		System.out.println(EcoreUtil.equals(ruleA, ruleB));
//		
//		return "";
//	}
//	
//	@RequestMapping(path = "/save", method = RequestMethod.GET) 
//	public String saveTest() {
//		String fileName = "Conflicts.henshin";
//		HenshinResourceSet set = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
//
//		Rule[] controlRules = (Rule[]) set.getModule(fileName).getUnits().toArray(new Rule[0]);
//		List<Rule> controlRulesA = new ArrayList<>();
//		controlRulesA.addAll(Arrays.asList(controlRules));
//		controlRulesA.removeIf(rule -> !rule.getName().equals("RuleD"));
//		Rule rule = controlRulesA.get(0);
//		
//		String ruleAsXML = dirtyDirtyDirty(rule, set);
//		System.out.println(ruleAsXML);
//		
//		return "";
//	}
//	
//	private String dirtyDirtyDirty(Rule rule, HenshinResourceSet set) {
////		set.getResourceFactoryRegistry().getExtensionToFactoryMap().put("henshin", new HenshinResourceFactory());
//		org.eclipse.emf.henshin.model.Module module = (Module) EcoreUtil.copy(rule.eContainer());
//		module.getUnits().clear();
//		module.getUnits().add(rule);
//		
//		File file = new File(Constants.ADAPTATIONS_PATH + rule.getName() + ".henshin");
//		set.saveEObject(module, file.getAbsolutePath());
//		
//		try {
//			System.out.println(Files.readString(file.toPath()));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		return "";
//	}
//	
//	@RequestMapping(path="/hi", method = RequestMethod.GET)
//	public String testForConflicts() {
//		String conflictFileName = "Conflicts.henshin";
////		String fileName = "improvement.henshin";
////		
//		String fileNameA = "LowFPS.henshin";
//		String fileNameB = "UntrustedActor.henshin";
//		String fileNameC = "UntrustedCompany.henshin";
////		
//		HenshinResourceSet set = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
//		
//		Rule[] controlRules = (Rule[]) set.getModule(conflictFileName).getUnits().toArray(new Rule[0]);
//		List<Rule> controlRulesA = new ArrayList<>();
//		controlRulesA.addAll(Arrays.asList(controlRules));
//		controlRulesA.removeIf(rule -> rule.getName().equals("RuleD"));
//		List<Rule> rules = new ArrayList<>();
//	
//		
////		File[] directoryListing = new File(Constants.ADAPTATIONS_PATH).listFiles();
////		for (File file : directoryListing) {
////			String fileName = file.getName();
////			if (fileName.endsWith(".henshin")) {
////				rules.addAll(Arrays.asList((Rule[]) set.getModule(fileName).getUnits().toArray(new Rule[0])));
////			}
////		}
////		
////		rules = rules.stream().filter(rule -> rule.isActivated()).collect(Collectors.toList());
//		rules.addAll(Arrays.asList((Rule[]) set.getModule(fileNameA).getUnits().toArray(new Rule[0])));
//		
//		CDAOptions options = new CDAOptions();
//		// TODO essential & initial cps
////		options.essentialCP = true;
//		options.setComplete(false); // if CP is found, stop
//		options.setReduceSameRuleAndSameMatch(true);  // if true, less amount of conflicts, but more time. what?
//		options.setIgnoreSameRules(false);
//		options.setIgnoreMultiplicities(true);
//		
//		CpaByAGG app = new CpaByAGG();
//		CPAResult result = null;
//		long time = 0;
//		
//		try {
//			HashSet<Rule> set1 = null;
//			HashSet<Rule> set2 = null;
//			
//			if (rules.size() > controlRules.length) {
//				set1 = new HashSet<>(controlRulesA);
//				set2 = new HashSet<>(rules);
//			}
//			else {
//				set2 = new HashSet<>(controlRulesA);
//				set1 = new HashSet<>(rules);
//			}
//			
//			app.init(set1, set2, options);
//			long start = System.nanoTime();
//			result = app.runConflictAnalysis();
//			time = System.nanoTime() - start;
//		}
//		catch (UnsupportedRuleException e) {
//			e.printStackTrace();
//		}
//		
//		try(PrintStream ps = new PrintStream("out.txt")) {
//			PrintStream stand = System.out;
//			System.setOut(ps);
//			
//			System.out.println("CPA took " + time/1e9d + "s");
//			
//			if (result != null) {
//				for (CriticalPair cp : result) {
//					System.out.println("Problems: " + cp.getFirstRule().getName() + "-" + cp.getSecondRule());
//				}
//			}
//			
//			System.setOut(stand);
//		}
//		catch (FileNotFoundException e) {
//			
//		}
//		System.out.println("CPA took " + time/1e9d + "s");
//		
//		if (result != null) {
//			for (CriticalPair cp : result) {
//				System.out.println("Problems: " + cp.getFirstRule().getName() + "-" + cp.getSecondRule());
//			}
//		}
//		
// 		
//		return "Hi Boi";
//	}
//	
//	@RequestMapping(path="/func", method = RequestMethod.GET)
//	public String check() {
//		String fileName = "SmartManufacturingFirstDraft.computingcontinuummodel";
////		String fileName = "SmartCityFirstDraft.computingcontinuummodel";
//		CloudEnvironment cloudModel = (CloudEnvironment) loader.loadFromFile(fileName);
//		
//		var result = new ModelAnalyzer().analyzeModel(new EGraphImpl(cloudModel));
//		 
//		System.out.println("Functions: " + result.get("functions").doubleValue());
//		
//		
//		
//		return "";
//	}

}
