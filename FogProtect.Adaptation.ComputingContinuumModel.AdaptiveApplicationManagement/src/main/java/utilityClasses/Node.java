package utilityClasses;

public class Node {

	private String name;
	
	private String atID;

	public Node(String name, String atID) {
		super();
		this.name = name;
		this.atID = atID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAtID() {
		return atID;
	}

	public void setAtID(String atID) {
		this.atID = atID;
	}
	
	
	
}
