package utilityClasses;

import java.util.ArrayList;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;

public class Model {

	private CloudEnvironment model;
	
	private ArrayList<Risk> risks;

	public Model(CloudEnvironment model, ArrayList<Risk> risks) {
		super();
		this.model = model;
		this.risks = risks;
	}

	public CloudEnvironment getModel() {
		return model;
	}

	public void setModel(CloudEnvironment model) {
		this.model = model;
	}

	public ArrayList<Risk> getRisks() {
		return risks;
	}

	public void setRisks(ArrayList<Risk> risks) {
		this.risks = risks;
	}
	
	
	
}
