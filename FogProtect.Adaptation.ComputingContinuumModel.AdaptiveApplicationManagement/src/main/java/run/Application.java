package run;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.annotation.EnableKafka;

import runtime.LoggingInterface;
import runtime.PersonalLogger;

@EnableKafka
@SpringBootApplication(scanBasePackages= {"restController", "runtime", "kafkaClasses"})
public class Application {
	
	@Autowired
	LoggingInterface loggingInterface;
	
	@Autowired
	PersonalLogger log;
	
	@Value("${IncludeAllLogs}")
	private boolean logAll;
	
	private static String className = Application.class.getName();
	
    public static void main(String[] args) {
    	LoggingInterface.setLogToGraylog(false); //True = loggingToGraylogServer
        SpringApplication.run(Application.class, args);
    }
    
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
    	log.setLogAll(logAll);
    	
    	log.info("Spring Boot Started... Setting up environment variables", className);
        //doStuffIfNeeded
    	log.info("Done!", className);
    }
}

