package runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import restController.ExperimentsController;
import riskpatternfinder.AdaptationFinderToImproveSystem;
import riskpatternfinder.AdaptationFinderToMitigateRisks;
import riskpatternfinder.SearchInitiator;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import utility.Adaptation;
import utility.AdaptationCombinationRepresentation;
import utility.AtomicAdaptation;
import utility.ComparisonFactor;
import utility.Constants;
import utility.ExecutedAdaptation;
import utility.MatchRepresentation;
import utility.Model;
import utility.NodeRepresentation;
import utilityClasses.Node;
import utilityClasses.Risk;

@Component
public class RiskFinder {

	@Autowired
	RuntimeModelLogic runtimeModelLogic;

	@Value("${ExecuteAdaptationURL}")
	String executeAdaptationURL;
	@Value("${AdaptationResultURL}")
	String adaptationResultURL;
	@Value("${FoundRisksURL}")
	String foundRisksURL;

	@Autowired
	PersonalLogger log;
	@Autowired
	ExperimentsController ec;
	@Autowired 
	EMFModelLoad loader;
	@Autowired
	DisallowedAdaptationsManager conflictManager;

	@Value("${demo}")
	boolean demo;

	private static String className = RiskFinder.class.getName();

	private Engine engine;
	
	public RiskFinder() {
		engine = new EngineImpl();
		engine.getOptions().put(Engine.OPTION_DETERMINISTIC, false);

	}

	// TODO Rework lookForConstraints
//		public void lookForConstraints(CloudEnvironment cloudEnvironment) {
//			HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.RISKPATTERNS_PATH);
//			Module module = resourceSet.getModule("constraints.henshin", false);
//			EGraph graph = new EGraphImpl(new EGraphImpl(cloudEnvironment));
//			UnitApplication app = new UnitApplicationImpl(engine);
//			app.setEGraph(graph);
//			app.setUnit(module.getUnit("conditionalCheckSequence"));
//			try {
//				if (!app.execute(null)) {
//					System.err.println("One condition was not fulfilled");
//				} else {
//					System.out.println("All conditions are fulfilled");
//				}
//			} catch (NullPointerException e) {
//				System.err.println("One condition was not fulfilled");
//			}
//
//		}

	public void lookForImprovementAdaptations(CloudEnvironment cloudEnvironment, AdaptationAlgorithm aa, int maxSeconds,
			int id, String urlAdaptationCoordinator, List<ComparisonFactor> factors, String taskId) {
		double timestampA = System.currentTimeMillis();
		JsonArray adaptations = new JsonArray();
		AdaptationFinderToImproveSystem adaptationFinder = new AdaptationFinderToImproveSystem(engine,
				cloudEnvironment.eResource());
		AdaptationCombinationRepresentation[] possibleAdaptations = adaptationFinder
				.generatePossibleAdaptations(cloudEnvironment, aa, maxSeconds, factors, conflictManager.getDisallowedRuleNames());
		double timestampB = System.currentTimeMillis();
		System.out.println("Search time in sec: " + (timestampB - timestampA) / 1000f);
		if (possibleAdaptations == null || possibleAdaptations.length == 0) {
			log.info("No adaptations to improve the systems functionality found.", className);
		} else {
			log.info("Possible Adaptation(s) to improve system found.", className);
			// alternative send all possible adaptations
			adaptations = this.createJsonArray(possibleAdaptations);
			if (urlAdaptationCoordinator.equals("experiment")) {
				// Update model with adaptation
				this.updateAsIsModel(possibleAdaptations[0].getRuntimeModel(), factors, maxSeconds, taskId);
			}
		}
		if (!urlAdaptationCoordinator.equals("experiment")) {
			if(possibleAdaptations != null && possibleAdaptations.length>1) {
				this.lookForImprovementAdaptations(possibleAdaptations[0].getRuntimeModel(), aa, maxSeconds, id, urlAdaptationCoordinator, factors, taskId);
			}else {
				runtimeModelLogic.sendAnswerToAdaptationCoordinator(id, urlAdaptationCoordinator, adaptations, taskId);	
			}
			
		}

	}

	private JsonArray createJsonArray(AdaptationCombinationRepresentation[] effectiveAdaptations) {
		JsonArray adaptations = new JsonArray();
		for (AdaptationCombinationRepresentation adaptation : effectiveAdaptations) {
			JsonObject adaptationJson = new JsonObject();
			CloudEnvironment referenceModel = adaptation.getRuntimeModel();

			log.info("Found adaptation(s):", className);
			String usedAdaptations = "";
			for (AtomicAdaptation adap : adaptation.getAtomicAdaptationArray()) {
				log.info("     " + adap.getRiskRuleName() + ": Adaptation: " + adap.getAdaptationName(), className);
				usedAdaptations += adap.getRiskRuleName() + ": Adaptation: " + adap.getAdaptationName() + " | ";
			}

			adaptationJson.addProperty("Name", usedAdaptations);
			adaptationJson.addProperty("Description", "Description will be added soon...");
			JsonObject referenceModelJson = new JsonParser()
					.parse(runtimeModelLogic.returnentiremodelasjson(referenceModel)).getAsJsonObject();
			adaptationJson.add("ReferenceModel", referenceModelJson);
			JsonArray risks = new JsonArray();
			/*
			 * ImprovementAdaptationProposals are only created when the "As is" model is
			 * risk free. Moreover, improvements will never add new risks to the "As is"
			 * model. Therefore, the list of risks will always be empty
			 */
			adaptationJson.add("Risks", risks);
			adaptationJson.addProperty("TypeOfManagementComponent", "Application");
			adaptations.add(adaptationJson);
		}
		return adaptations;
	}

	public void lookForRiskAdaptations(CloudEnvironment cloudEnvironment, AdaptationAlgorithm aa, int maxSeconds,
			int id, String urlAdaptationCoordinator, String urlFoundRisks, List<ComparisonFactor> factors, String taskId) {
		double timestampA = System.currentTimeMillis();
		// look for possible adaptations in the AsIsModel model
		AdaptationFinderToMitigateRisks adaptationFinder = new AdaptationFinderToMitigateRisks(
				cloudEnvironment.eResource(), factors);
		AdaptationCombinationRepresentation[] effectiveAdaptations = adaptationFinder
				.generatePossibleAdaptations(cloudEnvironment, aa, maxSeconds, factors, conflictManager.getDisallowedRuleNames());
		double timestampB = System.currentTimeMillis();
		System.out.println("Search time in sec: " + (timestampB - timestampA) / 1000f);
		// check if list with adaptations does exist
		if (effectiveAdaptations == null) {
			// there is no list, because there is no risk found within the runtime model
			// log into console, that there is no risk found
			log.info("No risks found.", className);
			JsonArray adaptations = new JsonArray();
			if (urlAdaptationCoordinator.equals("experiment")) {
				// if there is no risk we are looking for improvements
				this.lookForImprovementAdaptations(cloudEnvironment, aa, maxSeconds, id, urlAdaptationCoordinator,
						factors, taskId);
			} else {
				runtimeModelLogic.sendAnswerToAdaptationCoordinator(id, urlAdaptationCoordinator, adaptations, taskId);
			}

		} else {
			if (!urlAdaptationCoordinator.equals("experiment")) {
				runtimeModelLogic.sendFoundRisksToStatusInformation(adaptationFinder.getFirstFoundRisks(),
						urlFoundRisks, taskId);
			}else {
				ArrayList<Risk> risks = new ArrayList<Risk>();
				
				for (MatchRepresentation matchrep : adaptationFinder.getFirstFoundRisks()) {
					ArrayList<Node> nodes = new ArrayList<Node>();
					for(NodeRepresentation node : matchrep.getNodes()) {
						Node nodeToAdd = new Node(node.getName(), node.getAtId());
						nodes.add(nodeToAdd);
					}
					int riskid = ec.asismodel.getRisks().size();
					Risk risk = new Risk(matchrep.getRuleName(), "unknown", riskid, nodes);
					ec.asismodel.getRisks().add(risk);
					ec.notifyAllAboutChanges();
				}
			}
			// check if the list of adaptations is empty
			if (effectiveAdaptations.length == 0) {
				// OPTIONAL what to do when no adaptation that mitigates all risks is found?
				log.error("Risk(s) found without possible adaptation: ", className);
				String foundPCPInstances = "";
				for (MatchRepresentation matchRep : adaptationFinder.getFirstFoundRisks()) {
//					log.error("     Risk: \"" + matchRep.getRuleName() + "\"", className);
					foundPCPInstances += "Risk: \"" + matchRep.getRuleName() + "\" | ";
				}
				log.info("Found PCP instances:" + foundPCPInstances, className);

				Model nonPerfectSolution = adaptationFinder.lookForRisksInNonPerfectSolution();

				String remainingPCPInstances = "";

				String usedAdaptations = adaptationFinder.generatePathString(nonPerfectSolution);
				for (Adaptation adap : nonPerfectSolution.getAdaptations()) {
					usedAdaptations += adap.getRiskName() + ": " + adap.getRule().getName() + " | ";
				}

				JsonArray adaptations = new JsonArray();
				JsonObject adaptationJson = new JsonObject();
				CloudEnvironment referenceModel = (CloudEnvironment) nonPerfectSolution.getRuntimeModel().getRoots()
						.get(0);
				adaptationJson.addProperty("Name", usedAdaptations);
				adaptationJson.addProperty("Description", "Description will be added soon...");
				JsonObject referenceModelJson = new JsonParser()
						.parse(runtimeModelLogic.returnentiremodelasjson(referenceModel)).getAsJsonObject();
				adaptationJson.add("ReferenceModel", referenceModelJson);
				JsonArray risks = new JsonArray();

				for (MatchRepresentation matchRep : nonPerfectSolution.getRisks()) {
					remainingPCPInstances += "Risk: \"" + matchRep.getRuleName() + "\" | ";
					JsonObject risk = new JsonObject();
					risk.addProperty("Name", matchRep.getRuleName());
					risk.addProperty("Description", "Description will be added soon...");
					JsonArray nodesForRisk = new JsonArray();
					for (NodeRepresentation nodeRep : matchRep.getNodes()) {
						JsonObject nodeForNodesForRisk = new JsonObject();
						nodeForNodesForRisk.addProperty("Name", nodeRep.getObjectName());
						nodeForNodesForRisk.addProperty("@id", nodeRep.getAtId());
						nodesForRisk.add(nodeForNodesForRisk);
					}
					risk.add("Nodes", nodesForRisk);
					risks.add(risk);
				}

				log.info("Remaining PCP instances:" + remainingPCPInstances, className);

				adaptationJson.add("Risks", risks);
				adaptationJson.addProperty("TypeOfManagementComponent", "Application");
				adaptations.add(adaptationJson);
				if (demo == true) {
					try {
						Thread.sleep(60000); // 1min sleep timer when demo is running to show risks in the model
					} catch (InterruptedException e1) {
						// Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (!urlAdaptationCoordinator.equals("experiment")) {
					runtimeModelLogic.sendAnswerToAdaptationCoordinator(id, urlAdaptationCoordinator, adaptations, taskId);
				} else {
					log.error("PCP instances still exist, new loop started", usedAdaptations);
					// App pauses and waits for next event -> an operator is needed because the
					// software cannot solve all PCP instances with the given adaptations
					// OR a new loop has to start because in 10sec not all PCP instances could be
					// solved
					// Update model with adaptation
					this.updateAsIsModel((CloudEnvironment) nonPerfectSolution.getRuntimeModel().getRoots().get(0), factors, maxSeconds, taskId);
				}

				// the list is empty
				// no possible adaptations were found
				// risks still exist
			} else {
				log.info("Risk(s) found with possible adaptation: ", className);
				String foundPCPInstances = "";
				for (MatchRepresentation matchRep : adaptationFinder.getFirstFoundRisks()) {
					log.info("     Risk: \"" + matchRep.getRuleName() + "\"", className);
					foundPCPInstances += "Risk: \"" + matchRep.getRuleName() + "\" | ";
				}
				// there are effective adaptations within the list
				JsonArray adaptations = new JsonArray();
				for (AdaptationCombinationRepresentation adaptation : effectiveAdaptations) {
					// set proposal graph
					JsonObject adaptationJson = new JsonObject();
					CloudEnvironment referenceModel = adaptation.getRuntimeModel();

					// ignored for logger, no variable called proposal
//							System.out.println("Proposal created:\n" + runtimeModelLogic.returnentiremodelasjson(proposal));

					log.info("Found adaptation(s):", className);
					String usedAdaptations = "";
					for (AtomicAdaptation adap : adaptation.getAtomicAdaptationArray()) {
						log.info("     " + adap.getRiskRuleName() + ": Adaptation: " + adap.getAdaptationName(),
								className);
						usedAdaptations += adap.getRiskRuleName() + ": Adaptation: " + adap.getAdaptationName() + " | ";
					}

					adaptationJson.addProperty("Name", usedAdaptations);
					adaptationJson.addProperty("Description", "Description will be added soon...");
					JsonObject referenceModelJson = new JsonParser()
							.parse(runtimeModelLogic.returnentiremodelasjson(referenceModel)).getAsJsonObject();
					adaptationJson.add("ReferenceModel", referenceModelJson);
					JsonArray risks = new JsonArray();
					/*
					 * In this part of the if clause only adaptations are proposed that mitigate all
					 * risks. Therefore, the list of risks will be empty.
					 */
					adaptationJson.add("Risks", risks);
					adaptationJson.addProperty("TypeOfManagementComponent", "Application");
					adaptations.add(adaptationJson);
				}
				if (demo == true) {
					try {
						Thread.sleep(60000); // 1min sleep timer when demo is running to show risks in the model
					} catch (InterruptedException e1) {
						// Auto-generated catch block
						e1.printStackTrace();
					}
				}
				if (!urlAdaptationCoordinator.equals("experiment")) {
					runtimeModelLogic.sendAnswerToAdaptationCoordinator(id, urlAdaptationCoordinator, adaptations, taskId);
				} else {
					// Update model with adaptation
					this.updateAsIsModel(effectiveAdaptations[0].getRuntimeModel(), factors, maxSeconds, taskId);
				}
			}
		}
	}
	
	private void updateAsIsModel(CloudEnvironment ce, List<ComparisonFactor> factors, int timer, String taskId) {
		String temp = loader.modelToJsonString(ce);
		try {
			if(ec.oldasismodel == null) {
				ec.oldasismodel = new utilityClasses.Model(ec.asismodel.getModel(), new ArrayList<Risk>());
			}else {
				ec.oldasismodel.setModel(ec.asismodel.getModel());
				ec.oldasismodel.setRisks(ec.asismodel.getRisks());
			}
			ec.asismodel.setModel((CloudEnvironment) loader.loadEObjectFromString(temp));
			ec.asismodel.getRisks().clear();
			ec.notifyAllAboutChanges();
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		// start search for PCP instances again
		Thread a = new Thread(new Runnable() {

			@Override
			public void run() {
				// Auto-generated method stub
				lookForRiskAdaptations(ec.asismodel.getModel(), AdaptationAlgorithm.AStarSearch, timer, 1, "experiment",
						"experiment", factors, taskId);
			}
		});
		a.start();
	}

	// output generation for testing purposes
	public void saveGraph(EGraph outputGraph, int index) {
		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.RUNTIME_MODEL_PATH);
		resourceSet.saveEObject(outputGraph.getRoots().get(0), "AdaptedModel" + index + ".computingcontinuummodel");
	}

	public void executeImmediateAction(CloudEnvironment ce, String eventName, List<ComparisonFactor> factors, String taskId) {
		AdaptationFinderToMitigateRisks adaptationFinder = new AdaptationFinderToMitigateRisks(ce.eResource(), factors);
		AdaptationCombinationRepresentation[] effectiveAdaptations = adaptationFinder.generatePossibleAdaptations(ce,
				AdaptationAlgorithm.AStarSearch, 3, factors, conflictManager.getDisallowedRuleNames());
		// check if list with adaptations does exist
		if (effectiveAdaptations != null) {
			runtimeModelLogic.sendFoundRisksToStatusInformation(adaptationFinder.getFirstFoundRisks(), foundRisksURL, taskId);
		}
		if (demo == true) {
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (eventName.equals("DoorOpen")) {
			HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
			Module module = resourceSet.getModule("LossOfTrustworthiness.henshin", false);
			UnitApplication application = new UnitApplicationImpl(new EngineImpl());
			application.setUnit(module.getUnit("DisableDataFlow"));
			EGraph graph = new EGraphImpl(ce);
			application.setEGraph(graph);
			while (application.execute(null)) {
				log.info("Executed DisableDataFlow", className);
			}
			ce = (CloudEnvironment) graph.getRoots().get(0);
			String json = runtimeModelLogic.returnentiremodelasjson(ce);
			log.jsonInfo("TEST:" + json, className);
			try {
				runtimeModelLogic.executeAdaptation(0, json, executeAdaptationURL, adaptationResultURL, null, taskId);
			} catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}else if (eventName.equals("DataManipulationDetected")) {
			HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
			
//			Module module = resourceSet.getModule("DataSendToNormalDatabaseWhileUnderAttack.henshin", false);
//			UnitApplication application = new UnitApplicationImpl(new EngineImpl());
//			application.setUnit(module.getUnit("RedirectDataToQuarantineDatabase"));
//			EGraph graph = new EGraphImpl(ce);
//			application.setEGraph(graph);
//			while (application.execute(null)) {
//				log.info("Executed RedirectDataToQuarantineDatabase", className);
//			}
//			ce = (CloudEnvironment) graph.getRoots().get(0);
			
//			Module module2 = resourceSet.getModule("DataReadByAdminFromNormalDatabaseWhileUnderAttack.henshin", false);
//			UnitApplication application2 = new UnitApplicationImpl(new EngineImpl());
//			application2.setUnit(module2.getUnit("DeactivateAdminReadAccessFromNormalDatabase"));
//			EGraph graph2 = new EGraphImpl(ce);
//			application2.setEGraph(graph2);
//			while (application2.execute(null)) {
//				log.info("Executed DeactivateAdminReadAccessFromNormalDatabase", className);
//			}
//			ce = (CloudEnvironment) graph2.getRoots().get(0);
			
			Module module3 = resourceSet.getModule("RobotIsActiveWhileUnderAttack.henshin", false);
			UnitApplication application3 = new UnitApplicationImpl(new EngineImpl());
			application3.setUnit(module3.getUnit("DeactivateRobot"));
			EGraph graph3 = new EGraphImpl(ce);
			application3.setEGraph(graph3);
			while (application3.execute(null)) {
				log.info("Executed DeactivateRobot", className);
			}
			ce = (CloudEnvironment) graph3.getRoots().get(0);
			
//			Module module4 = resourceSet.getModule("DataReadByUserFromNormalDatabaseWhileUnderAttack.henshin", false);
//			UnitApplication application4 = new UnitApplicationImpl(new EngineImpl());
//			application4.setUnit(module4.getUnit("DeactivateUserReadAccessFromNormalDatabase"));
//			EGraph graph4 = new EGraphImpl(ce);
//			application4.setEGraph(graph4);
//			while (application4.execute(null)) {
//				log.info("Executed DeactivateUserReadAccessFromNormalDatabase", className);
//			}
//			ce = (CloudEnvironment) graph4.getRoots().get(0);
			
			String json = runtimeModelLogic.returnentiremodelasjson(ce);
			try {
				runtimeModelLogic.executeAdaptation(0, json, executeAdaptationURL, adaptationResultURL, null, taskId);
			} catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
