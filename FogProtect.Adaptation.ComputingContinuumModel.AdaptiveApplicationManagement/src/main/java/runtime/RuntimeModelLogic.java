package runtime;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import utility.MatchRepresentation;
import utility.NodeRepresentation;
import utilityClasses.HttpDeleteWithBody;

@Component
public class RuntimeModelLogic {

	@Autowired
	private EMFModelLoad loader;

	@Autowired
	PersonalLogger log;

	@Value("${ExecuteAdaptationURL}")
	String executeAdaptationURL;

	@Value("${CurrentUseCase}")
	String currentUC;
	
	@Value("${ExecuteAdaptationURLWP6}")
	String executeAdaptationURLWP6;

	@Autowired
	@Lazy
	private KafkaTemplate<String, String> kafkaTemplate;

	private static String className = RuntimeModelLogic.class.getName();

	public RuntimeModelLogic() {
		EPackage.Registry.INSTANCE.getEPackage("www.ComputingContinuumModel.com");
	}
	
	// A couple of hacks for UC3
	// Store the IDs of users from Keycloak to avoid extending the meta-model yet again.
	private static HashMap<String, String> UC3_User_IDs = new HashMap<String, String>(Map.of(
		    "atc-eu1", "d565ab40-29f7-438b-8a77-c7a9c0186135",
		    "atc-eu2", "e0318866-0ea8-49e8-a452-250bb4f7e433",
		    "atc-vbu1", "51b17f2f-8c73-4752-81d5-9ff66ff75825",
		    "atc-vbu2", "f05b4b6f-ac11-4168-aa88-b92498b7ee33"
	));
	// Store the previous "blocked" state of users / groups in order to avoid spamming WP6
	private static HashMap<String, Boolean> UC3_Is_Blocked = new HashMap<String, Boolean>(Map.of(
		    "atc-eu1", false,
		    "atc-eu2", false,
		    "atc-vbu1", false,
		    "atc-vbu2", false,
		    "ATC", false
	));

	// Returns the instance for id "i" as a JSON Represenation
	public String returnentiremodelasjson(long i) {
		// Accessing the model information
		String jsonString = "";
		try {
			jsonString = loader.getMapper().writerWithDefaultPrettyPrinter()
					.writeValueAsString(loader.getCloudEnvironmentsMonitored().get(i));
			jsonString = jsonString.replaceAll("#", "");
		} catch (JsonProcessingException e) {
			jsonString = "FAILURE";
			e.printStackTrace();
		}
		log.jsonInfo(jsonString, className);
		return jsonString;
	}

	// Returns a JSON Representation of a .computingcontinuummodel file
	public String returnentiremodelasjsonFromFile(String jsonModelFileName) {
		// Accessing the model information
		String jsonString = "";
		try {
			CloudEnvironment ce = loader.loadFromFile(jsonModelFileName);
			jsonString = loader.getMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ce);
			jsonString = jsonString.replaceAll("#", "");
		} catch (JsonProcessingException e) {
			jsonString = "FAILURE";
			e.printStackTrace();
		}
		log.jsonInfo(jsonString, className);
		return jsonString;
	}

	// Returns the CloudEnvironment object as a JSON Represenation
	public String returnentiremodelasjson(CloudEnvironment proposal) {
		// Accessing the model information
		String jsonString = "";
		try {
			jsonString = loader.getMapper().writerWithDefaultPrettyPrinter().writeValueAsString(proposal);
			jsonString = jsonString.replaceAll("#", "");
		} catch (JsonProcessingException e) {
			jsonString = "FAILURE";
			e.printStackTrace();
		}
		log.jsonInfo(jsonString, className);
		return jsonString;
	}

	// Returns every instance from the set of monitored CloudEnvironments
	public String returnentiremodelasjsonforall() {
		String everyInstance = "";
		for (Long i : loader.getCloudEnvironmentsMonitored().keySet()) {
			everyInstance += "Instance with id " + i + ":\n" + this.returnentiremodelasjson(i) + "\n\n";
		}
		return everyInstance;
	}

	public void sendAnswerToAdaptationCoordinator(int id, String urlAdaptationCoordinatorProposedAdaptation,
			JsonArray adaptations, String taskId) {
		JsonObject head = new JsonObject();
		head.addProperty("ID", id);
		head.addProperty("SieaTaskId", taskId);
		head.add("Adaptations", adaptations);
		try {
			makeRequest(urlAdaptationCoordinatorProposedAdaptation, head.toString());
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}

	private HashMap<String, String> analyzePlanUC2(JsonObject asIsModel) {
		int level = -1;
		HashMap<String, String> variables = null;
		for (JsonElement je : asIsModel.get("tosca_nodes_root").getAsJsonArray()) {
			JsonObject jo = je.getAsJsonObject();
			if (jo.get("name").getAsString().equals("ReadDataFlow (User-NormalDB)")) {
				if (jo.get("disab").getAsBoolean() == true) {
					level = 1;
				} else {
					level = 0;
				}
			}
			if (jo.get("name").getAsString().equals("WriteToQuarantineDatabase")) {
				if (jo.get("disab").getAsBoolean() == false) {
					level = 2;
				}
			}
		}
		if (level == 0) {
			variables = new HashMap<String, String>();
			variables.put("level", "LEVEL_0_ACCESS");
		} else if (level == 1) {
			variables = new HashMap<String, String>();
			variables.put("level", "LEVEL_1_ACCESS");
		} else if (level == 2) {
			variables = new HashMap<String, String>();
			variables.put("level", "LEVEL_2_ACCESS");
		} else {
			log.error("AsIs model does not contain the expected UC2 components.", className);
			// throw new Exception("AsIs model does not contain the expected components.");
		}
		return variables;
	}

	private HashMap<String, String> analyzePlanUC1(JsonObject asIsModel) {
		String lamp = "";
		HashMap<String, String> variables = null;
		int found = 0;
		for (JsonElement je : asIsModel.get("tosca_nodes_root").getAsJsonArray()) {
			JsonObject jo = je.getAsJsonObject();
			// Get private space of the compromised lamp post
			if (jo.get("@id").getAsString().equals("//@tosca_nodes_root.24")) {
				lamp = jo.get("trustworthy").getAsString();
				found++;
			}
		}
		if (found == 1) {
			variables = new HashMap<String, String>();
			if (lamp.compareTo("LOW") == 0) {
				variables.put("physical_tampering", "true");
				variables.put("cleared_by_operator", "false");
			} else if (lamp.compareTo("HIGH") == 0) {
				variables.put("physical_tampering", "false");
				variables.put("cleared_by_operator", "true");
			}
		} else {
			log.error("AsIs model does not contain the expected UC1 components.", className);
			// throw new Exception("AsIs model does not contain the expected components.");
		}
		return variables;
	}

	public void executeAdaptation(int id, String referenceModel, String executeAdaptationURL,
			String adaptationResultURL, String adaptationName, String taskId) throws Exception {
		boolean adaptationResult = false;

		JsonObject asIsModel = new Gson().fromJson(referenceModel, JsonObject.class);

		HashMap<String, String> variables = new HashMap<String, String>();

		PolicyGenerator.UseCase uc;

		log.info("Executing adaptation...", className);

		try {
			// We'll have to specialize this part of the code for each UC
			{
				if (currentUC.equalsIgnoreCase("UC2")) {
					log.info("Current use case is UC2", className);

					// TODO maybe add task Id?
					if (adaptationName != null && adaptationName.equals("TBD_adaptationName_with_triggers_timeawareadaptation")) {
						this.triggerTimeAwareAdaptation("FiaBContainerStatus", "timeawareadaptation_msg", 3000);
					}

					variables = analyzePlanUC2(asIsModel);
					if (variables != null) {
						uc = PolicyGenerator.UseCase.UC2;
					} else {
						throw new Exception("AsIs model does not contain the expected components for UC2.");
					}
				} else if (currentUC.equalsIgnoreCase("UC1")) {
					log.info("Current use case is UC1", className);
					variables = analyzePlanUC1(asIsModel);

					if (variables != null) {
						uc = PolicyGenerator.UseCase.UC1;
					} else {
						throw new Exception("AsIs model does not contain the expected components for UC1.");
					}

				} else if (currentUC.equalsIgnoreCase("UC3")) {
				    if (adaptationName != null && adaptationName.equals("TBD_adaptationName_with_triggers_timeawareadaptation")) {
					this.triggerTimeAwareAdaptation("FiaBContainerStatus", "timeawareadaptation_msg", 3000);
				    }
				    uc = PolicyGenerator.UseCase.UC1;
				} else {
					throw new Exception("AsIs model does not contain the expected components for any use case. currentUC=" + currentUC);
				}
			}
			
			System.out.println("ADAP: " + adaptationName);

			// TODO: CHECK HOW TO ACTUALLY EXECUTE THINGS IN UC1
			if (currentUC.equalsIgnoreCase("UC1")) {
				// the URL should probably be /api/v1/ssla
				// TODO maybe add task id
				
				// Event 1: Disable DataFlow 
				if(adaptationName.equalsIgnoreCase("CompromisedLampPost: Adaptation: CompromisedLampPost | ")) {
					log.info("UC1 Event: Redirection", className);

					// Send to WP6
					log.info("\tSending DELETE request to WP6... ", className);
					// int statusCodeWP6 = makeSSLAAndServicesDeleteRequest(executeAdaptationURLWP6,
					//		"src/main/resources/UC1/ssla_tls_server.xml", "src/main/resources/UC1/Redirection_Event/DELETE_Request/istio-gateways-config.yaml");
					// log.info("\t\t -> Status code for DELETE request to WP6: " + statusCodeWP6, className);
					
					log.info("\tSending POST request to WP6... ", className);
					// statusCodeWP6 = this.makeSSLAAndServicesPostRequest(executeAdaptationURLWP6,
					//		"src/main/resources/UC1/ssla_tls_server.xml", "src/main/resources/UC1/Redirection_Event/POST_Request/istio-gateways-config.yaml");
					// log.info("\t\t -> Status code for POST request to WP6: " + statusCodeWP6, className);
				
					// Send to WP2
					// curl --request POST --header 'Content-Type: application/x-www-form-urlencoded' --data origin=172.25.2.3 --data destination=172.25.2.5 --data redirect=true https://fogprotect-uc1.ubiwhere.com/api/traffic-cams/redirection/
				    JSONObject jsonObjectForWP2 = new JSONObject();
				    try {
				    	jsonObjectForWP2.put("origin", "172.25.2.3");	// ubitnvidia.ubiwhere.lan
				    	jsonObjectForWP2.put("destination", "172.25.2.5");	// ubitfognvidia.ubiwhere.lan
				    	jsonObjectForWP2.put("redirect", "true");
					} catch (JSONException e) {
						e.printStackTrace();
					} try {
						log.info("\tSending POST request to WP2... ", className);
				    	String returnValueWP2 = makeHTTPPostRequest(executeAdaptationURL, jsonObjectForWP2);
						log.info("\t\t -> Status code for POST request to WP2: " + returnValueWP2, className);
				    } catch (Exception e) {
						e.printStackTrace();
				    }
					adaptationResult = true;
					
				// Event 2: Clearance 
				} else {
					log.info("UC1 Event: Clearance", className);
					// Send to WP6
					log.info("\tSending DELETE request to WP6... ", className);
					// int statusCodeWP6 = makeSSLAAndServicesDeleteRequest(executeAdaptationURLWP6,
					//		"src/main/resources/UC1/ssla_tls_server.xml", "src/main/resources/UC1/Clearance_Event/DELETE_Request/istio-gateways-config.yaml");
					// log.info("\t\t -> Status code for DELETE request to WP6: " + statusCodeWP6, className);
					
					log.info("\tSending POST request to WP6... ", className);
					// statusCodeWP6 = this.makeSSLAAndServicesPostRequest(executeAdaptationURLWP6,
					//		"src/main/resources/UC1/ssla_tls_server.xml", "src/main/resources/UC1/Clearance_Event/POST_Request/istio-gateways-config.yaml");
					// log.info("\t\t -> Status code for POST request to WP6: " + statusCodeWP6, className);
					
					// Send to WP2
					// curl --request POST --header 'Content-Type: application/x-www-form-urlencoded' --data origin=172.25.2.3 --data destination=172.25.2.5 --data redirect=false https://fogprotect-uc1.ubiwhere.com/api/traffic-cams/redirection/
					JSONObject jsonObjectForWP2 = new JSONObject();
				    try {
				    	jsonObjectForWP2.put("origin", "172.25.2.3");	// ubitnvidia.ubiwhere.lan
				    	jsonObjectForWP2.put("destination", "172.25.2.5");	// ubitfognvidia.ubiwhere.lan
				    	jsonObjectForWP2.put("redirect", "false");
					} catch (JSONException e) {
						e.printStackTrace();
					} try {
						log.info("\tSending POST request to WP2... ", className);
				    	String returnValueWP2 = makeHTTPPostRequest(executeAdaptationURL, jsonObjectForWP2);
						log.info("\t\t -> Status code for POST request to WP2: " + returnValueWP2, className);
				    } catch (Exception e) {
						e.printStackTrace();
				    }
					adaptationResult = true;
				}

			} else if(currentUC.equalsIgnoreCase("UC2")) {
				// WP6 policy file
				String newPolicy = PolicyGenerator.generateXMLPolicy(variables, uc);
				log.jsonInfo("Generated policy:\n" + newPolicy, className);

				int response = 0;
				try {
					HashMap<String, String> properties = new HashMap<String, String>();
					properties.put("Content-Type", "application/xml");
					properties.put("Accept", "application/xml");
					response = makeHttpRequest(executeAdaptationURL, "PUT", "UTF-8", properties, newPolicy);
				} catch (Exception e) {
					e.printStackTrace();
				}
				// normal Info : Response Code : -
				log.info("Response Code: " + response, className);
				if (response != 200) {
					// logged as warn due to previous message "[WARN] ..."
					log.warn("adaptationResult is always set to true even if the response is != 200", className);
					adaptationResult = false;
				}
				if (response == 200) {
					// logged as warn due to previous message "[WARN] ..."
					adaptationResult = true;
				}
				
				//Execution to Nokias Kafka (
				String integrity_status = ""; //robot
				String intruder_alert = "";	//dasboard warning
				JsonObject integrityJSON = new JsonObject();
				JsonObject intruderJSON = new JsonObject();
				if(variables.get("level").equals("LEVEL_0_ACCESS")) {
					intruderJSON.addProperty("status", false);
				}else if(variables.get("level").equals("LEVEL_1_ACCESS")) {
					intruderJSON.addProperty("status", true);
				}else if(variables.get("level").equals("LEVEL_2_ACCESS")) {
					intruderJSON.addProperty("status", true);
				}
				boolean robotNeedsToStop = false;  
				for (JsonElement je : asIsModel.get("tosca_nodes_root").getAsJsonArray()) {
					JsonObject jo = je.getAsJsonObject();
					if (jo.get("name").getAsString().equals("Robot Operation")) {
						if (jo.get("isActive").getAsBoolean() == false) {
							robotNeedsToStop = true;
						}
					}
				}
				if(robotNeedsToStop==true) {
					integrityJSON.addProperty("status", false);
				}else {
					integrityJSON.addProperty("status", true);
				}
				integrity_status = integrityJSON.toString();
				intruder_alert = intruderJSON.toString();
				kafkaTemplate.send("integrity_status", integrity_status);
				log.info("Sent " + integrity_status + " to topic " + "integrity_status", className);
				kafkaTemplate.send("intruder_alert", intruder_alert);
				log.info("Sent " + intruder_alert + " to topic " + "intruder_alert", className);
						
				// UC3 events
			} else if(currentUC.equalsIgnoreCase("UC3")) {
			    log.info("Executing UC3 adaptation", className);

				JsonObject jsonToBeSent = new JsonObject();

				if(adaptationName != null && adaptationName.contains("blockUserAdaptation")) {
				    log.info("Executing UC3 BlockUser adaptation", className);
				    log.info("Checking users", className);
				    
					for (JsonElement je : asIsModel.get("tosca_nodes_root").getAsJsonArray()) {
						JsonObject jo = je.getAsJsonObject();

						// String uid = "51b17f2f-8c73-4752-81d5-9ff66ff75825"; 
						// String uid = "d565ab40-29f7-438b-8a77-c7a9c0186135";
						
						if (jo.get("type").getAsString().contains("DataProcessor")) {
							String userName = jo.get("name").getAsString(); 
							String uid = UC3_User_IDs.get(userName);
							log.info("Username: " + userName + " uid: " + uid, className);
							
							if (uid != null && UC3_Is_Blocked.get(userName) == false && jo.get("blocked").getAsBoolean() == true) {
								log.info("Blocking user " + userName, className);
								String toSend = new String(Files.readAllBytes(Paths.get("src/main/resources/UC3/year2_block_user_{USERNAME}.xml")));
								toSend = toSend.replace("{USERNAME}", userName);
								toSend = toSend.replace("{USERID}", uid);
								
								HashMap<String, String> properties = new HashMap<String, String>();
								properties.put("Content-Type", "application/xml");
								properties.put("Accept", "application/xml");								
							    try {
							    	// TODO: Maybe add this to an env variable
							    	int response = makeHttpRequest(executeAdaptationURL, "PUT", "UTF-8", properties, toSend);
							    	log.info("Blocked user response: " + Integer.toString(response), className);
							    }
							    catch (Exception e) {
									// Auto-generated catch block
									e.printStackTrace();
									log.info("Blocked user failed", className);
							    }
							    // Store the state of blocking users / groups
							    UC3_Is_Blocked.put(userName, true);
										
							    // We disable the automatic triggering of unblock access for testing purposes.
//								jsonToBeSent.addProperty("sub", uid);
//								jsonToBeSent.addProperty("user", userName);
//								jsonToBeSent.addProperty("org", "ATC");
//								jsonToBeSent.addProperty("reason", "asdfasdf");
//								new Thread(new Runnable() {
//									@Override
//									public void run() {
//									    log.info("Starting thread.", className);
//									    triggerTimeAwareAdaptation("unblock-access", jsonToBeSent.toString(), 30000);
//									}
//							   }).start();	
							}
						}
					}
				

					// Message will be sent after 10 sec
					// triggerTimeAwareAdaptation("unblock-access", jsonToBeSent.toString(), 10000);
					
				} else if(adaptationName != null && adaptationName.contains("reverseBlockUser")) {
				    log.info("Executing UC3 UnblockUser adaptation", className);
				    log.info("Checking users", className);
				    
					for (JsonElement je : asIsModel.get("tosca_nodes_root").getAsJsonArray()) {
						JsonObject jo = je.getAsJsonObject();
						if (jo.get("type").getAsString().contains("DataProcessor")) {						
							String userName = jo.get("name").getAsString(); 
							String uid = UC3_User_IDs.get(userName);
							
							if (uid != null && UC3_Is_Blocked.get(userName) == true && jo.get("blocked").getAsBoolean() == false) {
								log.info("Unblocking user " + userName + " uid " + uid, className);
								String toSend = new String(Files.readAllBytes(Paths.get("src/main/resources/UC3/year2_unblock_user_{USERNAME}.xml")));
								toSend = toSend.replace("{USERNAME}", userName);
								toSend = toSend.replace("{USERID}", uid);
								
								HashMap<String, String> properties = new HashMap<String, String>();
								properties.put("Content-Type", "application/xml");
								properties.put("Accept", "application/xml");								
							    try {
							    	// TODO: Maybe add this to an env variable
							    	int response = makeHttpRequest(executeAdaptationURL, "PUT", "UTF-8", properties, toSend);
							    	log.info("Unblock user response: " + Integer.toString(response), className);
							    }
							    catch (Exception e) {
									// Auto-generated catch block
									e.printStackTrace();
							    }
							    
							    // Store the state of blocking users / groups
							    UC3_Is_Blocked.put(userName, false);
							}
						}
					}
				
					// Message will be sent after 10 sec
					// triggerTimeAwareAdaptation("unblock-access", jsonToBeSent.toString(), 10000);
					
				} else if(adaptationName != null && adaptationName.equals("BlockRole")) {
					jsonToBeSent.addProperty("role", "Video-Booth-User");
					jsonToBeSent.addProperty("org", "ATC");
					jsonToBeSent.addProperty("reason", "asdfasdf");
					
					// Message will be sent after 1 sec
					triggerTimeAwareAdaptation("unblock-access", jsonToBeSent.toString(), 1000);

					
				} else if(adaptationName != null && adaptationName.contains("blockOrganisationAdaptation")) {
				    log.info("Executing UC3 BlockOrganisation adaptation", className);
				    log.info("Checking groups", className);
				    
					for (JsonElement je : asIsModel.get("tosca_nodes_root").getAsJsonArray()) {
						JsonObject jo = je.getAsJsonObject();
						
					
						if (jo.get("type").getAsString().contains("Group")) {
							String groupName = jo.get("name").getAsString();
							Boolean orgWasBlocked = UC3_Is_Blocked.get(groupName);
							if (orgWasBlocked != null && orgWasBlocked == false && jo.get("blocked").getAsBoolean() == true) {
								log.info("Blocking organization " + groupName, className);
								String toSend = new String(Files.readAllBytes(Paths.get("src/main/resources/UC3/year2_block_organization_{ORGANIZATION_NAME}.xml")));
								toSend = toSend.replace("{ORGANIZATION_NAME}", groupName);
								
								HashMap<String, String> properties = new HashMap<String, String>();
								properties.put("Content-Type", "application/xml");
								properties.put("Accept", "application/xml");								
							    try {
							    	// TODO: Maybe add this to an env variable
							    	int response = makeHttpRequest(executeAdaptationURL, "PUT", "UTF-8", properties, toSend);
							    	log.info("Blocked organization response: " + Integer.toString(response), className);
							    }
							    catch (Exception e) {
									// Auto-generated catch block
									e.printStackTrace();
									log.info("Blocked organization failed", className);
							    }
							    
							    UC3_Is_Blocked.put(groupName, true);
									
							    // We don't need to unblock organizations automatically
//								jsonToBeSent.addProperty("sub", uid);
//								jsonToBeSent.addProperty("user", userName);
//								jsonToBeSent.addProperty("org", "ATC");
//								jsonToBeSent.addProperty("reason", "asdfasdf");
//								new Thread(new Runnable() {
//									@Override
//									public void run() {
//									    log.info("Starting thread.", className);
//									    triggerTimeAwareAdaptation("unblock-access", jsonToBeSent.toString(), 30000);
//									}
//							   }).start();	
							}
						}
					}
					
					// Message will be sent after 1 sec
					// triggerTimeAwareAdaptation("unblock-access", jsonToBeSent.toString(), 1000);
				} else if(adaptationName != null && adaptationName.contains("reverseBlockOrganisation")) {
				    log.info("Executing UC3 UnblockingOrganisation adaptation", className);
				    log.info("Checking groups", className);
				    
					for (JsonElement je : asIsModel.get("tosca_nodes_root").getAsJsonArray()) {
						JsonObject jo = je.getAsJsonObject();
					
						if (jo.get("type").getAsString().contains("Group")) {
							String groupName = jo.get("name").getAsString();
							Boolean orgWasBlocked = UC3_Is_Blocked.get(groupName);
							if (orgWasBlocked != null && orgWasBlocked == true &&  jo.get("blocked").getAsBoolean() == false) {
								log.info("Unblocking organization " + groupName, className);
								String toSend = new String(Files.readAllBytes(Paths.get("src/main/resources/UC3/year2_unblock_organization_{ORGANIZATION_NAME}.xml")));
								toSend = toSend.replace("{ORGANIZATION_NAME}", groupName);
								
								HashMap<String, String> properties = new HashMap<String, String>();
								properties.put("Content-Type", "application/xml");
								properties.put("Accept", "application/xml");								
							    try {
							    	// TODO: Maybe add this to an env variable
							    	int response = makeHttpRequest(executeAdaptationURL, "PUT", "UTF-8", properties, toSend);
							    	log.info("Unblock organization response: " + Integer.toString(response), className);
							    }
							    catch (Exception e) {
									// Auto-generated catch block
									e.printStackTrace();
									log.info("Unblock organization failed", className);
							    }
							    
							    UC3_Is_Blocked.put(groupName, false);
							}
						}
					}
				} else {
				    log.error("Adaptation not known: " + adaptationName, className);
				}
				
				
			    adaptationResult = true;
			}

		} catch (NullPointerException e) {

			log.error("Could not create policy", className);
			e.printStackTrace();
			adaptationResult = false; // ForTestingOfUC1
		}

		// Send Result to Adaptation Coordinator
		sendAdaptationResult(id, referenceModel, adaptationResultURL, adaptationResult, taskId);

	}

	public void sendAdaptationResult(int id, String referenceModel, String urlAdapationCoordinatorAdapaptationResult,
			boolean successful, String taskId) {
		JsonObject head = new JsonObject();
		head.addProperty("ID", id);
		head.addProperty("SieaTaskId", taskId);
		head.addProperty("Successful", successful);
		head.add("ExecutedModel", new JsonParser().parse(referenceModel).getAsJsonObject());
		try {
			makeRequest(urlAdapationCoordinatorAdapaptationResult, head.toString());
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Post Request to given URL with given String as Body
	public int makeRequest(String url, String requestBody) throws IOException {
		log.jsonInfo(requestBody, className);
		HttpURLConnection request;
		URL restUrl = new URL(url);
		request = (HttpURLConnection) restUrl.openConnection();
		request.setRequestMethod("POST");
		request.setDoOutput(true);
		Charset encoding = Charset.forName("UTF-8");
		request.setRequestProperty("Content-Type", "application/json");
		request.getOutputStream().write(requestBody.getBytes(encoding));
		log.info("Request sent to " + url, className);
		int responseCode = request.getResponseCode();
		if (responseCode != 200) {
			log.error(request.getResponseMessage(), className);
		}
		return responseCode;
	}

	public int makeHttpRequest(String url, String method, String enc, Map<String, String> properties,
			String requestBody) throws IOException {

		HttpURLConnection request;
		URL restUrl = new URL(url);
		request = (HttpURLConnection) restUrl.openConnection();
		request.setRequestMethod(method);
		request.setDoOutput(true);
		Charset encoding = Charset.forName(enc);
		for (String k : properties.keySet()) {
			request.addRequestProperty(k, properties.get(k));
		}
		request.getOutputStream().write(requestBody.getBytes(encoding));
		int responseCode = request.getResponseCode();
		if (responseCode != 200) {
			InputStream response = request.getInputStream();
			String message = new String(response.readAllBytes(), StandardCharsets.UTF_8);
			log.jsonInfo(method, className);
		}
		return responseCode;
	}

	public EMFModelLoad getLoader() {
		return loader;
	}

	public void delete(long i) {
		loader.deleteCloundEnvironmentMonitored(i, this);
	}

	// Used to find an object by its EMF generated id (@id in json) in a given
	// CloudEnvironment graph
	public EObject searchForObjectInGivenModel(CloudEnvironment cloudEnvironment, String id) {
		EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
		for (tosca_nodes_Root element : nodes) {
			Resource resource = element.eResource();
			String toTest = ((XMIResource) resource).getURIFragment(element);
			if (toTest.equals(id)) {
				return element;
			}
		}
		return null;
	}

	// FIXME getRisks potential Error
	public void sendFoundRisksToStatusInformation(List<MatchRepresentation> firstFoundRisks, String url,
			String taskId) {
		JsonObject head = new JsonObject();
		head.addProperty("SieaTaskId", taskId);
		JsonArray risks = new JsonArray();
		for (MatchRepresentation matchRep : firstFoundRisks) {
			JsonObject risk = new JsonObject();
			risk.addProperty("Name", matchRep.getRuleName());
			risk.addProperty("Description", "Description will be added soon...");
			JsonArray nodesForRisk = new JsonArray();
			for (NodeRepresentation nodeRep : matchRep.getNodes()) {
				JsonObject nodeForNodesForRisk = new JsonObject();
				nodeForNodesForRisk.addProperty("Name", nodeRep.getObjectName());
				nodeForNodesForRisk.addProperty("@id", nodeRep.getAtId());
				nodesForRisk.add(nodeForNodesForRisk);
			}
			risk.add("Nodes", nodesForRisk);
			risks.add(risk);
		}

		head.add("risks", risks);

		try {
			makeRequest(url, head.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Deprecated
	// -------------------------------------------------------------------------------------------------------------------------

	// Used to create readable JsonStrings - Not used anymore because mapper can do
	// the same
	public String prettifyJson(String jsonString) {
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);
		return prettyJson;
	}

	/**
	 * Starts a thread, which pushes the message "producerMessage" into the topic
	 * "topicName" after a time "time"
	 */
	// @Async
	public void triggerTimeAwareAdaptation(String topicName, String producerMessage, Integer time) {
	    log.info("Starting TimeAwareAdaptation timer for " + Integer.toString(time), className);
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		kafkaTemplate.send(topicName, producerMessage);
		log.info("[TimeAwareAdaptation] Sended " + producerMessage + " to topic '" + topicName + "'", className);
	}

	/**
	 * 
	 * @param url              URL of WP6 to be called
	 * @param sslaFilePath     Path to SSLA file
	 * @param servicesFilePath Path to the services file (should be the
	 *                         istio-gateways-config file)
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public int makeSSLAAndServicesPostRequest(String url, String sslaFilePath, String servicesFilePath) {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Content-type", "application/xml");
		
		File sslaFile = new File(sslaFilePath);
		File servicesFile = new File(servicesFilePath);

		FileBody uploadFilePartSSLA = new FileBody(sslaFile);
		FileBody uploadFilePartServices = new FileBody(servicesFile);

		MultipartEntity reqEntity = new MultipartEntity();
		reqEntity.addPart("ssla", uploadFilePartSSLA);
		reqEntity.addPart("services", uploadFilePartServices);

		//log.info("Sending SSLA and services to WP6:", className);
		//this.printFileContent(sslaFilePath);
		//log.info("\n\n\n", className);
		//this.printFileContent(servicesFilePath);

		httpPost.setEntity(reqEntity);

		int statusCode = -1;
		try {
			statusCode = httpClient.execute(httpPost).getStatusLine().getStatusCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return statusCode;
	}
	
	/**
	 * 
	 * @param url              URL of WP6 to be called
	 * @param sslaFilePath     Path to SSLA file
	 * @param servicesFilePath Path to the services file (should be the
	 *                         istio-gateways-config file)
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public int makeSSLAAndServicesDeleteRequest(String url, String sslaFilePath, String servicesFilePath) {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpDeleteWithBody httpDelete = new HttpDeleteWithBody(url);
		httpDelete.setHeader("Content-type", "application/xml");
		
		File sslaFile = new File(sslaFilePath);
		File servicesFile = new File(servicesFilePath);

		FileBody uploadFilePartSSLA = new FileBody(sslaFile);
		FileBody uploadFilePartServices = new FileBody(servicesFile);

		MultipartEntity reqEntity = new MultipartEntity();
		reqEntity.addPart("ssla", uploadFilePartSSLA);
		reqEntity.addPart("services", uploadFilePartServices);

		//log.info("Sending SSLA and services to WP6:", className);
		//this.printFileContent(sslaFilePath);
		//log.info("\n\n\n", className);
		//this.printFileContent(servicesFilePath);

		httpDelete.setEntity(reqEntity);

		int statusCode = -1;
		try {
			statusCode = httpClient.execute(httpDelete).getStatusLine().getStatusCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return statusCode;
	}

	/**
	 * Prints content of a file
	 * 
	 * @param path path to the file
	 */
	private void printFileContent(String path) {

		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String line;
		try {
			line = in.readLine();
			while (line != null) {
				log.info(line, className);
				line = in.readLine();
			}
			in.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	// Send a HTTP Post request to URL "url" using the JSON object "jsonobject" 
	public static String makeHTTPPostRequest(String url, JSONObject jsonobject) {

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);

	    HttpEntity<String> request = new HttpEntity<String>(jsonobject.toString(), headers);
	    String resultAsJson = 
	    	      restTemplate.postForObject(url, request, String.class);
	    return resultAsJson;
	}
	
}
