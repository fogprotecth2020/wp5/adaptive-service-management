package runtime;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class PolicyGenerator {
	enum UseCase {
		UC1,
		UC2
	}
	
	private static final XMLPolicyGenerator xmlGenUC1 = new XMLPolicyGenerator("src/main/resources/policy_head_part_UC1.txt", "src/main/resources/policy_tail_part_UC1.txt");
	private static final XMLPolicyGenerator xmlGenUC2 = new XMLPolicyGenerator("src/main/resources/policy_head_part_UC2.txt", "src/main/resources/policy_tail_part_UC2.txt");
	
	public static String generateXMLPolicy(Map<String, String> variables, UseCase uc) {
		String policy;
		switch (uc) {
		case UC1:
			policy = xmlGenUC1.generateXMLPolicy(variables);
			break;
		case UC2:
			policy = xmlGenUC2.generateXMLPolicy_UC2(variables);
			break;
		default:
			throw new RuntimeException("UseCase not found!");
		}
		return policy;
	}

	private static class XMLPolicyGenerator {
		private String policyHeadPart;
		private String policyTailPart;
		// private String policyOPATemplate;
		
		public XMLPolicyGenerator(String head_path, String tail_path) {
			try {
				this.policyHeadPart = new String(Files.readAllBytes(Paths.get(head_path)));
				this.policyTailPart = new String(Files.readAllBytes(Paths.get(tail_path)));
	
				// System.out.println(this.policyHeadPart);
				// System.out.println(this.policyTailPart);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public String generateXMLPolicy(Map<String, String> variables) {
			StringBuilder opaBuilder = new StringBuilder();
			for (String key: variables.keySet()) {
				opaBuilder.append(key);
				opaBuilder.append(" := ");
				opaBuilder.append(variables.get(key));
				opaBuilder.append("\n");
			}
			
			StringBuilder sb = new StringBuilder();
			sb.append(this.policyHeadPart);
			sb.append("\n");
			sb.append(opaBuilder.toString());
			sb.append("\n");
			sb.append(this.policyTailPart);
			
			return sb.toString();
		}
		public String generateXMLPolicy_UC2(Map<String, String> variables) {
			StringBuilder opaBuilder = new StringBuilder();
			for (String key: variables.keySet()) {
				opaBuilder.append("<AuthorizationTarget>");
				opaBuilder.append(variables.get(key));
				opaBuilder.append("</AuthorizationTarget>");
			}
			
			StringBuilder sb = new StringBuilder();
			sb.append(this.policyHeadPart);
			sb.append("\n");
			sb.append(opaBuilder.toString());
			sb.append("\n");
			sb.append(this.policyTailPart);
			
			return sb.toString();
		}
	}
}
