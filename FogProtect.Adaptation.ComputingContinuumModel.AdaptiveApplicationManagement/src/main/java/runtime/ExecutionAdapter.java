package runtime;

import java.util.concurrent.CountDownLatch;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;

public abstract class ExecutionAdapter {
	protected boolean isSystemResponsePositive;
	protected CountDownLatch countDownLatch;
	

	public CountDownLatch getCountDownLatch() {
		return countDownLatch;
	}
	
	public void setCountDownLatch(CountDownLatch countDownLatch) {
		this.countDownLatch = countDownLatch;
	}

	public abstract boolean execute(CloudEnvironment proposal, int[] adaptationCaseNumber);
}
