package runtime;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import utility.Constants;

@Component
public class DisallowedAdaptationsManager {
	
	private List<String> disallowedRuleNames = new ArrayList<>();

	private JsonArray createAdaptationRulesAsJson() {
		JsonArray adaptationRules = new JsonArray();
		File[] directoryListing = new File(Constants.ADAPTATIONS_PATH).listFiles();
		
		//TODO what if there are no files?
		
		// iterate through the adaptation directory, and work with all files ending in .henshin
		for (File file : directoryListing) {
			String fileName = file.getName();
			if (fileName.endsWith(".henshin")) {
				JsonObject adaptationRule = new JsonObject();
				String rulesAsString = "";
				try {
					// as a .henshin file is purely text (xml-format), this works.
					rulesAsString = new String(Files.readAllBytes(file.toPath()));
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				if (!rulesAsString.equals("")) {
					JsonPrimitive rulesInJson = new JsonPrimitive(rulesAsString);
					JsonPrimitive fileNameJson = new JsonPrimitive(fileName);
					
					adaptationRule.add("Name", fileNameJson);
					adaptationRule.add("File", rulesInJson);
					adaptationRules.add(adaptationRule);
				}
			}
		}
			
		return adaptationRules;
	}
	
	public String collectAdaptationRules() {
		// if a new conflict identification is started, reset the disallowed rules
		// as in case of no conflict, no response will be sent.
		this.resetDisallowedRuleNames();
		return createAdaptationRulesAsJson().toString();
	}
	
	private void resetDisallowedRuleNames() {
		this.disallowedRuleNames = new ArrayList<>();
	}

	public void addDisallowedRules(List<String> completeNamesOfRules) {
		for (String completeNameOfRule: completeNamesOfRules) {
			disallowedRuleNames.add(completeNameOfRule);
		}
		
		Collections.sort(disallowedRuleNames);
	}
	
//	private void addToDisallowedRules(String fileName, String ruleName, HenshinResourceSet set) {
//		org.eclipse.emf.henshin.model.Module module = set.getModule(fileName);
//		Rule neededRule = module.getAllRules().stream().filter(rule -> rule.getName().equals(ruleName)).findFirst().orElse(null);
//		if (neededRule != null) {
//			disallowedRuleNames.add(neededRule);
//		}
//	}

	public List<String> getDisallowedRuleNames() {
		return this.disallowedRuleNames;
	}
	
}
