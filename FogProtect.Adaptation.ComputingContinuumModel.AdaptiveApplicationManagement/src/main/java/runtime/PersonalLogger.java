package runtime;

import java.util.logging.Logger;

import org.springframework.stereotype.Component;

@Component
public class PersonalLogger {
	
	// if true, includes messages with jsons
	private boolean logAll;
		
	// to get className - (Name of the class).class.getName()		
	// allows this class to get the specific logger for the class
		
	public void info(String msg, String className) {
		Logger.getLogger(className).info(msg);
	}
		
		
	public void jsonInfo(String msg, String className) {
		if (logAll) {
			Logger.getLogger(className).info(msg);
		}
	}
		
	public void setLogAll(boolean logAll) {
		this.logAll = logAll;
	}
		
	public void error(String msg, String className) {
		Logger.getLogger(className).severe(msg);
	}
	
	public void warn(String msg, String className) {
		Logger.getLogger(className).warning(msg);
	}
}
