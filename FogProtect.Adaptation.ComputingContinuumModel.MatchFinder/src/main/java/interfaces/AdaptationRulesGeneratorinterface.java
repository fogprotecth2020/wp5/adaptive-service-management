package interfaces;

import java.util.List;

import org.eclipse.emf.henshin.model.Rule;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import utility.MatchRepresentation;

public interface AdaptationRulesGeneratorinterface 
{
	/**
	 * Generates Henshin Rules that might resolve all risks in the runtime model.<br>
	 * Input: A list of all matches<br>
	 * Output: An array of Henshin Rules
	 * @param matches
	 * @return
	 */
	public Rule[] getAdaptationRules(List<MatchRepresentation> matches, CloudEnvironment cloudEnvironment);
}
