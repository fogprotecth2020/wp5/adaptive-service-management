package interfaces;

import java.util.List;

import org.eclipse.emf.henshin.interpreter.EGraph;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import utility.MatchRepresentation;

public interface SearchInitiatorInterface 
{
	/**
	 *  
	 *  
	 * @param cloudEnvironment CloudEnvironment object that should be checked for risks.<br>
	 * @return A list of MatchRepresentations containing all instances of rikspatterns found in the runtimemodel.
	 */
	public List<MatchRepresentation> findMatches(CloudEnvironment cloudEnvironment);
	
	/**
	 *  
	 *  
	 * @param graph EGraph object that should be checked for risks.<br>
	 * @return A list of MatchRepresentations containing all instances of rikspatterns found in the runtimemodel.
	 */
	public List<MatchRepresentation> findMatches(EGraph graph);
}
