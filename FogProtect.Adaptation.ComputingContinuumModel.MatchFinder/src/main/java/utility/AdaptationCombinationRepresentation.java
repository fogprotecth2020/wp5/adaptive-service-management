package utility;

import java.util.Comparator;

import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;

public class AdaptationCombinationRepresentation {
    private final int id;
    private AtomicAdaptation[] atomicAdaptationArray;
    private double totalCost = 0;
    private CloudEnvironment runtimeModel;
    private long createdAt = 0;
    private int amountOfWorkingFunctions = 0;
    private double energyConsumption = 0.0;
    private double performance;

    public AdaptationCombinationRepresentation(int id, AtomicAdaptation[] atomicAdaptationArray, EGraph runtimeModel,
	    double totalCost, long createdAt, int workingFunctions, double energyConsumption, double performance) {
		this.id = id;
		this.atomicAdaptationArray = atomicAdaptationArray;
//		this.setAtomicAdaptationArray(atomicAdaptationArray);
		this.runtimeModel = (CloudEnvironment) runtimeModel.getRoots().get(0);
		this.totalCost = totalCost;
		this.createdAt = createdAt;
		this.amountOfWorkingFunctions = workingFunctions;
		this.energyConsumption = energyConsumption;
		this.performance = performance;
    }

    //unused
    public void calculateCost() {
    	this.totalCost = new ModelAnalyzer().calculateTotalUsageCosts(new EGraphImpl(runtimeModel));
    }

    
//    /**
//     * This comparator allows comparison between
//     * AdaptationCombinationRepresentations based on the amount of working
//     * functions. <br>
//     * 1 = o1 > o2 <br>
//     * 0 = o1 = o2 <br>
//     * -1 = 01 < o2 <br>
//     * Using the default sort method of the Collections class will generate a list
//     * sorted in descending order (NOT ascending as usual!)
//     */
//    public class Comparator<AdaptationCombinationRepresentation> functionComparator{
//    	
//    	
//	public int compare(AdaptationCombinationRepresentation o1, AdaptationCombinationRepresentation o2) {
//	    if (o2.getAmountOfWorkingFunctions() < o1.getAmountOfWorkingFunctions()) {
//	    	return -1;
//	    } else {
//	    	if (o2.getAmountOfWorkingFunctions() > o1.getAmountOfWorkingFunctions()) {
//	    		return 1;
//	    	} else {
//	    		if (o2.getAmountOfWorkingFunctions() == o1.getAmountOfWorkingFunctions()) {
//	    			if (o2.getTotalCost() > o1.getTotalCost()) {
//	    				return -1;
//	    			} else {
//	    				if (o2.getTotalCost() < o1.getTotalCost()) {
//	    					return 1;
//	    				} else {
//	    					if (o2.getTotalCost() == o1.getTotalCost()) {
//	    						if (o2.getEnergyConsumption() > o1.getEnergyConsumption()) {
//	    							return -1;
//	    						} else {
//	    							if (o2.getEnergyConsumption() < o2.getEnergyConsumption()) {
//	    								return 1;
//	    							} else {
//	    								return 0;
//	    							}
//			    			
//			    		}
//			    	}
//			    }
//			}
//		    }
//		}
//	    }
//	    return 0;
//	}
//    }

    
    public CloudEnvironment getRuntimeModel() {
    	return runtimeModel;
    }

    public void setRuntimeModel(CloudEnvironment runtimeModel) {
		this.runtimeModel = runtimeModel;
    }

    public int getId() {
    	return id;
    }

    public double getTotalCost() {
    	return totalCost;
    }

    public void setTotalCost(double totalCost) {
    	this.totalCost = totalCost;
    }


    public AtomicAdaptation[] getAtomicAdaptationArray() {
    	return atomicAdaptationArray;
    }

    public void setAtomicAdaptationArray(AtomicAdaptation[] atomicAdaptationArray) {
    	this.atomicAdaptationArray = atomicAdaptationArray;
    }

    public long getCreatedAt() {
    	return createdAt;
    }

    public int getAmountOfWorkingFunctions() {
    	return amountOfWorkingFunctions;
    }
    
    public double getEnergyConsumption() {
    	return energyConsumption;
    }
    
    public double getPerformance() {
    	return performance;
    }

}
