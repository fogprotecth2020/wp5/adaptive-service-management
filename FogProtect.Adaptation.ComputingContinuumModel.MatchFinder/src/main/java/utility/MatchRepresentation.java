package utility;

import java.util.List;

public class MatchRepresentation {
    private final String ruleName;
    private final List<NodeRepresentation> nodes;

    public MatchRepresentation(String ruleName, List<NodeRepresentation> nodes) {
	this.ruleName = ruleName;
	this.nodes = nodes;
    }

    public List<NodeRepresentation> getNodes() {
	return nodes;
    }

    public String getRuleName() {
	return ruleName;
    }
}
