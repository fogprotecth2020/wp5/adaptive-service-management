package utility;

public enum ComparisonFactor {

	Risk, Function, Cost, EnergyConsumption, Performance

}
