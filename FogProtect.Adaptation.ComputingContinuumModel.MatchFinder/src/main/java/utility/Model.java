package utility;

import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.henshin.interpreter.EGraph;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import riskpatternfinder.AdaptationFinderToMitigateRisks;

public class Model {
    private EGraph runtimeModel;
    private MatchRepresentation[] risks;
    private Adaptation[] adaptations;
    private ExecutedAdaptation pathToModel = null;
    private double totalCost = 0.0;
    private boolean open = true;
    private int workingFunctions = 0;
    private double energyConsumption = 0.0;
    private double performance = 0.0;
    private long createdAt = 0;
    private AdaptationFinderToMitigateRisks adaptationFinderToMitigateRisks;
    private Resource resourceOfRuntimeModel;

    public Model(EGraph runtimeModel, long timer, AdaptationFinderToMitigateRisks adaptationFinderToMitigateRisks, Resource resource) {
		this.adaptationFinderToMitigateRisks = adaptationFinderToMitigateRisks;
		this.runtimeModel = runtimeModel;
//		this.calculateTotalCost();
//		this.calculateAmountOfWorkingFunctions();
		this.analyzeModel();
		long temp = System.nanoTime() - timer;
		this.createdAt = temp;
		this.resourceOfRuntimeModel = resource;
    }

    public void checkIfOpen() {
	if (adaptations != null) {
	    for (Adaptation adaptation : adaptations) {
		if (adaptation.isOpen()) {
		    return;
		}
	    }
	    this.open = false;
	}
    }
    
    private void analyzeModel() {
    	Map<String, Number> modelValues = new ModelAnalyzer().analyzeModel(runtimeModel);
    	this.workingFunctions = modelValues.get("functions").intValue();
    	this.totalCost = modelValues.get("cost").doubleValue();
    	this.energyConsumption = modelValues.get("energyConsumption").doubleValue();
    	this.performance = modelValues.get("performance").doubleValue();
    }

    //currently deprecated/unused
    public void calculateAmountOfWorkingFunctions() {
    	long startTime = System.nanoTime();
    	this.workingFunctions =
    			new ModelAnalyzer().getAmountOfWorkingFunctions((CloudEnvironment) runtimeModel.getRoots().get(0));
    	long endTime = System.nanoTime();
    	adaptationFinderToMitigateRisks.increaseTimeToCalculateWF((double) (endTime - startTime) / 1e9d);
    }

    public void calculateTotalCost() {
		long startTime = System.nanoTime();
		this.totalCost = new ModelAnalyzer().calculateTotalUsageCosts(runtimeModel);
		long endTime = System.nanoTime();
		adaptationFinderToMitigateRisks.increaseTimeToCalculateCost((double) (endTime - startTime) / 1e9d);
    }

    public EGraph getRuntimeModel() {
    	return runtimeModel;
    }

    public MatchRepresentation[] getRisks() {
    	return risks;
    }

    public void setRisks(MatchRepresentation[] risks) {
    	this.risks = risks;
    }

    public Adaptation[] getAdaptations() {
    	return adaptations;
    }

    public void setAdaptations(Adaptation[] adaptations) {
    	this.adaptations = adaptations;
    }

    public ExecutedAdaptation getPathToModel() {
    	return pathToModel;
    }

    public void setPathToModel(ExecutedAdaptation pathToModel) {
    	this.pathToModel = pathToModel;
    }

    public boolean isOpen() {
    	return open;
    }

    public void setOpen(boolean open) {
    	this.open = open;
    }

    public int getWorkingFunctions() {
    	return workingFunctions;
    }

    public double getTotalCost() {
		return totalCost;
	}

    public long getCreatedAt() {
    	return createdAt;
    }
    
    public double getEnergyConsumption() {
    	return energyConsumption;
    }
    
    public double getPerformance() {
    	return performance;
    }

	public AdaptationFinderToMitigateRisks getAdaptationFinderToMitigateRisks() {
		return adaptationFinderToMitigateRisks;
	}
	
	public Resource getResource() {
		return this.resourceOfRuntimeModel;
	}
    
    
}