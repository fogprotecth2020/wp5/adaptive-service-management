package utility;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;

public class NodeRepresentation {
    private final String name;
    private EClass nodeClass;
    private final int id;
    private String atId;
    private String objectName;
    private Resource resource;

    public NodeRepresentation(String name, int id, EObject eObject, Resource resource) {
	this.name = name;
	this.id = id;
	this.resource = resource;
	this.setAtId(eObject);
	this.objectName = ((tosca_nodes_Root) eObject).getName();
	
    }

    public int getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public String toString() {
	String output = "   ";
	output += name + " with id: " + id;
	return output;
    }

    public EClass getNodeClass() {
	return nodeClass;
    }

    public void setNodeClass(EClass nodeClass) {
	this.nodeClass = nodeClass;
    }

	public String getAtId() {
		return atId;
	}

	public void setAtId(EObject eObject) {
		tosca_nodes_Root objectCasted = (tosca_nodes_Root) eObject;
		atId = (resource).getURIFragment(objectCasted);
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
    
    
}
