package utility;

public class AtomicAdaptation {
    private String riskRuleName;
    private String adaptationName;
    //private int adaptationIndex;
    //private int adaptationType;

	//    public AtomicAdaptation(String riskRuleName, int adaptationType, int adaptationIndex) {
    public AtomicAdaptation(String riskRuleName, String adaptationName) {
	this.riskRuleName = riskRuleName;
	this.adaptationName = adaptationName;
//	this.adaptationType = adaptationType;
//	this.adaptationIndex = adaptationIndex;
    }

    public String getRiskRuleName() {
	return riskRuleName;
    }
    
    public String getAdaptationName() {
		return adaptationName;
	}

	public void setAdaptationName(String adaptationName) {
		this.adaptationName = adaptationName;
	}

//    public int getAdaptationIndex() {
//	return adaptationIndex;
//    }
//
//    public int getAdaptationType() {
//	return adaptationType;
//    }
//
//    public void setAdaptationType(int adaptationType) {
//	this.adaptationType = adaptationType;
//    }
//
//    public String toString() {
//	return riskRuleName + "_" + adaptationType + "_" + adaptationIndex;
//    }
}
