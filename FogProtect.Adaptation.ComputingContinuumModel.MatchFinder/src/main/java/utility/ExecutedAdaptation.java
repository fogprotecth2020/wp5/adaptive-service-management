package utility;

public class ExecutedAdaptation {
    private Adaptation adaptation;
    private Model runtimeModel;

    public ExecutedAdaptation(Adaptation adaptation, Model runtimeModel) {
	this.adaptation = adaptation;
	this.runtimeModel = runtimeModel;
    }

    public Adaptation getAdaptation() {
	return adaptation;
    }

    public Model getRuntimeModel() {
	return runtimeModel;
    }
}