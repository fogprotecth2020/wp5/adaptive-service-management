package utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;

import fogprotect.adaptation.ComputingContinuumModel.Application;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.Compute;
import fogprotect.adaptation.ComputingContinuumModel.DataFlow;
import fogprotect.adaptation.ComputingContinuumModel.Database;
import fogprotect.adaptation.ComputingContinuumModel.EnergyProfile;
import fogprotect.adaptation.ComputingContinuumModel.HardwareComponent;
import fogprotect.adaptation.ComputingContinuumModel.PerformanceProfile;
import fogprotect.adaptation.ComputingContinuumModel.QoSMetrics;
import fogprotect.adaptation.ComputingContinuumModel.Record;
import fogprotect.adaptation.ComputingContinuumModel.SecurityFeature;
import fogprotect.adaptation.ComputingContinuumModel.SoftwareComponent;

public class ModelAnalyzer {
	
	private Set<EObject> nodesWithPossibleAdditionalCosts = new HashSet<>();
	private Set<Compute> computeNodes = new HashSet<>();
	private Set<EnergyProfile> energyProfiles = new HashSet<>();
	private Set<QoSMetrics> qosMetrics = new HashSet<>();
	
    // calculates the usageCost in given graph
    public double calculateTotalUsageCosts(EGraph graph) {
    	double totalCost = 0.0;
    	
    	if (graph != null) {
    		// definition: only consider nodes used in functions
    		this.identifyImportantNodes(this.searchForWorkingFunctions(graph));
    		totalCost = this.calculateCosts();
    	}
    	
    	return totalCost;
    }
    
    // calculates the amount of WorkingFunctions in given CE
    public Integer getAmountOfWorkingFunctions(CloudEnvironment cloudEnvironment) {
		if (cloudEnvironment != null) {
			return this.weighFunctions(searchForWorkingFunctions(new EGraphImpl(cloudEnvironment)));
		}
		return 0;
	}
    
    // calculates the energyConsumption in given graph
    public double calculateEnergyConsumption(EGraph graph) {
    	double energyConsumption = 0.0;
    	
    	if (graph != null) {
    		this.identifyImportantNodes(this.searchForWorkingFunctions(graph));
    		energyConsumption = this.calculateEnergyConsumption();
    	}
    	
    	return energyConsumption;
    }
    
    
    private Integer weighFunctions(List<Match> functions) {
    	//TODO different weights to functions
    	// may require Refactoring of "amountOfWorkingFunctions"
    	
    	return functions.size();
    }
    

    // analyzes given model (currently: functions, cost and energyConsumption)
	public Map<String, Number> analyzeModel(EGraph graph) {
		int amountOfWorkingFunctions = 0;
		double costs = 0.0;
		double energyConsumption = 0.0;
		double performance = 0.0;
		
		if (graph != null) {
			
			// identify workingFunctions
			List<Match> workingFunctions = this.searchForWorkingFunctions(graph);
			amountOfWorkingFunctions = this.weighFunctions(workingFunctions);
			
			// important nodes: nodes involved in functions which may incur costs or energyConsumption
			this.identifyImportantNodes(workingFunctions);
			
			// calculate costs & energyConsumption
			costs = this.calculateCosts();
			energyConsumption = this.calculateEnergyConsumption();
			performance = this.calculatePerformance();
		}
		
		Map<String, Number> modelValues = new HashMap<>();
		modelValues.put("functions", amountOfWorkingFunctions);
		modelValues.put("cost", costs);
		modelValues.put("energyConsumption", energyConsumption);
		modelValues.put("performance", performance);
		
		return modelValues;
	}
	
	
	
	private List<Match> searchForWorkingFunctions(EGraph model) {
		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.FUNCTIONS_PATH);
    	Engine engine = new EngineImpl();
    	engine.getOptions().put(Engine.OPTION_DETERMINISTIC, false);
    	List<Match> functions = new ArrayList<>();
    	Rule[] ruleArray = (Rule[]) resourceSet.getModule("functions.henshin", false).getUnits().toArray(new Rule[0]);
//    	for (Rule rule : this.createRule(resourceSet, engine, "functions.henshin")) {
    	for (Rule rule : ruleArray) {
    		if (rule.isActivated()) {
    			try {
    				functions.addAll(InterpreterUtil.findAllMatches(engine,  rule, model, null));
    			}catch(NullPointerException e) {
    				System.out.println("ERR while finding trying to findAllMatches (Henshin) in ModelAnalyzer");
    			}
    			
    		}
    	}
    	return functions;
	}

	
	// importantNodes: nodes that are used for calculations
	private void identifyImportantNodes(List<Match> workingFunctions) {
		for (Match function : workingFunctions) {
			for (EObject node : function.getNodeTargets()) {
				
				// for Dataflows: find affected Records
				if (node instanceof DataFlow) {
					for (Record record : ((DataFlow) node).getConsistsOf()) {
						nodesWithPossibleAdditionalCosts.add(record);
					}
				}
				else if (node instanceof SoftwareComponent) {
					if (((SoftwareComponent) node).getHasQoSMetrics() != null) {
						qosMetrics.add(((SoftwareComponent) node).getHasQoSMetrics());
					}
				}
				else if (node instanceof Application) {
					Application application = (Application) node;
					if (application.getApplicationProducesEnergyProfile() != null) {
						energyProfiles.add(application.getApplicationProducesEnergyProfile());
					}
				}
				else if (node instanceof Compute) {
					this.analyzeComputeNode((Compute) node);
				}
				
			}
		}
	}
	
	// performance: fps und accuracy from qosmetrics
	
	private void analyzeComputeNode(Compute computeNode) {
		computeNodes.add(computeNode);
		
		for (HardwareComponent hardwareComponent : ((Compute) computeNode).getComprises()) {
			if (hardwareComponent.getHardwareComponentProducesEnergyProfile() != null) {
				energyProfiles.add(hardwareComponent.getHardwareComponentProducesEnergyProfile());
			}
			
		}
		
	}

	
	private double calculateEnergyConsumption() {
		double energyConsumption = 0.0;
		
		for (EnergyProfile energyProfile: energyProfiles) {
			energyConsumption += energyProfile.getAvgLoadConsumptionWatt();
		}
		
		return energyConsumption;
	}
	
	private double calculatePerformance() {
		double performance = 0.0;
		
		double a = 1.0;
		double b = 0.0;
		double c = 0.1;
		double d = 30.0;
		
		double alpha = 0.5;
		
		for (QoSMetrics qos : qosMetrics) {
			int x = qos.getAvgFPS();
			
			double normalizedFPS = normalizeFPS(x, a, b, c, d);			
			double aiModelAccuracy = qos.getAvgAIAccuracyInPercent() / 100.;

			
			performance += (normalizedFPS * alpha) + (aiModelAccuracy * (1 - alpha));
			
		}
		
		return performance;
	}
	
	private double normalizeFPS(int px, double pa, double pb, double pc, double pd) {
		return ( ((pa-pb) / (1 + Math.exp(-pc *(px-pd)))) + pb);
	}
	
	// currently views Records, Compute
	private double calculateCosts() {		
		double totalCost = 0.0;
		
		// cost = usageCost from computeNodes and encryptionCost from encrypted Objects
		
		totalCost += this.calculateUsageCost();
		
		totalCost += this.calculateCostToEncryption();
		
		return totalCost;
		
	}
	
	private double calculateUsageCost() {
		double computeCosts = 0.0;
		for (Compute computeNode : computeNodes) {
			computeCosts += computeNode.getUsageCostPerDay();
		}
		return computeCosts;
	}
    
	// TODO current case only evaluates records
    private double calculateCostToEncryption() {
    	double encryptionCost = 0.0;
    	for (EObject node : nodesWithPossibleAdditionalCosts) {
    		
    		if (node instanceof Record) {
    			Record record = (Record) node;
    			
    			if (record.getSecuredBy().size() > 0) {
    				encryptionCost += record.getCostToSecure();
    		}
    	}
    	
//    		if ((boolean) node.eGet(node.eClass().getEStructuralFeature("encrypted"))) {
//    			// add different values, based on type of node.
//    			if (node instanceof Database) {
//    				encryptionCost += (double) node.eGet(node.eClass().getEStructuralFeature("costToEncrypt"));
//    			}
//    			else if (node instanceof Record) {
//    				//TODO currently RecordEncryption is not considered
////    				encryptionCost += (double) node.eGet(node.eClass().getEStructuralFeature("costToEncrypt"));
//    			}
//    			
//    			
//    		}
    	}
    	
    	return encryptionCost;
    }
	
}
