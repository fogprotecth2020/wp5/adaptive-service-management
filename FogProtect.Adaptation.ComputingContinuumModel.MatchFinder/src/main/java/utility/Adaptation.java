package utility;

import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.model.Rule;

public class Adaptation {
    private String riskName = "";
    private Rule rule;
    private Match match;
    private boolean open = true;

    public Adaptation(Rule rule, String riskName) {
	this.rule = rule;
	this.riskName = riskName;
    }
    
    public Adaptation(Rule rule, String riskName, Match match) {
	this.rule = rule;
	this.riskName = riskName;
	this.setMatch(match);
    }
    public Rule getRule() {
	return rule;
    }

    public boolean isOpen() {
	return open;
    }

    public void setOpen(boolean open) {
	this.open = open;
    }

    public String getRiskName() {
	return riskName;
    }

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}
}