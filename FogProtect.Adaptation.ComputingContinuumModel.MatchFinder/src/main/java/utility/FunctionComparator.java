package utility;

import java.util.Comparator;
import java.util.List;


public class FunctionComparator implements Comparator<AdaptationCombinationRepresentation> {
	
	List<ComparisonFactor> factors;
	
	public FunctionComparator(List<ComparisonFactor> factors) {
		this.factors = factors;
	}
	
	/**
     * This comparator allows comparison between
     * AdaptationCombinationRepresentations based on the amount of working
     * functions. <br>
     * 1 = o1 > o2 <br>
     * 0 = o1 = o2 <br>
     * -1 = 01 < o2 <br>
     * Using the default sort method of the Collections class will generate a list
     * sorted in descending order (NOT ascending as usual!)
     */
    	
	@SuppressWarnings("incomplete-switch")
    @Override
	public int compare(AdaptationCombinationRepresentation o1, AdaptationCombinationRepresentation o2) {
    	int compare = 0;
		
		for (ComparisonFactor factor: factors) {
			//Risk is ignored, as ACRs are results from riskless Models
			switch(factor) {
			case Function:
				if (o1.getAmountOfWorkingFunctions() > o2.getAmountOfWorkingFunctions()) {
					compare = -1;
				}
				else if (o1.getAmountOfWorkingFunctions() < o2.getAmountOfWorkingFunctions()) {
					compare = 1;
				}
				break;
			case Cost: 
				if (o1.getTotalCost() < o2.getTotalCost()) {
					compare = -1;
				}
				else if (o1.getTotalCost() > o2.getTotalCost()) {
					compare = 1;
				}
				break;
			case EnergyConsumption:
				if (o1.getEnergyConsumption() < o2.getEnergyConsumption()) {
					compare = -1;
				}
				else if (o1.getEnergyConsumption() > o2.getEnergyConsumption()) {
					compare = 1;
				}
				break;
			case Performance:
				if (o1.getPerformance() > o2.getPerformance()) {
					compare = -1;
				}
				else if (o1.getPerformance() < o2.getPerformance()) {
					compare = 1;
				}
				break;
			}
			
			if (compare != 0) {
				break;
			}
		
		}
		
		
		return compare;
	}


}
