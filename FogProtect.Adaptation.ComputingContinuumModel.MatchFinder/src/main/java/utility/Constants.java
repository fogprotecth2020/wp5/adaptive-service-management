package utility;

import java.io.File;

public final class Constants {
	
	private static final String ucVersion = System.getenv("CurrentUseCase");
	
    public static final String RUNTIME_MODEL_PATH = new File(System.getProperty("user.dir")).getAbsolutePath()
	    + File.separator + "models" + File.separator + "runtime_models" + File.separator;
    public static final String RISKPATTERNS_PATH = new File(System.getProperty("user.dir")).getAbsolutePath()
	    + File.separator + "models" + File.separator + "riskpatterns" + File.separator + ucVersion + File.separator;
    public static final String ADAPTATIONS_PATH = new File(System.getProperty("user.dir")).getAbsolutePath()
	    + File.separator + "models" + File.separator + "adaptations" + File.separator + ucVersion + File.separator;
    public static final String IMPROVEMENTPATTERNS_PATH = new File(System.getProperty("user.dir")).getAbsolutePath()
    	    + File.separator + "models" + File.separator + "improvementpatterns" + File.separator;
    public static final String FUNCTIONS_PATH = new File(System.getProperty("user.dir")).getAbsolutePath() 
    		+ File.separator + "models" + File.separator + "functions" + File.separator + ucVersion + File.separator;
}
