package riskpatternfinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.InterpreterFactory;
import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.interpreter.RuleApplication;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import utility.AdaptationCombinationRepresentation;
import utility.ComparisonFactor;
import utility.Constants;
import utility.Model;

public abstract class AdaptationFinder
{
	protected Resource resource;
	protected List<String> disallowedRuleNames;
	
	public AdaptationFinder(Resource resource) 
	{
		this.resource = resource;
	}

	/**
	 * 
	 * @param cloudEnvironment The cloudEnvironment object is the root node of an EGraph.
	 * @return 
	 * 		<b>null</b> -> no risk found within the graph, thus there is no adaptation<br>
	 * 		<b>array.length == 0</b> -> there is at least one risk in the graph but no adaptation could be found to mitigate all risks<br>
	 * 		<b>array.length > 0</b> -> a list of adaptations that mitigate all found risks
	 */
	abstract public AdaptationCombinationRepresentation[] generatePossibleAdaptations(CloudEnvironment cloudEnvironment, AdaptationAlgorithm aa, int maxSeconds, List<ComparisonFactor> factors, List<String> disallowedRules);
	
	abstract protected void setUnallowedRules(List<String> allDisallowedRuleNames);
	
	protected boolean compareEMFModels(Model newModel, ArrayList<Model> models) {
		boolean sameModelIdentified = false;
		List<Diff> differences = null;
		for (Model riskyModel : models) {
			IComparisonScope scope2 = new DefaultComparisonScope(riskyModel.getRuntimeModel().getRoots().get(0),
					newModel.getRuntimeModel().getRoots().get(0), null);
			Comparison comparison = EMFCompare.builder().build().compare(scope2);
			differences = comparison.getDifferences();
			if (differences.isEmpty()) {
				sameModelIdentified = true;
				break;
			}
		}
		return sameModelIdentified;
	}
	
	protected List<Rule> loadHenshinRulesFromListOfCompleteNames(List<String> disallowedRuleNames) {
		List<Rule> disallowedRules = new ArrayList<>();
		
		if (!disallowedRuleNames.isEmpty()) {
			HenshinResourceSet set = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
			
			for (String completeRuleName: disallowedRuleNames) {
				String fileName = completeRuleName.split("-")[0];
				String ruleName = completeRuleName.split("-")[1];
				
				Module module = set.getModule(fileName);
				Rule disallowedRule = module.getAllRules().stream().filter(rule -> rule.getName().equals(ruleName)).findFirst().orElse(null);
				if (disallowedRule != null) {
					disallowedRules.add(disallowedRule);
				}
			}
		}
		return disallowedRules;
	}
	
	protected boolean isRuleAllowed(String fileName, String ruleName) {
		String rule = fileName + ".henshin-" + ruleName;
		return !disallowedRuleNames.contains(rule);
	}

	protected Map<String, List> executeAdaptationsAtAllPossibleMatches(Model runtimeModel, Rule[] adaptationRules) {
		List<Resource> copiedResources = new ArrayList<>();
		List<EGraph> newGraphs = new ArrayList<>();
		List<Rule> executedAdaptations = new ArrayList<>();
		
		Engine engine = InterpreterFactory.INSTANCE.createEngine();
		RuleApplication application = InterpreterFactory.INSTANCE.createRuleApplication(engine);
		
		EGraph graph = runtimeModel.getRuntimeModel();

		
		for(Rule rule: adaptationRules) {
			if (rule.isActivated()) {
				for (Match match: engine.findMatches(rule, graph, null)) {
					Resource copiedResource = new ResourceImpl();
					Copier copier = new Copier();
					copiedResource.getContents().addAll(copier.copyAll(runtimeModel.getResource().getContents()));
					copier.copyReferences();
						
					for (Node node: match.getRule().getLhs().getNodes()) {
						EObject image = copier.get(match.getNodeTarget(node));
						match.setNodeTarget(node, image);
					}
						
					// this... I do not know why it works.
					copiedResource.setURI(resource.getURI());

					EGraph copiedGraph = graph.copy(copier);
					
					application.setRule(rule);
					application.setEGraph(copiedGraph);
					application.setCompleteMatch(match);
					
					if(!application.execute(null)) {
						System.err.println("Adaptation failed.");
					}
					else {
						newGraphs.add(copiedGraph);
						copiedResources.add(copiedResource);
						executedAdaptations.add(rule);
					}
				}
			}
		}
		
		Map<String, List> result = new HashMap<>();
		result.put("newGraphs", newGraphs);
		result.put("resources", copiedResources);
		result.put("adaptationRules", executedAdaptations);
		
		return result;
	}
	
	protected Map<String, List> executeAdaptationAtAllPossibleMatches(Model runtimeModel, Rule adaptationRule) {
		return executeAdaptationsAtAllPossibleMatches(runtimeModel, new Rule[] {adaptationRule});
	}
	
}
