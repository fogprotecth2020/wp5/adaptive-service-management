package riskpatternfinder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.InterpreterFactory;
import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.interpreter.RuleApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.RuleApplicationImpl;
import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import riskpatternfinder.AdaptationFinderToMitigateRisks.AdaptationAlgorithm;
import utility.Adaptation;
import utility.AdaptationCombinationRepresentation;
import utility.AtomicAdaptation;
import utility.ComparisonFactor;
import utility.Constants;
import utility.ExecutedAdaptation;
import utility.FunctionComparator;
import utility.MatchRepresentation;
import utility.Model;

public class AdaptationFinderToImproveSystem extends AdaptationFinder {
	private ArrayList<Model> adaptedModels = new ArrayList<Model>();
	private Engine engine;
	private List<ComparisonFactor> factors;

	public AdaptationFinderToImproveSystem(Engine engine, Resource resource) {
		super(resource);
		this.engine = engine;
	}

	@Override
	public AdaptationCombinationRepresentation[] generatePossibleAdaptations(CloudEnvironment cloudEnvironment,
			AdaptationAlgorithm aa, int maxSeconds, List<ComparisonFactor> factors, List<String> disallowedRuleNames) {

		// create a new model object and insert the origin graph
		Model originRuntimeModel = new Model(new EGraphImpl(cloudEnvironment), System.nanoTime(),
				new AdaptationFinderToMitigateRisks(resource, factors), resource);
		this.factors = factors;
		setUnallowedRules(disallowedRuleNames);
		// start search
		this.startSearchforAdaptations(originRuntimeModel);
		// check if there is a model in the list of adapted models
		if (this.adaptedModels.size() == 0) {
			return null;
		}
		// convert findings
		return this.convertFindingsToRepresentation();
	}

	private AdaptationCombinationRepresentation[] convertFindingsToRepresentation() {
		ArrayList<AdaptationCombinationRepresentation> representations = new ArrayList<AdaptationCombinationRepresentation>();
		for (int i = 0; i < this.adaptedModels.size(); i++) {
			representations.add(new AdaptationCombinationRepresentation(i,
					this.generateAtomicAdaptationArray(this.adaptedModels.get(i)),
					this.adaptedModels.get(i).getRuntimeModel(), this.adaptedModels.get(i).getTotalCost(), 0,
					adaptedModels.get(i).getWorkingFunctions(), adaptedModels.get(i).getEnergyConsumption(), adaptedModels.get(i).getPerformance()));
		}
		Collections.sort(representations, new FunctionComparator(factors));

		return representations.toArray(new AdaptationCombinationRepresentation[0]);
	}

	private AtomicAdaptation[] generateAtomicAdaptationArray(Model runtimeModel) {
		ArrayList<AtomicAdaptation> list = new ArrayList<AtomicAdaptation>();
		while (runtimeModel.getPathToModel() != null) {
			String adaptationName = runtimeModel.getPathToModel().getAdaptation().getRule().getName();
			list.add(new AtomicAdaptation(runtimeModel.getPathToModel().getAdaptation().getRiskName(), adaptationName));
			runtimeModel = runtimeModel.getPathToModel().getRuntimeModel();
		}
		Collections.reverse(Arrays.asList(list));
		return list.toArray(new AtomicAdaptation[0]);
	}

	private void startSearchforAdaptations(Model runtimeModel) {
		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.ADAPTATIONS_PATH); 
		Module module = resourceSet.getModule("improvement.henshin", false);
		Rule[] ruleArray = (Rule[]) module.getUnits().toArray(new Rule[0]);
		
		List<Resource> copiedResources = new ArrayList<>();
		List<EGraph> newGraphs = new ArrayList<>();
		List<Rule> executedAdaptations = new ArrayList<>();
		
		for (Entry<String, List> entry : executeAdaptationsAtAllPossibleMatches(runtimeModel, ruleArray).entrySet()) {
			if (entry.getKey().equals("newGraphs"))
				newGraphs.addAll(entry.getValue());
			
			if (entry.getKey().equals("resources")) 
				copiedResources.addAll(entry.getValue());
			
			if (entry.getKey().equals("adaptationRules"))
				executedAdaptations.addAll(entry.getValue());
		}
		

		for (int i=0; i< newGraphs.size(); i++) {
			EGraph newGraph = newGraphs.get(i);
			if (newGraph != null) {
				List<MatchRepresentation> riskMatches = new SearchInitiator(engine,
						new AdaptationFinderToMitigateRisks(resource, factors)).findMatches(newGraph);
				
				if (riskMatches == null || riskMatches.size() == 0) {
					Model newModel = new Model(newGraph, System.nanoTime(),
							new AdaptationFinderToMitigateRisks(resource, factors), copiedResources.get(i));
					newModel.setPathToModel(
							new ExecutedAdaptation(new Adaptation(executedAdaptations.get(i), "improvement"), runtimeModel));
					if(!this.compareEMFModels(newModel, this.adaptedModels)) {
						// check if adaptatedModel is better than originalModel
						int b = newModel.getAdaptationFinderToMitigateRisks().compareRiskyModelsForBFS(newModel, Arrays.asList(new Model[] {runtimeModel}));
						
						if(b == 0) 
							this.adaptedModels.add(newModel);
							
						
					}
				}
			}
		}
	}
	
	
	
//	@Override
//	protected void startSearchForAdaptations(Model runtimeModel) {
//
//		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.IMPROVEMENTPATTERNS_PATH);
//
//		Module module = resourceSet.getModule("ImprovementPreconditions.henshin", false);
//		
//
//		List<Match> matches = InterpreterUtil.findAllMatches(engine, module, runtimeModel.getRuntimeModel());
//		Adaptation[] adaptations = this.gatherAdaptationsToRisks(matches);
//
//		for (Adaptation adaptation : adaptations) {
//
//			EGraph newGraph = this.executeAdaptation(runtimeModel.getRuntimeModel().copy(null), adaptation.getRule(), adaptation.getMatch());
//			if (newGraph != null) {
//				List<MatchRepresentation> riskMatches = new SearchInitiator(engine,
//						new AdaptationFinderToMitigateRisks(resource)).findMatches(newGraph);
//				if (riskMatches == null || riskMatches.size() == 0) {
//					Model newModel = new Model(newGraph, System.nanoTime(),
//							new AdaptationFinderToMitigateRisks(resource));
//					newModel.setPathToModel(
//							new ExecutedAdaptation(new Adaptation(adaptation.getRule(), "improvement"), runtimeModel));
//					this.adaptedModels.add(newModel);
//				}
//			}
//		}
//	}
	
	private Adaptation[] gatherAdaptationsToRisks(List<Match> matches) {
		ArrayList<Adaptation> adaptations = new ArrayList<Adaptation>();
		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
		for (Match match : matches) {
			if (isAdaptationExisting(resourceSet, match.getUnit().getName())) {
				System.out.println("RuleName: " + match.getUnit().getName());
				Rule[] rules = resourceSet.getModule(match.getUnit().getName() + ".henshin").getUnits()
						.toArray(new Rule[0]);
				for (int i = 0; i < rules.length; i++) {
					if (rules[i].isActivated()) {
						adaptations.add(
								new Adaptation(rules[i], match.getUnit().getName(), match));
					}
				}
			}
		}
		return adaptations.toArray(new Adaptation[0]);
	}
	
	private boolean isAdaptationExisting(HenshinResourceSet resourceSet, String ruleName) {
		try {
			resourceSet.getModule(ruleName + ".henshin");
		} catch (Exception e) {
			System.err.println("No file called \"" + ruleName + ".henshin\" found!");
			return false;
		}
		return true;
	}

	private Rule findRuleWithRHS(Rule[] ruleArrayWithRHS, String ruleName) {
		for (Rule rule : ruleArrayWithRHS) {
			if (rule.getName().equals(ruleName)) {
				return rule;
			}
		}
		return null;
	}

	private EGraph executeAdaptation(EGraph runtimeModel, Rule rule, Match partialMatch) {
		RuleApplicationImpl ruleApp = new RuleApplicationImpl(engine);
		ruleApp.setRule(rule);
		ruleApp.setEGraph(runtimeModel);
		ruleApp.setPartialMatch(partialMatch);
		if (!ruleApp.execute(null)) {
			runtimeModel = null;
		}
		return runtimeModel;
	}
	
	private void mergeRules() {
		String[] henshinFileNames = new String[] { "HighTrustworthiness", "MediumTrustworthiness",
				"LossOfTrustworthiness" };

		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);

		Module targetModule = resourceSet.getModule("improvement.henshin", false);

		for (String fileName : henshinFileNames) {
			Module sourceModule = resourceSet.getModule(fileName + ".henshin", false);
			targetModule.getUnits().addAll(sourceModule.getUnits());
		}

		try {
			targetModule.eResource().save(null);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void setUnallowedRules(List<String> allDisallowedRuleNames) {
		if (disallowedRuleNames == null) {
			super.disallowedRuleNames = new ArrayList<>();
		}
		else {
			super.disallowedRuleNames = disallowedRuleNames.stream().filter(fullName -> fullName.startsWith("improvements.henshin")).collect(Collectors.toList());
		}
	}
	
}