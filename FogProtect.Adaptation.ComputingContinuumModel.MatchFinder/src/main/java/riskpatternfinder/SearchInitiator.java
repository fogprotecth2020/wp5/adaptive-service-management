package riskpatternfinder;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;

import interfaces.SearchInitiatorInterface;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import utility.Constants;
import utility.MatchRepresentation;
import utility.NodeRepresentation;

public class SearchInitiator implements SearchInitiatorInterface {

	private EGraph graph;
	private Engine engine;
	private AdaptationFinderToMitigateRisks adaptationFinderToMitigateRisks;

	public SearchInitiator(Engine engine, AdaptationFinderToMitigateRisks adaptationFinderToMitigateRisks) {
		this.engine = engine;
		this.adaptationFinderToMitigateRisks = adaptationFinderToMitigateRisks;
	}

	@Override
	public List<MatchRepresentation> findMatches(EGraph graph) {
		if (graph == null) {
			return null;
		} else {
			this.graph = graph;
			return this.executeSearch();
		}
	}

	@Override
	public List<MatchRepresentation> findMatches(CloudEnvironment cloudEnvironment) {
		// set this.graph and check if setGraph returns "null"
		if (!setGraph(cloudEnvironment)) {
			return null;
		} else {
			return this.executeSearch();
		}
	}

	private List<MatchRepresentation> executeSearch() {
		return this.generateOutputForMatches(this.searchForRisks(engine));
	}

	// check if the cloudEnvironment object is null. If not initialize this.graph
	private boolean setGraph(CloudEnvironment cloudEnvironment) {
		if (cloudEnvironment != null) {
			this.graph = new EGraphImpl(cloudEnvironment);
			return true;
		} else {
			return false;
		}
	}

	private List<Match> searchForRisks(Engine engine) {
		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.RISKPATTERNS_PATH);
		List<Match> match = new ArrayList<Match>();
		for (Rule rule : this.createRule(resourceSet, engine, "PCPs.henshin")) {
			if (rule.isActivated() == true) {
				adaptationFinderToMitigateRisks.increaseAmountOfHenshinCalls();
				long startTime = System.nanoTime();
				match.addAll(InterpreterUtil.findAllMatches(engine, rule, graph, null));
				long endTime = System.nanoTime();
				adaptationFinderToMitigateRisks.increaseTimeToFindPCPInstances((double) (endTime - startTime) / 1e9d);
			}

		}
		return match;
	}

	private ArrayList<MatchRepresentation> generateOutputForMatches(List<Match> matches) {
		ArrayList<MatchRepresentation> matchRepresentations = new ArrayList<MatchRepresentation>();
		for (Match m : matches) {
			ArrayList<NodeRepresentation> nodeRepresentations = new ArrayList<NodeRepresentation>();
			List<EObject> nodes = m.getNodeTargets();
			for (EObject node : nodes) {
				EClass nodeClass = node.eClass();
				EAttribute id = (EAttribute) nodeClass.getEStructuralFeature("id");
				if (id != null) {
					nodeRepresentations.add(new NodeRepresentation(nodeClass.getName(), (int) node.eGet(id), node,
							adaptationFinderToMitigateRisks.getResource()));
				}
			}
			matchRepresentations.add(new MatchRepresentation(m.getRule().getName(), nodeRepresentations));
		}
		return matchRepresentations;
	}

	private Rule[] createRule(HenshinResourceSet resourceSet, Engine engine, String ruleFileName) {
		Module module = resourceSet.getModule(ruleFileName, false);
		EList<Unit> rules = module.getUnits();
		Rule[] ruleArray = new Rule[rules.size()];
		rules.toArray(ruleArray);
		return ruleArray;
	}
}
