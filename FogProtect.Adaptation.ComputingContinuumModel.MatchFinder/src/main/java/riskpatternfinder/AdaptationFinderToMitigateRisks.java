package riskpatternfinder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.interpreter.RuleApplication;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.RuleApplicationImpl;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Action.Type;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.impl.NodeImpl;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;

import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import utility.Adaptation;
import utility.AdaptationCombinationRepresentation;
import utility.AtomicAdaptation;
import utility.ComparisonFactor;
import utility.Constants;
import utility.ExecutedAdaptation;
import utility.FunctionComparator;
import utility.MatchRepresentation;
import utility.Model;

public class AdaptationFinderToMitigateRisks extends AdaptationFinder {
	private ArrayList<Model> riskyModels = new ArrayList<Model>();
	private ArrayList<Model> safeModels = new ArrayList<Model>();
	private ArrayList<Model> visitedModels = new ArrayList<Model>();

	private List<MatchRepresentation> firstFoundRisks;
	private int maxFuncInOriginModel = 0;
	private double costInOriginModel = 0;
	private double energyConsumptionInOriginModel = 0;

	private int amountOfHenshinCalls = 0;
	private double timeToFindPCPInstances = 0.0;
	private double timeToExecuteAdaptations = 0.0;
	private double timeToCalculateWF = 0.0;
	private double timeToCalculateCost = 0.0;
	private double timeForEMFCompareMethod = 0.0;
	private double timeForCompareRiskyModelsForBFS = 0.0;

	private List<ComparisonFactor> factors;
    
	public AdaptationFinderToMitigateRisks(Resource resource, List<ComparisonFactor> factors) {
		super(resource);
		this.factors = factors;
	}

	@Override
	public AdaptationCombinationRepresentation[] generatePossibleAdaptations(CloudEnvironment cloudEnvironment,
			AdaptationAlgorithm aa, int maxSeconds, List<ComparisonFactor> factors, List<String> disallowedRuleNames) {
		// create a new model object and insert the origin graph
		Model originRuntimeModel = new Model(new EGraphImpl(cloudEnvironment), 0, this, resource);
		maxFuncInOriginModel = originRuntimeModel.getWorkingFunctions();
		costInOriginModel = originRuntimeModel.getTotalCost();
		energyConsumptionInOriginModel = originRuntimeModel.getEnergyConsumption();
		this.factors = factors;

		// start search
		if (new SearchInitiator(new EngineImpl(), this).findMatches(originRuntimeModel.getRuntimeModel()).size() > 0) {
			setUnallowedRules(disallowedRuleNames);
			this.initiateSearch(originRuntimeModel, aa, maxSeconds);
		} else {
			return null;
		}

		// convert findings
		return this.convertFindingsToRepresentation();
	}

	static void shuffleArray(int[] ar) {
		// If running on Java 6 or older, use `new Random()` on RHS here
		Random rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

	private void initiateSearch(Model runtimeModel, AdaptationAlgorithm aa, int maxSeconds) {
		// look for risks in the origin model and add it to the risky or safe model list
		this.lookForRisksInRuntimeModel(runtimeModel, aa);
		if (this.riskyModels.size() > 0) {
			this.startSearchForAdaptations(aa, maxSeconds);
			System.out.println("Risky Models: " + riskyModels.size());
			System.out.println("Safe Models: " + safeModels.size());
		}
	}

	//TODO maybe use super between timestamps?
	@Override
	protected boolean compareEMFModels(Model newModel, ArrayList<Model> models) {
		long startTime = System.nanoTime();
		boolean sameModelIdentified = false;
		List<Diff> differences = null;
		for (Model model : models) {
			IComparisonScope scope2 = new DefaultComparisonScope(model.getResource(),
					newModel.getResource(), null);
			Comparison comparison = EMFCompare.builder().build().compare(scope2);
			differences = comparison.getDifferences();
			if (differences.isEmpty()) {
				sameModelIdentified = true;
				break;
			}
			long endTime = System.nanoTime();
			increaseTimeForEMFCompareMethod((double) (endTime - startTime) / 1e9d);
		}
		return sameModelIdentified;
	}

	public enum AdaptationAlgorithm {
		BestFirstSearch, DepthSearch, BreadthSearch, RandomSearch, AStarSearch
	}

	private void startSearchForAdaptations(AdaptationAlgorithm aa, int maxSeconds) {
		// initiate integer to count loops
		int count = 0;
		// search until no more adaptations are available or the counter reached max
		// Time in sec
		long timer = System.nanoTime();
		while (this.isAnyRiskyModelOpen() && ((System.nanoTime() - timer) / 1e9d) <= maxSeconds) {
			if (safeModels.size() > 1000) {
				this.deleteBadSafeEntries();
			}
			Model runtimeModel = null;
			// select which adaptation of the selected Model is to be executed
			switch (aa) {
			case BestFirstSearch:
				runtimeModel = this.pickAdaptation_best();
				break;
			case DepthSearch:
				runtimeModel = this.pickAdaptation_depth();
				break;
			case BreadthSearch:
				runtimeModel = this.pickAdaptation_breadth();
				break;
			case RandomSearch:
				runtimeModel = this.pickAdaptation_random();
				break;
			case AStarSearch:
				runtimeModel = this.pickAdaptation_aStar();
				break;
			default:
				break;
			}
			if (runtimeModel != null) {
				// perform all adaptations possible on runtimeModel
				for (int i = 0; i < runtimeModel.getAdaptations().length; i++) {
					this.performAdaptation(runtimeModel, i, timer, aa);
				}
			}
			count++;
		}
		System.out.println("count: " + count);
	}

	public String generatePathString(Model runtimeModel) {
		AtomicAdaptation[] adaptationPathArray = generateAtomicAdaptationArray(runtimeModel);
		String result = "";
		for (AtomicAdaptation adaptation : adaptationPathArray) {
			result += adaptation + " | ";
		}
		return result;
	}

	private void performAdaptation(Model runtimeModel, int id, long timer, AdaptationAlgorithm algorithm) {
		Adaptation executingAdaptation = runtimeModel.getAdaptations()[id];
		// set adaptation to closed
		executingAdaptation.setOpen(false);
		// check if there are any adaptations left open for the selected model
		runtimeModel.checkIfOpen();
		if (!runtimeModel.isOpen()) {
			riskyModels.remove(runtimeModel);
			visitedModels.add(runtimeModel);
		}
		
		
		List<Resource> copiedResources = new ArrayList<>();
		List<EGraph> newGraphs = new ArrayList<>();
		
		for (Entry<String, List> entry : executeAdaptationAtAllPossibleMatches(runtimeModel, executingAdaptation.getRule()).entrySet()) {
			if (entry.getKey().equals("newGraphs"))
				newGraphs.addAll(entry.getValue());
			
			if (entry.getKey().equals("resources")) 
				copiedResources.addAll(entry.getValue());
		}
		
		
		for (int i=0; i< newGraphs.size(); i++) {
			EGraph newGraph = newGraphs.get(i);
			if (newGraph != null) {
				// create a model object for the runtime model after the adaptation
				Model newModel = new Model(newGraph, timer, this, copiedResources.get(i));
				// save the runtime model and the adaptation, that was executed on the runtime
				// model to create the new model
				ExecutedAdaptation pathToNewModel = new ExecutedAdaptation(executingAdaptation, runtimeModel);
				// save the executedAdaptation object to the model
				newModel.setPathToModel(pathToNewModel);
				// look for risks in the model and add it to the risky or safe model list
				this.lookForRisksInRuntimeModel(newModel, algorithm);
			}
		}

	}

	private EObject searchForObjectInGivenModel(CloudEnvironment cloudEnvironment, String id) {
		if (id.equals("/")) {
			return cloudEnvironment;
		}
		EList<tosca_nodes_Root> nodes = cloudEnvironment.getTosca_nodes_root();
		for (tosca_nodes_Root element : nodes) {
			Resource resource = element.eResource();
			String toTest = resource.getURIFragment(element);
			if (toTest.equals(id)) {
				return element;
			}
		}
		return null;
	}

	private void lookForRisksInRuntimeModel(Model newModel, AdaptationAlgorithm algorithm) {
		// find risks in the given graph and put them in a list
		List<MatchRepresentation> matchReps = new SearchInitiator(new EngineImpl(), this)
				.findMatches(newModel.getRuntimeModel());
		// check if there is a risk within the runtime model
		if (matchReps != null && matchReps.size() > 0) {
			// check if the the list with risks in the origin model is empty. If it is, this
			// is the first search for risks executed.
			if (this.firstFoundRisks == null) {
				// save the risks found in the origin model
				this.setFirstFoundRisks(matchReps);
			}
			// put reference to MatchRepresentations in Model object
			newModel.setRisks(matchReps.toArray(new MatchRepresentation[0]));
			// create list with all possible adaptations for this runtime model
			newModel.setAdaptations(this.gatherAdaptationsToRisks(matchReps));
			// add the risky runtime model with its possible adaptations to the list of
			// risky models
			if (algorithm == AdaptationAlgorithm.BestFirstSearch || algorithm == AdaptationAlgorithm.AStarSearch) {
				if (algorithm == AdaptationAlgorithm.AStarSearch && (this.compareEMFModels(newModel, this.riskyModels)
						|| this.compareEMFModels(newModel, this.visitedModels))) {
					// The search algorithm is AStar but an identical model already exists in the
					// riskyModels or visitedModels list.
					// Thus nothing happens.
				} else {
					// The search algorithm is either BestFirstSearch or AStar and there is no
					// identical model already in the riskyModels or visitedModels list.
					// Thus the new model is added to the riskyModles list.
					// In the unlikely case that the newModel is null, compareRiskyModelsForBFS
					// returns -1.
					// Thus an index of -1 prevents the addition of the new model to the riskyModels
					// list.
					int index = this.compareRiskyModelsForBFS(newModel, this.riskyModels);
					if (index > -1) {
						riskyModels.add(index, newModel);
					}
				}
			} else {
				riskyModels.add(newModel);
			}
		} else {
			// if no risk has been found
			if (matchReps != null) {
				// set the model to closed
				newModel.setOpen(false);
				// the runtime model is risk free and is added to the safeModel list
				if (!this.compareEMFModels(newModel, this.safeModels)) {
					safeModels.add(newModel);
				}

			}
		}
	}

	public int compareRiskyModelsForBFS(Model newModel, List<Model> models) {
		long startTime = System.nanoTime();
		int index = models.size();
		for (int i = 0; i < models.size(); i++) {
			Model model = models.get(i);
			if (newModel != null) {

				if (this.compareModelsByComparisonFactors(newModel, model) == 1) {
					index = i;
					break;
				}
			} else {
				// Model was null
				index = -1;
			}
			long endTime = System.nanoTime();
			increaseTimeForCompareRiskyModelsForBFS((double) (endTime - startTime) / 1e9d);
		}
		return index;
	}

	private void deleteBadSafeEntries() {
		Model bestModel = null;
		for (int i = 0; i < safeModels.size(); i++) {
			Model safeModel = safeModels.get(i);
			if (bestModel == null) {
				bestModel = safeModel;
			} else {

				if (this.compareModelsByComparisonFactors(bestModel, safeModel) == -1) {
					bestModel = safeModel;
				}
			}
		}
		if (bestModel != null) {
			safeModels.clear();
			safeModels.add(bestModel);
		} else {
			System.out.println("Best Model is null!");
		}
	}

	private boolean isAnyRiskyModelOpen() {
		for (Model model : riskyModels) {
			if (model.isOpen()) {
				return true;
			}
		}
		return false;
	}

	private int compareModelsByComparisonFactors(Model m1, Model m2) {
		int compare = 0;

		for (ComparisonFactor factor : factors) {
			switch (factor) {
			case Risk:
				if (m1.getRisks() != null && m2.getRisks() != null) {
					if (m1.getRisks().length < m2.getRisks().length) {
						compare = 1;
					} else if (m1.getRisks().length > m2.getRisks().length) {
						compare = -1;
					}
				}
				break;
			case Function:
				if (m1.getWorkingFunctions() > m2.getWorkingFunctions()) {
					compare = 1;
				} else if (m1.getWorkingFunctions() < m2.getWorkingFunctions()) {
					compare = -1;
				}
				break;
			case Cost:
				if (m1.getTotalCost() < m2.getTotalCost()) {
					compare = 1;
				} else if (m1.getTotalCost() > m2.getTotalCost()) {
					compare = -1;
				}
				break;
			case EnergyConsumption:
				if (m1.getEnergyConsumption() < m2.getEnergyConsumption()) {
					compare = 1;
				} else if (m1.getEnergyConsumption() > m2.getEnergyConsumption()) {
					compare = -1;
				}
				break;
			case Performance:
				if (m1.getPerformance() > m2.getPerformance()) {
					compare = 1;
				} else if (m1.getPerformance() < m2.getPerformance()) {
					compare = -1;
				}
				break;
			}

			if (compare != 0) {
				break;
			}

		}

		return compare;
	}

	private EGraph executeAdaptation(EGraph runtimeModel, Rule rule, Match match) {
		Engine engine = new EngineImpl();
		engine.getOptions().put(Engine.OPTION_DETERMINISTIC, false);
//		engine.getOptions().put(Engine.OPTION_CHECK_DANGLING, false);
		RuleApplication application = new RuleApplicationImpl(engine);
		application.setUnit(rule);
		application.setEGraph(runtimeModel);
		application.setPartialMatch(match);
//		application.setCompleteMatch(match);
		increaseAmountOfHenshinCalls();
		long startTime = System.nanoTime();
		if (!application.execute(null)) {
			long endTime = System.nanoTime();
			increaseTimeToExecuteAdaptations((double) (endTime - startTime) / 1e9d);
			return null;
		} else {
			long endTime = System.nanoTime();
			increaseTimeToExecuteAdaptations((double) (endTime - startTime) / 1e9d);
//			EObject newObject = EcoreUtil.copy(runtimeModel.getRoots().get(0));		
//			EGraph newGraph = new EGraphImpl(newObject);
//			application.undo(null);
//			return newGraph;
			return runtimeModel;
		}
	}


	private Adaptation[] gatherAdaptationsToRisks(List<MatchRepresentation> matchReps) {
		ArrayList<Adaptation> adaptations = new ArrayList<Adaptation>();
		HenshinResourceSet resourceSet = new HenshinResourceSet(Constants.ADAPTATIONS_PATH);
		for (MatchRepresentation matchRep : matchReps) {
			if (isAdaptationExisting(resourceSet, matchRep)) {
				System.out.println("RuleName: " + matchRep.getRuleName());
				Rule[] rules = resourceSet.getModule(matchRep.getRuleName() + ".henshin").getUnits()
						.toArray(new Rule[0]);

				for (int i = 0; i < rules.length; i++) {
					// TODO test contains approach & performance of that
					if (rules[i].isActivated() && isRuleAllowed(matchRep.getRuleName(), rules[i].getName())) {
						adaptations.add(
								new Adaptation(rules[i], matchRep.getRuleName()));
					}
				}
			}
		}
		return adaptations.toArray(new Adaptation[0]);
	}

	private boolean isAdaptationExisting(HenshinResourceSet resourceSet, MatchRepresentation matchRep) {
		try {
			resourceSet.getModule(matchRep.getRuleName() + ".henshin");
		} catch (Exception e) {
			System.err.println("No file called \"" + matchRep.getRuleName() + ".henshin\" found!");
			return false;
		}
		return true;
	}

	private Model pickAdaptation_best() {
		if (riskyModels != null) {
			return riskyModels.get(0);
		} else {
			return null;
		}
	}

	private Model pickAdaptation_aStar() {
		if (riskyModels != null) {
			return riskyModels.get(0);
		} else {
			return null;
		}
	}

	private Model pickAdaptation_random() {
		int modelID = (int) ((Math.random() * ((riskyModels.size() - 0))));
		Model riskyModel = riskyModels.get(modelID);
		return riskyModel;
	}

	private Model pickAdaptation_breadth() {
		for (Model riskyModel : riskyModels) {
			return riskyModel;
		}
		return null;
	}

	private Model pickAdaptation_depth() {
		if (riskyModels.size() > 0) {
			Model riskyModel = riskyModels.get(riskyModels.size() - 1);
			return riskyModel;
		}
		return null;
	}

	private AdaptationCombinationRepresentation[] convertFindingsToRepresentation() {
		ArrayList<AdaptationCombinationRepresentation> representations = new ArrayList<AdaptationCombinationRepresentation>();
		for (int i = 0; i < this.safeModels.size(); i++) {
			representations.add(new AdaptationCombinationRepresentation(i,
					this.generateAtomicAdaptationArray(this.safeModels.get(i)),
					this.safeModels.get(i).getRuntimeModel(), this.safeModels.get(i).getTotalCost(),
					this.safeModels.get(i).getCreatedAt(), this.safeModels.get(i).getWorkingFunctions(),
					this.safeModels.get(i).getEnergyConsumption(), this.safeModels.get(i).getPerformance()));
		}
		Collections.sort(representations, new FunctionComparator(factors));

		return representations.toArray(new AdaptationCombinationRepresentation[0]);
	}

	private AtomicAdaptation[] generateAtomicAdaptationArray(Model runtimeModel) {
		ArrayList<AtomicAdaptation> list = new ArrayList<AtomicAdaptation>();
		while (runtimeModel.getPathToModel() != null) {
			String adaptationName = runtimeModel.getPathToModel().getAdaptation().getRule().getName();
//			String adaptationTypeAsString = adaptationName.substring(adaptationName.indexOf("_") + 1,
//					adaptationName.lastIndexOf("_"));
//			String adaptationNumberAsString = adaptationName.substring(adaptationName.lastIndexOf("_") + 1);
//			int adaptationType = Integer.valueOf(adaptationTypeAsString);
//			int adaptationNumber = Integer.valueOf(adaptationNumberAsString);
//			list.add(new AtomicAdaptation(runtimeModel.getPathToModel().getAdaptation().getRiskName(), adaptationType,
//					adaptationNumber));
			list.add(new AtomicAdaptation(runtimeModel.getPathToModel().getAdaptation().getRiskName(), adaptationName));
			runtimeModel = runtimeModel.getPathToModel().getRuntimeModel();
		}
		Collections.reverse(Arrays.asList(list));
		return list.toArray(new AtomicAdaptation[0]);
	}

	public Model lookForRisksInNonPerfectSolution() {
		Model bestModel = null;
		for (int i = 0; i < riskyModels.size(); i++) {
			Model riskyModel = riskyModels.get(i);
			if (bestModel != null) {

				if (this.compareModelsByComparisonFactors(bestModel, riskyModel) == -1) {
					bestModel = riskyModel;
				}

			} else {
				bestModel = riskyModel;
			}
		}
		return bestModel;
	}
	

	@Override
	protected void setUnallowedRules(List<String> allDisallowedRuleNames) {
		if (allDisallowedRuleNames == null) {
			super.disallowedRuleNames = new ArrayList<>();
		}
		else {
			super.disallowedRuleNames=  allDisallowedRuleNames.stream().filter(fullName -> !fullName.startsWith("improvement.henshin")).collect(Collectors.toList());
		}
	}

	public List<MatchRepresentation> getFirstFoundRisks() {
		return firstFoundRisks;
	}

	public void setFirstFoundRisks(List<MatchRepresentation> lastFoundRisks) {
		this.firstFoundRisks = lastFoundRisks;
	}

	public int getMaxFuncInOriginModel() {
		return maxFuncInOriginModel;
	}

	public double getEnergyConsumptionInOriginModel() {
		return this.energyConsumptionInOriginModel;
	}

	public void setMaxFuncInOriginModel(int maxFuncInOriginModel) {
		this.maxFuncInOriginModel = maxFuncInOriginModel;
	}

	public int getAmountOfHenshinCalls() {
		return amountOfHenshinCalls;
	}

	public void increaseAmountOfHenshinCalls() {
		this.amountOfHenshinCalls++;
	}

	public double getTimeToFindPCPInstances() {
		return timeToFindPCPInstances;
	}

	public void increaseTimeToFindPCPInstances(double timeToFindPCPInstances) {
		this.timeToFindPCPInstances += timeToFindPCPInstances;
	}

	public double getTimeToExecuteAdaptations() {
		return timeToExecuteAdaptations;
	}

	public void increaseTimeToExecuteAdaptations(double timeToExecuteAdaptations) {
		this.timeToExecuteAdaptations += timeToExecuteAdaptations;
	}

	public double getTimeToCalculateWF() {
		return timeToCalculateWF;
	}

	public void increaseTimeToCalculateWF(double timeToCalculateWF) {
		this.timeToCalculateWF += timeToCalculateWF;
	}

	public double getTimeToCalculateCost() {
		return timeToCalculateCost;
	}

	public void increaseTimeToCalculateCost(double timeToCalculateCost) {
		this.timeToCalculateCost += timeToCalculateCost;
	}

	public double getCostInOriginModel() {
		return costInOriginModel;
	}

	public double getTimeForEMFCompareMethod() {
		return timeForEMFCompareMethod;
	}

	public void increaseTimeForEMFCompareMethod(double timeForEMFCompareMethod) {
		this.timeForEMFCompareMethod += timeForEMFCompareMethod;
	}

	public double getTimeForCompareRiskyModelsForBFS() {
		return timeForCompareRiskyModelsForBFS;
	}

	public void increaseTimeForCompareRiskyModelsForBFS(double timeForCompareRiskyModelsForBFS) {
		this.timeForCompareRiskyModelsForBFS += timeForCompareRiskyModelsForBFS;
	}

	public Resource getResource() {
		return resource;
	}

}
