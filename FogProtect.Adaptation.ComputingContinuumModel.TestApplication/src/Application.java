import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class Application {
	private static String pathToAdaptation;
	private static Executor outputExecutor = Executors.newSingleThreadExecutor();

	public enum Trustworthy {
		HIGH, MEDIUM, LOW;
	}

	public static void main(String[] args) throws MalformedURLException {
		pathToAdaptation = System.getenv("PathToAdaptationCoordinator");
		Spark.port(7777);
		createRiskAssessmentPaths();
		createListenerPaths();
		createSecurityOrchestratorPaths();
		registerWithAdaptation();
	}

	private static void registerWithAdaptation() throws MalformedURLException {
		// Risk Assessment Listener
//		URL riskAssessmentURL = new URL(pathToAdaptation + "/riskassessment/addriskassessmentlistener");
//		// JSON OBJECT
//		makeRequest(riskAssessmentURL, ownAdress + "/riskassessment");

//		URL listenerURL = new URL(pathToAdaptation + "/fogprotect/addListener");
//		makeRequest(listenerURL, ownAdress + "/notification");
	}

	private static void makeRequest(URL restUrl, String requestBody) {
		HttpURLConnection request;
		try {
			request = (HttpURLConnection) restUrl.openConnection();
			request.setRequestMethod("POST");
			request.setDoOutput(true);
			Charset encoding = Charset.forName("UTF-8");
			request.setRequestProperty("Content-Type", "application/json");
			request.getOutputStream().write(requestBody.getBytes(encoding));
			int responseCode = request.getResponseCode();
			System.out.println("Response Code: " + responseCode);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void createListenerPaths() {
		Spark.post("/notification", new Route() {
			@Override
			public Object handle(Request request, Response response) throws Exception {
				String body = request.body();
				outputExecutor.execute(new Runnable() {
					@Override
					public void run() {
						String decodedWithEqualsSign = "";
						try {
							boolean risk = false;
							decodedWithEqualsSign = URLDecoder.decode(body, "UTF-8");
							System.out.println("Listener called with following String:" + decodedWithEqualsSign);
							JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();

							JsonObject asIs = receivedJson.get("AsIs").getAsJsonObject();

							int id = 0;
							JsonObject model = asIs.get("AsIsModel").getAsJsonObject();
							JsonArray toscaNodesRoot = model.get("tosca_nodes_root").getAsJsonArray();
							for (JsonElement ob : toscaNodesRoot) {
								JsonObject obCasted = ob.getAsJsonObject();
								if (obCasted.get("type").getAsString().equals("FogCompute")) {
									if (obCasted.get("compromised").getAsBoolean() == true) {
										JsonArray hosts = obCasted.get("hosts").getAsJsonArray();
										for (JsonElement sc : hosts) {
											JsonObject scCasted = sc.getAsJsonObject();
											String referencedObjectID = scCasted.get("referencedObjectID")
													.getAsString();
											for (JsonElement ob2 : toscaNodesRoot) {
												JsonObject ob2Casted = ob2.getAsJsonObject();
												if (ob2Casted.get("@id").getAsString().equals(referencedObjectID)) {
													if (ob2Casted.get("transferBy") != null) {
														JsonArray transferBy = ob2Casted.get("transferBy")
																.getAsJsonArray();
														if (transferBy.size() != 0) {
															risk = true;
															break;
														}
													}
												}
											}
										}
									}
								}
							}

							for (JsonElement ob : toscaNodesRoot) {
								JsonObject obCasted = ob.getAsJsonObject();
								if (obCasted.get("type").getAsString().equals("PrivateSpace")) {
									if (obCasted.get("trustworthy").getAsString().equals("LOW")) {
										String referencedObjectIDPS = obCasted.get("@id").getAsString();
										JsonArray consistsOf = obCasted.get("consistsOf").getAsJsonArray();
										for (JsonElement computeNode : consistsOf) {
											JsonObject computeNodeCasted = computeNode.getAsJsonObject();
											String referencedObjectID = computeNodeCasted.get("referencedObjectID")
													.getAsString();
											for (JsonElement ob2 : toscaNodesRoot) {
												JsonObject ob2Casted = ob2.getAsJsonObject();
												if (ob2Casted.get("@id").getAsString().equals(referencedObjectID)) {
													if (ob2Casted.get("hosts") != null) {
														JsonArray hosts = ob2Casted.get("hosts").getAsJsonArray();
														for (JsonElement dbms : hosts) {
															JsonObject dbmsCasted = dbms.getAsJsonObject();
															String referencedObjectID1 = dbmsCasted
																	.get("referencedObjectID").getAsString();
															for (JsonElement ob3 : toscaNodesRoot) {
																JsonObject ob3Casted = ob3.getAsJsonObject();
																if (ob3Casted.get("@id").getAsString()
																		.equals(referencedObjectID1)) {
																	if (ob3Casted.get("hosts") != null) {
																		JsonArray hosts2 = ob3Casted.get("hosts")
																				.getAsJsonArray();
																		for (JsonElement db : hosts2) {
																			JsonObject dbCasted = db.getAsJsonObject();
																			String referencedObjectID2 = dbCasted
																					.get("referencedObjectID")
																					.getAsString();
																			for (JsonElement ob4 : toscaNodesRoot) {
																				JsonObject ob4Casted = ob4
																						.getAsJsonObject();
																				if (ob4Casted.get("@id").getAsString()
																						.equals(referencedObjectID2)) {
																					if (ob4Casted.get(
																							"streamsFrom") != null) {
																						JsonArray streamsFrom = ob4Casted
																								.get("streamsFrom")
																								.getAsJsonArray();
																						for (JsonElement df : streamsFrom) {
																							JsonObject dfCasted = df
																									.getAsJsonObject();
																							String referencedObjectID3 = dfCasted
																									.get("referencedObjectID")
																									.getAsString();
																							for (JsonElement ob5 : toscaNodesRoot) {
																								JsonObject ob5Casted = ob5
																										.getAsJsonObject();
																								if (ob5Casted.get("@id")
																										.getAsString()
																										.equals(referencedObjectID3)) {
																									boolean disabled = Boolean
																											.parseBoolean(
																													ob5Casted
																															.get("disab")
																															.getAsString());
																									if (ob5Casted.get(
																											"transfersTo") != null) {
																										JsonArray transfersTo = ob5Casted
																												.get("transfersTo")
																												.getAsJsonArray();
																										for (JsonElement sc : transfersTo) {
																											JsonObject scCasted = sc
																													.getAsJsonObject();
																											String referencedObjectID4 = scCasted
																													.get("referencedObjectID")
																													.getAsString();
																											for (JsonElement ob6 : toscaNodesRoot) {
																												JsonObject ob6Casted = ob6
																														.getAsJsonObject();
																												if (ob6Casted
																														.get("@id")
																														.getAsString()
																														.equals(referencedObjectID4)) {
																													if (ob6Casted
																															.get("hostedOn") != null) {
																														JsonObject cCasted = ob6Casted
																																.get("hostedOn")
																																.getAsJsonObject();

																														String referencedObjectID5 = cCasted
																																.get("referencedObjectID")
																																.getAsString();
																														for (JsonElement ob7 : toscaNodesRoot) {
																															JsonObject ob7Casted = ob7
																																	.getAsJsonObject();
																															if (ob7Casted
																																	.get("@id")
																																	.getAsString()
																																	.equals(referencedObjectID5)) {
																																if (ob7Casted
																																		.get("partOf") != null) {
																																	JsonObject psCasted = ob7Casted
																																			.get("partOf")
																																			.getAsJsonObject();
																																	String referencedObjectID6 = psCasted
																																			.get("referencedObjectID")
																																			.getAsString();
																																	if (!referencedObjectID6
																																			.equals(referencedObjectIDPS)
																																			&& disabled == false) {
																																		risk = true;
																																		break;
																																	}
																																}
																															}
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}

							for (JsonElement ob : toscaNodesRoot) {
								JsonObject obCasted = ob.getAsJsonObject();
								if (obCasted.get("type").getAsString().equals("PrivateSpace")) {
									if ((obCasted.get("trustworthy").getAsString().equals("LOW"))) {
										JsonArray consistsOf = obCasted.get("consistsOf").getAsJsonArray();
										for (JsonElement computeNode : consistsOf) {
											JsonObject computeNodeCasted = computeNode.getAsJsonObject();
											String referencedObjectID = computeNodeCasted.get("referencedObjectID")
													.getAsString();
											for (JsonElement ob2 : toscaNodesRoot) {
												JsonObject ob2Casted = ob2.getAsJsonObject();
												if (ob2Casted.get("@id").getAsString().equals(referencedObjectID)) {
													if (ob2Casted.get("hosts") != null) {
														JsonArray hosts = ob2Casted.get("hosts").getAsJsonArray();
														for (JsonElement sc : hosts) {
															JsonObject scCasted = sc.getAsJsonObject();
															String referencedObjectID1 = scCasted
																	.get("referencedObjectID").getAsString();
															for (JsonElement ob3 : toscaNodesRoot) {
																JsonObject ob3Casted = ob3.getAsJsonObject();
																if (ob3Casted.get("@id").getAsString()
																		.equals(referencedObjectID1)) {
																	if (ob3Casted.get("transferBy") != null) {
																		JsonArray transferBy = ob3Casted
																				.get("transferBy").getAsJsonArray();
																		for (JsonElement df : transferBy) {
																			JsonObject dfCasted = df.getAsJsonObject();
																			String referencedObjectID2 = dfCasted
																					.get("referencedObjectID")
																					.getAsString();
																			for (JsonElement ob4 : toscaNodesRoot) {
																				JsonObject ob4Casted = ob4
																						.getAsJsonObject();
																				if (ob4Casted.get("@id").getAsString()
																						.equals(referencedObjectID2)) {
																					boolean disabled = Boolean
																							.parseBoolean(ob4Casted
																									.get("disab")
																									.getAsString());
																					if (ob4Casted.get(
																							"transfersTo") != null) {
																						JsonArray transfersTo = ob4Casted
																								.get("transfersTo")
																								.getAsJsonArray();
																						for (JsonElement sc2 : transfersTo) {
																							JsonObject sc2Casted = sc2
																									.getAsJsonObject();
																							String referencedObjectID3 = sc2Casted
																									.get("referencedObjectID")
																									.getAsString();
																							for (JsonElement ob5 : toscaNodesRoot) {
																								JsonObject ob5Casted = ob5
																										.getAsJsonObject();
																								if (ob5Casted.get("@id")
																										.getAsString()
																										.equals(referencedObjectID3)) {
																									if (ob5Casted.get(
																											"controlledBy") != null) {
																										JsonArray controlledBy = ob5Casted
																												.get("controlledBy")
																												.getAsJsonArray();
																										if (controlledBy
																												.size() != 0
																												&& disabled == false) {
																											risk = true;
																											break;
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
							Thread.sleep(3000); // 3 Sekunden Pause
							JsonObject head = new JsonObject();
							if (risk == true) {
								head.addProperty("NotificationType", "ResultOfRiskCalculation");
//								head.addProperty("NotificationType", "ImmediateAction");
								head.addProperty("EventName", "DoorOpen");
								head.addProperty("OverallRiskLevel", "VeryHigh");
								head.addProperty("AcceptableRiskLevel", "Medium");
								head.addProperty("RiskLevel", 0);
								JsonArray risks = new JsonArray();
								JsonObject risk1 = new JsonObject();
								risk1.addProperty("RiskName", "CompromisedFogCompute");
								risks.add(risk1);
								head.add("Risks", risks);
								URL url = new URL(pathToAdaptation + "/fogprotect/adaptationcoordinator/notify");
								makeRequest(url, head.toString());
							} else {
								head.addProperty("NotificationType", "ResultOfRiskCalculation");
								head.addProperty("OverallRiskLevel", "VeryLow");
								head.addProperty("AcceptableRiskLevel", "Medium");
								URL url = new URL(pathToAdaptation + "/fogprotect/adaptationcoordinator/notify");
								makeRequest(url, head.toString());
							}
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
							response.status(400);
						} catch (InterruptedException e) {
							e.printStackTrace();
							response.status(400);
						} catch (MalformedURLException e) {
							e.printStackTrace();
							response.status(400);
						}
					}
				});
				response.status(200);
				return "RiskAssessmentListener response!";
			}
		});
	}

	private static void createRiskAssessmentPaths() {
		Spark.post("/riskassessment", new Route() {
			@Override
			public Object handle(Request request, Response response) throws Exception {
				String body = request.body();
				outputExecutor.execute(new Runnable() {
					@Override
					public void run() {
						String decodedWithEqualsSign = "";
						try {
							
							decodedWithEqualsSign = URLDecoder.decode(body, "UTF-8");
							System.out.println("Riskassessment called with following String:" + decodedWithEqualsSign);
							JsonObject receivedJson = new JsonParser().parse(decodedWithEqualsSign).getAsJsonObject();

							JsonObject head = new JsonObject();
							head.addProperty("NotificationType", "EvaluationOfAdaptationProposals");
							head.addProperty("AcceptableRiskLevel", "Medium");

							JsonArray adaptationRisks = new JsonArray();
							
							for (JsonElement e : receivedJson.get("AdaptationProposals").getAsJsonArray()) {
								boolean risk = false;
								JsonObject proposal = e.getAsJsonObject();
								
								
								int id = proposal.get("ID").getAsInt();
								JsonObject model = proposal.get("AdaptationProposalModel").getAsJsonObject();
								JsonArray toscaNodesRoot = model.get("tosca_nodes_root").getAsJsonArray();
								for (JsonElement ob : toscaNodesRoot) {
									JsonObject obCasted = ob.getAsJsonObject();
									if (obCasted.get("type").getAsString().equals("FogCompute")) {
										if (obCasted.get("compromised").getAsBoolean() == true) {
											JsonArray hosts = obCasted.get("hosts").getAsJsonArray();
											for (JsonElement sc : hosts) {
												JsonObject scCasted = sc.getAsJsonObject();
												String referencedObjectID = scCasted.get("referencedObjectID")
														.getAsString();
												for (JsonElement ob2 : toscaNodesRoot) {
													JsonObject ob2Casted = ob2.getAsJsonObject();
													if (ob2Casted.get("atid").getAsString().equals(referencedObjectID)) {
														if (ob2Casted.get("transferBy") != null) {
															JsonArray transferBy = ob2Casted.get("transferBy")
																	.getAsJsonArray();
															if (transferBy.size() != 0) {
																risk = true;
																break;
															}
														}
													}
												}
											}
										}
									}
								}

								for (JsonElement ob : toscaNodesRoot) {
									JsonObject obCasted = ob.getAsJsonObject();
									if (obCasted.get("type").getAsString().equals("PrivateSpace")) {
										if (obCasted.get("trustworthy").getAsString().equals("LOW")) {
											String referencedObjectIDPS = obCasted.get("atid").getAsString();
											JsonArray consistsOf = obCasted.get("consistsOf").getAsJsonArray();
											for (JsonElement computeNode : consistsOf) {
												JsonObject computeNodeCasted = computeNode.getAsJsonObject();
												String referencedObjectID = computeNodeCasted.get("referencedObjectID")
														.getAsString();
												for (JsonElement ob2 : toscaNodesRoot) {
													JsonObject ob2Casted = ob2.getAsJsonObject();
													if (ob2Casted.get("atid").getAsString().equals(referencedObjectID)) {
														if (ob2Casted.get("hosts") != null) {
															JsonArray hosts = ob2Casted.get("hosts").getAsJsonArray();
															for (JsonElement dbms : hosts) {
																JsonObject dbmsCasted = dbms.getAsJsonObject();
																String referencedObjectID1 = dbmsCasted
																		.get("referencedObjectID").getAsString();
																for (JsonElement ob3 : toscaNodesRoot) {
																	JsonObject ob3Casted = ob3.getAsJsonObject();
																	if (ob3Casted.get("atid").getAsString()
																			.equals(referencedObjectID1)) {
																		if (ob3Casted.get("hosts") != null) {
																			JsonArray hosts2 = ob3Casted.get("hosts")
																					.getAsJsonArray();
																			for (JsonElement db : hosts2) {
																				JsonObject dbCasted = db.getAsJsonObject();
																				String referencedObjectID2 = dbCasted
																						.get("referencedObjectID")
																						.getAsString();
																				for (JsonElement ob4 : toscaNodesRoot) {
																					JsonObject ob4Casted = ob4
																							.getAsJsonObject();
																					if (ob4Casted.get("atid").getAsString()
																							.equals(referencedObjectID2)) {
																						if (ob4Casted.get(
																								"streamsFrom") != null) {
																							JsonArray streamsFrom = ob4Casted
																									.get("streamsFrom")
																									.getAsJsonArray();
																							for (JsonElement df : streamsFrom) {
																								JsonObject dfCasted = df
																										.getAsJsonObject();
																								String referencedObjectID3 = dfCasted
																										.get("referencedObjectID")
																										.getAsString();
																								for (JsonElement ob5 : toscaNodesRoot) {
																									JsonObject ob5Casted = ob5
																											.getAsJsonObject();
																									if (ob5Casted
																											.get("atid")
																											.getAsString()
																											.equals(referencedObjectID3)) {
																										boolean disabled = Boolean
																												.parseBoolean(
																														ob5Casted
																																.get("disab")
																																.getAsString());
																										if (ob5Casted.get(
																												"transfersTo") != null) {
																											JsonArray transfersTo = ob5Casted
																													.get("transfersTo")
																													.getAsJsonArray();
																											for (JsonElement sc : transfersTo) {
																												JsonObject scCasted = sc
																														.getAsJsonObject();
																												String referencedObjectID4 = scCasted
																														.get("referencedObjectID")
																														.getAsString();
																												for (JsonElement ob6 : toscaNodesRoot) {
																													JsonObject ob6Casted = ob6
																															.getAsJsonObject();
																													if (ob6Casted
																															.get("atid")
																															.getAsString()
																															.equals(referencedObjectID4)) {
																														if (ob6Casted
																																.get("hostedOn") != null) {
																															JsonObject cCasted = ob6Casted
																																	.get("hostedOn")
																																	.getAsJsonObject();

																															String referencedObjectID5 = cCasted
																																	.get("referencedObjectID")
																																	.getAsString();
																															for (JsonElement ob7 : toscaNodesRoot) {
																																JsonObject ob7Casted = ob7
																																		.getAsJsonObject();
																																if (ob7Casted
																																		.get("atid")
																																		.getAsString()
																																		.equals(referencedObjectID5)) {
																																	if (ob7Casted
																																			.get("partOf") != null) {
																																		JsonObject psCasted = ob7Casted
																																				.get("partOf")
																																				.getAsJsonObject();
																																		String referencedObjectID6 = psCasted
																																				.get("referencedObjectID")
																																				.getAsString();
																																		if (!referencedObjectID6
																																				.equals(referencedObjectIDPS)
																																				&& disabled == false) {
																																			risk = true;
																																			break;
																																		}
																																	}
																																}
																															}
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}

								for (JsonElement ob : toscaNodesRoot) {
									JsonObject obCasted = ob.getAsJsonObject();
									if (obCasted.get("type").getAsString().equals("PrivateSpace")) {
										if ((obCasted.get("trustworthy").getAsString().equals("LOW"))) {
											JsonArray consistsOf = obCasted.get("consistsOf").getAsJsonArray();
											for (JsonElement computeNode : consistsOf) {
												JsonObject computeNodeCasted = computeNode.getAsJsonObject();
												String referencedObjectID = computeNodeCasted.get("referencedObjectID")
														.getAsString();
												for (JsonElement ob2 : toscaNodesRoot) {
													JsonObject ob2Casted = ob2.getAsJsonObject();
													if (ob2Casted.get("atid").getAsString().equals(referencedObjectID)) {
														if (ob2Casted.get("hosts") != null) {
															JsonArray hosts = ob2Casted.get("hosts").getAsJsonArray();
															for (JsonElement sc : hosts) {
																JsonObject scCasted = sc.getAsJsonObject();
																String referencedObjectID1 = scCasted
																		.get("referencedObjectID").getAsString();
																for (JsonElement ob3 : toscaNodesRoot) {
																	JsonObject ob3Casted = ob3.getAsJsonObject();
																	if (ob3Casted.get("atid").getAsString()
																			.equals(referencedObjectID1)) {
																		if (ob3Casted.get("transferBy") != null) {
																			JsonArray transferBy = ob3Casted
																					.get("transferBy").getAsJsonArray();
																			for (JsonElement df : transferBy) {
																				JsonObject dfCasted = df.getAsJsonObject();
																				String referencedObjectID2 = dfCasted
																						.get("referencedObjectID")
																						.getAsString();
																				for (JsonElement ob4 : toscaNodesRoot) {
																					JsonObject ob4Casted = ob4
																							.getAsJsonObject();
																					if (ob4Casted.get("atid").getAsString()
																							.equals(referencedObjectID2)) {
																						boolean disabled = Boolean
																								.parseBoolean(ob4Casted
																										.get("disab")
																										.getAsString());
																						if (ob4Casted.get(
																								"transfersTo") != null) {
																							JsonArray transfersTo = ob4Casted
																									.get("transfersTo")
																									.getAsJsonArray();
																							for (JsonElement sc2 : transfersTo) {
																								JsonObject sc2Casted = sc2
																										.getAsJsonObject();
																								String referencedObjectID3 = sc2Casted
																										.get("referencedObjectID")
																										.getAsString();
																								for (JsonElement ob5 : toscaNodesRoot) {
																									JsonObject ob5Casted = ob5
																											.getAsJsonObject();
																									if (ob5Casted
																											.get("atid")
																											.getAsString()
																											.equals(referencedObjectID3)) {
																										if (ob5Casted.get(
																												"controlledBy") != null) {
																											JsonArray controlledBy = ob5Casted
																													.get("controlledBy")
																													.getAsJsonArray();
																											if (controlledBy
																													.size() != 0
																													&& disabled == false) {
																												risk = true;
																												break;
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							
								
								
								JsonObject risk1 = new JsonObject();
								risk1.addProperty("AdaptationProposalId", id);
								JsonObject riskLevel = new JsonObject();
								//risk = true;
								if (risk == true) {
									riskLevel.addProperty("OverallRiskLevel", "VeryHigh");
								} else {
									riskLevel.addProperty("OverallRiskLevel", "VeryLow");
								}
								risk1.add("RiskLevel", riskLevel);
								adaptationRisks.add(risk1);
							}
							
							head.add("AdaptationRisks", adaptationRisks);
							URL url = new URL(pathToAdaptation + "/fogprotect/adaptationcoordinator/notify");
							makeRequest(url, head.toString());
									
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
							response.status(400);
						} catch (MalformedURLException e) {
							e.printStackTrace();
							response.status(400);
						}
					}
				});
				response.status(202);
				return "RiskAssessmentListener response!";
			}
		});
	}

	private static void createSecurityOrchestratorPaths() {
		Spark.put("/policy-enforcement", new Route() {
			@Override
			public Object handle(Request request, Response response) throws Exception {
				String body = request.body();
				outputExecutor.execute(new Runnable() {
					@Override
					public void run() {
						System.out.println("WP6 Mock Up with following String: " + body);
					}
				});
				response.status(200);
				return "WP6 Security Orchestrator response!";
			}
		});
	}

}
