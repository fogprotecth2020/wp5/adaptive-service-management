package FogProtect.managedSystemModelGenerators;

import FogProtect.managedSystemModelGenerators.util.ChatterBox;
import FogProtect.managedSystemModelGenerators.util.StoryMaker;
import FogProtect.managedSystemModelGenerators.util.Booth;

import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelFactory;
import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelPackage;
import fogprotect.adaptation.ComputingContinuumModel.DBMS;
import fogprotect.adaptation.ComputingContinuumModel.DataFlow;
import fogprotect.adaptation.ComputingContinuumModel.DataProcessor;
import fogprotect.adaptation.ComputingContinuumModel.DataSubject;
import fogprotect.adaptation.ComputingContinuumModel.Database;
import fogprotect.adaptation.ComputingContinuumModel.FogCompute;
import fogprotect.adaptation.ComputingContinuumModel.Group;
import fogprotect.adaptation.ComputingContinuumModel.IaaSOperator;
import fogprotect.adaptation.ComputingContinuumModel.IoTDevice;
import fogprotect.adaptation.ComputingContinuumModel.PrivateSpace;
import fogprotect.adaptation.ComputingContinuumModel.Record;
import fogprotect.adaptation.ComputingContinuumModel.SoftwareComponent;
import fogprotect.adaptation.ComputingContinuumModel.StoredDataSet;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import fogprotect.adaptation.ComputingContinuumModel.Application;
import fogprotect.adaptation.ComputingContinuumModel.CloudCompute;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import java.util.ArrayList;

public class SmartMediaModelGenerator {

	public static void main(String[] args) {
		ComputingContinuumModelPackage.eINSTANCE.eClass();
		ComputingContinuumModelFactory factory = ComputingContinuumModelFactory.eINSTANCE;
		
		// Environment
		CloudEnvironment cloudEnv = factory.createCloudEnvironment();
		EList<tosca_nodes_Root> tosca_nodes_root = cloudEnv.getTosca_nodes_root();

		Group editorGroup;
		editorGroup = factory.createGroup();
		editorGroup.setName("EditorRole");

		Group boothGroup;
		boothGroup = factory.createGroup();
		boothGroup.setName("BoothRole");

		Group chatterGroup;
		chatterGroup = factory.createGroup();
		chatterGroup.setName("ChatterRole");

		Group atcGroup;
		atcGroup = factory.createGroup();
		atcGroup.setName("ATC");

		Group vrtGroup;
		vrtGroup = factory.createGroup();
		vrtGroup.setName("VRT");
		
		ChatterBox chatterBox = new  ChatterBox(factory, chatterGroup, vrtGroup, "VRT");
		StoryMaker atcStoryMaker = new StoryMaker(factory, editorGroup, atcGroup, "ATC");
		StoryMaker vrtStoryMaker = new StoryMaker(factory, editorGroup, atcGroup, "VRT");
		Booth vrtBooth1 = new Booth(factory, boothGroup, vrtGroup, "VRT1");
		Booth vrtBooth2 = new Booth(factory, boothGroup, vrtGroup, "VRT2");

		ArrayList<String> atcStoryMakerUserNames = new ArrayList<String>();
		ArrayList<String> vrtStoryMakerUserNames = new ArrayList<String>();
		ArrayList<String> vrtBooth1UserNames = new ArrayList<String>();
		ArrayList<String> vrtBooth2UserNames = new ArrayList<String>();

		atcStoryMakerUserNames.add("atc_eu1");
		atcStoryMakerUserNames.add("atc_eu2");

		vrtStoryMakerUserNames.add("vrt_eu1");
		vrtStoryMakerUserNames.add("vrt_eu2");

		vrtBooth1UserNames.add("vrt_eu3");
		vrtBooth2UserNames.add("vrt_eu4");

		vrtStoryMaker.populate(vrtStoryMakerUserNames);
		atcStoryMaker.populate(atcStoryMakerUserNames);
		vrtBooth1.populate(vrtBooth1UserNames);
		vrtBooth2.populate(vrtBooth2UserNames);

		atcStoryMaker.connectToChatterBox(chatterBox);
		vrtStoryMaker.connectToChatterBox(chatterBox);
		vrtBooth1.connectToChatterBox(chatterBox);
		vrtBooth2.connectToChatterBox(chatterBox);

		chatterBox.addToRoot(tosca_nodes_root);
		atcStoryMaker.addToRoot(tosca_nodes_root);
		vrtStoryMaker.addToRoot(tosca_nodes_root);
		vrtBooth1.addToRoot(tosca_nodes_root);
		vrtBooth2.addToRoot(tosca_nodes_root);

		tosca_nodes_root.add(atcGroup);
		tosca_nodes_root.add(vrtGroup);
		tosca_nodes_root.add(boothGroup);
		tosca_nodes_root.add(editorGroup);
		tosca_nodes_root.add(chatterGroup);
		
		// Below is based on https://www.vogella.com/tutorials/EclipseEMFPersistence/article.html
		// As of here we preparing to save the model content

		// Register the XMI resource factory for the .website extension

		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		// Map<String, Object> m = reg.getExtensionToFactoryMap();
		// m.put("models", new XMIResourceFactoryImpl());

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();
		resSet.setResourceFactoryRegistry(reg);
		// resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		reg.getExtensionToFactoryMap().put("continuummodel", new XMIResourceFactoryImpl());

		// create a resource
		Resource resource = resSet.createResource(URI
							  .createURI("GenSmartMedia.continuummodel"));
		// Get the first model element and cast it to the right type, in my
		// example everything is hierarchical included in this first node
		resource.getContents().add(cloudEnv);
		// resource.getContents().addAll(tosca_nodes_root);
        
		// now save the content.
		try {
		    // resource.save(Collections.EMPTY_MAP);
		    resource.save(null);
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}

	}
}
