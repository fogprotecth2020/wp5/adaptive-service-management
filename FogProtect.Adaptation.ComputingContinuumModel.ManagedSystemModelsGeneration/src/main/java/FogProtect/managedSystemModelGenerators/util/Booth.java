package FogProtect.managedSystemModelGenerators.util;

import FogProtect.managedSystemModelGenerators.util.NameMerger;

import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelFactory;
import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelPackage;
import fogprotect.adaptation.ComputingContinuumModel.DBMS;
import fogprotect.adaptation.ComputingContinuumModel.DataFlow;
import fogprotect.adaptation.ComputingContinuumModel.DataProcessor;
import fogprotect.adaptation.ComputingContinuumModel.DataSubject;
import fogprotect.adaptation.ComputingContinuumModel.Database;
import fogprotect.adaptation.ComputingContinuumModel.FogCompute;
import fogprotect.adaptation.ComputingContinuumModel.Group;
import fogprotect.adaptation.ComputingContinuumModel.IaaSOperator;
import fogprotect.adaptation.ComputingContinuumModel.IoTDevice;
import fogprotect.adaptation.ComputingContinuumModel.PrivateSpace;
import fogprotect.adaptation.ComputingContinuumModel.Record;
import fogprotect.adaptation.ComputingContinuumModel.SoftwareComponent;
import fogprotect.adaptation.ComputingContinuumModel.StoredDataSet;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import fogprotect.adaptation.ComputingContinuumModel.Application;
import fogprotect.adaptation.ComputingContinuumModel.CloudCompute;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.Group;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import java.util.ArrayList;

public class Booth {
    String companyName_;
    ComputingContinuumModelFactory factory_;

    Application boothService;
    Application boothUI;
    FogCompute boothFog;
    SoftwareComponent boothServiceSC;
    SoftwareComponent boothUISC;
    DataFlow videoDataFlow;
    DataFlow metadataDataFlow;
    PrivateSpace boothPS;
    DataSubject boothSubject;
    Record videoRec;
    Record metadataRec;
    Group boothOperatorGroup;
    Group companyGroup;

    ArrayList<DataProcessor> boothOperators;

    public Booth(ComputingContinuumModelFactory factory, Group role, Group company, String companyName) {
	companyName_ = companyName;
	factory_ = factory;

	// PrivateSpace
	boothPS = factory.createPrivateSpace();
	boothPS.setName(NameMerger.mergeNames("Booth PrivateSpace", companyName));

	// Groups
	// boothOperatorGroup = factory.createGroup();
	// boothOperatorGroup.setName("BoothOperatorRole");
	// companyGroup = factory.createGroup();
	// companyGroup.setName(companyName);
	boothOperatorGroup = role;
	companyGroup = company;

	// Applications
	boothService = factory.createApplication();
	boothUI = factory.createApplication();

	boothService.setName(NameMerger.mergeNames("Booth Service", companyName));
	boothUI.setName(NameMerger.mergeNames("Booth UI", companyName));

	// Compute
	boothFog = factory.createFogCompute();
	boothFog.setName(NameMerger.mergeNames("Booth Fog", companyName));

	// SoftwareComponent
	boothServiceSC = factory.createSoftwareComponent();
	boothUISC = factory.createSoftwareComponent();

	boothServiceSC.setName(NameMerger.mergeNames("Booth Service SC", companyName));
	boothUISC.setName(NameMerger.mergeNames("Booth UI SC", companyName));

	// DataSubject
	boothSubject = factory.createDataSubject();
	boothSubject.setName(NameMerger.mergeNames("Booth Subject", companyName));

	// Record
	videoRec = factory.createRecord();
	videoRec.setName(NameMerger.mergeNames("Video Record", companyName));
	metadataRec = factory.createRecord();
	metadataRec.setName(NameMerger.mergeNames("Metadata Record", companyName));

	// DataFlow
	videoDataFlow = factory.createDataFlow();
	metadataDataFlow = factory.createDataFlow();
	videoDataFlow.setName(NameMerger.mergeNames("Booth Video DataFlow", companyName));
	metadataDataFlow.setName(NameMerger.mergeNames("Booth Metadata DataFlow", companyName));

	// Relations Application - SoftwareComponent
	boothService.getContainsSoftwareComponent().add(boothServiceSC);
	boothUI.getContainsSoftwareComponent().add(boothUISC);

	// Relations Applicatin - Compute
	boothService.setHostedByCompute(boothFog);
	boothUI.setHostedByCompute(boothFog);

	// Relations DataFlow - SoftwareComponent
	videoDataFlow.getTransfersTo().add(boothServiceSC);
	videoDataFlow.getTransfersTo().add(boothUISC);
	metadataDataFlow.getTransfersTo().add(boothServiceSC);
	metadataDataFlow.getTransfersTo().add(boothUISC);

	// Relations PrivateSpace - Compute
	boothPS.getConsistsOf().add(boothFog);

	// Relations DataSubject - Record
	boothSubject.getOwns().add(videoRec);
	boothSubject.getOwns().add(metadataRec);

	// Not sure if this one is needed.
	// // Relations DataFlow - Record
	// videoDataFlow.getConsistsOf().add(videoRec);
	// metadataDataFlow.getConsistsOf().add(metadataRec);
    }

    public void connectToChatterBox(ChatterBox chatterBox) {
	// Relations DataFlow - DBMS
	videoDataFlow.getTransfersTo().add(chatterBox.chatterBoxVideoDBMS);
	metadataDataFlow.getTransfersTo().add(chatterBox.chatterBoxMetadataDBMS);

	// Relations DataSet - Record
	chatterBox.chatterBoxVideoSDS.getConsistsOf().add(videoRec);
	chatterBox.chatterBoxMetadataSDS.getConsistsOf().add(metadataRec);
    }

    public void populate(ArrayList<String> userNames) {
	boothOperators = new ArrayList<DataProcessor>();

	for (String name: userNames) {
	    // DataProcessor
	    DataProcessor boothOperator = factory_.createDataProcessor();
	    // boothOperator.setName(NameMerger.mergeNames(name, companyName_));
	    boothOperator.setName(name);

	    // Relations Actors - SoftwareComponent
	    boothOperator.getActorUsesSoftwareComponent().add(boothServiceSC);
	    boothOperator.getActorUsesSoftwareComponent().add(boothUISC);

	    // Relations DataProcessor - Groups
	    boothOperator.getGroupsActor().add(boothOperatorGroup);
	    boothOperator.getGroupsActor().add(companyGroup);

	    boothOperators.add(boothOperator);
	}
    }

    public void addToRoot(EList<tosca_nodes_Root> tosca_nodes_root) {
	tosca_nodes_root.add(boothService);
	tosca_nodes_root.add(boothUI);
	tosca_nodes_root.add(boothFog);
	tosca_nodes_root.add(boothServiceSC);
	tosca_nodes_root.add(boothUISC);
	tosca_nodes_root.add(videoDataFlow);
	tosca_nodes_root.add(metadataDataFlow);
	tosca_nodes_root.add(boothPS);
	tosca_nodes_root.add(boothSubject);
	tosca_nodes_root.add(videoRec);
	tosca_nodes_root.add(metadataRec);
	tosca_nodes_root.add(boothOperatorGroup);
	tosca_nodes_root.add(companyGroup);

	for (DataProcessor boothOperator  : boothOperators) {
	    tosca_nodes_root.add(boothOperator);
	}
    }
}
