package FogProtect.managedSystemModelGenerators.util;

import FogProtect.managedSystemModelGenerators.util.NameMerger;

import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelFactory;
import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelPackage;
import fogprotect.adaptation.ComputingContinuumModel.DBMS;
import fogprotect.adaptation.ComputingContinuumModel.DataFlow;
import fogprotect.adaptation.ComputingContinuumModel.DataProcessor;
import fogprotect.adaptation.ComputingContinuumModel.DataSubject;
import fogprotect.adaptation.ComputingContinuumModel.Database;
import fogprotect.adaptation.ComputingContinuumModel.FogCompute;
import fogprotect.adaptation.ComputingContinuumModel.Group;
import fogprotect.adaptation.ComputingContinuumModel.IaaSOperator;
import fogprotect.adaptation.ComputingContinuumModel.IoTDevice;
import fogprotect.adaptation.ComputingContinuumModel.PrivateSpace;
import fogprotect.adaptation.ComputingContinuumModel.Record;
import fogprotect.adaptation.ComputingContinuumModel.SoftwareComponent;
import fogprotect.adaptation.ComputingContinuumModel.StoredDataSet;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import fogprotect.adaptation.ComputingContinuumModel.Application;
import fogprotect.adaptation.ComputingContinuumModel.CloudCompute;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.Group;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import java.util.ArrayList;

public class StoryMaker {
    String companyName_;
    ComputingContinuumModelFactory factory_;

    Application storyMakerService;
    Application storyMakerUI;
    CloudCompute storyMakerCloud;
    SoftwareComponent storyMakerServiceSC;
    SoftwareComponent storyMakerUISC;
    DataFlow videoDataFlow;
    DataFlow metadataDataFlow;
    PrivateSpace storyMakerPS;
    Group editorGroup;
    Group companyGroup;

    ArrayList<DataProcessor> videoEditors;

    public StoryMaker(ComputingContinuumModelFactory factory, Group role, Group company, String companyName) {
	companyName_ = companyName;
	factory_ = factory;

	// PrivateSpace
	storyMakerPS = factory.createPrivateSpace();
	storyMakerPS.setName(NameMerger.mergeNames("Story Maker PrivateSpace", companyName));

	// Groups
	// editorGroup = factory.createGroup();
	// editorGroup.setName("EditorRole");
	// companyGroup = factory.createGroup();
	// companyGroup.setName(companyName);
	editorGroup = role;
	companyGroup = company;

	// Applications
	storyMakerService = factory.createApplication();
	storyMakerUI = factory.createApplication();

	storyMakerService.setName(NameMerger.mergeNames("Story Maker Service", companyName));
	storyMakerUI.setName(NameMerger.mergeNames("Story Maker UI", companyName));

	// Compute
	storyMakerCloud = factory.createCloudCompute();
	storyMakerCloud.setName(NameMerger.mergeNames("Story Maker Cloud", companyName));

	// SoftwareComponent
	storyMakerServiceSC = factory.createSoftwareComponent();
	storyMakerUISC = factory.createSoftwareComponent();

	storyMakerServiceSC.setName(NameMerger.mergeNames("Story Maker Service SC", companyName));
	storyMakerUISC.setName(NameMerger.mergeNames("Story Maker UI SC", companyName));

	// DataFlow
	videoDataFlow = factory.createDataFlow();
	metadataDataFlow = factory.createDataFlow();
	videoDataFlow.setName(NameMerger.mergeNames("Story Maker Video DataFlow", companyName));
	metadataDataFlow.setName(NameMerger.mergeNames("Story Maker Metadata DataFlow", companyName));

	// Relations Application - SoftwareComponent
	storyMakerService.getContainsSoftwareComponent().add(storyMakerServiceSC);
	storyMakerUI.getContainsSoftwareComponent().add(storyMakerUISC);

	// Relations Applicatin - Compute
	storyMakerService.setHostedByCompute(storyMakerCloud);
	storyMakerUI.setHostedByCompute(storyMakerCloud);

	// Relations DataFlow - SoftwareComponent
	videoDataFlow.getTransfersTo().add(storyMakerServiceSC);
	videoDataFlow.getTransfersTo().add(storyMakerUISC);
	metadataDataFlow.getTransfersTo().add(storyMakerServiceSC);
	metadataDataFlow.getTransfersTo().add(storyMakerUISC);

	// Relations PrivateSpace - Compute
	storyMakerPS.getConsistsOf().add(storyMakerCloud);
    }

    public void connectToChatterBox(ChatterBox chatterBox) {
	// Not sure if this one is needed
	// // Relations DataFlow - Record
	// videoDataFlow.getConsistsOf().add(chatterBox.videoRec);
	// metadataDataFlow.getConsistsOf().add(chatterBox.metadataRec);

	// Relations DataFlow - DBMS
	videoDataFlow.getTransfersTo().add(chatterBox.chatterBoxVideoDBMS);
	metadataDataFlow.getTransfersTo().add(chatterBox.chatterBoxMetadataDBMS);
    }

    public void populate(ArrayList<String> userNames) {
	videoEditors = new ArrayList<DataProcessor>();

	for (String name: userNames) {
	    DataProcessor videoEditor = factory_.createDataProcessor();
	    // videoEditor.setName(NameMerger.mergeNames(name, companyName_));
	    videoEditor.setName(name);

	    // Relations Actors - SoftwareComponent
	    videoEditor.getActorUsesSoftwareComponent().add(storyMakerServiceSC);
	    videoEditor.getActorUsesSoftwareComponent().add(storyMakerUISC);

	    // Relations DataProcessor - Groups
	    videoEditor.getGroupsActor().add(editorGroup);
	    videoEditor.getGroupsActor().add(companyGroup);

	    videoEditors.add(videoEditor);
	}
    }

    public void addToRoot(EList<tosca_nodes_Root> tosca_nodes_root) {
	tosca_nodes_root.add(storyMakerService);
	tosca_nodes_root.add(storyMakerUI);
	tosca_nodes_root.add(storyMakerCloud);
	tosca_nodes_root.add(storyMakerServiceSC);
	tosca_nodes_root.add(storyMakerUISC);
	tosca_nodes_root.add(videoDataFlow);
	tosca_nodes_root.add(metadataDataFlow);
	tosca_nodes_root.add(storyMakerPS);
	tosca_nodes_root.add(editorGroup);
	tosca_nodes_root.add(companyGroup);

	for (DataProcessor videoEditor  : videoEditors) {
	    tosca_nodes_root.add(videoEditor);
	}
    }
}
