package FogProtect.managedSystemModelGenerators.util;

import FogProtect.managedSystemModelGenerators.util.NameMerger;

import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelFactory;
import fogprotect.adaptation.ComputingContinuumModel.ComputingContinuumModelPackage;
import fogprotect.adaptation.ComputingContinuumModel.DBMS;
import fogprotect.adaptation.ComputingContinuumModel.DataFlow;
import fogprotect.adaptation.ComputingContinuumModel.DataProcessor;
import fogprotect.adaptation.ComputingContinuumModel.DataSubject;
import fogprotect.adaptation.ComputingContinuumModel.Database;
import fogprotect.adaptation.ComputingContinuumModel.FogCompute;
import fogprotect.adaptation.ComputingContinuumModel.Group;
import fogprotect.adaptation.ComputingContinuumModel.IaaSOperator;
import fogprotect.adaptation.ComputingContinuumModel.IoTDevice;
import fogprotect.adaptation.ComputingContinuumModel.PrivateSpace;
import fogprotect.adaptation.ComputingContinuumModel.Record;
import fogprotect.adaptation.ComputingContinuumModel.SoftwareComponent;
import fogprotect.adaptation.ComputingContinuumModel.StoredDataSet;
import fogprotect.adaptation.ComputingContinuumModel.tosca_nodes_Root;
import fogprotect.adaptation.ComputingContinuumModel.Application;
import fogprotect.adaptation.ComputingContinuumModel.CloudCompute;
import fogprotect.adaptation.ComputingContinuumModel.CloudEnvironment;
import fogprotect.adaptation.ComputingContinuumModel.Group;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class ChatterBox {
    Application chatterBoxService;
    Application chatterBoxUI;
    CloudCompute chatterBoxCloud;
    SoftwareComponent chatterBoxServiceSC;
    SoftwareComponent chatterBoxUISC;
    DataProcessor chatterBoxManager;
    DataFlow videoDataFlow;
    DataFlow metadataDataFlow;
    PrivateSpace chatterBoxPS;
    Database chatterBoxVideoStorageDB;
    Database chatterBoxMetadataStorageDB;
    StoredDataSet chatterBoxVideoSDS;
    StoredDataSet chatterBoxMetadataSDS;
    DBMS chatterBoxVideoDBMS;
    DBMS chatterBoxMetadataDBMS;
    Group managerGroup;
    Group companyGroup;


    public ChatterBox(ComputingContinuumModelFactory factory, Group role, Group company, String companyName) {
	// PrivateSpace
	chatterBoxPS = factory.createPrivateSpace();
	chatterBoxPS.setName(NameMerger.mergeNames("ChatterBox PrivateSpace", companyName));

	// Groups
	// managerGroup = factory.createGroup();
	// managerGroup.setName("ManagerRole");
	// companyGroup = factory.createGroup();
	// companyGroup.setName(companyName);
	managerGroup = role;
	companyGroup = company;

	// Applications
	chatterBoxService = factory.createApplication();
	chatterBoxUI = factory.createApplication();

	chatterBoxService.setName(NameMerger.mergeNames("ChatterBox Service", companyName));
	chatterBoxUI.setName(NameMerger.mergeNames("ChatterBox UI", companyName));

	// Databases
	chatterBoxVideoStorageDB = factory.createDatabase();
	chatterBoxVideoStorageDB.setName(NameMerger.mergeNames("ChatterBox Video DB", companyName));
	chatterBoxMetadataStorageDB = factory.createDatabase();
	chatterBoxMetadataStorageDB.setName(NameMerger.mergeNames("ChatterBox Metadata DB", companyName));

	// StoredDataSets
	chatterBoxVideoSDS = factory.createStoredDataSet();
	chatterBoxVideoSDS.setName(NameMerger.mergeNames("ChatterBox Video SDS", companyName));
	chatterBoxMetadataSDS = factory.createStoredDataSet();
	chatterBoxMetadataSDS.setName(NameMerger.mergeNames("ChatterBox Metadata SDS", companyName));

	// DBMS
	chatterBoxVideoDBMS = factory.createDBMS();
	chatterBoxVideoDBMS.setName(NameMerger.mergeNames("ChatterBox Video DBMS", companyName));
	chatterBoxMetadataDBMS = factory.createDBMS();
	chatterBoxMetadataDBMS.setName(NameMerger.mergeNames("ChatterBox Metadata DBMS", companyName));

	// Compute
	chatterBoxCloud = factory.createCloudCompute();
	chatterBoxCloud.setName(NameMerger.mergeNames("ChatterBox Cloud", companyName));

	// SoftwareComponent
	chatterBoxServiceSC = factory.createSoftwareComponent();
	chatterBoxUISC = factory.createSoftwareComponent();

	chatterBoxServiceSC.setName(NameMerger.mergeNames("ChatterBox Service SC", companyName));
	chatterBoxUISC.setName(NameMerger.mergeNames("ChatterBox UI SC", companyName));

	// DataProcessor
	chatterBoxManager = factory.createDataProcessor();
	chatterBoxManager.setName(NameMerger.mergeNames("ChatterBox Manager", companyName));

	// DataFlow
	videoDataFlow = factory.createDataFlow();
	metadataDataFlow = factory.createDataFlow();
	videoDataFlow.setName(NameMerger.mergeNames("ChatterBox Video DataFlow", companyName));
	metadataDataFlow.setName(NameMerger.mergeNames("ChatterBox Metadata DataFlow", companyName));

	// Relations Application - SoftwareComponent
	chatterBoxService.getContainsSoftwareComponent().add(chatterBoxServiceSC);
	chatterBoxUI.getContainsSoftwareComponent().add(chatterBoxUISC);

	// Relations Applicatin - Compute
	chatterBoxService.setHostedByCompute(chatterBoxCloud);
	chatterBoxUI.setHostedByCompute(chatterBoxCloud);

	// Relations DataFlow - SoftwareComponent
	videoDataFlow.getTransfersTo().add(chatterBoxServiceSC);
	videoDataFlow.getTransfersTo().add(chatterBoxUISC);
	metadataDataFlow.getTransfersTo().add(chatterBoxServiceSC);
	metadataDataFlow.getTransfersTo().add(chatterBoxUISC);

	// Relations DBMS - Database
	chatterBoxVideoStorageDB.setHostedOn(chatterBoxVideoDBMS);
	chatterBoxMetadataStorageDB.setHostedOn(chatterBoxMetadataDBMS);

	// Relations Database - Dataset
	chatterBoxVideoSDS.setStoredIn(chatterBoxVideoStorageDB);
	chatterBoxMetadataSDS.setStoredIn(chatterBoxMetadataStorageDB);

	// Relations PrivateSpace - Compute
	chatterBoxPS.getConsistsOf().add(chatterBoxCloud);

	// Relations Actors - SoftwareComponent
	chatterBoxManager.getActorUsesSoftwareComponent().add(chatterBoxServiceSC);
	chatterBoxManager.getActorUsesSoftwareComponent().add(chatterBoxUISC);

	// Relations DataFlow - DBMS
	videoDataFlow.getTransfersTo().add(chatterBoxVideoDBMS);
	metadataDataFlow.getTransfersTo().add(chatterBoxMetadataDBMS);

	// Relations DataProcessor - Groups
	chatterBoxManager.getGroupsActor().add(managerGroup);
	chatterBoxManager.getGroupsActor().add(companyGroup);
    }

    public void addToRoot(EList<tosca_nodes_Root> tosca_nodes_root) {
	tosca_nodes_root.add(chatterBoxService);
	tosca_nodes_root.add(chatterBoxUI);
	tosca_nodes_root.add(chatterBoxCloud);
	tosca_nodes_root.add(chatterBoxServiceSC);
	tosca_nodes_root.add(chatterBoxUISC);
	tosca_nodes_root.add(chatterBoxManager);
	tosca_nodes_root.add(videoDataFlow);
	tosca_nodes_root.add(metadataDataFlow);
	tosca_nodes_root.add(chatterBoxPS);
	tosca_nodes_root.add(chatterBoxVideoStorageDB);
	tosca_nodes_root.add(chatterBoxMetadataStorageDB);
	tosca_nodes_root.add(chatterBoxVideoSDS);
	tosca_nodes_root.add(chatterBoxMetadataSDS);
	tosca_nodes_root.add(chatterBoxVideoDBMS);
	tosca_nodes_root.add(chatterBoxMetadataDBMS);
	tosca_nodes_root.add(managerGroup);
	tosca_nodes_root.add(companyGroup);
    }
}
