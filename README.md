# RADAR_intern
Java Version between 11 - 14

Install add ons:
	Eclipse PDE
	Eclipse Henshin
	EMF - Eclipse Modeling Framework SDK 
	Ecore Diagram Editor (SDK)
	Eclipse Modeling Tools
	Eclipse Sirius

Prepare Projects:
	import MetaModel project into eclipse
		navigate to model -> MetaModel.genmodel
		open MetaModel.genmodel with EMF Generator
		Right click in the file and select "Generate All"

	use maven command: clean install on MetaModel project
	
	right click MetaModel.editor and "Run As" -> "Eclipse Application"
	
	In the now opened Eclipse Instance:
		import 	AdaptationCoordinator
				AdaptiveApplicationManagement
				KnowledgeBaseAsIsModel
				design
				ManagedSystemModels
				MatchFinder
				TestApplication
	
		use maven command: clean install on MatchFinder project
		use maven command: clean install on AdaptiveApplicationManagement project
		use maven command: clean install on AdaptationCoordinator project
		use maven command: clean install on KnowledgeBaseAsIsModel project
		use maven command: clean install on TestApplication project
		
		change Launch configuration (manually or import files)
			For manual configuration:
				Right click Application.java -> "Run As" -> "Run Configurations"
					Select "Environment"
						Select Add
							Add Name Value pairs from below
	
Run Program:
	run Application.java from AdaptationCoordinator
	run KafkaclientApplication.java from KnowledgeBaseAsIsModel
	run Application.java from AdaptiveApplicationManagement
	run Application.java from TestApplication
	
	Register managed system
		Send POST request to localhost:7772/fogprotect/knowledgebaseasismodel/registerManagedSystem
	Execute Monitoring Event
		Send POST request to localhost:7772/fogprotect/knowledgebaseasismodel/monitoringEvent
					
Launch configurations:

	Launch configuration for AdaptationCoordinator:
		Name:  AdaptationResultURL
		Value: http://localhost:7772/fogprotect/knowledgebaseasismodel/adaptationResult
		Name:  AdaptiveApplicationManagementURL_AdaptationCollection
		Value: http://localhost:7771/fogprotect/adaptiveApplicationManagement/adaptationCollection
		Name:  AdaptiveApplicationManagementURL_AdaptationRequest
		Value: http://localhost:7771/fogprotect/adaptiveApplicationManagement/adaptationRequest
		Name:  AdaptiveApplicationManagementURL_CallForAction
		Value: http://localhost:7771/fogprotect/adaptiveApplicationManagement/callForAction
		Name:  AdaptiveApplicationManagementURL_DisallowedAdaptations
		Value: http://localhost:7771/fogprotect/adaptiveApplicationManagement/disallowAdaptations
		Name:  AdaptiveApplicationManagementURL_ImmediateAction
		Value: http://localhost:7771/fogprotect/adaptiveApplicationManagement/immediateAction
		Name:  AdaptiveInfrastructureManagementURL_AdaptationCollection
		Value: null
		Name:  AdaptiveInfrastructureManagementURL_AdaptationRequest
		Value: null
		Name:  AdaptiveInfrastructureManagementURL_CallForAction
		Value: null
		Name:  AdaptiveInfrastructureManagementURL_DisallowedAdaptations
		Value: null
		Name:  AdaptiveInfrastructureManagementURL_ImmediateAction
		Value: null
		Name:  IncludeAllLogs
		Value: false
		Name:  KnowledgebaseasismodelURL
		Value: http://localhost_7772/fogprotect/knowledgebaseasismodel/mostRecentAsIsModel
		Name:  KnowledgebaseoldasismodelULR
		Value: http://localhost:7772/fogprotect/knowledgebaseasismodel/secondMostRecentAsIsModel
		Name:  RiskAssessmentURL
		Value: http://localhost:7777/riskassessment

	Launch configuration for AdaptiveApplicationManagement:
		Name:  AdaptationResultURL
		Value: http://localhost:7770/fogprotect/adaptationCoordinator/adaptationResult
		Name:  CurrentUseCase
		Value: UC1
		Name:  demo
		Value: false
		Name:  ExecuteAdaptationURL
		Value: null
		Name:  FoundRisksURL
		Value: http://localhost:7770/fogprotect/adaptationCoordinator/getRisks
		Name:  IncludeAllLogs
		Value: false
		Name:  KafkaServerURL (only works when an external Kafka Server is installed. Value is always needed - Error message can be blocked by adding "logging.level.org.apache.kafka.clients.NetworkClient = OFF" to application.properties file)
		Value: http://localhost:9092
		Name:  ProposedAdaptationURL
		Value: http://localhost:7770/fogprotect/adaptationCoordinator/proposedAdaptation
		Name:  RegisterAtAdaptationCoordinatorURL
		Value: http://localhost:7770/fogprotect/addManagementComponent

	Launch configuration for KnowledgeBaseAsIsModel:
		Name:  AsIsModel
		Value: SmartCityModel
		Name:  DashboardResetURL
		Value: null
		Name:  IncludeAllLogs
		Value: false
		Name:  KafkaServerURL
		Value: http://localhost:9092 (only works when an external Kafka Server is installed. Value is always needed - Error message can be blocked by adding "logging.level.org.apache.kafka.clients.NetworkClient = OFF" to application.properties file)
		Name:  ListenerURL
		Value: http://localhost:7777/notification
		Name:  ListenerURL2
		Value: http://localhost:7777/notification
		Name:  PolicyResetURL
		Value: http://localhost:7770/fogprotect/adaptationCoordinator/adaptationResult
		Name:  ResetAsIsModelDB
		Value: true

	Launch configuration for TestApplication:
		Name:  PathToAdaptationCoordinator
		Value: http://localhost:7770

Example REST bodies:

	registerManagedSystem:
		{
			"type": "CloudEnvironment",
			"@id": "/",
			"tosca_nodes_root": [
				{
					"type": "FogCompute",
					"@id": "//@tosca_nodes_root.0",
					"name": "Fog Node",
					"id": 0,
					"hosts": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.4"
						},
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.3"
						}
					],
					"jurisdiction": "PT",
					"usageCostPerDay": 0.0,
					"capacity": 0,
					"transferCostPerGB": 0.0,
					"costIncurred": false,
					"compromised": false
				},
				{
					"type": "IoTDevice",
					"@id": "//@tosca_nodes_root.1",
					"name": "CCTV Camera",
					"id": 0,
					"hosts": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.6"
						}
					],
					"jurisdiction": "PT",
					"usageCostPerDay": 0.0,
					"capacity": 0,
					"transferCostPerGB": 0.0,
					"costIncurred": false,
					"compromised": false
				},
				{
					"type": "DataSubject",
					"@id": "//@tosca_nodes_root.2",
					"name": "Data Subject / Pedestrian / Car Driver",
					"id": 0,
					"location": "PT",
					"owns": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.5"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.9"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.10"
						}
					]
				},
				{
					"type": "SoftwareComponent",
					"@id": "//@tosca_nodes_root.3",
					"name": "Pre-processing & Analysis",
					"id": 0,
					"hostedOn": {
						"type": "FogCompute",
						"referencedObjectID": "//@tosca_nodes_root.0"
					},
					"controlledBy": [
						{
							"type": "DataController",
							"referencedObjectID": "//@tosca_nodes_root.13"
						}
					],
					"neededCapacity": 0,
					"hasToBeDeployedOnFogNode": false,
					"transferBy": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.7"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.8"
						}
					]
				},
				{
					"type": "SoftwareComponent",
					"@id": "//@tosca_nodes_root.4",
					"name": "Video Streamer",
					"id": 0,
					"hostedOn": {
						"type": "FogCompute",
						"referencedObjectID": "//@tosca_nodes_root.0"
					},
					"controlledBy": [
						{
							"type": "DataController",
							"referencedObjectID": "//@tosca_nodes_root.13"
						}
					],
					"neededCapacity": 0,
					"hasToBeDeployedOnFogNode": false,
					"transferBy": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.8"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.15"
						}
					]
				},
				{
					"type": "Record",
					"@id": "//@tosca_nodes_root.5",
					"name": "Raw Video File",
					"id": 0,
					"partOf": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.7"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.8"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.15"
						},
						{
							"type": "StoredDataSet",
							"referencedObjectID": "//@tosca_nodes_root.28"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.32"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.17"
						}
					],
					"sensitive": false,
					"encrypted": false,
					"belongsTo": {
						"type": "DataSubject",
						"referencedObjectID": "//@tosca_nodes_root.2"
					},
					"personal": true
				},
				{
					"type": "SoftwareComponent",
					"@id": "//@tosca_nodes_root.6",
					"name": "Camera Streaming Service",
					"id": 0,
					"hostedOn": {
						"type": "IoTDevice",
						"referencedObjectID": "//@tosca_nodes_root.1"
					},
					"neededCapacity": 0,
					"hasToBeDeployedOnFogNode": false,
					"transferBy": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.7"
						}
					]
				},
				{
					"type": "DataFlow",
					"@id": "//@tosca_nodes_root.7",
					"name": "Data Flow (camera - fog node)",
					"id": 0,
					"consistsOf": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.5"
						}
					],
					"disab": false,
					"amountOfDataInGB": 0.0,
					"transfersTo": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.6"
						},
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.3"
						}
					],
					"connectionType": "Local"
				},
				{
					"type": "DataFlow",
					"@id": "//@tosca_nodes_root.8",
					"name": "Data Flow (pre-processing - video streamer)",
					"id": 0,
					"consistsOf": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.9"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.10"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.5"
						}
					],
					"disab": false,
					"amountOfDataInGB": 0.0,
					"transfersTo": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.3"
						},
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.4"
						}
					],
					"connectionType": "Local"
				},
				{
					"type": "Record",
					"@id": "//@tosca_nodes_root.9",
					"name": "Anonymised Video File",
					"id": 0,
					"partOf": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.8"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.15"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.32"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.17"
						}
					],
					"sensitive": false,
					"encrypted": false,
					"belongsTo": {
						"type": "DataSubject",
						"referencedObjectID": "//@tosca_nodes_root.2"
					},
					"personal": false
				},
				{
					"type": "Record",
					"@id": "//@tosca_nodes_root.10",
					"name": "Metadata",
					"id": 0,
					"partOf": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.8"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.15"
						}
					],
					"sensitive": false,
					"encrypted": false,
					"belongsTo": {
						"type": "DataSubject",
						"referencedObjectID": "//@tosca_nodes_root.2"
					},
					"personal": false
				},
				{
					"type": "CloudCompute",
					"@id": "//@tosca_nodes_root.11",
					"name": "Cloud Centre",
					"id": 0,
					"hosts": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.14"
						},
						{
							"type": "DBMS",
							"referencedObjectID": "//@tosca_nodes_root.30"
						}
					],
					"jurisdiction": "PT",
					"operatedByIaaSOperator": [
						{
							"type": "IaaSOperator",
							"referencedObjectID": "//@tosca_nodes_root.26"
						}
					],
					"usageCostPerDay": 0.0,
					"capacity": 0,
					"transferCostPerGB": 0.0,
					"costIncurred": false,
					"compromised": false
				},
				{
					"type": "CloudCompute",
					"@id": "//@tosca_nodes_root.12",
					"name": "Monitoring Platform",
					"id": 0,
					"hosts": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.16"
						}
					],
					"jurisdiction": "PT",
					"usageCostPerDay": 0.0,
					"capacity": 0,
					"transferCostPerGB": 0.0,
					"operatedByPaaSOperator": [
						{
							"type": "PaaSOperator",
							"referencedObjectID": "//@tosca_nodes_root.27"
						}
					],
					"costIncurred": false,
					"compromised": false
				},
				{
					"type": "DataController",
					"@id": "//@tosca_nodes_root.13",
					"name": "Data Controller",
					"id": 0,
					"location": "PT",
					"controls": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.3"
						},
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.4"
						}
					]
				},
				{
					"type": "SoftwareComponent",
					"@id": "//@tosca_nodes_root.14",
					"name": "Cloud Centre Software",
					"id": 0,
					"hostedOn": {
						"type": "CloudCompute",
						"referencedObjectID": "//@tosca_nodes_root.11"
					},
					"neededCapacity": 0,
					"hasToBeDeployedOnFogNode": false,
					"transferBy": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.15"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.17"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.24"
						}
					]
				},
				{
					"type": "DataFlow",
					"@id": "//@tosca_nodes_root.15",
					"name": "Data Flow (Video Streamer - Cloud Centre)",
					"id": 0,
					"consistsOf": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.10"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.9"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.5"
						}
					],
					"disab": false,
					"amountOfDataInGB": 0.0,
					"transfersTo": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.4"
						},
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.14"
						}
					],
					"connectionType": "VPN"
				},
				{
					"type": "SoftwareComponent",
					"@id": "//@tosca_nodes_root.16",
					"name": "Monitoring Software",
					"id": 0,
					"hostedOn": {
						"type": "CloudCompute",
						"referencedObjectID": "//@tosca_nodes_root.12"
					},
					"creates": [
						{
							"type": "StoredDataSet",
							"referencedObjectID": "//@tosca_nodes_root.28"
						}
					],
					"neededCapacity": 0,
					"hasToBeDeployedOnFogNode": false,
					"transferBy": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.17"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.32"
						}
					]
				},
				{
					"type": "DataFlow",
					"@id": "//@tosca_nodes_root.17",
					"name": "Data Flow (Cloud Centre - Monitoring Software)",
					"id": 0,
					"consistsOf": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.5"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.18"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.19"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.9"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.25"
						}
					],
					"disab": false,
					"amountOfDataInGB": 0.0,
					"transfersTo": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.14"
						},
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.16"
						}
					],
					"connectionType": "Internet"
				},
				{
					"type": "Record",
					"@id": "//@tosca_nodes_root.18",
					"name": "Video Analytic",
					"id": 0,
					"partOf": [
						{
							"type": "StoredDataSet",
							"referencedObjectID": "//@tosca_nodes_root.28"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.32"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.17"
						}
					],
					"sensitive": false,
					"encrypted": false,
					"personal": false
				},
				{
					"type": "Record",
					"@id": "//@tosca_nodes_root.19",
					"name": "Incident",
					"id": 0,
					"partOf": [
						{
							"type": "StoredDataSet",
							"referencedObjectID": "//@tosca_nodes_root.28"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.32"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.17"
						}
					],
					"sensitive": false,
					"encrypted": false,
					"personal": false
				},
				{
					"type": "DataProcessor",
					"@id": "//@tosca_nodes_root.20",
					"name": "Data Processor / Law Enforcement Agent / First Responder",
					"id": 0,
					"location": "PT",
					"accesses": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.32"
						}
					]
				},
				{
					"type": "DataSubject",
					"@id": "//@tosca_nodes_root.21",
					"name": "Data Subject / Pedestrian / Car Driver (2)",
					"id": 0,
					"location": "PT",
					"owns": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.25"
						}
					]
				},
				{
					"type": "IoTDevice",
					"@id": "//@tosca_nodes_root.22",
					"name": "Smartphone",
					"id": 0,
					"hosts": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.23"
						}
					],
					"jurisdiction": "PT",
					"usageCostPerDay": 0.0,
					"capacity": 0,
					"transferCostPerGB": 0.0,
					"costIncurred": false,
					"compromised": false
				},
				{
					"type": "SoftwareComponent",
					"@id": "//@tosca_nodes_root.23",
					"name": "Reporter of Incident",
					"id": 0,
					"hostedOn": {
						"type": "IoTDevice",
						"referencedObjectID": "//@tosca_nodes_root.22"
					},
					"neededCapacity": 0,
					"hasToBeDeployedOnFogNode": false,
					"transferBy": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.24"
						}
					]
				},
				{
					"type": "DataFlow",
					"@id": "//@tosca_nodes_root.24",
					"name": "Data Flow (Smartphone - Monitoring Platform)",
					"id": 0,
					"consistsOf": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.25"
						}
					],
					"disab": false,
					"amountOfDataInGB": 0.0,
					"transfersTo": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.23"
						},
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.14"
						}
					],
					"connectionType": "Internet"
				},
				{
					"type": "Record",
					"@id": "//@tosca_nodes_root.25",
					"name": "Incident Report",
					"id": 0,
					"partOf": [
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.24"
						},
						{
							"type": "StoredDataSet",
							"referencedObjectID": "//@tosca_nodes_root.28"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.32"
						},
						{
							"type": "DataFlow",
							"referencedObjectID": "//@tosca_nodes_root.17"
						}
					],
					"sensitive": false,
					"encrypted": false,
					"belongsTo": {
						"type": "DataSubject",
						"referencedObjectID": "//@tosca_nodes_root.21"
					},
					"personal": false
				},
				{
					"type": "IaaSOperator",
					"@id": "//@tosca_nodes_root.26",
					"name": "Ubiwhere",
					"id": 0,
					"operatesCompute": [
						{
							"type": "CloudCompute",
							"referencedObjectID": "//@tosca_nodes_root.11"
						}
					]
				},
				{
					"type": "PaaSOperator",
					"@id": "//@tosca_nodes_root.27",
					"name": "ThirdParty",
					"id": 0,
					"operatesCompute": [
						{
							"type": "CloudCompute",
							"referencedObjectID": "//@tosca_nodes_root.12"
						}
					]
				},
				{
					"type": "StoredDataSet",
					"@id": "//@tosca_nodes_root.28",
					"name": "Monitoring Data Set",
					"id": 0,
					"consistsOf": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.18"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.19"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.5"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.25"
						}
					],
					"storedIn": {
						"type": "Database",
						"referencedObjectID": "//@tosca_nodes_root.31"
					},
					"createdBySoftwareComponent": {
						"type": "SoftwareComponent",
						"referencedObjectID": "//@tosca_nodes_root.16"
					}
				},
				{
					"type": "FogCompute",
					"@id": "//@tosca_nodes_root.29",
					"name": "Fog Node 1",
					"id": 0,
					"jurisdiction": "PT",
					"usageCostPerDay": 0.0,
					"capacity": 0,
					"transferCostPerGB": 0.0,
					"costIncurred": false,
					"compromised": false
				},
				{
					"type": "DBMS",
					"@id": "//@tosca_nodes_root.30",
					"name": "DBMS",
					"id": 0,
					"hostedOn": {
						"type": "CloudCompute",
						"referencedObjectID": "//@tosca_nodes_root.11"
					},
					"neededCapacity": 0,
					"hasToBeDeployedOnFogNode": false,
					"hosts": [
						{
							"type": "Database",
							"referencedObjectID": "//@tosca_nodes_root.31"
						}
					]
				},
				{
					"type": "Database",
					"@id": "//@tosca_nodes_root.31",
					"name": "Database",
					"id": 0,
					"hostedOn": {
						"type": "DBMS",
						"referencedObjectID": "//@tosca_nodes_root.30"
					},
					"stores": [
						{
							"type": "StoredDataSet",
							"referencedObjectID": "//@tosca_nodes_root.28"
						}
					],
					"encrypted": false
				},
				{
					"type": "DataFlow",
					"@id": "//@tosca_nodes_root.32",
					"name": "Data Flow (DataProcessor - Monitoring Platform)",
					"id": 0,
					"consistsOf": [
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.18"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.9"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.5"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.19"
						},
						{
							"type": "Record",
							"referencedObjectID": "//@tosca_nodes_root.25"
						}
					],
					"accessedBy": {
						"type": "DataProcessor",
						"referencedObjectID": "//@tosca_nodes_root.20"
					},
					"disab": false,
					"amountOfDataInGB": 0.0,
					"transfersTo": [
						{
							"type": "SoftwareComponent",
							"referencedObjectID": "//@tosca_nodes_root.16"
						}
					],
					"connectionType": "Local"
				}
			]
		}
	
	monitoringEvent:
		{
    		"RequestType":"changeAttribute",
    		"@id": "//@tosca_nodes_root.31",
    		"attribute": "trustworthy",
    		"newValue": "HIGH",
    		"attributeType": "Trustworthy"
		}
